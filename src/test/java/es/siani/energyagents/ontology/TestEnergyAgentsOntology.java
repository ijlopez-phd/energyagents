package es.siani.energyagents.ontology;

import java.io.InputStream;

import org.junit.Test;

import es.siani.energyagents.ontology.as00.EnergyAgentsOntology;
import es.siani.energyagents.ontology.as00.EventConcept;
import es.siani.energyagents.ontology.as00.IntervalConcept;
import es.siani.energyagents.ontology.as00.ProgramConcept;
import es.siani.energyagents.ontology.as00.TargetsConcept;
import es.siani.simpledr.AsdrException;
import es.siani.simpledr.AsdrUtils;
import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;
import es.siani.simpledr.model.AsdrProgram;
import junit.framework.TestCase;

public class TestEnergyAgentsOntology extends TestCase {
	
	/**
	 * Initializes resources.
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		return;
	}

	
	/**
	 * Closes resources.
	 */
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		return;
	}
	
	
	/**
	 * Tests the conversion of a minimal AsdrProgram object to a ProgramConcept object.
	 * @throws AsdrException 
	 */
	@Test
	public void testCreateProgramMinimal() throws AsdrException {
		
		// Load the AsdrProgram from the file "p20a_minimal_01.xml".
		InputStream istream = this.getClass().getResourceAsStream("/program/p20a_minimal_01.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		
		ProgramConcept program = EnergyAgentsOntology.createProgram(asdrProgram);
		assertEquals("minimalProgram01", program.getName());
		assertEquals(1, program.getEvents().size());
		
		EventConcept event = program.getEvents().get(0);
		AsdrEvent asdrEvent = asdrProgram.getEvents().get(0);
		assertEquals("setpoint", event.getSignalType());
		assertEquals(asdrEvent.getStart().getTime(), event.getStart().getTime());
		assertEquals(asdrEvent.getDuration(), event.getDuration());
		assertEquals(asdrEvent.getNotificationDuration(), event.getNotificationDuration());		
		assertEquals(asdrEvent.getPriority(), event.getPriority());
		assertEquals(asdrEvent.isResponseRequired(), event.isResponseRequired());
		assertEquals(1, event.getIntervals().size());
		
		IntervalConcept interval = event.getIntervals().get(0);
		AsdrInterval asdrInterval = asdrEvent.getIntervals().get(0);
		assertEquals(asdrInterval.getDuration(), interval.getDuration());
		assertEquals(asdrInterval.getPayload(), interval.getPayload(), 0.001);
		
		TargetsConcept targets = event.getTargets();
		assertEquals(0, targets.getGroups().size());
		assertEquals(0, targets.getResources().size());
		assertEquals(0, targets.getParties().size());
		assertEquals(0, targets.getVendors().size());
		
		return;
	}
	
	
	/**
	 * Tests the conversion of a simple AsdrProgram object to a ProgramConcept object.
	 * @throws AsdrException
	 */
	@Test
	public void testCreateProgramSimple() throws AsdrException {
		
		// Load the AsdrProgram from the file "p20a_simple_01.xml"
		InputStream istream = this.getClass().getResourceAsStream("/program/p20a_simple_01.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		
		ProgramConcept program = EnergyAgentsOntology.createProgram(asdrProgram);
		assertEquals("simpleProgram01", program.getName());
		assertEquals(2, program.getEvents().size());
		
		EventConcept event = program.getEvents().get(0);
		assertEquals("setpoint", event.getSignalType());
		assertEquals(2, event.getIntervals().size());
		
		event = program.getEvents().get(1);
		assertEquals("level", event.getSignalType());
		assertEquals(1, event.getPriority());
		assertEquals(3, event.getIntervals().size());
		
		return;
	}	
}
