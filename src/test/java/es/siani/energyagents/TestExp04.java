package es.siani.energyagents;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.transform.stream.StreamSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Provides;

import es.siani.energyagents.agent.AgentTracker;
import es.siani.energyagents.behaviours.as04.AuctionBoard;
import es.siani.energyagents.config.Configuration;
import es.siani.energyagents.config.ConfigurationLoader;
import es.siani.energyagents.injection.BehavioursExp04Module;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.injection.PlatformExp04Module;
import es.siani.energyagents.injection.XmppMockModule;
import es.siani.energyagents.services.LoadForecastService;
import es.siani.simpledr.AsdrUtils;
import es.siani.simpledr.model.AsdrProgram;

@RunWith(JUnit4.class)
public class TestExp04 {
	
	private static final int ID_SIMULATION = 1;
	private static final String SCENARIO_CODE = "ieee13";
	private static final String STORE_NAME = "ieee13";
	private static final String ASPEM_01_NAME = "aspem01";
	
	private AgentsPlatform platform;
	private GridopContainer gridopContainer;
	private AspemContainer aspem01;	
	
	private AgentTracker gridopTracker;
	private AgentTracker aspemTracker01;
	private AgentTracker[] brokers01Tracker;
	
	private Injector injector;
	
	
	/**
	 * Constructor.
	 */
	public TestExp04() throws Exception {
		return;
	}
	
	
	/**
	 * Initialize resources for each test.
	 */
	@Before
	public void setUp() throws Exception {
		
		InjectionUtils.setXmppModule(new XmppMockModule());
		InjectionUtils.setBehavioursModule(new BehavioursExp04Module());
		InjectionUtils.setPlatformModule(new PlatformExp04Module());
		InjectionUtils.setConfigModule(new ConfigModule00());
		
		this.platform = null;
		this.gridopContainer = null;
		this.aspem01 = null;
		return;
	}
	
	/**
	 * Close resources for each test.
	 */
	@After
	public void tearDown() {
		
		if ((aspem01 != null) && (!aspem01.isKilled())) {
			aspem01.kill();
			aspem01 = null;
			aspemTracker01 = null;
			brokers01Tracker = null;
		}
		
		if ((gridopContainer != null) && (!gridopContainer.isKilled())) {
			gridopContainer.kill();
			gridopContainer = null;
		}
		
		return;
	}
	
	
	/**
	 * Initialize the elements of the platform.
	 */
	private void initPlatform(int nBrokers) {
		
		Injector injector = InjectionUtils.injector();
		this.platform = injector.getInstance(AgentsPlatform.class);		
		
		this.gridopTracker = mock(AgentTracker.class);
		this.gridopContainer = platform.createGridopContainer(gridopTracker);
		
		if (nBrokers < 0) {
			throw new IllegalArgumentException();
		}
		
		aspemTracker01 = mock(AgentTracker.class);
		aspem01 = platform.createAspemContainer(ASPEM_01_NAME, aspemTracker01);
		brokers01Tracker = new AgentTracker[nBrokers];
		for (int n = 0; n < nBrokers; n++) {
			AgentTracker tracker = mock(AgentTracker.class);
			aspem01.addBrokerAgent(Integer.toString(n), tracker);
			brokers01Tracker[n] = tracker;
		}
		
		AuctionBoard board = injector.getInstance(AuctionBoard.class);
		board.clear(ASPEM_01_NAME);
		
		return;
	}
	
	
	
	/**
	 * Test an auction with the following characteristics:
	 *    - 1 blocks.
	 *    - producer P1 offers (50)
	 *    - consumer C1 demands (50)
	 *    
	 * Expected solution:
	 * 
	 *    - P1 sells all his production (50).
	 *    - C1 buys all his demand (50) to P1, the only producer.
	 *    
	 * The Log file shows the following result:
	 * 
	 *     CONSUMER,C-50.0/50.0
	 *     PRODUCER,P-50.0/50.0
	 *     
	 */	
	@Test
	public void testAuction01() throws Exception {
		
		InputStream istream = this.getClass().getResourceAsStream("/program/p_exp03_01.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		
		// Create a mock of the load's forecast service.
		final String NAME_PRODUCER = "PRODUCER";
		final String NAME_CONSUMER = "CONSUMER";
		LoadForecastService mockForecast = mock(LoadForecastService.class);
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(NAME_CONSUMER), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 250f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(NAME_PRODUCER), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 250f, 200f, 0f});
		InjectionUtils.setServicesModule(new ForecastMockModule(mockForecast));
		
		initPlatform(0);
		
		AgentTracker producerTracker = mock(AgentTracker.class);
		AgentTracker consumerTracker = mock(AgentTracker.class);
		aspem01.addBrokerAgent(
				NAME_PRODUCER,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_1_producer.xml")),
				producerTracker);
		aspem01.addBrokerAgent(
				NAME_CONSUMER,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_1_consumer.xml")),
				consumerTracker);
		

		// Run the simulation.
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, STORE_NAME, asdrProgram);
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:00:00Z").getTime()));
		platform.informSimulationFinished(ID_SIMULATION);
		
		verify(producerTracker).auctionOfferAccepted(0, 50.0f);
		verify(consumerTracker).auctionDemandCovered(0, 50.0f);
		verify(producerTracker).distributeEvent(new float[] {2.0f});
		verify(consumerTracker, never()).distributeEventReceived();
		
		return;
	}
	
	
	/**
	 * Test an auction with the following characteristics:
	 *    - 3 blocks.
	 *    - producer P1 offers (150, 150, 0)
	 *    - producer P2 offers (150, 100, 150)
	 *    - consumer C1 demands (50, 50, 150)
	 *    - consumer C2 demands (100, 100, 0)
	 *    - P1 is cheaper than P2
	 *    
	 * Expected solution:
	 * 
	 *    - P1 sells all his production (150) for the blocks 1 and 2 (since he is cheaper than P2).
	 *    - P2 doesn't achieve to sell any production for the blocks 1 and 2.
	 *    - P2 sells all his production (150) for the block 3, since he is the only productor.
	 *    
	 * The Log file shows the following result:
	 * 
	 *     c01,C-50.0/50.0,C-50.0/50.0,C-150.0/150.0
	 *     c02,C-100.0/100.0,C-100.0/100.0,-
	 *     p01,P-150.0/150.0,P-150.0/150.0,-
	 *     p02,-,-,P-150.0/150.0
	 *     
	 */
	@Test
	public void testAuction02() throws Exception {
		
		InputStream istream = this.getClass().getResourceAsStream("/program/p_exp03_02.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		
		// Create a mock of the load's forecast service.
		final String P01_NAME = "p01";
		final String P02_NAME = "p02";
		final String C01_NAME = "c01";
		final String C02_NAME = "c02";
		LoadForecastService mockForecast = mock(LoadForecastService.class);
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(P01_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 250f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(P02_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {400f, 250f, 150f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(C01_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 250f, 150f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(C02_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 200f, 100f, 0f});		
		InjectionUtils.setServicesModule(new ForecastMockModule(mockForecast));
		
		initPlatform(0);
		
		AgentTracker p01Tracker = mock(AgentTracker.class);
		AgentTracker p02Tracker = mock(AgentTracker.class);
		AgentTracker c01Tracker = mock(AgentTracker.class);
		AgentTracker c02Tracker = mock(AgentTracker.class);
		aspem01.addBrokerAgent(
				P01_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_2_p01.xml")),
				p01Tracker);
		aspem01.addBrokerAgent(
				P02_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_2_p02.xml")),
				p02Tracker);		
		aspem01.addBrokerAgent(
				C01_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_2_c01.xml")),
				c01Tracker);
		aspem01.addBrokerAgent(
				C02_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_2_c02.xml")),
				c02Tracker);		
		

		// Run the simulation.
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, STORE_NAME, asdrProgram);
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:00:00Z").getTime()));
		platform.informSimulationFinished(ID_SIMULATION);
		
//		verify(p01Tracker).auctionOfferAccepted(0, 150f);
//		verify(p01Tracker).auctionOfferAccepted(1, 150f);
//		verify(p02Tracker).auctionOfferAccepted(2, 150f);
//		verify(c01Tracker).auctionDemandCovered(0, 50f);
//		verify(c01Tracker).auctionDemandCovered(1, 50f);
//		verify(c01Tracker).auctionDemandCovered(2, 150f);
//		verify(c02Tracker).auctionDemandCovered(0, 100f);
//		verify(c02Tracker).auctionDemandCovered(1, 100f);
		
//		verify(c01Tracker, never()).distributeEventReceived();
//		verify(c02Tracker).distributeEvent(new float[] {2.0f});
//		verify(p01Tracker).distributeEvent(new float[] {2.0f});
//		verify(p02Tracker).distributeEvent(new float[] {1.0f, 3.0f});
		
		return;
	}
	
	
	@Test
	public void testWeightedAuctionBoard() throws Exception {
		InputStream istream = this.getClass().getResourceAsStream("/program/p_exp03_01.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		
		// Create a mock of the load's forecast service.
		final String P01_NAME = "p01";
		final String P02_NAME = "p02";
		final String C01_NAME = "c01";
		final String C02_NAME = "c02";
		final String C03_NAME = "c03";
		LoadForecastService mockForecast = mock(LoadForecastService.class);
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(P01_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 200f, 150f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(P02_NAME), isA(Date.class), isA(Date.class)))
		.thenReturn(new Float[] {300f, 200f, 150f, 0f});		
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(C01_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 200f, 150f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(C02_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 200f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(C03_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 200f, 100f, 0f});		
		InjectionUtils.setServicesModule(new ForecastMockModule(mockForecast));
		
		initPlatform(0);
		
		AgentTracker p01Tracker = mock(AgentTracker.class);
		aspem01.addBrokerAgent(
				P01_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_1_producer.xml")),
				p01Tracker);
		AgentTracker p02Tracker = mock(AgentTracker.class);
		aspem01.addBrokerAgent(
				P02_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_1_producer.xml")),
				p02Tracker);		
		AgentTracker c01Tracker = mock(AgentTracker.class);
		aspem01.addBrokerAgent(
				C01_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_1_consumer.xml")),
				c01Tracker);
		AgentTracker c02Tracker = mock(AgentTracker.class);
		aspem01.addBrokerAgent(
				C02_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_1_consumer.xml")),
				c02Tracker);
		AgentTracker c03Tracker = mock(AgentTracker.class);
		aspem01.addBrokerAgent(
				C03_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_1_consumer.xml")),
				c03Tracker);		
		

		// Run the simulation.
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, STORE_NAME, asdrProgram);
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:00:00Z").getTime()));
		platform.informSimulationFinished(ID_SIMULATION);
		
		return;
	}
	
	
	
	/**
	 * Some consumers do not find production for their demand.
	 */
	@Test
	public void testAuctionWithLackOfProduction() throws Exception {
		InputStream istream = this.getClass().getResourceAsStream("/program/p_exp03_01.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		
		// Create a mock of the load's forecast service.
		final String P01_NAME = "p01";
		final String C01_NAME = "c01";
		final String C02_NAME = "c02";
		final String C03_NAME = "c03";
		LoadForecastService mockForecast = mock(LoadForecastService.class);
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(P01_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 250f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(C01_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 250f, 150f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(C02_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 220f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(C03_NAME), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 210f, 100f, 0f});		
		InjectionUtils.setServicesModule(new ForecastMockModule(mockForecast));
		
		initPlatform(0);
		
		AgentTracker p01Tracker = mock(AgentTracker.class);
		AgentTracker c01Tracker = mock(AgentTracker.class);
		AgentTracker c02Tracker = mock(AgentTracker.class);
		AgentTracker c03Tracker = mock(AgentTracker.class);
		aspem01.addBrokerAgent(
				P01_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_2_p01.xml")),
				p01Tracker);
		aspem01.addBrokerAgent(
				C01_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_2_c01.xml")),
				c01Tracker);
		aspem01.addBrokerAgent(
				C02_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_2_c02.xml")),
				c02Tracker);
		aspem01.addBrokerAgent(
				C03_NAME,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_2_c02.xml")),
				c03Tracker);		
		

		// Run the simulation.
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, STORE_NAME, asdrProgram);
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:00:00Z").getTime()));
		platform.informSimulationFinished(ID_SIMULATION);
		
//		verify(p01Tracker).auctionOfferAccepted(0, 130f);
//		verify(c01Tracker).auctionDemandCovered(0, 50f);
//		verify(c02Tracker).auctionDemandCovered(0, 80f);
		
//		verify(c01Tracker, never()).distributeEventReceived();
//		verify(c02Tracker, never()).distributeEventReceived();
//		verify(c03Tracker).distributeEvent(new float[] {1.0f});
//		verify(p01Tracker).distributeEvent(new float[] {2.0f});	
		
		return;		
	}
	
	
	
	/**
	 * 
	 * 
	 * Injection module for getting a Configuration file loaded from the
	 * file "config-00.properties".
	 * 
	 *
	 */
	public static class ConfigModule00 extends AbstractModule {

		@Override
		protected void configure() {
			return;
		}
		
		@Provides
		Configuration configurationLoader() {
			ConfigurationLoader loader = new ConfigurationLoader(
					this.getClass().getResource("/samples/properties/config-00.properties").getFile());
			Configuration config = loader.getConfiguration();
			config.set(Configuration.PROP_AUCTION_USE_START_PRICE, "false");
			
			return config;
		}
	}	
	
	
	
	/**
	 * 
	 * Mock injection module in order to simulate the behaviour of the forecast service.
	 */
	class ForecastMockModule extends AbstractModule {

		private final LoadForecastService forecastService;


		/**
		 * Constructor.
		 */
		public ForecastMockModule(LoadForecastService forecastService) {
			this.forecastService = forecastService;
			return;
		}


		/**
		 * Binding configuration.
		 */
		@Override
		protected void configure() {
			bind(LoadForecastService.class).toInstance(forecastService);
			return;
		}
	}
}
