package es.siani.energyagents.xmpp;

import org.junit.Ignore;

import com.google.inject.Guice;
import com.google.inject.Injector;

import es.siani.energyagents.injection.PlatformModule;
import es.siani.energyagents.injection.XmppModule;
import junit.framework.TestCase;

@Ignore
public class TestSimpleXmppImpl extends TestCase {
	
	/** This Injector instantiates a SimpleXmppImpl object for the SimpleXmpp class. */
	protected static Injector _injector = Guice.createInjector(
			new PlatformModule(),
			new XmppModule());
	
	/**
	 * Test the connection and disconnection actions.
	 */
	public void testConnectionDisconnection() {
		SimpleXmppImpl xmpp = _injector.getInstance(SimpleXmppImpl.class);
		
		xmpp.connect("broker01");
		assertNotNull(xmpp.conn);
		assertTrue(xmpp.conn.isConnected());
		
		xmpp.disconnect();
		assertNull(xmpp.conn);
		
		return;
	}
	

}
