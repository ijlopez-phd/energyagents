package es.siani.energyagents.utils;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Provides;

import es.siani.energyagents.config.Configuration;
import es.siani.energyagents.config.ConfigurationLoader;
import es.siani.energyagents.injection.InjectionUtils;

@RunWith(JUnit4.class)
public class TestCronLevelsSchedule {
	
	private static final TimeZone UTC_TZ = TimeZone.getTimeZone("UTC");
	private Injector injector = InjectionUtils.injector();
	
	/**
	 * Initialize resources for each test.
	 */
	@Before
	public void setUp() throws Exception {
		return;
	}
	
	
	/**
	 * Close resources for each test.
	 */
	@After
	public void tearDown() {
		return;
	}
	
	
	/**
	 * Test parsing the schedule definition.
	 */
	@Test
	public void testParseSchedule() {
		
		StringBuilder sch = new StringBuilder();
		sch.append("0 0 1 8 * 101;\n");
		sch.append("30 8 1 8 * 201;\n");
		sch.append("0 14 1 8 * 102;");
		sch.append("30 20 1 8 * 202;");
		sch.append("0  23 1 8 * 103;");
		
		CronLevelsScheduleStore schedulesStore = injector.getInstance(CronLevelsScheduleStore.class);
		schedulesStore.clearValues();
		CronScheduleItem[] items = schedulesStore.parseSchedule(sch.toString());
		
		assertEquals(5, items.length);
		
		CronScheduleItem item = items[0];
		Calendar cal = Calendar.getInstance(UTC_TZ);
		cal.clear();
		cal.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.AUGUST, 1, 0, 0, 0);
		assertEquals(101, item.getValue(), 0.0001);
		assertEquals(cal.getTimeInMillis(), item.getTime().getTime(), 1000);
		
		item = items[1];
		cal.clear();
		cal.set(CronLevelsScheduleStore.WORKING_YEAR, 7, 1, 8, 30, 0);
		assertEquals(201, item.getValue(), 0.0001);
		assertEquals(cal.getTimeInMillis(), item.getTime().getTime(), 1000);
		
		item = items[2];
		cal.clear();
		cal.set(CronLevelsScheduleStore.WORKING_YEAR, 7, 1, 14, 0, 0);
		assertEquals(102, item.getValue(), 0.0001);
		assertEquals(cal.getTimeInMillis(), item.getTime().getTime(), 1000);		
		
		return;
	}
	
	
	/**
	 * Test parsing a schedule line.
	 */
	@Test
	public void testParseLine() {
		
		CronLevelsScheduleStore schedulesStore = injector.getInstance(CronLevelsScheduleStore.class);
		schedulesStore.clearValues();
		
		// Check a correct line (minutes to zero).
		Calendar cal = Calendar.getInstance(UTC_TZ);
		String line = "0 6 1 1 * 2"; // 1-1-yyyyT6:00H  = 2
		cal.set(CronLevelsScheduleStore.WORKING_YEAR, 0, 1, 6, 0, 0);
		CronScheduleItem item = schedulesStore.parseLine(line);
		assertEquals(cal.getTimeInMillis(), item.getTime().getTime(), 1000);
		assertEquals(2.0, item.getValue(), 0.0001);
		
		// Check a correct line (minutes to thirty).
		line = "30 6 1 1 * 3"; // 1-1-yyyyT6:30H = 3
		cal.set(CronLevelsScheduleStore.WORKING_YEAR, 0, 1, 6, 30, 0);
		item = schedulesStore.parseLine(line);
		assertEquals(cal.getTimeInMillis(), item.getTime().getTime(), 1000);
		assertEquals(3.0, item.getValue(), 0.0001);
		
		// Check a non-valid line (with ranges of values).
		line = "30 6 1-5 2-3 * 4";
		try {
			item = schedulesStore.parseLine(line);
			fail("An execption should have been triggered.");
		} catch (IllegalArgumentException iex) {
			// It's ok.
		}
		
		// Check a non-valid line (with list of values).
		line = "30 6 1,5 2,3 * 4";
		try {
			item = schedulesStore.parseLine(line);
			fail("An execption should have been triggered.");
		} catch (IllegalArgumentException iex) {
			// It's ok.
		}
		
		// Check a non-valid line (the day of the week is defined).
		line = "30 6 1 1 2 4";
		try {
			item = schedulesStore.parseLine(line);
			fail("An execption should have been triggered.");
		} catch (IllegalArgumentException iex) {
			// It's ok.
		}
		
		// The below restriction does not exist anymore.
//		// Check a non-valid line (the value of minutes field is not valid).
//		line = "3 6 1 1 * 4";
//		try {
//			item = schedulesStore.parseLine(line);
//			fail("An execption should have been triggered.");
//		} catch (IllegalArgumentException iex) {
//			// It's ok.
//		}
		
		return;
	}
	
	
	/**
	 * Check saving a schedule into the store.
	 */
	@Test
	public void testSaveScheduleDefinition() {
		
		final String name01 = "sch01";
		final String name02 = "sch02";
		
		StringBuilder definition01 = new StringBuilder();
		definition01.append("0 0 1 8 * 101;\n");
		definition01.append("30 8 1 8 * 201;\n");
		definition01.append("0 14 1 8 * 102;");
		definition01.append("30 20 1 8 * 202;");
		definition01.append("0  23 1 8 * 103;");
		
		StringBuilder definition02 = new StringBuilder();
		definition02.append("30 5 1 9 * 202;");
		
		CronLevelsScheduleStore store = injector.getInstance(CronLevelsScheduleStore.class);
		
		store.clearValues();
		store.saveSchedule(name01, definition01.toString());
		store.saveSchedule(name02, definition02.toString());
		
		Map<String, CronScheduleItem[]> values = store.getStore();
		assertEquals(2, values.size());
		assertEquals(5, values.get(name01).length);
		assertEquals(1, values.get(name02).length);
		
		return;
	}
	
	
	/**
	 * Test getting a value of a schedule for a specific moment.
	 */
	@Test
	public void testGetValue() {
		
		final String name = "sch01";
		StringBuilder definition = new StringBuilder();
		definition.append("0 0 1 8 * 101;\n");
		definition.append("30 8 1 8 * 201;\n");
		definition.append("0 14 1 8 * 102;");
		definition.append("30 20 1 8 * 202;");
		definition.append("0  23 1 8 * 103;");
		
		CronLevelsScheduleStore store = injector.getInstance(CronLevelsScheduleStore.class);
		store.clearValues();
		store.saveSchedule(name, definition.toString());
		
		Calendar cal = Calendar.getInstance(UTC_TZ);
		cal.clear();
		cal.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.AUGUST, 1, 2, 25, 0);
		Double value = store.getValue(name, cal.getTime());
		assertEquals(101, value, 0.0001);
		
		cal.clear();
		cal.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.AUGUST, 1, 8, 31, 0);
		value = store.getValue(name, cal.getTime());
		assertEquals(201, value, 0.0001);
		
		cal.clear();
		cal.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.AUGUST, 1, 22, 0, 0);
		value = store.getValue(name, cal.getTime());
		assertEquals(202, value, 0.0001);
		
		cal.clear();
		cal.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.AUGUST, 3, 0, 0, 0);
		value = store.getValue(name, cal.getTime());
		assertEquals(103, value, 0.0001);	
		
		cal.clear();
		cal.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.JULY, 1, 0, 0, 0);
		value = store.getValue(name, cal.getTime());
		assertNull(value);
		
		return;
	}
	
	
	/**
	 * Test getting the values corresponding to a range of time.
	 */
	@Test
	public void testGetValues() {
		
		final String name = "sch01";
		StringBuilder definition = new StringBuilder();
		definition.append("0 0 1 8 * 101;\n");
		definition.append("30 8 1 8 * 201;\n");
		definition.append("0 14 1 8 * 102;");
		definition.append("30 20 1 8 * 202;");
		definition.append("0  23 1 8 * 103;");
		
		CronLevelsScheduleStore store = injector.getInstance(CronLevelsScheduleStore.class);
		store.clearValues();
		store.saveSchedule(name, definition.toString());
		
		Calendar calStart = Calendar.getInstance(UTC_TZ);
		calStart.clear();
		calStart.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.AUGUST, 1, 2, 0, 0); // 2am
		Calendar calEnd = Calendar.getInstance(UTC_TZ);
		calEnd.clear();
		calEnd.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.AUGUST, 1, 21, 0, 0); // 21pm
		
		CronScheduleItem[] items = store.getValues(name, calStart.getTime(), calEnd.getTime());
		assertEquals(4, items.length);
		assertEquals(calStart.getTimeInMillis(), items[0].getTime().getTime());
		assertEquals(101, items[0].getValue(), 0.001);
		assertEquals(201, items[1].getValue(), 0.001);
		assertEquals(102, items[2].getValue(), 0.001);
		assertEquals(202, items[3].getValue(), 0.001);
		
		// This date is previous to the period covered by the schedule. Null should be returned.
		calStart.clear();
		calStart.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.JULY, 1, 0, 0, 0);
		calEnd.clear();
		calEnd.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.JULY, 2, 0, 0, 0);
		items = store.getValues(name, calStart.getTime(), calEnd.getTime());
		assertEquals(0, items.length);
		
		
		// This date is later to the period covered by the schedule. The last value should be returned.
		calStart.clear();
		calStart.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.SEPTEMBER, 1, 0, 0, 0);
		calEnd.clear();
		calEnd.set(CronLevelsScheduleStore.WORKING_YEAR, Calendar.SEPTEMBER, 2, 0, 0, 0);
		items = store.getValues(name, calStart.getTime(), calEnd.getTime());
		assertEquals(1, items.length);
		assertEquals(103, items[0].getValue(), 0.0001);
		assertEquals(calStart.getTimeInMillis(), items[0].getTime().getTime());
		
		return;
	}
	
	
	/**
	 * Test get the values by block.
	 */
	@Test
	public void testGetValuesByBlock() {
		
		InjectionUtils.setConfigModule(new ConfigModule00());
		
		final String name = "sch01";
		StringBuilder definition = new StringBuilder();
		definition.append("0 0 1 8 * 101;\n");
		definition.append("30 0 1 8 * 102;\n");
		definition.append("0 1 1 8 * 103;\n");
		definition.append("30 1 1 8 * 103;\n");
		definition.append("0 2 1 8 * 201;\n");
		definition.append("30 2 1 8 * 202;\n");
		
		CronLevelsScheduleStore store = InjectionUtils.injector().getInstance(CronLevelsScheduleStore.class);
		store.clearValues();
		store.saveSchedule(name, definition.toString());
		
		// Case #0: Perfect case: start time and end time fit with the schedule.
		Calendar cal = Calendar.getInstance(UTC_TZ);
		cal.set(2013, Calendar.AUGUST, 1, 0, 0, 0);
		Date startTime = cal.getTime();
		cal.set(2013, Calendar.AUGUST, 1, 3, 0, 0);
		Date endTime = cal.getTime();
		
		int[] values = store.getValuesByBlock(name, startTime, endTime);
		assertEquals(6, values.length);
		int[] expectedValues = new int[] {101, 102, 103, 103, 201, 202};
		Assert.assertArrayEquals(expectedValues, values);
		
		// Case #1: End time is preivous to the beginning of the schedule.
		cal.set(2013, Calendar.JULY, 1, 0, 0, 0);
		startTime = cal.getTime();
		cal.set(2013, Calendar.JULY, 1, 1, 0, 0);
		endTime = cal.getTime();
		values = store.getValuesByBlock(name, startTime, endTime);
		expectedValues = new int[] {0, 0};
		Assert.assertArrayEquals(expectedValues, values);
		
		// Case #2: Start time is after the last value of the schedule.
		cal.set(2013, Calendar.SEPTEMBER, 1, 0, 0, 0);
		startTime = cal.getTime();
		cal.set(2013, Calendar.SEPTEMBER, 1, 1, 0, 0);
		endTime = cal.getTime();
		values = store.getValuesByBlock(name, startTime, endTime);
		expectedValues = new int[] {202, 202};
		Assert.assertArrayEquals(expectedValues, values);
		
		// Case #3: Start time is before the beginning of the schedule.
		cal.set(2013, Calendar.JULY, 31, 23, 30, 0);
		startTime = cal.getTime();
		cal.set(2013, Calendar.AUGUST, 1, 2, 30, 0);
		endTime = cal.getTime();
		values = store.getValuesByBlock(name, startTime, endTime);
		expectedValues = new int[] {0, 101, 102, 103, 103, 201};
		Assert.assertArrayEquals(expectedValues, values);
		
		// Case #4: Again the start time is before the beginning of the schedule.
		cal.set(2013, Calendar.JULY, 31, 22, 0, 0);
		startTime = cal.getTime();
		cal.set(2013, Calendar.AUGUST, 1, 0, 30, 0);
		endTime = cal.getTime();
		values = store.getValuesByBlock(name, startTime, endTime);
		expectedValues = new int[] {0, 0, 0, 0, 101};
		Assert.assertArrayEquals(expectedValues, values);
		
		// Case #5: Star time is within the schedule but not the end time.
		cal.set(2013, Calendar.AUGUST, 1, 1, 0, 0);
		startTime = cal.getTime();
		cal.set(2013, Calendar.AUGUST, 1, 4, 0, 0);
		endTime = cal.getTime();
		values = store.getValuesByBlock(name, startTime, endTime);
		expectedValues = new int[] {103, 103, 201, 202, 202, 202};
		Assert.assertArrayEquals(expectedValues, values);
		
		return;
	}
	
	
	/**
	 * Test the method that verify if a cron value corresponds to a consuming level.
	 */
	@Test
	public void testIsConsumer() {
		
		assertTrue(CronLevelsScheduleStore.isConsumer(201));
		assertTrue(CronLevelsScheduleStore.isConsumer(202));
		assertTrue(CronLevelsScheduleStore.isConsumer(203));
		assertTrue(CronLevelsScheduleStore.isConsumer(211));
		assertTrue(CronLevelsScheduleStore.isConsumer(212));
		assertTrue(CronLevelsScheduleStore.isConsumer(213));
		
		assertFalse(CronLevelsScheduleStore.isConsumer(101));
		assertFalse(CronLevelsScheduleStore.isConsumer(102));
		assertFalse(CronLevelsScheduleStore.isConsumer(103));
		assertFalse(CronLevelsScheduleStore.isConsumer(111));
		assertFalse(CronLevelsScheduleStore.isConsumer(112));
		assertFalse(CronLevelsScheduleStore.isConsumer(113));
		
		return;
	}
	
	
	/**
	 * Test the method that verify if a cron value corresponds to a producing level.
	 */
	@Test
	public void testIsProducer() {
		
		assertTrue(CronLevelsScheduleStore.isProducer(101));
		assertTrue(CronLevelsScheduleStore.isProducer(102));
		assertTrue(CronLevelsScheduleStore.isProducer(103));
		assertTrue(CronLevelsScheduleStore.isProducer(111));
		assertTrue(CronLevelsScheduleStore.isProducer(112));
		assertTrue(CronLevelsScheduleStore.isProducer(113));
		
		assertFalse(CronLevelsScheduleStore.isProducer(201));
		assertFalse(CronLevelsScheduleStore.isProducer(202));
		assertFalse(CronLevelsScheduleStore.isProducer(203));
		assertFalse(CronLevelsScheduleStore.isProducer(211));
		assertFalse(CronLevelsScheduleStore.isProducer(212));
		assertFalse(CronLevelsScheduleStore.isProducer(213));		
		
		return;
	}
	
	
	/**
	 * Test the method that verify if a cron value is a combined value.
	 */
	@Test
	public void testIsCombined() {
		
		assertTrue(CronLevelsScheduleStore.isCombined(111));
		assertTrue(CronLevelsScheduleStore.isCombined(211));
		assertTrue(CronLevelsScheduleStore.isCombined(113));
		assertTrue(CronLevelsScheduleStore.isCombined(213));
		
		assertFalse(CronLevelsScheduleStore.isCombined(101));
		assertFalse(CronLevelsScheduleStore.isCombined(201));
		assertFalse(CronLevelsScheduleStore.isCombined(103));
		assertFalse(CronLevelsScheduleStore.isCombined(203));
		
		return;
	}
	
	
	/**
	 * Injection module for getting a Configuration file loaded from the
	 * file "config-00.properties".
	 *
	 */
	public class ConfigModule00 extends AbstractModule {

		@Override
		protected void configure() {
			return;
		}
		
		@Provides
		Configuration configurationLoader() {
			ConfigurationLoader loader = new ConfigurationLoader(
					this.getClass().getResource("/samples/properties/config-00.properties").getFile());
			return loader.getConfiguration();
		}
		
	}
}
