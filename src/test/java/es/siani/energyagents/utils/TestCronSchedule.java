package es.siani.energyagents.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

import com.google.inject.Injector;

import es.siani.energyagents.injection.InjectionUtils;
import junit.framework.TestCase;

public class TestCronSchedule extends TestCase {
	
	private Injector injector = InjectionUtils.injector();
	
	
	/**
	 * Test saving new schedule definitions.
	 */
	@Test
	public void testSaveScheduleDefinition() {
		
		CronStandardScheduleStore sch = injector.getInstance(CronStandardScheduleStore.class);
		
		String definition = "*  6-8  * 1-3 *   6; 20-40 5-6 1 2 * 2";
		sch.saveSchedule("levels_1", definition);
		
		definition = "10-15,20-26  5-6,9  9,1-5  9-12,2  *  3; 10-15,20-26  5-6,9  9,1-5  9-12,2  *  3";
		sch.saveSchedule("levels_2", definition);
		
		assertEquals(2, sch.schedules.size());
		
		return;
	}
	
	
	/**
	 * Test getting the value from a schedule that corresponds to a specific moment.
	 */
	@Test
	public void testGetValue() {
		
		CronStandardScheduleStore sch = injector.getInstance(CronStandardScheduleStore.class);
		
		StringBuilder sb = new StringBuilder();
		sb.append("*  5-8    20-30  1-3,6-9  *  3.5;\n");
		sb.append("*  1-2    20-30  1-3,6-9  *  4.5;\n");
		sb.append("*  20-23  20-30  1-3,6-9  *  5.5;");
		
		final String name = "sch_1";
		
		sch.saveSchedule(name, sb.toString());
		
		Calendar cal = new GregorianCalendar(2013, 2, 22, 5, 0);
		assertEquals(3.5, sch.getValue(name, cal.getTime()), 0.01);
		
		cal.set(Calendar.HOUR, 2);
		assertEquals(4.5, sch.getValue(name, cal.getTime()), 0.01);
		
		cal.set(Calendar.HOUR, 21);
		assertEquals(5.5, sch.getValue(name, cal.getTime()), 0.01);
		
		return;
	}
	
	
	/**
	 * Test the method 'checkFieldValue'.
	 */
	@Test
	public void testCheckFieldValue() {
		
		CronStandardScheduleStore sch = injector.getInstance(CronStandardScheduleStore.class);
		
		assertTrue(sch.checkFieldValue(Calendar.HOUR_OF_DAY, "5", 5));
		assertTrue(sch.checkFieldValue(Calendar.HOUR_OF_DAY, "5-10", 6));
		assertFalse(sch.checkFieldValue(Calendar.HOUR_OF_DAY, "5-10", 1));
		
		assertTrue(sch.checkFieldValue(Calendar.DAY_OF_WEEK, "0", Calendar.SUNDAY));
		assertTrue(sch.checkFieldValue(Calendar.DAY_OF_WEEK, "7", Calendar.SUNDAY));
		assertTrue(sch.checkFieldValue(Calendar.DAY_OF_WEEK, "0-3", Calendar.WEDNESDAY));
		assertFalse(sch.checkFieldValue(Calendar.DAY_OF_WEEK, "0-3", Calendar.THURSDAY));
		
		assertTrue(sch.checkFieldValue(Calendar.MONTH, "1-4", 3));
		assertFalse(sch.checkFieldValue(Calendar.MONTH, "1-4", 5));
		
		return;
	}
	
	
	/**
	 * Test the method 'checkScheduleLine'.
	 */
	@Test
	public void testCheckScheduleLine() {
		
		CronStandardScheduleStore sch = injector.getInstance(CronStandardScheduleStore.class);
		
		Calendar cal = new GregorianCalendar(2013, 2, 1, 6, 0);
		assertEquals(
				6f,
				sch.checkScheduleLine("*  6-8  * 1-3 *   6", cal), 0.01);
		
		cal.set(Calendar.MINUTE, 25);
		assertEquals(
				2f,
				sch.checkScheduleLine("20-40 5-6 1 2 * 2", cal), 0.01);
		
		assertEquals(
				3f,
				sch.checkScheduleLine("10-15,20-26  5-6,9  9,1-5  9-12,2  *  3", cal), 0.01);
		
		cal.set(Calendar.MINUTE, 30);
		assertNull(
				sch.checkScheduleLine("10-15,20-26  5-6,9  9,1-5  9-12,2  *  3", cal));		
		
		return;
		
	}
	
	
	/**
	 * Test the method 'splitScheduleLines'.
	 */
	@Test
	public void testSplitScheduleLines() {
		
		CronStandardScheduleStore sch = injector.getInstance(CronStandardScheduleStore.class);
		
		final String line_1 = "*  6-8  * 1-3 *   6";
		final String line_2 = "20-40 5-6 1 2 * 2"; 
		
		StringBuilder sb = new StringBuilder();
		sb.append(line_1);
		sb.append("; ");
		sb.append(line_2);
		sb.append(";");
		
		String[] lines = sch.splitScheduleLines(sb.toString());
		assertEquals(2, lines.length);
		assertEquals(line_1, lines[0]);
		assertEquals(line_2, lines[1]);
		
		sb = new StringBuilder();
		sb.append(line_1);
		sb.append(";\n");
		sb.append(line_2);
		sb.append(";");
		
		lines = sch.splitScheduleLines(sb.toString());
		assertEquals(2, lines.length);
		assertEquals(line_1, lines[0]);
		assertEquals(line_2, lines[1]);
		
		return;
	}
	
	

}
