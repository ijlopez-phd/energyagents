package es.siani.energyagents;

import java.io.InputStream;

import javax.xml.transform.stream.StreamSource;

import org.junit.Assert;
import org.junit.Test;

import com.google.inject.Injector;

import es.siani.energyagents.AgentsContainerListener;
import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.AspemContainer;
import es.siani.energyagents.EnergyAgentsException;
import es.siani.energyagents.GridopContainer;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.injection.XmppMockModule;
import es.siani.energyagents.model.UserPrefs;
import jade.wrapper.ControllerException;
import junit.framework.TestCase;
import static org.mockito.Mockito.*;

public class TestAspemContainer extends TestCase {
	
	// A mock of the XmppModule is used in this TestCase (a real XMPP server is not needed).
	static {
		InjectionUtils.setXmppModule(new XmppMockModule());
	}
	
	private Injector injector = InjectionUtils.injector();
	
	private AgentsPlatform platform = injector.getInstance(AgentsPlatform.class);
	
	private GridopContainer gridopContainer;
	
	
	
	/**
	 * Initialize resources.
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		// An instance of the Gridop container must exist during the tests.
		this.gridopContainer = platform.createGridopContainer(null);
		
		return;
	}
	
	
	/**
	 * Close resources used in the tests.
	 */
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		
		// Kill the Gridop container.
		this.gridopContainer.kill();
		
		return;
	}
	
	
	/**
	 * Test the creation and finalization of a Container of broker agents.
	 * @throws ControllerException 
	 */
	@Test
	public void testCreateKillContainer() throws ControllerException {
		
		final String cName = "test-container-1";
		AspemContainer bcontainer = platform.createAspemContainer(cName, null);
		
		assertNotNull(bcontainer);
		assertEquals(cName, bcontainer.getName());
		assertEquals(false, bcontainer.isKilled());
		
		bcontainer.kill();
		assertEquals(true, bcontainer.isKilled());
		
		return;
	}
	
	
	/**
	 * Test that the container calls the registered listeners.
	 */
	@Test
	public void testListener() {
		
		final String cName = "test-container-1";
		final AspemContainer bcontainer = platform.createAspemContainer(cName, null);
		
		AgentsContainerListener mockListener = mock(AgentsContainerListener.class);
		bcontainer.addListener(mockListener);

		bcontainer.kill();
		verify(mockListener).containerKilled(cName, bcontainer);
		
		return;
	}
	

	/**
	 * Tests the creation of broker agents.
	 */
	@Test
	public void testAddBrokerAgent() throws EnergyAgentsException, ControllerException {
		
		final String cName = "test-container-2";
		final String agentName = "home01";
		
		AspemContainer bcontainer = platform.createAspemContainer(cName, null);
		bcontainer.addBrokerAgent(agentName, null);
		
		assertNotNull(bcontainer.container().getAgent(agentName));
		assertEquals(
				agentName + "@" + this.platform.getId(),
				bcontainer.container().getAgent(agentName).getName());
		
		bcontainer.kill();
		
		return;
	}
	
	
	/**
	 * Tests the searching of created broker agents.
	 */
	@Test
	public void testIsBrokerAgent() throws EnergyAgentsException, ControllerException {
		
		final String cName = "test-container-3";
		final String agentName = "home02";
		AspemContainer bcontainer = platform.createAspemContainer(cName, null);
		bcontainer.addBrokerAgent(agentName, null);
		
		assertEquals(true, bcontainer.existBrokerAgent(agentName));
		assertEquals(false, bcontainer.existBrokerAgent("non-existent-agent"));
		
		bcontainer.kill();
		return;
	}
	
	
	/**
	 * Test parsing the UserPrefs.
	 */
	@Test
	public void testParseXmlUserPrefs() throws EnergyAgentsException {
		
		AspemContainer bcontainer = platform.createAspemContainer("test-container", null);
		
		// Each sample is parsed twice: without using schemas and using them.
		
		InputStream istream = this.getClass().getResourceAsStream("/samples/user-prefs/user-prefs-00.xml");
		UserPrefs userPrefs = bcontainer.parseXmlUserPrefs(new StreamSource(istream), false);
		istream = this.getClass().getResourceAsStream("/samples/user-prefs/user-prefs-00.xml");
		userPrefs = bcontainer.parseXmlUserPrefs(new StreamSource(istream), true);
		assertEquals(1, (int)userPrefs.getPriority());
		assertEquals("schedule-00", userPrefs.getLevelsSchedule().getName());
		assertEquals("hard", userPrefs.getLoadMode().toString());
		
		istream = this.getClass().getResourceAsStream("/samples/user-prefs/user-prefs-01.xml");
		userPrefs = bcontainer.parseXmlUserPrefs(new StreamSource(istream), false);
		istream = this.getClass().getResourceAsStream("/samples/user-prefs/user-prefs-01.xml");
		userPrefs = bcontainer.parseXmlUserPrefs(new StreamSource(istream), true);
		assertEquals("schedule-01", userPrefs.getLevelsSchedule().getName());		
		assertEquals("* * * 6-10 * 70;", userPrefs.getLevelsSchedule().getDefinition());
		assertEquals(0, userPrefs.getPriority().intValue());
		assertEquals("none", userPrefs.getLoadMode().toString());
		
		istream = this.getClass().getResourceAsStream("/samples/user-prefs/user-prefs-02.xml");
		userPrefs = bcontainer.parseXmlUserPrefs(new StreamSource(istream), false);
		istream = this.getClass().getResourceAsStream("/samples/user-prefs/user-prefs-02.xml");
		userPrefs = bcontainer.parseXmlUserPrefs(new StreamSource(istream), true);		
		assertNull(userPrefs.getLevelsSchedule());
		assertEquals(0, userPrefs.getPriority().intValue());
		assertEquals("none", userPrefs.getLoadMode().toString());
		
		istream = this.getClass().getResourceAsStream("/samples/user-prefs/user-prefs-03.xml");
		userPrefs = bcontainer.parseXmlUserPrefs(new StreamSource(istream), false);
		istream = this.getClass().getResourceAsStream("/samples/user-prefs/user-prefs-03.xml");
		userPrefs = bcontainer.parseXmlUserPrefs(new StreamSource(istream), true);		
		assertEquals("* * * 6-10 * 70;", userPrefs.getLevelsSchedule().getDefinition());
		assertEquals(1, userPrefs.getPriority().intValue());
//		assertEquals(2, userPrefs.getEasyLevel().intValue());
//		assertEquals(3, userPrefs.getHardLevel().intValue());
		assertEquals("easy", userPrefs.getLoadMode().toString());
		Assert.assertArrayEquals(new float[] {110,  120,  130}, userPrefs.getLevelsPrices(), 0.001f);
		
		return;
	}
}
