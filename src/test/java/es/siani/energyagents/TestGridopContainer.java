package es.siani.energyagents;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.inject.Injector;

import es.siani.energyagents.AgentsContainerListener;
import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.GridopContainer;
import es.siani.energyagents.injection.InjectionUtils;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class TestGridopContainer {
	
	private Injector injector = InjectionUtils.injector();
	private AgentsPlatform platform = injector.getInstance(AgentsPlatform.class);
	

	/**
	 * Checks the creation and finalization of the Gridop Container.
	 */
	@Test
	public void testCreateContainer() {
		
		GridopContainer container = platform.createGridopContainer(null);
		
		assertNotNull(container);
		assertEquals(false, container.isKilled());
		
		container.kill();
		assertEquals(true, container.isKilled());
		
		return;
	}
	
	
	/**
	 * Tests that the container calls the registered listeners.
	 */
	@Test
	public void testListener() {
		
		GridopContainer container = platform.createGridopContainer(null);
		String name = container.getName();
		
		AgentsContainerListener mockListener = mock(AgentsContainerListener.class);
		container.addListener(mockListener);
		
		container.kill();
		verify(mockListener).containerKilled(name, container);
		
		return;
	}
}
