package es.siani.energyagents.services;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

import com.google.inject.Injector;

import es.siani.energyagents.config.ConfigurationLoader;
import es.siani.energyagents.data.DataProviderHelper;
import es.siani.energyagents.injection.InjectionUtils;

@Ignore
@RunWith(JUnit4.class)
public class TestLoadForecast {
	
	/** Injector made up of the default modules. */
	private final Injector injector = InjectionUtils.injector();
	
	/** Interface for accessing the methods of the Forecast service. */
	private LoadForecastService srv;	
	
	
	/**
	 * Initialize resources for each test.
	 */
	@Before
	public void setUp() throws Exception {
		ConfigurationLoader.setDefaultConfigurationFile(
				this.getClass().getResource("/samples/properties/config-psql.properties").getPath());
		
		DataProviderHelper dataProvider = injector.getInstance(DataProviderHelper.class);
		this.srv = new LoadForecastServiceBatisImpl(dataProvider);
		
		return;
	}
	
	
	/**
	 * Close resources of each test.
	 */
	@After
	public void tearDown() throws Exception {
		return;
	}
	
	
	/**
	 * Test getting from the datastore the load of Asbox for a specific level during a period of time.
	 */
	@Test
	public void testGetLoadForLevel() {
		
		Calendar begin = new GregorianCalendar(2000, Calendar.AUGUST, 1, 12, 0, 0);
		Calendar end = new GregorianCalendar(2000, Calendar.AUGUST, 1, 13, 0, 0);
		
		Float load = this.srv.getLoadForLevel("ieee13", "i1B645", 1, begin.getTime(), end.getTime());
		assertNotNull(load);
		assertTrue(load > 0);
		
		return;
	}
	
	
	/**
	 * Test getting from the datastore the loads of an Asbox for all levels during a period of time.
	 */
	@Test
	public void testGetLoadForAllLevels() {
		
		Calendar begin = new GregorianCalendar(2000, Calendar.AUGUST, 1, 12, 0, 0);
		Calendar end = new GregorianCalendar(2000, Calendar.AUGUST, 1, 13, 0, 0);
		
		Float[] loads = this.srv.getLoadForAllLevels("ieee13", "i1B645", begin.getTime(), end.getTime());
		
		assertNotNull(loads);
		assertEquals(4, loads.length);
		assertEquals(24.9795, loads[0], 0.0001);
		assertEquals(24.9795, loads[1], 0.0001);
		assertEquals(24.9795, loads[2], 0.0001);
		assertEquals(0.00f, loads[3], 0.0001);
		
		return;
	}

}
