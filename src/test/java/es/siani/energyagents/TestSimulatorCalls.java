package es.siani.energyagents;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.inject.Injector;

import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.EnergyAgentsException;
import es.siani.energyagents.GridopContainer;
import es.siani.energyagents.agent.AgentTracker;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.model.PauseResult;
import es.siani.simpledr.AsdrException;
import es.siani.simpledr.AsdrUtils;
import es.siani.simpledr.model.AsdrProgram;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class TestSimulatorCalls {
	
	private Injector injector = InjectionUtils.injector();
	
	
	/**
	 * Initializes resources.
	 */
	@Before
	public void setUp() throws Exception {
		return;
	}
	
	
	/**
	 * Closes resources.
	 */
	@After
	public void tearDown() throws Exception {
		return;
	}
	
	
	/**
	 * Tests informing that a new simulation has been started, paused or finished.
	 */
	@Test
	public void testPlatformServices() throws EnergyAgentsException, AsdrException, ParseException {
		
		final int ID_SIM = 1;
		
		AgentsPlatform platform = injector.getInstance(AgentsPlatform.class);
		
		AgentTracker gridopTracker = mock(AgentTracker.class);
		GridopContainer container = platform.createGridopContainer(gridopTracker);
		
		InputStream istream = this.getClass().getResourceAsStream("/program/p20a_simple_01.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		
		
		platform.informSimulationStarted(ID_SIM, "testSimulation", "", asdrProgram);
		verify(gridopTracker).simulationStarted();
		
		Date modelTime = new SimpleDateFormat("dd/MM/yyyy").parse("31/12/2012");
		platform.informSimulationPaused(ID_SIM, new Date(), modelTime);
		verify(gridopTracker).simulationPaused();
		
		platform.informSimulationFinished(ID_SIM);
		verify(gridopTracker).simulationFinished();
		
		container.kill();
		return;
	}
	
	
	/**
	 * Tests the pauses scheduled by the Gridop agent.
	 */
	@Test
	public void testPauses() throws AsdrException, EnergyAgentsException, ParseException, DatatypeConfigurationException  {
		
		AgentsPlatform platform = injector.getInstance(AgentsPlatform.class);
		
		AgentTracker gridopTracker = mock(AgentTracker.class);
		GridopContainer container = platform.createGridopContainer(gridopTracker);
		
		InputStream istream = this.getClass().getResourceAsStream("/program/p20a_pauses_01.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		
		final int ID_SIM = 1;
		platform.informSimulationStarted(ID_SIM, "testSimulationPauses", "", asdrProgram);
		verify(gridopTracker).simulationStarted();
		
		Date modelTime = df.parse("2013-01-01T00:00:00Z");
		PauseResult result = platform.informSimulationPaused(ID_SIM, new Date(), modelTime);
		modelTime = df.parse("2013-01-02T00:00:00Z");
		assertEquals(result.getNextPause(), modelTime.getTime());
		
		result = platform.informSimulationPaused(ID_SIM, new Date(), modelTime);
		modelTime = df.parse("2013-01-02T00:00:59Z");
		assertEquals(result.getNextPause(), modelTime.getTime());
		
		result = platform.informSimulationPaused(ID_SIM, new Date(), modelTime);
		assertEquals(result.getNextPause(), -1);
		
		modelTime = df.parse("2013-02-01T00:00:00Z");
		result = platform.informSimulationPaused(ID_SIM, new Date(), modelTime);
		assertEquals(result.getNextPause(), -1);
		verify(gridopTracker, times(4)).simulationPaused();
		
		platform.informSimulationFinished(ID_SIM);
		verify(gridopTracker).simulationFinished();
		
		container.kill();
		return;
	}
}
