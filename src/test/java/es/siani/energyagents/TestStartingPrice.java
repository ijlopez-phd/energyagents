package es.siani.energyagents;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Singleton;
import javax.xml.transform.stream.StreamSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Provides;

import es.siani.energyagents.agent.AgentTracker;
import es.siani.energyagents.behaviours.as04.AuctionBoard;
import es.siani.energyagents.config.Configuration;
import es.siani.energyagents.config.ConfigurationLoader;
import es.siani.energyagents.injection.BehavioursExp04Module;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.injection.PlatformExp04Module;
import es.siani.energyagents.injection.XmppMockModule;
import es.siani.energyagents.services.LoadForecastService;
import es.siani.simpledr.AsdrUtils;
import es.siani.simpledr.model.AsdrProgram;

@RunWith(JUnit4.class)
public class TestStartingPrice {
	
	private static final int ID_SIMULATION = 1;
	private static final String SCENARIO_CODE = "ieee13";
	private static final String STORE_NAME = "ieee13";
	private static final String ASPEM_01_NAME = "aspem01";	
	
	private Injector injector;
	
	private AgentsPlatform platform;
	private GridopContainer gridopContainer;
	private AspemContainer aspem01;	
	
	private AgentTracker gridopTracker;
	private AgentTracker aspemTracker01;
	private AgentTracker[] brokers01Tracker;	
	
	
	/**
	 * Constructor.
	 */
	public TestStartingPrice() throws Exception {
		return;
	}
	
	
	/**
	 * Initialize resources for each test.
	 */
	@Before
	public void setUp() throws Exception {
		
		InjectionUtils.setXmppModule(new XmppMockModule());
		InjectionUtils.setConfigModule(new ConfigModuleWithStartPrice());		
		InjectionUtils.setBehavioursModule(new BehavioursExp04Module());
		InjectionUtils.setPlatformModule(new PlatformExp04Module());
		
		this.platform = null;
		this.gridopContainer = null;
		this.aspem01 = null;
		return;
	}
	
	
	/**
	 * Close resources for each test.
	 */
	@After
	public void tearDown() {
		
		if ((aspem01 != null) && (!aspem01.isKilled())) {
			aspem01.kill();
			aspem01 = null;
			aspemTracker01 = null;
			brokers01Tracker = null;
		}
		
		if ((gridopContainer != null) && (!gridopContainer.isKilled())) {
			gridopContainer.kill();
			gridopContainer = null;
		}		
	}
	
	
	/**
	 * Initialize the elements of the platform.
	 */
	private void initPlatform(int nBrokers) {
		
		Injector injector = InjectionUtils.injector();
		this.platform = injector.getInstance(AgentsPlatform.class);		
		
		this.gridopTracker = mock(AgentTracker.class);
		this.gridopContainer = platform.createGridopContainer(gridopTracker);
		
		if (nBrokers < 0) {
			throw new IllegalArgumentException();
		}
		
		aspemTracker01 = mock(AgentTracker.class);
		aspem01 = platform.createAspemContainer(ASPEM_01_NAME, aspemTracker01);
		brokers01Tracker = new AgentTracker[nBrokers];
		for (int n = 0; n < nBrokers; n++) {
			AgentTracker tracker = mock(AgentTracker.class);
			aspem01.addBrokerAgent(Integer.toString(n), tracker);
			brokers01Tracker[n] = tracker;
		}
		
		AuctionBoard board = injector.getInstance(AuctionBoard.class);
		board.clear(ASPEM_01_NAME);
		
		return;
	}
	
	
	/**
	 * There is exchange because the price of the producer (60) is smaller than the starting price
	 * of the consumer.
	 */	
	@Test
	public void testProducerPriceSmallerThanStartingPrice() throws Exception {
		
		InputStream istream = this.getClass().getResourceAsStream("/program/p_exp03_01.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		
		// Create a mock of the load's forecast service.
		final String NAME_PRODUCER = "PRODUCER";
		final String NAME_CONSUMER = "CONSUMER";
		LoadForecastService mockForecast = mock(LoadForecastService.class);
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(NAME_CONSUMER), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 250f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(NAME_PRODUCER), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 250f, 200f, 0f});
		InjectionUtils.setServicesModule(new ForecastMockModule(mockForecast));
		
		initPlatform(0);
		
		AgentTracker producerTracker = mock(AgentTracker.class);
		AgentTracker consumerTracker = mock(AgentTracker.class);
		aspem01.addBrokerAgent(
				NAME_PRODUCER,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_1_cheap_producer.xml")),
				producerTracker);
		aspem01.addBrokerAgent(
				NAME_CONSUMER,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_1_consumer.xml")),
				consumerTracker);
		

		// Run the simulation.
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, STORE_NAME, asdrProgram);
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:00:00Z").getTime()));
		platform.informSimulationFinished(ID_SIMULATION);
		
		verify(producerTracker).distributeEvent(new float[] {2.0f});
		verify(consumerTracker, never()).distributeEventReceived();
		
		return;		
	}
	
	
	/**
	 * There is not exchange because the price of the producer (120) is greater than the starting price
	 * of the consumer.
	 */
	@Test
	public void testProducerPriceGreaterThanStartingPrice() throws Exception {
		
		InputStream istream = this.getClass().getResourceAsStream("/program/p_exp03_01.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		
		// Create a mock of the load's forecast service.
		final String NAME_PRODUCER = "PRODUCER";
		final String NAME_CONSUMER = "CONSUMER";
		LoadForecastService mockForecast = mock(LoadForecastService.class);
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(NAME_CONSUMER), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 250f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq("ieee13"),eq(NAME_PRODUCER), isA(Date.class), isA(Date.class)))
				.thenReturn(new Float[] {300f, 250f, 200f, 0f});
		InjectionUtils.setServicesModule(new ForecastMockModule(mockForecast));
		
		initPlatform(0);
		
		AgentTracker producerTracker = mock(AgentTracker.class);
		AgentTracker consumerTracker = mock(AgentTracker.class);
		aspem01.addBrokerAgent(
				NAME_PRODUCER,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_1_producer.xml")),
				producerTracker);
		aspem01.addBrokerAgent(
				NAME_CONSUMER,
				new StreamSource(this.getClass().getResourceAsStream("/samples/user-prefs/exp03_1_consumer.xml")),
				consumerTracker);
		

		// Run the simulation.
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, STORE_NAME, asdrProgram);
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:00:00Z").getTime()));
		platform.informSimulationFinished(ID_SIMULATION);
		
		verify(producerTracker).distributeEvent(new float[] {1.0f});
		verify(consumerTracker).distributeEvent(new float[] {1.0f});
		verify(producerTracker).abortAuctionReceived();
		verify(consumerTracker).abortAuctionReceived();
		
		return;
	}
	
	
	/**
	 * 
	 * 
	 * Injection module for getting a Configuration file loaded from the
	 * file "config-00.properties".
	 * 
	 *
	 */
	public static class ConfigModuleWithStartPrice extends AbstractModule {

		@Override
		protected void configure() {
			return;
		}
		
		@Provides @Singleton
		Configuration configurationLoader() {
			ConfigurationLoader loader = new ConfigurationLoader(
					this.getClass().getResource("/samples/properties/config-00.properties").getFile());
			
			Configuration config = loader.getConfiguration();
			config.set(Configuration.PROP_AUCTION_USE_START_PRICE, "true");
			
			return config;
		}
	}
	
	
	/**
	 * 
	 * Mock injection module in order to simulate the behaviour of the forecast service.
	 */
	class ForecastMockModule extends AbstractModule {

		private final LoadForecastService forecastService;


		/**
		 * Constructor.
		 */
		public ForecastMockModule(LoadForecastService forecastService) {
			this.forecastService = forecastService;
			return;
		}


		/**
		 * Binding configuration.
		 */
		@Override
		protected void configure() {
			bind(LoadForecastService.class).toInstance(forecastService);
			return;
		}
	}	
}
