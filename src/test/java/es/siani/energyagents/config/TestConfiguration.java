package es.siani.energyagents.config;

import org.junit.Test;
import junit.framework.TestCase;

public class TestConfiguration extends TestCase {
	
	public static final String DB_DRIVER = "org.postgresql.Driver";
	public static final String DB_URL = "jdbc:postgresql://localhost/agencyservices";
	public static final String DB_USERNAME = "agencyservices";
	public static final String DB_PASSWORD = "agencyservices";
	
	
	/**
	 * 
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}
	
	
	/**
	 * 
	 */
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * Test loading a default configuration file.
	 */
	@Test
	public void testGetConfigurationDefaultPath() {
		
		ConfigurationLoader.setDefaultConfigurationFile(
				this.getClass().getResource("/samples/properties/config-00.properties").getPath());
		ConfigurationLoader configLoader = new ConfigurationLoader();
		Configuration config = configLoader.getConfiguration();
		
		assertEquals(DB_DRIVER, config.get(Configuration.PROP_DB_DRIVER));
		assertEquals(DB_URL, config.get(Configuration.PROP_DB_URL));
		assertEquals(DB_USERNAME, config.get(Configuration.PROP_DB_USERNAME));
		assertEquals(DB_PASSWORD, config.get(Configuration.PROP_DB_PASSWORD));		
		
		return;
	}
	
	
	/**
	 * Test loading a specific configuration file.
	 */
	@Test
	public void testGetConfigurationSpecificPath() {
		
		ConfigurationLoader configLoader = new ConfigurationLoader(
				this.getClass().getResource("/samples/properties/config-00.properties").getPath());
		Configuration config = configLoader.getConfiguration();
		
		assertEquals(DB_DRIVER, config.get(Configuration.PROP_DB_DRIVER));
		assertEquals(DB_URL, config.get(Configuration.PROP_DB_URL));
		assertEquals(DB_USERNAME, config.get(Configuration.PROP_DB_USERNAME));
		assertEquals(DB_PASSWORD, config.get(Configuration.PROP_DB_PASSWORD));
		
		return;
	}
	
	
	/**
	 * Test loading the internal configuration file.
	 */
	@Test 
	public void testGetConfigurationInternalPath() {
		
		ConfigurationLoader configLoader = new ConfigurationLoader();
		Configuration config = configLoader.getConfiguration();
		
		assertEquals(DB_DRIVER, config.get(Configuration.PROP_DB_DRIVER));
		assertEquals(DB_URL, config.get(Configuration.PROP_DB_URL));
		assertEquals(DB_USERNAME, config.get(Configuration.PROP_DB_USERNAME));
		assertEquals(DB_PASSWORD, config.get(Configuration.PROP_DB_PASSWORD));
		
		return;
	}
}
