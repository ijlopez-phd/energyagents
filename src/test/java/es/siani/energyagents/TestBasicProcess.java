package es.siani.energyagents;

import java.util.Date;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

import com.google.inject.Injector;

import es.siani.energyagents.agent.AgentTracker;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.injection.XmppMockModule;
import es.siani.energyagents.model.PauseResult;
import es.siani.energyagents.utils.PauseScheduler;
import es.siani.simpledr.AsdrException;
import es.siani.simpledr.AsdrUtils;
import es.siani.simpledr.model.AsdrProgram;
import junit.framework.TestCase;
import static org.mockito.Mockito.*;

public class TestBasicProcess extends TestCase {
	
	// A mock of the XmppModule is used in this TestCase (a real XMPP server is not needed).
	static {
		InjectionUtils.setXmppModule(new XmppMockModule());
	}	
	
	private Injector injector = InjectionUtils.injector();
	
	
	/**
	 * Initialize resources.
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		return;
	}
	
	
	/**
	 * Close resources.
	 */
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		return;
	}
	
	
	/**
	 * Test the basic Aspem process of the DistributeActionEvent.
	 */
	@Test
	public void testBasicProcess() throws EnergyAgentsException, AsdrException, ParseException {
		
		AgentsPlatform platform = injector.getInstance(AgentsPlatform.class);
		
		AgentTracker gridopTracker = mock(AgentTracker.class);
		GridopContainer gridopContainer = platform.createGridopContainer(gridopTracker);
		
		AgentTracker aspemTrackerA = mock(AgentTracker.class);
		AspemContainer aspemContainerA = platform.createAspemContainer("AspemA", aspemTrackerA);
		AgentTracker brokerTrackerA1 = mock(AgentTracker.class);
		aspemContainerA.addBrokerAgent("brokerA1", brokerTrackerA1);
		AgentTracker brokerTrackerA2 = mock(AgentTracker.class);
		aspemContainerA.addBrokerAgent("brokerA2", brokerTrackerA2);
		AgentTracker brokerTrackerA3 = mock(AgentTracker.class);
		aspemContainerA.addBrokerAgent("brokerA3", brokerTrackerA3);
		
		AgentTracker aspemTrackerB = mock(AgentTracker.class);
		AspemContainer aspemContainerB = platform.createAspemContainer("AspemB", aspemTrackerB);
		AgentTracker brokerTrackerB1 = mock(AgentTracker.class);
		aspemContainerB.addBrokerAgent("brokerB1", brokerTrackerB1);
		AgentTracker brokerTrackerB2 = mock(AgentTracker.class);
		aspemContainerB.addBrokerAgent("brokerB2", brokerTrackerB2);
		AgentTracker brokerTrackerB3 = mock(AgentTracker.class);
		aspemContainerB.addBrokerAgent("brokerB3", brokerTrackerB3);		
		
		InputStream istream = this.getClass().getResourceAsStream("/program/p20a_simple_02.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		
		final int ID_SIM = 1;
		platform.informSimulationStarted(ID_SIM, "testSimulation", "", asdrProgram);
		verify(gridopTracker).simulationStarted();
		
		long modelTimeMillis = df.parse("2013-01-01T00:00:00Z").getTime();
		while (modelTimeMillis != PauseScheduler.NO_PAUSE) {
			PauseResult result = platform.informSimulationPaused(ID_SIM, new Date(), new Date(modelTimeMillis)); 
			modelTimeMillis = result.getNextPause();
		}
		verify(aspemTrackerA, times(1)).distributeEventReceived();
		verify(brokerTrackerA1, times(1)).distributeEventReceived();
		verify(brokerTrackerA2, times(1)).distributeEventReceived();
		verify(brokerTrackerA3, times(1)).distributeEventReceived();
		verify(aspemTrackerB, times(1)).distributeEventReceived();
		verify(brokerTrackerB1, times(1)).distributeEventReceived();
		verify(brokerTrackerB2, times(1)).distributeEventReceived();
		verify(brokerTrackerB3, times(1)).distributeEventReceived();		

		
		platform.informSimulationFinished(ID_SIM);
		verify(gridopTracker).simulationFinished();
		
		gridopContainer.kill();		
		
		return;
	}	
}
