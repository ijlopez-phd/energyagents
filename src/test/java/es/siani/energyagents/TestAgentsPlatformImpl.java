package es.siani.energyagents;

import org.junit.Test;

import jade.core.AID;
import jade.wrapper.gateway.JadeGateway;

import com.google.inject.Injector;

import junit.framework.TestCase;
import es.siani.energyagents.agent.ProxyAgent;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.injection.XmppMockModule;

public class TestAgentsPlatformImpl extends TestCase {
	
	// A mock of the XmppModule is used in this TestCase (a real XMPP server is not needed).
	static {
		InjectionUtils.setXmppModule(new XmppMockModule());
	}	
	
	private Injector injector = InjectionUtils.injector();
	private AgentsPlatform platform;
	
	
	/**
	 * Initialize resources.
	 */
	@Override
	public void setUp() throws Exception {
		super.setUp();
		
		this.platform = injector.getInstance(AgentsPlatform.class);
		return;
	}
	
	
	/**
	 * Close resources.
	 */
	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * Test the creation and finalization of the Gridop container.
	 */
	@Test
	public void testCreateGridopContainer() {
		GridopContainer container = this.platform.createGridopContainer(null);
		assertNotNull(container);
		
		container.kill();
		assertTrue(container.isKilled());
		
		return;
	}
	
	
	/**
	 * Test the creation and finalization of the Aspem container.
	 */
	@Test
	public void testCreateAspemContainer() {
		GridopContainer gridopContainer = this.platform.createGridopContainer(null);
		
		final String ASPEM_NAME = "aspem01";
		AspemContainer aspemContainer = this.platform.createAspemContainer(ASPEM_NAME, null);
		assertNotNull(aspemContainer);
		assertEquals(ASPEM_NAME, aspemContainer.getName());
		
		aspemContainer.kill();
		assertTrue(aspemContainer.isKilled());
		
		gridopContainer.kill();
		
		return;
	}
	
	
	/**
	 * Test the search of the Gridop Agent through the yellow-pages service.
	 */
	@Test
	public void testGetGridopAgentAID() throws Exception {
		GridopContainer container = this.platform.createGridopContainer(null);
		
		ProxyAgent.Command command = new ProxyAgent.Command(
				ProxyAgent.Command.CMD_SEARCH_GRIDOP_AGENT);
		JadeGateway.execute(command);
		
		assertEquals(1, command.getOutput().length);
		if (!(command.getOutput()[0] instanceof AID)) {
			fail("The argument is not an instance of AID.");
		}
		
		
		AID gridopAID = (AID)command.getOutput()[0];
		assertEquals(container.gridopAgent.getName(), gridopAID.getName());
		
		container.kill();
		return;
	}
	
	
	/**
	 * Test the search of the Broker agents associated to a specific container.
	 */
	@Test
	public void testGetBrokerAgentsAID() throws Exception {
		
		final String ASPEM_A_NAME = "AspemA";
		final String ASPEM_B_NAME = "AspemB";
		
		GridopContainer gridopContainer = this.platform.createGridopContainer(null);
		AspemContainer aspemContainerA = this.platform.createAspemContainer(ASPEM_A_NAME, null);
		AspemContainer aspemContainerB = this.platform.createAspemContainer(ASPEM_B_NAME, null);
		
		aspemContainerA.addBrokerAgent("brokerA1", null);
		aspemContainerA.addBrokerAgent("brokerA2", null);
		aspemContainerA.addBrokerAgent("brokerA3", null);
		
		aspemContainerB.addBrokerAgent("brokerB1", null);
		
		ProxyAgent.Command command = new ProxyAgent.Command(
				ProxyAgent.Command.CMD_SEARCH_BROKER_AGENTS,
				new Object[] {ASPEM_A_NAME});
		JadeGateway.execute(command);
		
		assertEquals(3, command.getOutput().length);
		if (!(command.getOutput() instanceof AID[])) {
			fail("The argument is not an instance of AID[].");
		}
		
		AID brokersAIDs[] = (AID[])command.getOutput();
		String strBrokersAIDs =
				"brokerA1" + "brokerA2" + "brokerA3";
		for (AID brokerAID : brokersAIDs) {
			assertTrue(strBrokersAIDs.contains(brokerAID.getLocalName()));
		}
		
		command = new ProxyAgent.Command(
				ProxyAgent.Command.CMD_SEARCH_BROKER_AGENTS,
				new Object[] {ASPEM_B_NAME});
		JadeGateway.execute(command);
		
		assertEquals(1, command.getOutput().length);
		if (!(command.getOutput() instanceof AID[])) {
			fail("The argument is not an instance of AID[].");
		}
		
		brokersAIDs = (AID[])command.getOutput();
		assertEquals("brokerB1", brokersAIDs[0].getLocalName());
		
		aspemContainerA.kill();
		aspemContainerB.kill();
		gridopContainer.kill();
		
		return;
	}
	
	
	/**
	 * Test the search of Aspem agents through the yellow-pages service.
	 */
	@Test
	public void testGetAspemAgentsAID() throws Exception {
		
		final String ASPEM_NAME_01 = "aspem01";
		final String ASPEM_NAME_02 = "aspem02";
		
		GridopContainer gridopContainer = this.platform.createGridopContainer(null);
		
		AspemContainer aspem01Container = this.platform.createAspemContainer(ASPEM_NAME_01, null); 
		AspemContainer aspem02Container = this.platform.createAspemContainer(ASPEM_NAME_02, null);
		
		ProxyAgent.Command command = new ProxyAgent.Command(
				ProxyAgent.Command.CMD_SEARCH_ASPEM_AGENTS);
		JadeGateway.execute(command);
		
		assertEquals(2, command.getOutput().length);
		if (!(command.getOutput() instanceof AID[])) {
			fail("The argument is not an instance of AID[].");
		}
		AID[] aids = (AID[])command.getOutput();
		
		// Join the name of both Aspem agents. In this way it can be found out whether
		// the names of the Aspem agents are contained in the array of AIDs.
		String joinedNames = 
				aspem01Container.getAspemAgentController().getName() +
				aspem02Container.getAspemAgentController().getName();
		
		assertTrue(joinedNames.indexOf(aids[0].getName()) > -1);
		assertTrue(joinedNames.indexOf(aids[1].getName()) > -1);
		
		
		aspem01Container.kill();
		aspem02Container.kill();
		gridopContainer.kill();
		
		return;
	}
	
	
	/**
	 * Test the DF.
	 */
	@Test
	public void testDirectoryFacilitator() throws Exception {
		
		final String ASPEM_NAME = "aspem";
		final int N_BROKERS = 200;
		
		// Check storing more than 100 brokers (this is the limit defined by the DF instantiated by default).
		this.platform.createGridopContainer(null);
		AspemContainer aspemContainer = this.platform.createAspemContainer(ASPEM_NAME, null);
		for (int n = 0; n < N_BROKERS; n++) {
			aspemContainer.addBrokerAgent("broker_" + n, null);
		}
		
		ProxyAgent.Command command = new ProxyAgent.Command(
				ProxyAgent.Command.CMD_SEARCH_BROKER_AGENTS, new Object[] {ASPEM_NAME});
		JadeGateway.execute(command);
		
		assertEquals(N_BROKERS, command.getOutput().length);
		if (!(command.getOutput() instanceof AID[])) {
			fail("The argument is not an instance of AID[].");
		}
		
		return;
	}
	
}
