package es.siani.energyagents;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.transform.stream.StreamSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.inject.AbstractModule;
import com.google.inject.Injector;

import es.siani.energyagents.agent.AgentTracker;
import es.siani.energyagents.injection.BehavioursExp01Module;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.injection.PlatformExp01Module;
import es.siani.energyagents.injection.ServicesModule;
import es.siani.energyagents.injection.XmppMockModule;
import es.siani.energyagents.services.LoadForecastService;
import es.siani.energyagents.utils.CronScheduleStore;
import es.siani.simpledr.AsdrUtils;
import es.siani.simpledr.model.AsdrProgram;

@RunWith(JUnit4.class)
public class TestExp01 {
	
	private static final int ID_SIMULATION = 1;
	private static final String SCENARIO_CODE = "ieee13";
	private static final String STORE_NAME = "ieee13";
	private static final String ASPEM_01_NAME = "aspem01";
	private static final String ASPEM_02_NAME = "aspem02";
	private static final String BROKER_01A = "broker01a";
	private static final String BROKER_01B = "broker01b";
	private static final String BROKER_02A = "broker02a";
	private static final String BROKER_02B = "broker02b";
	
	private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	private final String userPrefsTemplate = 
			"<userPrefs><levelsSchedule name='%s'><![CDATA[0 0 1 1 * %d;]]></levelsSchedule><priority>%d</priority></userPrefs>";
	
	private AgentsPlatform platform;
	private GridopContainer gridopContainer;
	private AspemContainer aspemContainer01;
	private AspemContainer aspemContainer02;
	private AgentTracker gridopTracker;
	private AgentTracker aspemTracker01;
	private AgentTracker aspemTracker02;
	private AgentTracker brokerTracker01a;
	private AgentTracker brokerTracker01b;
	private AgentTracker brokerTracker02a;
	private AgentTracker brokerTracker02b;
	
	// A mock of the XmppModule is used in this TestCase (a real XMPP server is not needed).
	static {
		InjectionUtils.setXmppModule(new XmppMockModule());
		InjectionUtils.setBehavioursModule(new BehavioursExp01Module());
		InjectionUtils.setPlatformModule(new PlatformExp01Module());
	}
	
	private Injector injector = InjectionUtils.injector();	
	
	
	/**
	 * Initialize resources for each test.
	 */
	@Before
	public void setUp() throws Exception {
		this.platform = injector.getInstance(AgentsPlatform.class);
		
		this.gridopTracker = mock(AgentTracker.class);
		this.gridopContainer = platform.createGridopContainer(gridopTracker);
		
		this.aspemTracker01 = mock(AgentTracker.class);
		this.aspemContainer01 = platform.createAspemContainer(ASPEM_01_NAME, this.aspemTracker01);
		
		this.aspemTracker02 = mock(AgentTracker.class);
		this.aspemContainer02 = platform.createAspemContainer(ASPEM_02_NAME, this.aspemTracker02);
		
		this.brokerTracker01a = mock(AgentTracker.class);
		this.brokerTracker01b = mock(AgentTracker.class);
		this.brokerTracker02a = mock(AgentTracker.class);
		this.brokerTracker02b = mock(AgentTracker.class);
		
		return;
	}
	
	
	/**
	 * Close resources of each test.
	 */
	@After
	public void tearDown() {
		
		if (!this.aspemContainer01.isKilled()) {
			this.aspemContainer01.kill();
		}
		
		if (!this.aspemContainer02.isKilled()) {
			this.aspemContainer02.kill();
		}
		
		if (!this.gridopContainer.isKilled()) {
			this.gridopContainer.kill();
		}
		
		// Restore the DataModule of the injection utils.
		InjectionUtils.setServicesModule(new ServicesModule());
		
		CronScheduleStore csh = InjectionUtils.injector().getInstance(CronScheduleStore.class);
		csh.clearValues();
		
		return;
	}
	
	
	/**
	 * Test starting and finishing a simulation.
	 */
	@Test
	public void testStartingFinishing() throws Exception {
		
		InputStream istream = this.getClass().getResourceAsStream("/program/p20b_level.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, "", asdrProgram);
		platform.informSimulationFinished(ID_SIMULATION);
		
		verify(gridopTracker, times(1)).simulationStarted();
		verify(gridopTracker, times(1)).simulationFinished();
		
		return;
	}
	
	
	/**
	 * Test behaviours when a Level signal is sent.
	 */
	@Test
	public void testLevelSignal() throws Exception {
		
		// Init the brokers.
		this.aspemContainer01.addBrokerAgent(BROKER_01A, null, brokerTracker01a);
		this.aspemContainer01.addBrokerAgent(BROKER_01B, null, brokerTracker01b);
		this.aspemContainer02.addBrokerAgent(BROKER_02A, null, brokerTracker02a);
		this.aspemContainer02.addBrokerAgent(BROKER_02B, null, brokerTracker02b);
		
		
		// Load the program.
		InputStream istream = this.getClass().getResourceAsStream("/program/p20b_level.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		
		// Run the simulation.
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, STORE_NAME, asdrProgram);
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:00:00Z").getTime()));
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:30:00Z").getTime()));		
		platform.informSimulationFinished(ID_SIMULATION);
		
		verify(gridopTracker, times(1)).simulationStarted();
		verify(gridopTracker, times(2)).simulationPaused();
		verify(aspemTracker01, times(2)).distributeEventReceived();
		verify(aspemTracker02, times(2)).distributeEventReceived();
		verify(brokerTracker01a, times(2)).distributeEventReceived();
		verify(brokerTracker01b, times(2)).distributeEventReceived();
		verify(brokerTracker02a, times(2)).distributeEventReceived();		
		verify(brokerTracker02b, times(2)).distributeEventReceived();		
		verify(gridopTracker, times(1)).simulationFinished();
		
		return;
	}
	
	
	/**
	 * Test when a signal with delta value equal to zero is sent.
	 */
	@Test
	public void testDeltaZero() throws Exception {
		// Create a mock of the load's forecast service.
		LoadForecastService mockForecast = mock(LoadForecastService.class);
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_01A), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_01B), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_02A), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_02B), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 100f, 0f});		
		InjectionUtils.setServicesModule(new ForecastMockModule(mockForecast));
		
		// Init the brokers.
		try {
			StreamSource userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch01", 0, 1).getBytes("UTF-8")));
			this.aspemContainer01.addBrokerAgent(BROKER_01A, userPrefsSource, brokerTracker01a);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch01", 0, 0).getBytes("UTF-8")));
			this.aspemContainer01.addBrokerAgent(BROKER_01B, userPrefsSource, brokerTracker01b);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch01", 0, 1).getBytes("UTF-8")));
			this.aspemContainer02.addBrokerAgent(BROKER_02A, userPrefsSource, brokerTracker02a);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch01", 0, 0).getBytes("UTF-8")));
			this.aspemContainer02.addBrokerAgent(BROKER_02B, userPrefsSource, brokerTracker02b);
		} catch (Exception ex) {
			throw new IllegalStateException();
		}		
		
		// Get the AsdrProgram.
		InputStream istream = this.getClass().getResourceAsStream("/program/p20b_delta_zero.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		
		// Run the simulation.
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, STORE_NAME, asdrProgram);
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:00:00Z").getTime()));
		platform.informSimulationFinished(ID_SIMULATION);
		
		// Check the result of the simulation.
		verify(brokerTracker01a, times(0)).distributeEventReceived();
		verify(brokerTracker01b, times(0)).distributeEventReceived();
		verify(brokerTracker02a, times(0)).distributeEventReceived();
		verify(brokerTracker02b, times(0)).distributeEventReceived();
		
		return;		
	}
	
	
	/**
	 * Test behaviours when a Delta signal is sent.
	 * In this example there is not easy nor hard loads.
	 */
	@Test
	public void testDeltaSignalNormal() throws Exception {
		
		// Create a mock of the load's forecast service.
		LoadForecastService mockForecast = mock(LoadForecastService.class);
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_01A), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_01B), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_02A), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 100f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_02B), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 100f, 0f});		
		InjectionUtils.setServicesModule(new ForecastMockModule(mockForecast));		
		
		// Init the brokers.
		try {
			StreamSource userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch01", 0, 1).getBytes("UTF-8")));
			this.aspemContainer01.addBrokerAgent(BROKER_01A, userPrefsSource, brokerTracker01a);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch01", 0, 0).getBytes("UTF-8")));
			this.aspemContainer01.addBrokerAgent(BROKER_01B, userPrefsSource, brokerTracker01b);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch01", 0, 1).getBytes("UTF-8")));
			this.aspemContainer02.addBrokerAgent(BROKER_02A, userPrefsSource, brokerTracker02a);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch01", 0, 0).getBytes("UTF-8")));
			this.aspemContainer02.addBrokerAgent(BROKER_02B, userPrefsSource, brokerTracker02b);
		} catch (Exception ex) {
			throw new IllegalStateException();
		}		
		
		// Get the AsdrProgram.
		InputStream istream = this.getClass().getResourceAsStream("/program/p20b_delta_ex1.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		
		// Run the simulation.
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, STORE_NAME, asdrProgram);
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:00:00Z").getTime()));
		platform.informSimulationFinished(ID_SIMULATION);
		
		// Check the result of the simulation.
		verify(brokerTracker01a, times(1)).distributeEventReceived();
		verify(brokerTracker01b, times(1)).distributeEventReceived();
		verify(brokerTracker02a, times(1)).distributeEventReceived();
		verify(brokerTracker02b, times(1)).distributeEventReceived();
		
		verify(brokerTracker01a).distributeEvent(new float[] {1.0f, 0.0f});
		verify(brokerTracker01b).distributeEvent(new float[] {1.0f, 2.0f});
		verify(brokerTracker02a).distributeEvent(new float[] {1.0f, 0.0f});
		verify(brokerTracker02b).distributeEvent(new float[] {1.0f, 2.0f});
		
		return;
	}
	
	
	/**
	 * Test behaviours when a Delta signal is sent.
	 * In this example some clients define hard loads that should not be used when possible.
	 */
	@Test
	public void testDeltaSignalHard() throws Exception {
		
		// Create a mock of the load's forecast service.
		LoadForecastService mockForecast = mock(LoadForecastService.class);
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_01A), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 150f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_01B), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 150f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_02A), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 150f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_02B), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 150f, 0f});		
		InjectionUtils.setServicesModule(new ForecastMockModule(mockForecast));		
		
		// Init the brokers.
		try {
			StreamSource userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch01", 0, 0).getBytes("UTF-8")));
			this.aspemContainer01.addBrokerAgent(BROKER_01A, userPrefsSource, brokerTracker01a);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch02", 201, 1).getBytes("UTF-8")));
			this.aspemContainer01.addBrokerAgent(BROKER_01B, userPrefsSource, brokerTracker01b);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch03", 0, 0).getBytes("UTF-8")));
			this.aspemContainer02.addBrokerAgent(BROKER_02A, userPrefsSource, brokerTracker02a);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch04", 201, 1).getBytes("UTF-8")));
			this.aspemContainer02.addBrokerAgent(BROKER_02B, userPrefsSource, brokerTracker02b);
		} catch (Exception ex) {
			throw new IllegalStateException();
		}		
		
		// Get the AsdrProgram.
		InputStream istream = this.getClass().getResourceAsStream("/program/p20b_delta_ex1.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		
		// Run the simulation.
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, STORE_NAME, asdrProgram);
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:00:00Z").getTime()));
		platform.informSimulationFinished(ID_SIMULATION);
		
		// Check the result of the simulation.
		verify(brokerTracker01a, times(1)).distributeEventReceived();
		verify(brokerTracker01b, never()).distributeEventReceived();
		verify(brokerTracker02a, times(1)).distributeEventReceived();
		verify(brokerTracker02b, never()).distributeEventReceived();
		
		verify(brokerTracker01a).distributeEvent(new float[] {2.0f, 2.0f});
		verify(brokerTracker02a).distributeEvent(new float[] {2.0f, 2.0f});
		
		return;		
	}
	
	
	/**
	 * Test behaviours when a Delta signal is sent.
	 * In this example all the demand can be meet through the easy loads..
	 */
	@Test
	public void testDeltaSignalEasy() throws Exception {
		
		// Create a mock of the load's forecast service.
		LoadForecastService mockForecast = mock(LoadForecastService.class);
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_01A), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 150f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_01B), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 150f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_02A), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 150f, 0f});
		when(mockForecast.getLoadForAllLevels(eq(STORE_NAME), eq(BROKER_02B), isA(Date.class), isA(Date.class)))
			.thenReturn(new Float[] {300f, 250f, 150f, 0f});		
		InjectionUtils.setServicesModule(new ForecastMockModule(mockForecast));		
		
		// Init the brokers.
		try {
			StreamSource userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch01", 102, 0).getBytes("UTF-8")));
			this.aspemContainer01.addBrokerAgent(BROKER_01A, userPrefsSource, brokerTracker01a);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch02", 0, 0).getBytes("UTF-8")));
			this.aspemContainer01.addBrokerAgent(BROKER_01B, userPrefsSource, brokerTracker01b);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch03", 102, 0).getBytes("UTF-8")));
			this.aspemContainer02.addBrokerAgent(BROKER_02A, userPrefsSource, brokerTracker02a);
			
			userPrefsSource = new StreamSource(
					new ByteArrayInputStream(String.format(userPrefsTemplate, "sch04", 0, 0).getBytes("UTF-8")));
			this.aspemContainer02.addBrokerAgent(BROKER_02B, userPrefsSource, brokerTracker02b);
		} catch (Exception ex) {
			throw new IllegalStateException();
		}		
		
		// Get the AsdrProgram.
		InputStream istream = this.getClass().getResourceAsStream("/program/p20b_delta_ex1.xml");
		AsdrProgram asdrProgram = AsdrUtils.createProgram(istream);
		
		
		// Run the simulation.
		platform.informSimulationStarted(ID_SIMULATION, SCENARIO_CODE, STORE_NAME, asdrProgram);
		platform.informSimulationPaused(
				ID_SIMULATION,
				new Date(),
				new Date(df.parse("2013-01-01T00:00:00Z").getTime()));
		platform.informSimulationFinished(ID_SIMULATION);
		
		// Check the result of the simulation.
		verify(brokerTracker01a, times(1)).distributeEventReceived();
		verify(brokerTracker01b, never()).distributeEventReceived();
		verify(brokerTracker02a, times(1)).distributeEventReceived();
		verify(brokerTracker02b, never()).distributeEventReceived();
		
		verify(brokerTracker01a).distributeEvent(new float[] {2.0f, 2.0f});
		verify(brokerTracker02a).distributeEvent(new float[] {2.0f, 2.0f});
		
		return;
	}	
	
	
	/**
	 * Mock injection module in order to simulate the behaviour of
	 * the forecast service.
	 *
	 */
	class ForecastMockModule extends AbstractModule {
		
		private final LoadForecastService forecastService;
		
		
		/**
		 * Constructor.
		 */
		public ForecastMockModule(LoadForecastService forecastService) {
			this.forecastService = forecastService;
			return;
		}

		
		/**
		 * Binding configuration.
		 */
		@Override
		protected void configure() {
			bind(LoadForecastService.class).toInstance(forecastService);
			return;
		}
	}
}
