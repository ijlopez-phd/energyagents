package es.siani.energyagents.xmpp;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

import es.siani.energyagents.EnergyAgentsException;
import es.siani.energyagents.injection.annotations.XmppServerPort;
import es.siani.energyagents.injection.annotations.XmppServerUrl;

public class SimpleXmppImpl implements SimpleXmpp, PacketListener {
	
	Connection conn = null;
	private final ConnectionConfiguration xmppConfig;
	private final XmppEntitiesGenerator entitiesGenerator;
	private final String serverUrl;
	private String nodeName;
	private boolean msgSent;
	
//	static {
//		ProviderManager provider = ProviderManager.getInstance();
//		provider.addIQProvider(
//				"oadrDistributeEvent",
//				"http://openadr.org/oadr-2.0a/2012/07",
//				new IqDistributeProvider());
//	}
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public SimpleXmppImpl(
			@XmppServerUrl String serverUrl, @XmppServerPort int serverPort,
			XmppEntitiesGenerator entitiesGenerator) {
		
		this.xmppConfig = new ConnectionConfiguration(serverUrl, serverPort);
		this.entitiesGenerator = entitiesGenerator;
		this.serverUrl = serverUrl;
		return;
	}
	

	/**
	 * Connect to the XMPP server.
	 * If the account does not exist, then a new one is created.
	 */	
	@Override
	public void connect(String nodeName) {
		
		if (this.conn != null) {
			throw new IllegalStateException("A connection is already being used.");
		}
		
		this.nodeName = nodeName;
		
		// Create the account if it doesn't exist.
		String accountName = this.entitiesGenerator.createBrokerAccountName(nodeName);
		String password = this.entitiesGenerator.createPassword(accountName);
		this.conn = new XMPPConnection(this.xmppConfig);
		try {
			this.conn.connect();			
			this.conn.getAccountManager().createAccount(accountName, password);
		} catch (XMPPException ex) {
			if (ex.getXMPPError().getCode() != 409) { // Conflict code error (the account already exists)
				getLogger().log(Level.SEVERE, ex.getMessage());
				throw new EnergyAgentsException(ex);
			}
		}
		
		// Log in to the XMPP server.
		try {
			this.conn.login(accountName, password, RESOURCE);
		} catch (XMPPException ex) {
			getLogger().log(Level.SEVERE, ex.getMessage());
			throw new EnergyAgentsException(ex);
		}
		
		// Add a listener that notifies about all the packages that have been sent.
		conn.addPacketSendingListener(this, new PacketFilter() {
			@Override
			public boolean accept(Packet packet) {
				return true;
			}
			
		});
		
		return;
	}
	

	/**
	 * Disconnect from the XMPP server and remove the account associated to the
	 * connection instance passed as argument.
	 */	
	@Override
	public void disconnect() {
		
		if (this.conn == null) {
			return;
		}
		
		this.conn.disconnect();
		this.conn = null;
		
//		try {
//			this.conn.getAccountManager().deleteAccount();
//		} catch (XMPPException ex) {
//			getLogger().log(
//					Level.SEVERE, "Failed to remove the account '" +
//							this.conn.getUser() + "' from the XMPP server.", ex);
//			throw new EnergyAgentsException(ex);
//		}

		return;
	}
	

	/**
	 * Send a message to the local agent.
	 */	
	@Override
	public synchronized void send(String content) {
		
		if (this.conn == null) {
			throw new IllegalStateException("The connection has not been established.");
		}
		
		Message msg = new Message(this.entitiesGenerator.createLocalAccountName(this.nodeName) + "@" + this.serverUrl);
		msg.setBody(content);
		this.msgSent = false;
		this.conn.sendPacket(msg);
		while (!this.msgSent) {
			try {
				this.wait();
			} catch (InterruptedException ex) {
				getLogger().log(Level.SEVERE, "Failed to suspend the thread that sends the XMPP message.");
				throw new IllegalStateException(ex);
			}
		}
		
//		Chat chat = conn.getChatManager().createChat(
//				this.entitiesGenerator.createLocalAccountName(this.nodeName) + "@" + this.serverUrl,
//				this);	
//		try {
//			chat.sendMessage(content);
//		} catch (XMPPException e) {
//			getLogger().log(Level.SEVERE, "No se pudo enviar el maldito mensaje.!!!");
//			e.printStackTrace();
//		}
		
		return;
	}
	
	

	/**
	 * This method is called whenever a packet is going to be sent through this connection.
	 */
	@Override
	public synchronized void processPacket(Packet packet) {
		this.msgSent = true;
		this.notifyAll();
		return;
	}	
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(SimpleXmppImpl.class.getName());
	}
	
	
	/**
	 * Inner Class.
	 * Build a 'set' IQ message from a DistributeEventAction.
	 */
	public static class IqDistributeEvent extends IQ {
		
		private String content;
		
		
		/**
		 * Constructor.
		 */
		public IqDistributeEvent() {
			this.setType(IQ.Type.SET);
			return;
		}
		
		public void setContent(String content) {
			this.content = content;
		}
		
		public String getContent() {
			return this.content;
		}
		
		/**
		 * XML to be sent.
		 */
		@Override
		public String getChildElementXML() {
			return this.content;
		}
	}
	
	public static class IqDistributeProvider implements IQProvider {
		
		/**
		 * Constructor.
		 */
		public IqDistributeProvider() {
			return;
		}
		
		/**
		 * Parse the IQ DistributeEvent message.
		 */
		@Override
		public IQ parseIQ(XmlPullParser parser) throws Exception {
			IqDistributeEvent answer = new IqDistributeEvent();
			
			boolean done = false;
			while (!done) {
				int eventType = parser.next();
				if (eventType == XmlPullParser.START_TAG) {
					// do nothing
				} else if (eventType == XmlPullParser.END_TAG) {
					if (parser.getName().equals("oadrDistributeEvent")) {
						done = true;
					}
				}
			}
			
			return answer;
		}
	}
}
