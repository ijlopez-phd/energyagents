package es.siani.energyagents.xmpp;

public interface SimpleXmpp {
	
	public static final String RESOURCE = "energyagents";
	public static final String EMPTY_MESSAGE = "no-message";
	
	
	/**
	 * Connect to the XMPP server.
	 * If the account does not exist, then a new one is created.
	 */
	public abstract void connect(String nodeName);
	
	
	/**
	 * Disconnect from the XMPP server and remove the account associated to the
	 * connection.
	 */
	public abstract void disconnect();
	
	
	/**
	 * Send a message to the local agent.
	 */
	public abstract void send(String content);
}
