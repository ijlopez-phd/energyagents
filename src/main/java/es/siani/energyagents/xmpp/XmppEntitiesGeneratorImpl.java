package es.siani.energyagents.xmpp;

public class XmppEntitiesGeneratorImpl implements XmppEntitiesGenerator {
	
	/**
	 * @see es.siani.energyagents.xmpp.XmppEntitiesGenerator#createBrokerAccountName(String)
	 */	
	@Override
	public String createBrokerAccountName(String nodeName) {
		return "ba-" + nodeName;
	}
	

	/**
	 * @see es.siani.energyagents.xmpp.XmppEntitiesGenerator#createLocalAccountName(String)
	 */	
	@Override
	public String createLocalAccountName(String nodeName) {
		return "la-" + nodeName;
	}	

	
	/**
	 * @see es.siani.energyagents.xmpp.XmppEntitiesGenerator#createPassword(String)
	 */
	@Override
	public String createPassword(String accountName) {
		return accountName;
	}
}
