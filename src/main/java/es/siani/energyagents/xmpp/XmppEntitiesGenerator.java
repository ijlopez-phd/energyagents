package es.siani.energyagents.xmpp;

public interface XmppEntitiesGenerator {
	
	/**
	 * Create the name of the broker agent's account.
	 */
	public abstract String createBrokerAccountName(String nodeName);
	
	
	/**
	 * Create the name of the local agent's account.
	 */
	public abstract String createLocalAccountName(String nodeName);
	
	
	/**
	 * Create a password for an account.
	 */
	public abstract String createPassword(String accountName);

}
