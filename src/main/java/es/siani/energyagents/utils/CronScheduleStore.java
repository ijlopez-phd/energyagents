package es.siani.energyagents.utils;

import java.util.Date;

public interface CronScheduleStore {
	
	public void saveSchedule(String name, String definition);
	
	public Double getValue(String name, Date moment);
	
	public CronScheduleItem[] getValues(String name, Date startTime, Date endTime);
	
	public int[] getValuesByBlock(String name, Date startTime, Date endTime);
	
	public void clearValues();
	
}
