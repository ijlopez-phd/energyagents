package es.siani.energyagents.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import es.siani.energyagents.config.Configuration;

public class CronLevelsScheduleStore implements CronScheduleStore {
	
	public final static int NO_VALUE = 0;
	public final static int EASY_OFFSET = 100;
	public final static int EASY_COMBINED_OFFSET = 110;
	public final static int EASY_BOTTOM_VALUE = 101;
	public final static int EASY_TOP_VALUE = 113;
	public final static int HARD_OFFSET = 200;
	public final static int HARD_COMBINED_OFFSET = 210;
	public final static int HARD_BOTTOM_VALUE = 201;
	public final static int HARD_TOP_VALUE = 213;
	public final static double NORMAL_VALUE = 0.0;
	public final static int EASY_COMBINED = 110;
	public final static int HARD_COMBINED = 210;
	
	private final static int LF_MINUTE = 0;
	private final static int LF_HOUR = 1;
	private final static int LF_DAY = 2;
	private final static int LF_MONTH = 3;
	private final static int LF_DAY_OF_WEEK = 4;
	public final static int WORKING_YEAR = Calendar.getInstance().get(Calendar.YEAR);
	private final static TimeZone UTC_TZ = TimeZone.getTimeZone("UTC");
	
	private final long BLOCK_MILLIS;
	
	
	/** Cache of all the parsed schedules. */
	HashMap<String, CronScheduleItem[]> schedules = new HashMap<String, CronScheduleItem[]>();
	
	/**
	 * Constructor.
	 */
	@Inject
	public CronLevelsScheduleStore(Configuration config) {
		BLOCK_MILLIS = Integer.parseInt(config.get(Configuration.PROP_AUCTION_BLOCK_SECONDS)) * 1000;		
		return;
	}
	
	
	/**
	 * Reset the cache of schedules.
	 */
	@Override
	public void clearValues() {
		schedules.clear();
		return;
	}	

	
	/**
	 * Save the definitiono of a schedule. The schedule is previously parsed in order to check that
	 * its syntax is valid.
	 */
	@Override
	public void  saveSchedule(String name, String definition) {
		
		if ((name == null) || (definition == null)) {
			getLogger().log(Level.SEVERE, "Storing schedule definition: null name or definition are not allowed.");
			throw new IllegalArgumentException();
		}
		
		if (schedules.get(name) == null) {
			schedules.put(name, parseSchedule(definition));
		}
		
		return;
	}

	
	/**
	 * Get the value of a schedule for a specific moment.
	 */
	@Override
	public Double getValue(String name, Date moment) {
		
		CronScheduleItem[] items = schedules.get(name);
		if (items == null) {
			getLogger().log(Level.SEVERE, "Getting schedule definition: the schedule name does not correspond to any stored definition.");
			throw new IllegalArgumentException();			
		}
		
		// First, the date to process is setted as it was at the current year.
		final long momentMillis;
		{
			Calendar calMoment = Calendar.getInstance(UTC_TZ);
			calMoment.setTime(moment);
			calMoment.set(Calendar.YEAR, WORKING_YEAR);
			momentMillis = calMoment.getTimeInMillis();
		}
		
		final int N_ITEMS = items.length;
		if (momentMillis < items[0].getTimeMillis()) {
			return null;
		} else if (N_ITEMS == 1) {
			return items[0].getValue();
		}
		
		for (int n = 0; n < items.length - 1; n++) {
			if ((momentMillis >= items[n].getTimeMillis()) && (momentMillis < items[n + 1].getTimeMillis())) {
				return items[n].getValue();
			}
		}
		
		return items[N_ITEMS - 1].getValue();
	}
	
	
	/**
	 * Get the load option fo each block of time of the specified parameter.
	 */
	@Override
	public int[] getValuesByBlock(String name, Date startTime, Date endTime) {

		// Get the startTime and the endTime in millis (they are setted to the working year).
		final long startMillis = calculateDateInMillis(startTime);
		final long endMillis = calculateDateInMillis(endTime);
		final int N_BLOCKS = (int)((endMillis - startMillis) / BLOCK_MILLIS);
		
		// Init the output values.
		int[] outValues = new int[N_BLOCKS];
		for (int n = 0; n < N_BLOCKS; n++) {
			outValues[n] = NO_VALUE;
		}
		
		CronScheduleItem[] scheduleItems = schedules.get(name);
		if (scheduleItems == null) {
			getLogger().log(Level.SEVERE, "Getting schedule definition: the schedule '" + name + "' does not exit.");
			throw new IllegalArgumentException("Getting schedule definition: the schedule '" + name + "' does not exit.");			
		} else if (scheduleItems.length == 0) {
			// There are not values.
			return outValues;
		}
		
		// Check if the period of time is within the range of values.
		final int nItems = scheduleItems.length;
		final long firstItemMillis = scheduleItems[0].getTimeMillis();
		if (endMillis < firstItemMillis) {
			// There are not schedule items for this period of time. Default values are returned.
			return outValues;
		} else if (startMillis > scheduleItems[nItems - 1].getTimeMillis()) {
			int value = (int)scheduleItems[nItems - 1].getValue();
			for (int n = 0; n < N_BLOCKS; n++) {
				outValues[n] = value;
			}
			return outValues;
		}
		
		 
		int idxBlock = 0;
		int idxItem = 0;
		long currentBlockMillis = startMillis;
		if (startMillis < firstItemMillis) {
			while (currentBlockMillis < firstItemMillis) {
				currentBlockMillis += BLOCK_MILLIS;
				idxBlock++;
			}
		} else {
			for (; idxItem < scheduleItems.length - 1; idxItem++) {
				if (scheduleItems[idxItem + 1].getTimeMillis() > startMillis) {
					break;
				}
			}
		}
		
		long nextItemStartMillis = (idxItem == (nItems - 1))? endWorkingYear() : scheduleItems[idxItem + 1].getTimeMillis();  
		double currentItemValue = scheduleItems[idxItem].getValue();
		for (; idxBlock < N_BLOCKS; idxBlock++, currentBlockMillis += BLOCK_MILLIS) {
			if (currentBlockMillis >= nextItemStartMillis) {
				CronScheduleItem nextItem = scheduleItems[++idxItem];
				currentItemValue = nextItem.getValue();
				nextItemStartMillis = 
						(idxItem == (nItems - 1))? endWorkingYear() : scheduleItems[idxItem + 1].getTimeMillis();
			}
			
			outValues[idxBlock] = (int)currentItemValue;
		}
		
		return outValues;
	}	
	

	/**
	 * Get the set of values for a period of time.
	 */
	@Override
	public CronScheduleItem[] getValues(String name, Date startTime, Date endTime) {
		
		CronScheduleItem[] scheduleItems = schedules.get(name);
		if (scheduleItems == null) {
			getLogger().log(Level.SEVERE, "Getting schedule definition: the schedule name does not correspond to any stored definition.");
			throw new IllegalArgumentException();			
		} else if (scheduleItems.length == 0) {
			// There are not values.
			return new CronScheduleItem[0];
		}
		
		final long startMillis = calculateDateInMillis(startTime);
		final long endMillis = calculateDateInMillis(endTime);
		
		// Check if the period of time is within the range of values.
		if (endMillis < scheduleItems[0].getTimeMillis()) {
			// There are not values for this period of time.
			return new CronScheduleItem[0];
		}		
	
		// Find the first block is wihtin range.
		final int nItems = scheduleItems.length;		
		int idxFirst = nItems -1;
		for (int n = 0; n < nItems; n++) {
			if (scheduleItems[n].getTimeMillis() >= startMillis) {
				idxFirst = n;
				break;
			}
		}

		
		// Find the last block is within range.
		int idxLast = nItems - 1;
		for (int n = idxFirst; n < nItems; n++) {
			if (scheduleItems[n].getTimeMillis() > endMillis) {
				idxLast = n - 1;
				break;
			}
		}
		
		
		ArrayList<CronScheduleItem> outItems = new ArrayList<CronScheduleItem>();
		for (int n = idxFirst; n <= idxLast; n++) {
			outItems.add(scheduleItems[n]);
		}
		
		if (outItems.get(0).getTimeMillis() > startMillis) {
			if (idxFirst == 0) {
				outItems.add(0, new CronScheduleItem(new Date(startMillis), NORMAL_VALUE));
			} else {
				outItems.add(0, new CronScheduleItem(new Date(startMillis), scheduleItems[idxFirst - 1].getValue()));
			}
		} else if (startMillis > outItems.get(0).getTimeMillis()) {
			outItems.set(0, new CronScheduleItem(new Date(startMillis), outItems.get(0).getValue()));
		}

		
		return outItems.toArray(new CronScheduleItem[0]);
	}
	
	
	/**
	 * Tell if a value corresponds to the consumer role.
	 */	
	public static boolean isConsumer(int value) {
		if ((value >= HARD_BOTTOM_VALUE) && (value <= HARD_TOP_VALUE)) {
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Tell if a value corresponds to the producer role.
	 */
	public static boolean isProducer(int value) {
		if ((value >= EASY_BOTTOM_VALUE) && (value <= EASY_TOP_VALUE)) {
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Get the value of the signal Level from a scheduler's value.
	 */
	public static int getLevel(int value) {
		if (isConsumer(value)) {
			return isCombined(value)? value - HARD_COMBINED_OFFSET : value - HARD_OFFSET;
		} else if (isProducer(value)) {
			return isCombined(value)? value - EASY_COMBINED_OFFSET : value - EASY_OFFSET;
		}
		
		return 0; // AND mask with the last two bits.
	}
	
	
	/**
	 * Tell if the Combined flag is enabled.
	 */
	public static boolean isCombined(int value) {
		
		if ((value >= HARD_BOTTOM_VALUE) && (value <= HARD_TOP_VALUE)) {
			if (value > HARD_COMBINED) {
				return true;
			}
		} else if ((value >= EASY_BOTTOM_VALUE) && (value <= EASY_TOP_VALUE)) {
			if (value > EASY_COMBINED) {
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * Get the end time (in millis) of the working year.
	 */
	long endWorkingYear() {
		Calendar cal = Calendar.getInstance(UTC_TZ);
		cal.set(WORKING_YEAR, 11, 31, 23, 59, 59);
		return cal.getTimeInMillis();
	}
	
	
	/**
	 * Transform a date in millis. Previously the date is setted to the working year.
	 */
	long calculateDateInMillis(Date date) {
		
		Calendar cal = Calendar.getInstance(UTC_TZ);
		cal.setTime(date);
		cal.set(Calendar.YEAR, WORKING_YEAR);
		
		return cal.getTimeInMillis();
	}
	
	
	/**
	 * Build an array with all the lines of a schedule converted to objects of the type CronScheduleItem.
	 */
	CronScheduleItem[] parseSchedule(String definition) {
		
		BufferedReader reader = new BufferedReader(new StringReader(definition));
		
		ArrayList<CronScheduleItem> items = new ArrayList<CronScheduleItem>();
		try {
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				String[] sublines = line.trim().split(";");
				for (int n = 0; n < sublines.length; n++) {
					String subline = sublines[n].trim();
					if (subline.length() > 0) {
						items.add(parseLine(subline));
					}
				}
			}
		} catch (IOException ex) {
			throw new IllegalArgumentException();
		}
		
		Collections.sort(items, new CronScheduleItemComparator());
		
		return items.toArray(new CronScheduleItem[0]);
	}
	
	
	/**
	 * Get the date represented by a line of an Schedule.
	 * All values are not supported. The fields 'minute', 'hour', 'day' and 'month' must be explicitely defined.
	 * The field 'day_of_week' is not considered.
	 */
	CronScheduleItem parseLine(String line) {
		
		String[] fields = line.trim().split("\\s+");
		if (fields.length != 6) {
			getLogger().log(Level.SEVERE, "Getting value from schedule's line: The number of items of the line is not valid.");			
			throw new IllegalArgumentException();
		}
		
		if (fields[LF_MINUTE].equals("*") 
				|| fields[LF_HOUR].equals("*") 
				|| fields[LF_DAY].equals("*")
				|| fields[LF_MONTH].equals("*")
				|| !fields[LF_DAY_OF_WEEK].equals("*")) {
			getLogger().log(Level.SEVERE, "Failed to parse the items of the schedule line.");			
			throw new IllegalArgumentException();
		}
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		try {
			cal.set(WORKING_YEAR, Integer.parseInt(fields[LF_MONTH]) - 1,
					Integer.parseInt(fields[LF_DAY]),
					Integer.parseInt(fields[LF_HOUR]),
					Integer.parseInt(fields[LF_MINUTE]), 0);
			cal.set(Calendar.MILLISECOND, 0);
		} catch (NumberFormatException nex) {
			getLogger().log(Level.SEVERE, "Failed to parse the items of the schedule line.", nex);
			throw new IllegalArgumentException();
		}

		
		return new CronScheduleItem(cal.getTime(), Double.valueOf(fields[5]));
	}
	
	
	/**
	 * Return the store.
	 * This method is provided for testing purposes.
	 */
	HashMap<String, CronScheduleItem[]> getStore() {
		return schedules;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(CronLevelsScheduleStore.class.getName());
	}


	/**
	 * Inner class.
	 * Compare two objects of the type CronScheduleItem.
	 *
	 */
	private class CronScheduleItemComparator implements Comparator<CronScheduleItem> {

		@Override
		public int compare(CronScheduleItem o1, CronScheduleItem o2) {
			final long o1Millis = o1.getTimeMillis();
			final long o2Millis = o2.getTimeMillis();
			
			if (o1Millis < o2Millis) {
				return -1;
			} else if (o1Millis > o2Millis) {
				return 1;
			}
			
			return 0;
		}
	}
}
