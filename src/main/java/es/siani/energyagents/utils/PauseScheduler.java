package es.siani.energyagents.utils;

import es.siani.energyagents.ontology.as00.ProgramConcept;
import es.siani.energyagents.ontology.as00.ScheduleConcept;

public interface PauseScheduler {
	
	public final int NO_PAUSE = -1;
	
	/**
	 * According to the demand-response program, this method:
	 *  - Calculates the next time the simulator must call the Gridop agent,
	 *  - and returns the events that must be fired. 
	 */
	public ScheduleConcept process(long currentTime);
	
	
	/**
	 * Initiates the scheduler with demand-response program.
	 * This method must be called before start using the getNextPause.
	 */
	public void init(ProgramConcept program);

}
