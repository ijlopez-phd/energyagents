package es.siani.energyagents.utils;

import java.util.Comparator;

import es.siani.energyagents.ontology.as00.EventConcept;
import static es.siani.energyagents.utils.NotificationUtils.*;

public class EventNotificationStartComparator implements Comparator<EventConcept> {
	
	/**
	 * Compare two EventConcept instances according to the start of the notification
	 * period of the events. 
	 */
	@Override
	public int compare(EventConcept o1, EventConcept o2) {
		
		final long timeO1 = getNotificationStart(o1);
		final long timeO2 = getNotificationStart(o2);
		
		if (timeO1 < timeO2) {
			return -1;
		} else if (timeO1 > timeO2) {
			return 1;
		}
		
		return 0;
	}

}
