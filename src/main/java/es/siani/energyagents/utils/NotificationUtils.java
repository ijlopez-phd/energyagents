package es.siani.energyagents.utils;

import java.util.ArrayList;
import java.util.List;

import es.siani.energyagents.ontology.as00.EventConcept;
import es.siani.energyagents.ontology.as00.IntervalConcept;
import es.siani.energyagents.ontology.as00.TargetsConcept;
import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;
import es.siani.simpledr.model.AsdrSignalTypeEnum;
import es.siani.simpledr.model.AsdrTargets;

public class NotificationUtils {
	
	/**
	 * Calculates when an event should be notified.
	 * @param event
	 * @return
	 */
	public static long getNotificationStart(EventConcept event) {
		return event.getStart().getTime() - (event.getNotificationDuration() * 1000);
	}
	
	
	/**
	 * Converts an EventConcept instance of the ontology into an instance of
	 * an AsdrEvent.
	 */
	public static AsdrEvent toAsdrEvent(EventConcept event) {
		
		List<AsdrInterval> asdrIntervals = new ArrayList<AsdrInterval>();
		
		for (IntervalConcept interval : event.getIntervals()) {
			asdrIntervals.add(new AsdrInterval(interval.getDuration(), interval.getPayload()));
		}
		
		return new AsdrEvent(
				AsdrSignalTypeEnum.valueOf(event.getSignalType().toUpperCase()),
				event.getStart(),
				event.getNotificationDuration(),
				event.getPriority(),
				asdrIntervals);
	}
	
	
	/**
	 * Converts an AsdrEvent into an EventConcept.
	 */
	public static EventConcept toEventConcept(AsdrEvent asdrEvent) {
		
		// Conversion of the intervals.
		List<IntervalConcept> intervals = new ArrayList<IntervalConcept>();
		for (AsdrInterval asdrInterval : asdrEvent.getIntervals()) {
			intervals.add(new IntervalConcept(asdrInterval.getDuration(), asdrInterval.getPayload()));
		}
		
		// Conversion of targets
		TargetsConcept targets = new TargetsConcept();
		AsdrTargets asdrTargets = asdrEvent.getTargets();
		if (asdrTargets.groups != null) {
			List<String> groups = targets.getGroups();
			for (String group : asdrTargets.groups) {
				groups.add(new String(group));
			}
		}
		if (asdrTargets.parties != null) {
			List<String> parties = targets.getParties();
			for (String party : asdrTargets.parties) {
				parties.add(new String(party));
			}
		}
		if (asdrTargets.resources != null) {
			List<String> resources = targets.getResources();
			for (String resource : asdrTargets.resources) {
				resources.add(new String(resource));
			}
		}
		if (asdrTargets.vendors != null) {
			List<String> vendors = targets.getVendors();
			for (String vendor : asdrTargets.vendors) {
				vendors.add(new String(vendor));
			}
		}
		
		
		return new EventConcept(
				asdrEvent.getSignalType().value(),
				asdrEvent.getStart(),
				asdrEvent.getDuration(),
				asdrEvent.getNotificationDuration(),
				asdrEvent.getPriority(),
				asdrEvent.isResponseRequired(),
				intervals,
				targets);
	}
	
	
	/**
	 * Build a list of IntervalConcept objects from a list of AsdrInterval objects.
	 */	
	public static List<IntervalConcept> toIntervalConcept(List<AsdrInterval> asdrIntervals) {
		List<IntervalConcept> outList = new ArrayList<IntervalConcept>();
		for (AsdrInterval asdrInterval : asdrIntervals) {
			outList.add(new IntervalConcept(asdrInterval.getDuration(), asdrInterval.getPayload()));
		}
		
		return outList;		
	}
	
	
//	/**
//	 * Sort a Map object on the values.
//	 */
//	public static <K, V extends Comparable<? super V>> Map<K, V> sortMapByValue(Map<K, V> map) {
//		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>( map.entrySet() );
//		Collections.sort( list, new Comparator<Map.Entry<K, V>>() {
//			public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 ) {
//				return (o1.getValue()).compareTo( o2.getValue() );
//			}
//		});
//
//		Map<K, V> result = new LinkedHashMap<K, V>();
//		for (Map.Entry<K, V> entry : list) {
//			result.put( entry.getKey(), entry.getValue() );
//		}
//		
//		return result;
//	}	
}
