package es.siani.energyagents.utils;

import java.util.Date;

public class CronScheduleItem {
	
	private final Date time;
	private final double value;
	
	/**
	 * Constructor.
	 */
	public CronScheduleItem(Date time, double value) {
		this.time = time;
		this.value = value;
		return;
	}
	
	
	/**
	 * Get the Time field.
	 */
	public Date getTime() {
		return time;
	}
	
	
	/**
	 * Get the Value field.
	 */
	public double getValue() {
		return value;
	}
	
	
	/**
	 * Get the Time in millis.
	 */
	public long getTimeMillis() {
		return time.getTime();
	}
}
