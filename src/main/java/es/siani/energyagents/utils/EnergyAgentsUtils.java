package es.siani.energyagents.utils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import jade.core.AID;
import es.siani.simpledr.model.AsdrInterval;

public class EnergyAgentsUtils {
	
	private static final String LOGGER_NAME = "es.siani.energyagents.debug";
	
	/**
	 * Log.
	 */
	public static void log(String message) {
		jade.util.Logger.getLogger(LOGGER_NAME).log(Level.INFO, message);
	}
	
	/**
	 * Log the delta assigned to each ASPEm for each intervla.
	 */
	public static void logAspemsLoadDistribution(Map<AID, Float[]> data) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<AID, Float[]> aspemData : data.entrySet()) {
			sb.append("\n\n>>#>#>#>#>#> Aspem: ");
			sb.append(aspemData.getKey().getLocalName());
			Float[] intervalsData = aspemData.getValue();
			for (int nInterval = 0; nInterval < intervalsData.length; nInterval++) {
				sb.append("  " + intervalsData[nInterval]);
			}			
		}
		sb.append("\n\n");
		
		jade.util.Logger.getLogger(LOGGER_NAME).log(Level.INFO, sb.toString());
		
		return;
	}
	
	
	/**
	 * Log the level assigned to each broker for each interval.
	 */
	public static void logBrokersLoadDistribution(AID target, List<AsdrInterval> intervals) {
		StringBuilder sb = new StringBuilder();
		sb.append("\n\n>#>#>#>#>#> Broker: ");
		sb.append(target.getLocalName());
		for (AsdrInterval intv : intervals) {
			sb.append("\n   - (");
			sb.append(intv.getDuration());
			sb.append(",");
			sb.append(intv.getPayload());
			sb.append(")");
		}
		sb.append("\n\n");
		
		jade.util.Logger.getLogger(LOGGER_NAME).log(Level.INFO, sb.toString());
		
		return;
	}
	
	
	/**
	 * Round a float number with a specific number of decimals.
	 */
	public static float round(float value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
	    return bd.floatValue();
	}	
}
