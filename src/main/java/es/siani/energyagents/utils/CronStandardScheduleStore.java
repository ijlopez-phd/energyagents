package es.siani.energyagents.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CronStandardScheduleStore {
	
	HashMap<String, String[]> schedules = new HashMap<String, String[]>();
	
	/**
	 * Constructor.
	 */
	public CronStandardScheduleStore() {
		
		return;
	}
	
	
	/**
	 * Save the definition of a schedule. This repository is intended to be used by all the
	 * ASBoxs in order to avoid to store duplicated definitions.
	 */
	public void saveSchedule(String name, String definition) {
		
		if ((name == null) || (definition == null)) {
			getLogger().log(Level.SEVERE, "Storing schedule definition: null name or definition are not allowed.");
			throw new IllegalArgumentException();
		}
		
		
		if (this.schedules.get(name) == null) {
			this.schedules.put(name, splitScheduleLines(definition));
		}
		
		return;
	}
	
	
	/**
	 * Return the value corresponding to the specified schedule at the specified time.
	 * If there is not value for the specified time returns null.
	 */
	public Double getValue(String scheduleName, Date moment) {
		
		String[] lines = this.schedules.get(scheduleName);
		if (lines == null) {
			getLogger().log(Level.SEVERE, "Getting schedule definition: the schedule name does not correspond to any stored definition.");
			throw new IllegalArgumentException();			
		}
		
		// Return the value of the first line that fits the date.
		Calendar cal = new GregorianCalendar();
		cal.setTime(moment);
		for (int n = 0; n < lines.length; n++) {
			Double lineValue = this.checkScheduleLine(lines[n], cal);
			if (lineValue != null) {
				return lineValue;
			}
		}
		
		
		// No line of the schedule fitted the date.
		return null;
	}
	
	
	/**
	 * Clear the buffer of values used by this instance.
	 */
	public void clearValues() {
		this.schedules.clear();
		return;
	}
	
	
	/**
	 * Split the definition of the schedule into the lines it is composed of.
	 * Each line corresponds to one value. 
	 */
	String[] splitScheduleLines(String definition) {
		
		BufferedReader reader = new BufferedReader(new StringReader(definition));
		
		ArrayList<String> outList = new ArrayList<String>();
		try {
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				String[] sublines = line.trim().split(";");
				for (int n = 0; n < sublines.length; n++) {
					String subline = sublines[n].trim();
					if (subline.length() > 0) {
						outList.add(subline);
					}
				}
			}
		} catch (IOException ioex) {
			
		}
		
		return outList.toArray(new String[outList.size()]);
	}
	
	
	/**
	 * Check if a date fits with a specific line of the schedule.
	 * If it fits the value related to the line is returned, otherwise
	 * null is returned.
	 */
	Double checkScheduleLine(String line, Calendar cal) {
		
		String[] fields = line.trim().split("\\s+");
		if (fields.length != 6) {
			getLogger().log(Level.SEVERE, "Getting value from schedule's line: The number of items of the line is not valid.");			
			throw new IllegalArgumentException();
		}
		
		final int[] FIELDS_TYPES = {
				Calendar.MINUTE, Calendar.HOUR_OF_DAY, Calendar.DAY_OF_MONTH, Calendar.MONTH, Calendar.DAY_OF_WEEK};		
		
		lbl_fields:
			// Fields are 5: minute, hour, day of month, month, day of week.
			for (int nField = 0; nField < 5; nField++) {
				String fieldValue = fields[nField];
				if (fieldValue.equals("*")) {
					// Asterisk means that all the values are accepted in this field.
					continue;
				} else if (fieldValue.contains(",")) {
					// The field contains a list of possible values
					String[] subfields = fieldValue.split(",");
					for (int nSubfield = 0; nSubfield < subfields.length; nSubfield++) {
						if (checkFieldValue(
								FIELDS_TYPES[nField], subfields[nSubfield], cal.get(FIELDS_TYPES[nField]))) {
							continue lbl_fields;
						}
					}
				} else {
					// The field contains one value (it can be a range).
					if (checkFieldValue(
							FIELDS_TYPES[nField], fieldValue, cal.get(FIELDS_TYPES[nField]))) {
						continue;
					}
				}

				// The value of this field doesn't fit with the specified in the date.
				return null;
			}
		
		return Double.parseDouble(fields[5]);
	}
	
	
	/**
	 * Check if the value of the field fits with the specifed range or value.
	 */
	boolean checkFieldValue(int fieldType, String desc, int fieldValue) {
		
		if (fieldType == Calendar.DAY_OF_WEEK) {
			desc = desc.replaceAll("7", "0");
			fieldValue--;
		}
		
		if (!desc.contains("-")) {
			// It is a simple value (not a range).
			return Integer.parseInt(desc) == fieldValue;
		}
		
		// It is a range.
		String[] bounds = desc.split("-");
		if (bounds.length != 2) {
			getLogger().log(Level.SEVERE, "Getting range's bounds: The format of the range is not valid.");			
			throw new IllegalArgumentException();
		}		
		return (Integer.parseInt(bounds[0]) <= fieldValue) && (Integer.parseInt(bounds[1]) >= fieldValue);
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(CronStandardScheduleStore.class.getSimpleName());
	}

}
