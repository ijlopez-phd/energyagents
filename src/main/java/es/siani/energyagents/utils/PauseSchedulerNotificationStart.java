package es.siani.energyagents.utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.inject.Inject;

import es.siani.energyagents.ontology.as00.EventConcept;
import es.siani.energyagents.ontology.as00.ProgramConcept;
import es.siani.energyagents.ontology.as00.ScheduleConcept;
import static es.siani.energyagents.utils.NotificationUtils.*;

public class PauseSchedulerNotificationStart implements PauseScheduler {
	
	private final Comparator<EventConcept> comparator;
	private PriorityQueue<EventConcept> nonNotifiedEvents = null;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public PauseSchedulerNotificationStart(Comparator<EventConcept> comparator) {
		this.comparator = comparator;
		return;
	}
	
	
	/**
	 * Init the scheduler.
	 */
	@Override
	public void init(ProgramConcept program) {
		
		this.nonNotifiedEvents = new PriorityQueue<EventConcept>(
				(program.getEvents().size() == 0)? 1 : program.getEvents().size(),
				this.comparator);
		for (EventConcept event : program.getEvents()) {
			this.nonNotifiedEvents.add(event);
		}
		
		return;
	}
	
	
	/**
	 * According to the demand-response program, this method calculates the next
	 * time the simulator must call the Gridop agent.
	 */
	@Override
	public ScheduleConcept process(long currentTime) {
		
		if (this.nonNotifiedEvents == null) {
			throw new IllegalStateException("The Pause Scheduler has not been initiated");
		}
		
		
		EventConcept event = this.nonNotifiedEvents.peek();
		if (event == null) {
			return new ScheduleConcept(ScheduleConcept.NO_PAUSE, currentTime);
		}
		
		long nextEventPause = getNotificationStart(event);
		if (currentTime == nextEventPause) {
			// This call corresponds to a scheduled event. It searches for the next
			// scheduled pause.
			ArrayList<EventConcept> events = new ArrayList<EventConcept>();
			nextEventPause = PauseScheduler.NO_PAUSE;
			do {
				events.add(this.nonNotifiedEvents.remove());
				event = this.nonNotifiedEvents.peek();
			} while ((event != null) && ((nextEventPause = getNotificationStart(event)) == currentTime));
			
			return new ScheduleConcept(nextEventPause, currentTime, events);
			
		} else if (currentTime < nextEventPause) {
			// There is no changes. This stop does not correspond to an event scheduled
			// by this algorithm.
			return new ScheduleConcept(nextEventPause, currentTime);
		}
		
		getLogger().log(Level.WARNING, "Gridop Scheduler is out of sync!!!");
		return new ScheduleConcept(ScheduleConcept.NO_PAUSE, currentTime);
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(PauseSchedulerNotificationStart.class.getSimpleName());
	}
}
