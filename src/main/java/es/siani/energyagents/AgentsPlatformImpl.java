package es.siani.energyagents;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import jade.content.lang.Codec;
import jade.content.onto.Ontology;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import jade.wrapper.gateway.JadeGateway;

import javax.inject.Inject;

import es.siani.energyagents.agent.AgentTracker;
import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.agent.EnergyAgentsDF;
import es.siani.energyagents.agent.ProxyAgent;
import es.siani.energyagents.agent.XmlUserPrefsHandler;
import es.siani.energyagents.injection.annotations.PlatformId;
import es.siani.energyagents.model.PauseResult;
import es.siani.energyagents.ontology.as00.EnergyAgentsOntology;
import es.siani.energyagents.ontology.as00.SimulationDescriptionConcept;
import es.siani.energyagents.ontology.as00.SimulationIdConcept;
import es.siani.energyagents.ontology.as00.SimulationPauseConcept;
import es.siani.simpledr.model.AsdrProgram;



public class AgentsPlatformImpl implements AgentsPlatform {
	
	/** Name of the platform. */
	private final String id;
	
	/** Codec used to transmit the messages. */
	private final Codec codec;
	
	/** Ontoloy that must use the agents of the platform. */
	private final Ontology ontology;
	
	/** XML handler to parse the XML that describes the UserPrefs. */
	final XmlUserPrefsHandler userPrefsHandler;
	
	/** Configuration parameters. */
	final Map<String, String> properties;
	


	/**
	 * Constructor.
	 */
	@Inject
	public AgentsPlatformImpl(@PlatformId String id, Codec codec, Ontology ontology, XmlUserPrefsHandler userPrefsHandler) {
		
		this.id = id;
		this.codec = codec;
		this.ontology = ontology;
		this.userPrefsHandler = userPrefsHandler;
		this.properties = new HashMap<String, String>();
		
		// Intializes the Proxy agent.
		JadeGateway.init(ProxyAgent.class.getName(), new Object[] {this}, null);		
		
		return;
	}
	
	
	/**
	 * @see es.siani.energyagents.AgentsPlatform#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}
	
	
	/**
	 * @see es.siani.energyagents.AgentsPlatform#getLanguage()
	 */
	@Override
	public Codec getCodec() {
		return this.codec;
	}
	
	
	/**
	 * @see es.siani.energyagents.AgentsPlatform#getOntology()
	 */
	@Override
	public Ontology getOntology() {
		return this.ontology;
	}
	
	
	/**
	 * @see es.siani.energyagents.AgentsPlatform#setProperty(String, String)
	 */
	@Override
	public void setProperty(String name, String value) {
		if (name == null) {
			throw new IllegalArgumentException();
		}
		
		this.properties.put(name, value);
		return;
	}
	
	
	/**
	 * @see es.siani.energyagents.AgentsPlatform#getProperty(String)
	 */	
	@Override
	public String getProperty(String name) {
		return this.properties.get(name);
	}
	
	
	/**
	 * @see es.siani.energyagents.AgentsPlatform#clearProperties()
	 */
	@Override
	public void clearProperties() {
		this.properties.clear();
	}
	
	
	/**
	 * @see es.siani.energyagents.AgentsPlatform#createAspemContainer(String)
	 */	
	@Override
	public AspemContainer createAspemContainer(String name, AgentTracker tracker) {
		AspemContainer container = new AspemContainer(name, this, this.userPrefsHandler);
		container.initAspemAgent(tracker);
		return container;
	}
	
	
	/**
	 * @see es.siani.energyagents.AgentsPlatform#createGridopContainer()
	 */	
	@Override
	public GridopContainer createGridopContainer(AgentTracker tracker) {
		GridopContainer container = new GridopContainer(this);
		initDirectoryFacilitator(container);		
		container.initGridopAgent(tracker);
		return container;
	}
	
	
	/**
	 * Init the Directory Facilitator used by the Platform.
	 */
	void initDirectoryFacilitator(AgentsContainer container) {
		
		String dfPath = this.properties.get(AgentsPlatform.PROP_JADE_DF_PATH);
		if (dfPath == null) {
			// Use the internal path
			dfPath = GridopContainer.class.getResource(EnergyAgentsGlobals.DF_INTERNAL_PATH).getPath();
		}
		
		try {
			AgentController dfAgent = container.container.createNewAgent(
					EnergyAgentsGlobals.DF_NAME,
					EnergyAgentsDF.class.getName(),
					new Object[] {dfPath});
			dfAgent.start();
		} catch (StaleProxyException ex) {
			getLogger().log(Level.SEVERE, "Failed to create the Directory Facilitator agent.", ex);
			throw new IllegalStateException(ex);
		}
		
		return;
	}
	
	
	/**
	 * Looks for the Gridop agent that is commanding the platform.
	 */
	public AID getGridopAgentAID(Agent searcher) {
		
		DFAgentDescription template = new DFAgentDescription();
		template.addLanguages(this.getCodec().getName());
		template.addOntologies(this.getOntology().getName());
		
		ServiceDescription sd = new ServiceDescription();
		sd.setType(EnergyAgent.SD_TYPE_GRIDOP);
		template.addServices(sd);
		
		AID gridopAgentAID = null;
		try {
			DFAgentDescription[] results = 
					DFService.search(searcher, new AID(EnergyAgentsGlobals.DF_NAME, AID.ISLOCALNAME), template);
			if (results.length != 1) {
				getLogger().log(Level.SEVERE, "No Gridop Agent seems to be registered in the platform.");
				throw new IllegalStateException("No Gridop Agent has been registered.");
			}
			
			gridopAgentAID = results[0].getName();
		} catch (FIPAException ex) {
			getLogger().log(Level.SEVERE, "Failed to find the Gridop agent in the yellow-pages service.", ex);
		}
		
		return gridopAgentAID;
	}
	
	
	/**
	 * Looks for the ASPEM agents registered in the platform.
	 */
	public AID[] getAspemAgentsAID(Agent searcher) {
		
		DFAgentDescription template = new DFAgentDescription();
		template.addLanguages(this.getCodec().getName());
		template.addOntologies(this.getOntology().getName());
		
		ServiceDescription sd = new ServiceDescription();
		sd.setName(EnergyAgent.SD_AGENT_TYPE);
		sd.setType(EnergyAgent.SD_TYPE_ASPEM);
		template.addServices(sd);
		
		AID[] aids = null;
		try {
			DFAgentDescription[] results = 
					DFService.search(searcher, new AID(EnergyAgentsGlobals.DF_NAME, AID.ISLOCALNAME), template);
			if (results == null) {
				results = new DFAgentDescription[0];
			}
			
			aids = new AID[results.length];
			for (int idx = 0; idx < results.length; idx++) {
				aids[idx] = results[idx].getName();
			}
		} catch (FIPAException ex) {
			getLogger().log(Level.SEVERE, "Failed to search the ASPEM agents registered in the platform.");
		}
		
		return aids;
	}
	
	
	/**
	 * Looks for the Broker agents registered in the specified container.
	 */
	@Override
	public AID[] getBrokerAgentsAID(Agent searcher, String containerName) {
		
		DFAgentDescription template = new DFAgentDescription();
		template.addLanguages(this.getCodec().getName());
		template.addOntologies(this.getOntology().getName());
		
		ServiceDescription sd = new ServiceDescription();
		sd.setName(EnergyAgent.SD_AGENT_TYPE);
		sd.setType(EnergyAgent.SD_TYPE_BROKER);	
		template.addServices(sd);
		
		sd = new ServiceDescription();
		sd.setName(EnergyAgent.SD_CONTAINER_NAME);
		sd.setType(containerName);
		template.addServices(sd);
		
		AID[] aids;
		try {
			DFAgentDescription[] results = 
					DFService.search(searcher, new AID(EnergyAgentsGlobals.DF_NAME, AID.ISLOCALNAME), template);
			if (results == null) {
				results = new DFAgentDescription[0];
			}
			
			aids = new AID[results.length];
			for (int idx = 0; idx < results.length; idx++) {
				aids[idx] = results[idx].getName();
			}
		} catch (FIPAException ex) {
			getLogger().log(Level.SEVERE, "Failed to search Broker agents registered in the container.");
			return new AID[0];
		}
		
		return aids;
	}
	
	
	/**
	 * @see es.siani.energyagents.AgentsPlatform#getDefaultFacilitatorAID()
	 */
	@Override
	public AID getDefaultFacilitatorAID() {
		return new AID(EnergyAgentsGlobals.DF_NAME, AID.ISLOCALNAME);
	}	
	
	
	/**
	 * Returns a Message template that includes the basic filters corresponding
	 * properties of the platform.
	 */
	@Override
	public MessageTemplate createBasicMessageTemplate() {
		MessageTemplate basicTemplate = MessageTemplate.and(
				MessageTemplate.MatchOntology(this.getOntology().getName()),
				MessageTemplate.MatchLanguage(this.getCodec().getName()));
		
		return basicTemplate;
	}
	
	
	/**
	 * @see es.siani.energyagents.AgentsPlatform#informSimulationStarted(int, name, AsdrProgram)
	 */
	@Override
	public void informSimulationStarted(int idSimulation, String scenarioCode, String storeName, AsdrProgram program) throws EnergyAgentsException {
		
		try {
			ProxyAgent.Command command = new ProxyAgent.Command(
					ProxyAgent.Command.CMD_REQUEST_START_SIMULATION,
					new Object[] {
							new SimulationDescriptionConcept(idSimulation, scenarioCode, storeName, EnergyAgentsOntology.createProgram(program))
					});
			JadeGateway.execute(command);
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE,
					"The command '" +
					ProxyAgent.Command.CMD_REQUEST_START_SIMULATION +
					"' cound not be sent.", ex);
			throw new EnergyAgentsException(ex);
		}
		
		return;
	}

	
	/**
	 * @see es.siani.energyagents.AgentsPlatform#informSimulationFinished(int)
	 */	
	@Override
	public void informSimulationFinished(int idSimulation) throws EnergyAgentsException {
		
		try {
			ProxyAgent.Command command = new ProxyAgent.Command(
					ProxyAgent.Command.CMD_REQUEST_FINISH_SIMULATION,
					new Object[] {new SimulationIdConcept(idSimulation)});
			JadeGateway.execute(command);
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "The command '" + ProxyAgent.Command.CMD_REQUEST_FINISH_SIMULATION + "' cound not be sent.", ex);
			throw new EnergyAgentsException(ex);
		}
		
		return;
	}


	/**
	 * @see es.siani.energyagents.AgentsPlatform#informSimulationPaused(int, Date, Date)
	 */	
	@Override
	public PauseResult informSimulationPaused(int idSimulation, Date realTime, Date modelTime) throws EnergyAgentsException {
		
		PauseResult pauseResult;
		try {
			ProxyAgent.Command command = new ProxyAgent.Command(
					ProxyAgent.Command.CMD_REQUEST_PAUSE_SIMULATION,
					new Object[] {new SimulationPauseConcept(idSimulation, realTime, modelTime)});
			JadeGateway.execute(command);
			pauseResult = (PauseResult)(command.getOutput()[0]);
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "The command '" + ProxyAgent.Command.CMD_REQUEST_PAUSE_SIMULATION + "' cound not be sent.", ex);
			throw new EnergyAgentsException(ex);
		}
		
		return pauseResult;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(AgentsPlatformImpl.class.getName());
	}
}
