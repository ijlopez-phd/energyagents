package es.siani.energyagents.behaviours.as04;

import java.util.List;
import java.util.Map;

import es.siani.energyagents.ontology.as04.OfferBlockConcept;
import jade.core.AID;

public interface ProductionAssigner {
	
	/**
	 * Determine to which consumers offer the available production. 
	 */
	public Map<AID, List<OfferBlockConcept>> assign(AuctionWorkingData workingData, float overbookingFactor);

	
	/**
	 * Set if the consumers must be randomized or ordered.
	 */
	public void useRandomItems(boolean isSorted);	
}
