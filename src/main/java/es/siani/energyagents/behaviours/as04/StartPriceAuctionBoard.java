package es.siani.energyagents.behaviours.as04;

import jade.core.AID;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.ontology.as04.DemandBlockConcept;

public class StartPriceAuctionBoard extends AuctionBoard {
	
	private Map<String, List<List<CapacityItem>>> capacityData = new HashMap<String, List<List<CapacityItem>>>();
	
	private int[] maxLevels;
	private int[] _minPpG;
	private int[] delator;
	private int lastIdxCapacityItem;
	private Map<Float,List<Integer>> ratio;
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#init(String, int)
	 */	
	@Override
	public void init(String aspemName, int nBlocks) {
		super.init(aspemName, nBlocks);
		
		List<List<CapacityItem>> capacityBlocks = new ArrayList<List<CapacityItem>>();
		for (int nBlock = 0; nBlock < nBlocks; nBlock++) {
			capacityBlocks.add(new ArrayList<CapacityItem>());
		}
		
		capacityData.put(aspemName, capacityBlocks);
		
		maxLevels = new int[nBlocks];
		
		_minPpG = new int[nBlocks];
		
		delator = new int[nBlocks];
		
		ratio = new HashMap<Float,List<Integer>>();

		return;
	}


	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#clear(String)
	 */	
	@Override
	public void clear(String aspemName) {
		super.clear(aspemName);
		capacityData.clear();;
		return;
	}
	
	
	@Override
	public void closeAuction() {
//		AuctionLogger alogger = InjectionUtils.injector().getInstance(AuctionLogger.class);
//		List<Float> keysList = Arrays.asList(ratio.keySet().toArray(new Float[0]));
//		Collections.sort(keysList);
//		for (Float key : keysList) {
//			List<Integer> values = ratio.get(key);
//			String strValues = "";
//			for (Integer value : values) {
//				strValues += " " + value;
//			}
//			alogger.log("# M: " + key.toString() + "  " + strValues);
//		}
		
		return;
	}
	
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#addBlocks(String, AID, List)
	 */	
	@Override
	public void addBlocks(String aspemName, AID node, List<DemandBlockConcept> blocksDemand) {
		
		List<List<BoardItem>> blocksLists = data.get(aspemName);
		for (DemandBlockConcept blockDemand : blocksDemand) {
			final int nBlock = blockDemand.getIdx();
			final float amount = blockDemand.getLoad();
			blocksLists.get(nBlock).add(new BoardItem(node, amount, blockDemand.getStartPrice()));
		}
		
		return;
	}
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#addProducer(String, int, int, float[], float[])
	 */
	public void addProducer(String aspemName, int idProducer, int nBlock, float[] capacity, float[] price) {
		List<CapacityItem> items = capacityData.get(aspemName).get(nBlock);
		for (int nLevel = 0; nLevel < capacity.length; nLevel++) {
			items.add(
					new CapacityItem(lastIdxCapacityItem++, nLevel, idProducer, capacity[nLevel], price[nLevel]));	
		}
		
		if (capacity.length > maxLevels[nBlock]) {
			maxLevels[nBlock] = capacity.length;
		}
		
		return;
	}
	
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#addProducer(String, int, float)
	 */	
	@Override
	public void addProducer(String aspemName, int nBlock, float capacity) {
		throw new UnsupportedOperationException();
	}
	
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#closedRegistering()
	 */
	@Override
	public void closeRegistering(String aspemName) {
		super.closeRegistering(aspemName);
		
		List<List<BoardItem>> demandLists = (List<List<BoardItem>>)data.get(aspemName);
		final int N_BLOCKS = demandLists.size();
		
		// Sort the capacity items by the price.
		List<List<CapacityItem>> cd = capacityData.get(aspemName);
		CapacityItemComparatorAsc capacityItemComparator = new CapacityItemComparatorAsc();
		for (int nBlock = 0; nBlock < N_BLOCKS; nBlock++) {
			Collections.sort(cd.get(nBlock), capacityItemComparator);
		}
		
		
		// Sort the demand items by the the StartPrice.
		for (int nBlock = 0; nBlock < N_BLOCKS; nBlock++) {
			Collections.sort(demandLists.get(nBlock), new BoardItemStartPriceComparatorAsc());
		}
		
		
		// Create the repository of producer indexes for each BoardItem.
		for (int nBlock = 0; nBlock < N_BLOCKS; nBlock++) {
			for (BoardItem item : demandLists.get(nBlock)) {
				item.producers = new ArrayList<Integer>();
				ratio.put(item.startPrice, new ArrayList<Integer>());				
			}
		}
		
		
		// Find out which producers start at which consumer.
		for (int nBlock = 0; nBlock < N_BLOCKS; nBlock++) {
			List<BoardItem> demandItems = demandLists.get(nBlock);
			List<CapacityItem> capacityItems = cd.get(nBlock);
			for (CapacityItem capacityItem : capacityItems) {
				int fromIndex = findStartIndex(demandItems, capacityItem.price);
				if (fromIndex >= 0) {
					List<Integer> itemProducers = demandItems.get(fromIndex).producers;
					if (itemProducers.indexOf(capacityItem.idProducer) < 0) {
						itemProducers.add(capacityItem.idProducer);
					}
				}
			}
		}
		
		// Find out how much available capacity there is for each DemandItem
		for (int nBlock = 0; nBlock < N_BLOCKS; nBlock++) {
			List<CapacityItem> capacityItems = cd.get(nBlock);
			for (BoardItem item : demandLists.get(nBlock)) {
				calculateAvailableCapacityFor(nBlock, item, capacityItems);
			}
		}
		
		AuctionLogger alogger = InjectionUtils.injector().getInstance(AuctionLogger.class);
		for (int nBlock = 0; nBlock < N_BLOCKS; nBlock++) {
			int nNeededProducers = 0;
			List<BoardItem> itemsToRemove = new ArrayList<BoardItem>();
			List<BoardItem> demandItems = demandLists.get(nBlock);
			final int N_DEMAND_ITEMS = demandItems.size();
			if (N_DEMAND_ITEMS == 0) {
				continue;
			}
			Set<Integer> idxsProducers = new HashSet<Integer>();
			for (int idx = 0; idx < N_DEMAND_ITEMS; idx++) {
				BoardItem item = demandItems.get(idx);
				final int nProducers = item.producers.size();
				if ((nProducers > 0) && (item.totalCapacity >= item.amount)) {
					float CpP = item.totalCapacity / (float)nProducers;
					nNeededProducers += (int)Math.ceil(item.amount / CpP);
					idxsProducers.addAll(item.producers);
				} else {
					itemsToRemove.add(item);
				}
			}
			
			for (BoardItem item : itemsToRemove) {
				demandItems.remove(item);
			}
			
			final int nProducers = idxsProducers.size();
			final int nConsumers = demandItems.size();
			_minPpG[nBlock] = (int)Math.ceil(nNeededProducers / (float)N_DEMAND_ITEMS);
			if (_minPpG[nBlock] > 0) {
				delator[nBlock] = (idxsProducers.size() / _minPpG[nBlock]) / N_DEMAND_ITEMS;				
			}			
			

			if ((nConsumers > 0) && ((nProducers / nConsumers) > _minPpG[nBlock])) {
				_minPpG[nBlock] = nProducers / nConsumers;
			}
			
			alogger.log(
					"# blck: " + nBlock +
					"   minPpG:" + _minPpG[nBlock] +
					"   di:" + demandItems.size() +
					"   ci:" + idxsProducers.size() +
					"   delator:" + delator[nBlock]);
		}

		
		for (int nBlock = 0; nBlock < N_BLOCKS; nBlock++) {
			int counter = 0;
			for (BoardItem item : demandLists.get(nBlock)) {
				item.previousSurplusProducers = counter;
				final int nProducers = item.producers.size();
				if (nProducers > _minPpG[nBlock]) {
					counter += nProducers - _minPpG[nBlock];
				} else if (nProducers < _minPpG[nBlock]) {
					counter -= _minPpG[nBlock] - nProducers;
				}
				
				List<Integer> itemCounters = ratio.get(item.startPrice);
				itemCounters.add(counter);
			}
		}
		
		return;
	}
	
	
	private void calculateAvailableCapacityFor(int nBlock, BoardItem demandItem, List<CapacityItem> capacityItems) {
		final float startPrice = demandItem.startPrice;
		for (CapacityItem capacityItem : capacityItems) {
			if (capacityItem.price <= startPrice) {
				demandItem.totalCapacity += capacityItem.capacity;
			}
		}
		
		return;
	}
	
	
	@Override
	public List<BoardItem> getConsumersList(String aspemName, int nBlock, int idProducer, float[] capacity, float[] price) {
		
		List<BoardItem> demandItems = data.get(aspemName).get(nBlock);
		final int nConsumers = demandItems.size();
		if (nConsumers == 0) {
			return new ArrayList<BoardItem>();
		}
		
		// Build the list of demand items keeping their real order.
		Set<BoardItem> selectedItems = new HashSet<BoardItem>();		
		final int N_LEVELS = capacity.length;		
		for (int nLevel = 0; nLevel < N_LEVELS; nLevel++) {
			
			final int fromIdx = findStartIndex(demandItems, price[nLevel]);
			if (fromIdx == -1) {
				// There are not items with the starting price greater than this price.
				continue;
			}
			
			BoardItem firstDemandItem = demandItems.get(fromIdx);
			
			// Find out the Start Index.
			int startIdx = 0;
			if (delator[nBlock] >= 1) {
				int nProducer = firstDemandItem.producers.indexOf(idProducer);
				if (nProducer == -1) {
					continue;
					//throw new IllegalStateException("The algorithm hasn't been thought correctly!!!!");
				}				
				if (nProducer < _minPpG[nBlock] ) {
					startIdx = fromIdx;
				} else {
					int toCount = (nProducer - _minPpG[nBlock]) + firstDemandItem.previousSurplusProducers;
					for (int lookingIdx = fromIdx + 1; lookingIdx < nConsumers; lookingIdx++) {
						BoardItem item = demandItems.get(lookingIdx);
						int nItemProducers = item.producers.size();
						if (nItemProducers < _minPpG[nBlock]) {
							toCount -= _minPpG[nBlock] - nItemProducers;
							if (toCount <= 0) {
								startIdx = lookingIdx;
								break;
							}
						}
					}
				}
			} else {
				List<BoardItem> items = extractRealOrderList(demandItems, fromIdx);
				final int nProducers = calculateNumberOfProducersFor(items);
				int nConsumersGroups = (int)(nProducers / _minPpG[nBlock]);
				if (nConsumersGroups == 0) {
					nConsumersGroups = 1;
				}
				final int nConsumersPerGroup = (int)Math.round(nConsumers / nConsumersGroups);
				final int nProducersPerGroup = (int)Math.round(nProducers / nConsumersGroups);
				if (nProducersPerGroup < _minPpG[nBlock]) {
					return items;
				}
				
				// Correction of previousSurplusProducers
				final int firstSurplus = firstDemandItem.previousSurplusProducers;
				int accumulatedIdx = 0;
				final int correctedMinPpG = _minPpG[nBlock] + 1;
				for (startIdx = fromIdx; startIdx < nConsumers;) {
					BoardItem item = demandItems.get(startIdx);
					accumulatedIdx += item.producers.indexOf(idProducer);
					accumulatedIdx += item.previousSurplusProducers - firstSurplus;
					if (accumulatedIdx < correctedMinPpG) {
						break;
					}
					
					startIdx += nConsumersPerGroup;
				}
			}
			
			float capacityToAllocate = capacity[nLevel];
			for (int idx = startIdx; (capacityToAllocate > 0) && (idx < nConsumers); idx++) {
				BoardItem item = demandItems.get(idx);
				capacityToAllocate -= item.amount;
				selectedItems.add(item);
			}
			
			
			if (capacityToAllocate > 0) {
				for (int idx = fromIdx; (capacityToAllocate > 0) && (idx < startIdx); idx++) {
					BoardItem item = demandItems.get(idx);
					capacityToAllocate -= item.amount;
					selectedItems.add(item);
				}
			}

		} // for levels
		
//		for (BoardItem item : selectedItems) {
//			Integer nValues = ratio.get(item.startPrice);
//			if (nValues == null) {
//				nValues = 0;
//			}
//			ratio.put(item.startPrice, nValues + 1);
//		}

		return new ArrayList<BoardItem>(selectedItems);
	}
	
	
	public int calculateNumberOfProducersFor(List<BoardItem> items) {
		Set<Integer> idxsProducers = new HashSet<Integer>();
		for (BoardItem item : items) {
			idxsProducers.addAll(item.producers);
		}
		return idxsProducers.size();
	}
	
	
	//@Override
//	public List<BoardItem> getConsumersListB(String aspemName, int nBlock, int nProducer, float[] capacity, float[] price) {
//		
//		HashSet<BoardItem> selectedItems = new HashSet<BoardItem>();
//		List<BoardItem> demandItems = data.get(aspemName).get(nBlock);
//		List<CapacityItem> capacityItems = capacityData.get(aspemName).get(nBlock);
//		
//		final int N_LEVELS = capacity.length;
//		
//		for (int nLevel = N_LEVELS - 1; nLevel >= 0; nLevel--) {
//			
//			int fromIdx = findStartIndex(demandItems, price[nLevel]);
//			if (fromIdx == -1) {
//				// There are not items with the starting price greater than this price.
//				continue;
//			}
//			
//			// Build the table of items.
//			List<BoardItem> items = extractRealOrderList(demandItems, fromIdx);
//			//Collections.sort(items);
//			
//			AlgorithmData ad = calculateAlgorithmData(items, capacityItems, nProducer);
//			int startIdx;
//			if (ad.delator >= 1) {
//				int idxGroup = ad.idxProducer / ad.nMinProducersPerConsumerGroup;
//				startIdx = idxGroup * ad.nConsumersPerGroup;
//				if (startIdx >= ad.nConsumers) {
//					startIdx = startIdx % ad.nConsumers; 
//				}
//				
//			} else {
//				int nProducersPerGroup = (int)Math.round(ad.nProducers / (float)ad.nConsumerGroups);
//				if (nProducersPerGroup < ad.nMinProducersPerConsumerGroup) {
//					selectedItems.addAll(items);
//					continue;
//				}
//				
//				int idxGroup = ad.idxProducer / nProducersPerGroup;
//				startIdx = idxGroup * ad.nConsumersPerGroup;
//			}
//			
//			float levelCapacity = capacity[nLevel];
//			float allocatedCapacity = 0;
//			for (int idx = startIdx; (allocatedCapacity < levelCapacity) && (idx < ad.nConsumers); idx++) {
//				BoardItem item = items.get(idx);
//				selectedItems.add(item);
//				allocatedCapacity += item.amount;
//			}
//
//			if (allocatedCapacity < ad.totalCapacity) {
//				for (int idx = 0; (allocatedCapacity < levelCapacity) && (idx < startIdx); idx++) {
//					BoardItem item = items.get(idx);
//					selectedItems.add(item);
//					allocatedCapacity += item.amount;
//				}
//			}			
//			
//		}
//		
//		return new ArrayList<BoardItem>(selectedItems);
//	}
	
	
	private int findStartIndex(List<BoardItem> items, float price) {
		for (int nItem = 0; nItem < items.size(); nItem++) {
			if (price <= items.get(nItem).startPrice) {
				return nItem;
			}
		}
		
		return -1;
	}
	
	
	private List<BoardItem> extractRealOrderList(List<BoardItem> sourceList, int from) {
		ArrayList<BoardItem> targetList = new ArrayList<BoardItem>();
		for (int idx = from; idx < sourceList.size(); idx++) {
			targetList.add(sourceList.get(idx));
		}
		
		//Collections.sort(targetList, new BoardItemIndexComparatorAsc());
		
		return targetList;
	}
	
	
	private float calculateDemand(List<BoardItem> itemsList) {
		float totalDemand = 0;
		for (BoardItem item : itemsList) {
			totalDemand += item.amount;
		}
		
		return totalDemand;
	}
	
	
//	private ListCapacityData calculateCapacity(int idProducer, List<BoardItem> itemsList, List<CapacityItem> capacityItems) {
//		Set<Integer> producers = new HashSet<Integer>();
//		float totalCapacity = 0;
//		float totalDemand = 0;
//		int insideId = -1;
//		for (BoardItem demandItem : itemsList) {
//			totalDemand += demandItem.amount;
//			for (int n = 0; n < capacityItems.size(); n++) {
//				CapacityItem capacityItem = capacityItems.get(n);
//				if (capacityItem.price <= demandItem.startPrice) {
//					totalCapacity += capacityItem.capacity;
//					producers.add(capacityItem.idProducer);
//					if ((capacityItem.idProducer == idProducer) && (insideId == -1)) {
//						insideId = producers.size() - 1;
//					}
//				} else {
//					break;
//				}
//			}
//		}
//		
//		return new ListCapacityData(insideId, totalCapacity, totalDemand, producers.size());
//	}
	
	
//	private AlgorithmData calculateAlgorithmData(List<BoardItem> items, List<CapacityItem> capacityItems, int idProducer) {
//		
//		ListCapacityData lcd = calculateCapacity(idProducer, items, capacityItems);
//		
//		final int nConsumers = items.size();
//		int nConsumersGroups = nConsumers;
//		int nConsumersPerGroup = 1;
//		final float demandMean = lcd.totalDemand /(float)nConsumers;
//		
//		final float capacityMean = lcd.totalCapacity / (float)lcd.nProducers;
//		int nMinProducersPerConsumerGroup = (int)Math.ceil(demandMean / capacityMean);
//		
//		int delator = (int)(lcd.nProducers / (float)nMinProducersPerConsumerGroup) / nConsumers;
//		if (delator > 1) {
//			nMinProducersPerConsumerGroup = lcd.nProducers / nConsumers;
//			
//		} else if (delator == 0) {
//			nConsumersGroups = (int)(lcd.nProducers / nMinProducersPerConsumerGroup);
//			if (nConsumersGroups == 0) {
//				nConsumersGroups = 1;
//			}
//			nConsumersPerGroup = (int)Math.round(nConsumers / (float)nConsumersGroups);
//		}
//		
//		AlgorithmData ad = new AlgorithmData();
//		ad.delator = delator;
//		ad.nConsumers = nConsumers;
//		ad.nConsumerGroups = nConsumersGroups;
//		ad.nConsumersPerGroup = nConsumersPerGroup;
//		ad.nMinProducersPerConsumerGroup = nMinProducersPerConsumerGroup;
//		ad.nProducers = lcd.nProducers;
//		ad.idxProducer = lcd.idProducer;
//		ad.totalCapacity = lcd.totalCapacity;
//		
//		return ad;
//	}
	
	
	/**
	 * 
	 * 
	 * Data of a producer registered with the Board.
	 * 
	 *
	 */
	private class CapacityItem {
		int idx;
		int nLevel;
		int idProducer;
		float capacity;
		float price;
		
		/**
		 * Constructor.
		 */
		public CapacityItem(int idx, int nLevel, int idProducer, float capacity, float price) {
			this.idx = idx;
			this.nLevel = nLevel;
			this.idProducer = idProducer;			
			this.capacity = capacity;
			this.price = price;
			return;
		}
	}
	
	
	/**
	 * 
	 * 
	 * Algorithm fields.
	 * 
	 * 
	 */
	private class AlgorithmData {
		int nConsumers;
		int nConsumerGroups;
		int nConsumersPerGroup;
		int nMinProducersPerConsumerGroup;
		int nProducers;
		int idxProducer;
		int delator;
		float totalCapacity;
	}
	
	
	/**
	 * 
	 * Description of the capacity available for a list of Demand blocks.
	 *
	 */
	private class ListCapacityData {
		float totalCapacity;
		float totalDemand;
		int nProducers;
		int idProducer;
		
		/**
		 * Constructor.
		 */
		public ListCapacityData(int idProducer, float totalCapacity, float totalDemand, int nProducers) {
			this.idProducer = idProducer;
			this.totalCapacity = totalCapacity;
			this.totalDemand = totalDemand;
			this.nProducers = nProducers;
			return;
		}
	}
	
	
	/**
	 * 
	 * 
	 * Comparator for sorting instances of CapacityItem by the price in ascending order.
	 * 
	 * 
	 */
	private class CapacityItemComparatorAsc implements Comparator<CapacityItem> {
		
		@Override
		public int compare(CapacityItem o1, CapacityItem o2) {
			if (o1.price < o2.price) {
				return -1;
			} else if (o1.price > o2.price) {
				return 1;
			}
			
			return 0;
		}
		
	}
	
	
	
	/**
	 * 
	 * 
	 * Comparator for sorting the class BoardItem by the price in ascending order.
	 * 
	 * 
	 */
	private class BoardItemStartPriceComparatorAsc implements Comparator<BoardItem> {
		@Override
		public int compare(BoardItem o1, BoardItem o2) {
			if (o1.startPrice < o2.startPrice) {
				return -1;
			} else if (o1.startPrice > o2.startPrice) {
				return 1;
			}
			
			return 0;
		}
	}
	
	
	
	/**
	 * 
	 * 
	 * Comparator for sortirng the class BoardItem by the Index in ascending order.
	 * 
	 * 
	 */
	private class BoardItemIndexComparatorAsc implements Comparator<BoardItem> {
		@Override
		public int compare(BoardItem o1, BoardItem o2) {
			if (o1.idx < o2.idx) {
				return -1;
			} else if (o1.idx > o2.idx) {
				return 1;
			}
			
			return 0;
		}
	}

}
