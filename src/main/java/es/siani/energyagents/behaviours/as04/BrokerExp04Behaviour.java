package es.siani.energyagents.behaviours.as04;

import static es.siani.energyagents.EnergyAgentsExp03Globals.SD_CONSUMER_TYPE;
import static es.siani.energyagents.EnergyAgentsExp03Globals.SD_PRODUCER_TYPE;
import jade.content.ContentManager;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.xml.transform.stream.StreamSource;

import com.google.inject.Injector;

import es.siani.energyagents.agent.AgentTracker;
import es.siani.energyagents.behaviours.as00.BrokerProcessBehaviour;
import es.siani.energyagents.behaviours.as00.SendRequestBehaviour;
import es.siani.energyagents.behaviours.as00.SimulationRequestMatcher;
import es.siani.energyagents.config.Configuration;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.ontology.as00.CreatedEventPredicate;
import es.siani.energyagents.ontology.as00.DistributeEventAction;
import es.siani.energyagents.ontology.as00.EiResponseConcept;
import es.siani.energyagents.ontology.as00.EventConcept;
import es.siani.energyagents.ontology.as00.IntervalConcept;
import es.siani.energyagents.ontology.as04.AbortAuctionAction;
import es.siani.energyagents.ontology.as04.AuctionRoleConcept;
import es.siani.energyagents.ontology.as04.ClearAuctionAction;
import es.siani.energyagents.ontology.as04.DemandBlockConcept;
import es.siani.energyagents.ontology.as04.FinishAuctionAction;
import es.siani.energyagents.ontology.as04.OfferBlockConcept;
import es.siani.energyagents.ontology.as04.OfferConfirmationConcept;
import es.siani.energyagents.ontology.as04.OfferLevelConcept;
import es.siani.energyagents.ontology.as04.OffersConfirmationPredicate;
import es.siani.energyagents.ontology.as04.PrepareAuctionAction;
import es.siani.energyagents.ontology.as04.ProducerBlockConcept;
import es.siani.energyagents.ontology.as04.ProducerRegisteredPredicate;
import es.siani.energyagents.ontology.as04.ReadyAuctionPredicate;
import es.siani.energyagents.ontology.as04.RegisterOffersAction;
import es.siani.energyagents.ontology.as04.RegisterProducerAction;
import es.siani.energyagents.ontology.as04.ResultAbortAuctionPredicate;
import es.siani.energyagents.ontology.as04.ResultClearAuctionPredicate;
import es.siani.energyagents.ontology.as04.ResultFinishAuctionPredicate;
import es.siani.energyagents.ontology.as04.ResultStartAuctionStagePredicate;
import es.siani.energyagents.ontology.as04.StartAuctionAction;
import es.siani.energyagents.services.LoadForecastService;
import es.siani.energyagents.utils.CronLevelsScheduleStore;
import es.siani.energyagents.utils.CronScheduleStore;
import es.siani.energyagents.utils.EnergyAgentsUtils;
import es.siani.energyagents.xmpp.SimpleXmpp;
import es.siani.simpledr.EasyDistributeEvent;
import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;
import es.siani.simpledr.model.AsdrSignalTypeEnum;

@SuppressWarnings("serial")
public class BrokerExp04Behaviour extends BrokerProcessBehaviour {
	
	private static final String BLOCK_PREFIX = "block";
	
	private final long BLOCK_MILLIS;
	
	private final float OVERBOOKING_FACTOR;
	
	private AuctionWorkingData wd = null;
	
	private AuctionLogger auctionLogger = null;
	
	private String defaultDfName = null;
	
	private boolean usingStartingPrice = false;
	
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public BrokerExp04Behaviour() {
		Injector injector = InjectionUtils.injector();
		Configuration config = injector.getInstance(Configuration.class);
		BLOCK_MILLIS = Integer.parseInt(config.get(Configuration.PROP_AUCTION_BLOCK_SECONDS)) * 1000;
		OVERBOOKING_FACTOR = Float.parseFloat(config.get(Configuration.PROP_AUCTION_OVERBOOKING_FACTOR));
		usingStartingPrice = Boolean.parseBoolean(config.get(Configuration.PROP_AUCTION_USE_START_PRICE));
		
		// Initialize the auction handler.
		auctionLogger = injector.getInstance(AuctionLogger.class);
		
		return;
	}
	
	
	/**
	 * Tell if the behaviour has finished its execution.
	 */
	@Override
	public boolean done() {
		return true;
	}	

	
	/**
	 * Handle the messages received from the ASPEM.
	 */
	@Override
	public void action() {
		
		// Add the behaviour that listens to the requests of the ASPEM.
		agent.addBehaviour(new ReceiveDistributeActionBehaviour(
				new MessageTemplate(
						new SimulationRequestMatcher(agent, agent.getAID(), DistributeEventAction.class))));
		
		agent.addBehaviour(new ReceivePrepareAuctionBehaviour(
				new MessageTemplate(
						new SimulationRequestMatcher(agent, agent.getAID(), PrepareAuctionAction.class))));
		
		agent.addBehaviour(new ReceiveStartAuctionBehaviour(
				new MessageTemplate(
						new SimulationRequestMatcher(agent, agent.getAID(), StartAuctionAction.class))));
		
		agent.addBehaviour(new ReceiveRegisterProducerBehaviour(
				new MessageTemplate(
						new SimulationRequestMatcher(agent, agent.getAID(), RegisterProducerAction.class))));
		
		agent.addBehaviour(new ReceiveClearAuctionBehaviour(
				new MessageTemplate(
						new SimulationRequestMatcher(agent, agent.getAID(), ClearAuctionAction.class))));
		
		agent.addBehaviour(new ReceiveProductionOffersBehaviour(
				new MessageTemplate(
						new SimulationRequestMatcher(agent, agent.getAID(), RegisterOffersAction.class))));
		
		agent.addBehaviour(new ReceiveFinishAuctionBehaviour(
				new MessageTemplate(
						new SimulationRequestMatcher(agent, agent.getAID(), FinishAuctionAction.class))));
		
		agent.addBehaviour(new ReceiveAbortAuctionBehaviour(
				new MessageTemplate(
						new SimulationRequestMatcher(agent, agent.getAID(), AbortAuctionAction.class))));		
		
		return;
	}
	
	
	/**
	 * Create a name for the block N.
	 */
	private static String blockName(int n) {
		return BLOCK_PREFIX + n;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(BrokerExp04Behaviour.class.getName());
	}
	
	
	
	/**
	 * Track distribute events.
	 */
	private void trackDistributeEvent(AsdrEvent event) {
		AgentTracker tracker = agent.getTracker();
		if (tracker == null) {
			return;
		}
		
		tracker.distributeEventReceived();
		if (event != null) {
			tracker.distributeEvent(event.getIntervalsPayload());
		}
		
		return;
	}
	
	
	/**
	 * 
	 * 
	 * Receive DistributeAction events from the ASPEM.
	 * 
	 * 
	 */
	private class ReceiveDistributeActionBehaviour extends AchieveREResponder {

		/**
		 * Constructor.
		 */
		public ReceiveDistributeActionBehaviour(MessageTemplate mt) {
			super(agent, mt);
			return;
		}
		
		
		/**
		 * Process a request received from the ASPEM. 
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			// Extract the content of the message.
			DistributeEventAction action;
			try {
				action = (DistributeEventAction)agent.getContentManager().extractContent(request);
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
				throw new IllegalStateException();				
			}
			
	
			// Extract the DistributeEvent received.
			EasyDistributeEvent receivedEasy = new EasyDistributeEvent(
					action.getDistributeEvent().getMarketContext(),
					new StreamSource(new StringReader(action.getDistributeEvent().getContent())));
			
			
			// Find new events in the received DistributeEvent.
			EasyDistributeEvent easy = agent.getDistributeEvent();
			easy.updateEventsStatus(action.getModelTimestamp());
			List<AsdrEvent> newOadrEvents = agent.getDistributeEvent().findNewEvents(receivedEasy);
			final int nEvents = newOadrEvents.size();
			if (nEvents == 1) {
			
				// There is one new event. 
				AsdrEvent event = newOadrEvents.get(0);

				// Track the event.
				trackDistributeEvent(event);

				if (!event.getSignalType().equals(AsdrSignalTypeEnum.LEVEL)) {
					// Only of events of the type Level are supported in this experiment.
					throw new RefuseException("Only events with signals of the type LEVEL are supported on this experiment.");
				}
				
				// The Level signal is sent to the local agent.
				agent.getSimpleXmpp().send(action.getDistributeEvent().getContent());
				agent.setDistributeEvent(receivedEasy);
				auctionLogger.logBrokerResult(agent.getLocalName(), defaultDfName, event);
			} else if (nEvents > 1) {
				throw new RefuseException("Processing multiple events at the same time is not supported.");
			}


			// Reply to the ASPEM.
			ACLMessage reply = request.createReply();
			reply.setPerformative(ACLMessage.INFORM);
			try {
				agent.getContentManager().fillContent(
						reply,
						new CreatedEventPredicate(
								new EiResponseConcept(EiResponseConcept.CODE_OK, request.getConversationId())));
			} catch (Exception ex) {
				throw new IllegalStateException(ex);
			}
			
			return reply;
		}
	}


	
	/**
	 * 
	 * 
	 * Receive PrepareAuction message from the ASPEM.
	 * 
	 * 
	 */
	private class ReceivePrepareAuctionBehaviour extends AchieveREResponder {

		/**
		 * Constructor.
		 */
		public ReceivePrepareAuctionBehaviour(MessageTemplate mt) {
			super(agent, mt);
			return;
		}
		
		
		/**
		 * Process a request received from the ASPEM. 
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			// Extract the content of the message.
			PrepareAuctionAction action;
			try {
				action = (PrepareAuctionAction)agent.getContentManager().extractContent(request);
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
				throw new IllegalStateException();				
			}
			
			if (agent.getTracker() != null) {
				agent.getTracker().prepareAuctionReceived();
			}
			
			defaultDfName = new AID(action.getDfName(), AID.ISGUID).getLocalName();
			DFAgentDescription dfServices = new DFAgentDescription();
			BlockDescription[] blocks =
					new BlockDescription[(int)((action.getEndTime().getTime() - action.getStartTime().getTime()) / BLOCK_MILLIS)];
					
			AuctionRoleConcept auctionRole = fillBlocksAndServices(action, dfServices, blocks);
			
			List<DemandBlockConcept> demandBlocks = new ArrayList<DemandBlockConcept>();
			List<ProducerBlockConcept> producerBlocks = new ArrayList<ProducerBlockConcept>();
			if (auctionRole.consumer() || auctionRole.producer()) {
				AID facilitatorAID = new AID(action.getDfName(), AID.ISGUID);
				dfServices.setName(agent.getAID());
				dfServices.addLanguages(agent.getPlatform().getCodec().getName());
				dfServices.addOntologies(agent.getPlatform().getOntology().getName());
				try {
					DFService.register(agent, facilitatorAID, dfServices);
				} catch (FIPAException ex) {
					getLogger().log(Level.SEVERE, "Failed to register the agent's auction services with the FIPA yellow-pages service.");
					throw new IllegalStateException(ex);
				}
				
				wd = new AuctionWorkingData();
				wd.agentName = agent.getLocalName();
				wd.auctionId = action.getId();
				wd.auctionFacilitatorAID = facilitatorAID;
				wd.blocks = blocks;
				
				if (auctionRole.consumer() && auctionRole.producer()) {
					wd.role = AuctionAgentRole.PROSUMER;
				} else if (auctionRole.consumer()) {
					wd.role = AuctionAgentRole.CONSUMER;
				} else {
					wd.role = AuctionAgentRole.PRODUCER;
				}
				
				if (auctionRole.consumer() || auctionRole.producer()) {
					for (int nBlock = 0; nBlock < wd.blocks.length; nBlock++) {
						BlockDescription block = wd.blocks[nBlock];
						if (block.role == AuctionAgentRole.CONSUMER) {
							demandBlocks.add(
									new DemandBlockConcept(nBlock, block.demand, agent.getUserPrefs().getStartingPrice()));
						} else if (block.role == AuctionAgentRole.PRODUCER) {
							if (!usingStartingPrice) {
								float capacity = 0;
								for (float levelCapacity : block.offerAmount) {
									capacity += levelCapacity;
								}
								producerBlocks.add(new ProducerBlockConcept(nBlock, capacity));
							} else {
								producerBlocks.add(new ProducerBlockConcept(nBlock, block.offerAmount, block.offerPrice));
							}
						}
					}
				}
			}
			
			// Build the reply message.
			ACLMessage reply = request.createReply();
			reply.setPerformative(ACLMessage.INFORM);
			try {
				agent.getContentManager().fillContent(
						reply,
						new ReadyAuctionPredicate(auctionRole, demandBlocks, producerBlocks));
			} catch (Exception ex) {
				throw new IllegalStateException(ex);
			}
			
			return reply;
		}
		
		
		/**
		 * Fill the structure BlockDescription of each block, and fill the services the agent can carry out for each block.
		 * Tell if the broker participates as Producer, Consumer or neither of them.
		 */
		private AuctionRoleConcept fillBlocksAndServices(
				PrepareAuctionAction action, DFAgentDescription dfServices, BlockDescription[] blocks) {
			
			CronScheduleStore scheduler = InjectionUtils.injector().getInstance(CronScheduleStore.class);
			int[] cronBlocksValues = scheduler.getValuesByBlock(
					agent.getUserPrefs().getLevelsSchedule().getName(), action.getStartTime(), action.getEndTime());			
			
			int[] eventLevels = signalLevelsByBlock(action);
			
			// Register with the DF the role that may adopt this broker for each block of time.
			long nowMillis = action.getStartTime().getTime();
			LoadForecastService forecastService = InjectionUtils.injector().getInstance(LoadForecastService.class);
			AuctionRoleConcept auctionRole = new AuctionRoleConcept();
			for (int nCronBlock = 0; nCronBlock < cronBlocksValues.length; nCronBlock++, nowMillis += BLOCK_MILLIS) {
				
				int cronBlockValue = cronBlocksValues[nCronBlock];
				if (cronBlockValue == CronLevelsScheduleStore.NO_VALUE) {
					blocks[nCronBlock] = new BlockDescription();
					continue;
				}
				
				BlockDescription blockDescription = new BlockDescription();
				boolean isConsumer = CronLevelsScheduleStore.isConsumer(cronBlockValue);
				boolean isProducer = CronLevelsScheduleStore.isProducer(cronBlockValue);
				int cronBlockLevel = CronLevelsScheduleStore.getLevel(cronBlockValue);

				final int eventLevel = eventLevels[nCronBlock];
				if (isConsumer && (eventLevel >= cronBlockLevel) || (!isConsumer && (cronBlockLevel > eventLevel))) {

					Float[] loadLevels = forecastService.getLoadForAllLevels(
							action.getStoreName(),
							agent.getLocalName(),
							new Date(nowMillis),
							new Date(nowMillis + BLOCK_MILLIS));						

					float amount = 0;
					if (isConsumer) {
						blockDescription.demand = loadLevels[0] - loadLevels[eventLevel];
						amount = blockDescription.demand;
					} else { // is a Producer
						final float[] userLevelPrices = agent.getUserPrefs().getLevelsPrices();
						ArrayList<Float[]> offersList = new ArrayList<Float[]>();
						for (int nOffer = eventLevel; nOffer < cronBlockLevel; nOffer++) {
							float offerAmount = loadLevels[nOffer] - loadLevels[nOffer + 1];
							if (offerAmount > 0) {
								offersList.add(new Float[] {offerAmount, userLevelPrices[nOffer]});
								amount += offerAmount;
							}
						}

						final int nOffers = offersList.size();
						blockDescription.offerAmount = new float[nOffers];
						blockDescription.offerPrice = new float[nOffers];						
						for (int nOffer = 0; nOffer < nOffers; nOffer++) {
							Float[] offerData = offersList.get(nOffer);
							blockDescription.offerAmount[nOffer] = offerData[0];
							blockDescription.offerPrice[nOffer] = offerData[1];
						}
					}
					
					if (amount > 0) {
						if (isConsumer) {
							auctionRole.markAsConsumer();
						} else {
							auctionRole.markAsProducer();							
						}
						
						blockDescription.role = isConsumer? AuctionAgentRole.CONSUMER : AuctionAgentRole.PRODUCER;
						blockDescription.cronLevel = cronBlockLevel;
						blockDescription.eventLevel = eventLevel;
						blockDescription.combined = CronLevelsScheduleStore.isCombined(cronBlockValue);
						
						ServiceDescription sd = new ServiceDescription();
						sd.setName(blockName(nCronBlock));
						sd.setType(isConsumer? SD_CONSUMER_TYPE : SD_PRODUCER_TYPE);
						dfServices.addServices(sd);						
					}
				}
					
				blocks[nCronBlock] = blockDescription;
			}
			
			return auctionRole;
		}
		
		
		/**
		 * Return the signal level associated to each block of the auction.
		 * The signal level is deduced from the Intervals of the event.
		 */
		private int[] signalLevelsByBlock(PrepareAuctionAction action) {
			
			final long startMillis = action.getStartTime().getTime();
			final int N_BLOCKS =  (int)((action.getEndTime().getTime() - startMillis) / BLOCK_MILLIS);
			
			int[] signals = new int[N_BLOCKS];

			Iterator<IntervalConcept> itIntervals = action.getIntervals().iterator();
			IntervalConcept currentInterval = itIntervals.next();
			long nextIntervalEnd = startMillis  + (currentInterval.getDuration() * 1000);
			long currentMillis = startMillis;
			for (int n = 0; n < N_BLOCKS; n++, currentMillis += BLOCK_MILLIS) {
				if (currentMillis == nextIntervalEnd) {
					currentInterval = itIntervals.next();
					nextIntervalEnd += currentInterval.getDuration() * 1000;
				}
				
				signals[n] = (int)currentInterval.getPayload();
			}
			
			return signals;
		}
	}
	
	
	
	/**
	 * 
	 * 
	 * Receive StartAuction message from the ASPEM.
	 * 
	 * 
	 */
	private class ReceiveStartAuctionBehaviour extends AchieveREResponder {
		
		/**
		 * Constructor.
		 */
		public ReceiveStartAuctionBehaviour(MessageTemplate mt) {
			super(agent, mt);
			return;
		}
		
		
		/**
		 * Process the message StartAuction.
		 * (This message in only received by the Producers.)
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			StartAuctionAction action;
			try {
				action = (StartAuctionAction)agent.getContentManager().extractContent(request);
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
				throw new IllegalStateException();				
			}
			
			if (agent.getTracker() != null) {
				agent.getTracker().startAuctionReceived();
			}
			
			if (action.getId() != wd.auctionId) {
				getLogger().log(Level.SEVERE, "Auction messages out of sync!!!");
				throw new IllegalStateException();
			}
			
			AID[] availableConsumers = fillConsumers(request.getSender().getLocalName(), action.getIdxProducer());
			if (availableConsumers.length == 0) {
				// Do not continue as Producer anymore. He was the only consumer for his own production (prosumer).
				if (wd.role == AuctionAgentRole.PROSUMER) {
					wd.role = AuctionAgentRole.CONSUMER;
				}
				ACLMessage reply = request.createReply();
				reply.setPerformative(ACLMessage.INFORM);
				try {
					agent.getContentManager().fillContent(reply, new ResultStartAuctionStagePredicate(false));
				} catch (Exception ex) {
					throw new IllegalStateException(ex);
				}
				
				return reply;
			}
			
			wd.aspemName = request.getSender().getLocalName();
			
			SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
					agent,
					availableConsumers,
					new ACLMessage(ACLMessage.REQUEST),
					new RegisterProducerAction());
			sendBehaviour.registerHandleAllResultNotifications(new ReceiveConsumersPing(request));
			agent.addBehaviour(sendBehaviour);

			return null;					
		}
		
		
		/**
		 * Search all the brokers that work as consumers during a period at which
		 * this agent works as producer.
		 */
		private AID[] fillConsumers(String aspemName, int idxProducer) {
			
			AuctionBoard consumersBoard =
					InjectionUtils.injector().getInstance(AuctionBoard.class);
			
			BlockDescription[] blocks = wd.blocks;
			Set<AID> consumersAIDs = new HashSet<AID>();
			for (int nBlock = 0; nBlock < blocks.length; nBlock++) {
				BlockDescription block = blocks[nBlock];
				if (block.role == AuctionAgentRole.PRODUCER) {
					block.consumers = 
							consumersBoard.getConsumersList(
									aspemName,
									nBlock,
									idxProducer,
									block.offerAmount,
									block.offerPrice);
					for (BoardItem item : block.consumers) {
						consumersAIDs.add(item.aid);
					}
				}
			}
			
			return consumersAIDs.toArray(new AID[0]);
		}
		
		
		/**
		 * 
		 * 
		 * Collect consumers pings to the messages that request the registration of a producer.
		 * These replies of the consumers just mean that they have received the call for registration.
		 * 
		 * 
		 */
		private class ReceiveConsumersPing extends OneShotBehaviour {
			
			private final ACLMessage request;
			
			
			/**
			 * Constructor.
			 */
			public ReceiveConsumersPing(ACLMessage request) {
				this.request = request;
				return;
			}

			
			/**
			 * Reply to the ASPEM. The reply indicates that all the produces have received
			 * the call for offers.
			 */
			@Override
			public void action() {
				
				// Send the message to the ASPEM grid.
				ACLMessage reply = request.createReply();
				reply.setPerformative(ACLMessage.INFORM);
				try {
					agent.getContentManager().fillContent(reply, new ResultStartAuctionStagePredicate(true));
				} catch (Exception ex) {
					throw new IllegalStateException(ex);
				}
				agent.send(reply);
				return;
			}
		}
	}
	
	
	
	/**
	 * 
	 * 
	 * Receive Calls for production offers.
	 * 
	 * 
	 */
	private class ReceiveRegisterProducerBehaviour extends AchieveREResponder {
		
		/**
		 * Constructor.
		 */
		public ReceiveRegisterProducerBehaviour(MessageTemplate mt) {
			super(agent, mt);
			return;
		}
		
		
		/**
		 * Process the message CallForProducing.
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			wd.nExpectedProducersOffers++;
			
			// Build the reply message.
			ACLMessage reply = request.createReply();
			reply.setPerformative(ACLMessage.INFORM);
			try {
				agent.getContentManager().fillContent(reply, new ProducerRegisteredPredicate());
			} catch (Exception ex) {
				throw new IllegalStateException(ex);
			}			

			return reply;			
		}
	}
	
	

	/**
	 * 
	 * 
	 * Receive the order for Clearing the current auction.
	 * 
	 * 
	 */
	private class ReceiveClearAuctionBehaviour extends AchieveREResponder {
		
		/**
		 * Constructor.
		 */
		public ReceiveClearAuctionBehaviour(MessageTemplate mt) {
			super(agent, mt);
			return;
		}
		
		
		/**
		 * Process the message CallForProducing.
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
//			ClearAuctionAction action;
//			try {
//				action = (ClearAuctionAction)agent.getContentManager().extractContent(request);
//			} catch (Exception ex) {
//				getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
//				throw new IllegalStateException();				
//			}
			
			if (agent.getTracker() != null) {
				agent.getTracker().clearAuctionReceived();
			}
			
			
			// Init a behaviour that is continuously checking if the Clearing process has finished.
			agent.addBehaviour(new CheckClearingFinalizationBehaviour(request));			
			
			// Check the role that the broker takes on the clearing process.
			if (wd.role == AuctionAgentRole.CONSUMER) {
				wd.clearingFinishedAsProducer = true;
				wd.clearingFinishedAsConsumer = (wd.nExpectedProducersOffers == 0)? true : false;
				return null; // If this agent is only participating as consumer, there is nothing more to do.
			} else if (wd.role == AuctionAgentRole.PRODUCER) {
				wd.clearingFinishedAsConsumer = true;
				wd.clearingFinishedAsProducer = false;
			} else if (wd.role == AuctionAgentRole.PROSUMER){
				wd.clearingFinishedAsProducer = false;
				wd.clearingFinishedAsConsumer = (wd.nExpectedProducersOffers == 0)? true : false;				
			} else {
				getLogger().log(Level.SEVERE, "Wrong call for clearing the auction!!! This agent is not participating in the auction.");
				throw new IllegalStateException();
			}
			

			// Only Producers and Prosumers reach this point.
				
			// Construct a list with all the consumers that have sent a call for demand to this producer.
			Set<AID> consumers = new HashSet<AID>();
			for (int nBlock = 0; nBlock < wd.blocks.length; nBlock++) {
				if (wd.blocks[nBlock].role == AuctionAgentRole.PRODUCER) {
					for (BoardItem item : wd.blocks[nBlock].consumers) {
						consumers.add(item.aid);
					}
				}
			}
			consumers.remove(agent.getAID());
			wd.nExpectedOfferscConfirmations = consumers.size();
			
			// Calculate the offers that are going to be sent. The offers may be sent to a subset of the consumers.
			if (wd.nExpectedOfferscConfirmations > 0) {
				ProductionAssigner assigner = InjectionUtils.injector().getInstance(ProductionAssigner.class);
				Map<AID, List<OfferBlockConcept>> offers = assigner.assign(wd, OVERBOOKING_FACTOR);
				for (AID consumerAID : consumers) {
					List<OfferBlockConcept> consumerOffers = offers.get(consumerAID);
					SendRequestBehaviour sendRequest = new SendRequestBehaviour(
							agent,
							new AID[] {consumerAID},
							new ACLMessage(ACLMessage.REQUEST),
							new RegisterOffersAction(consumerOffers, false));
					sendRequest.registerHandleAllResultNotifications(
							new ReceiveAuctionOfferConfirmation(sendRequest.ALL_RESULT_NOTIFICATIONS_KEY));
					agent.addBehaviour(sendRequest);					

				}
			} else {
				wd.clearingFinishedAsProducer = true;
			}
			
			return null;
		}

		
		
		/**
		 * 
		 * 
		 * Send to the ASPEM agent the message that informs about the finalization of
		 * the Clearing process.
		 * 
		 *
		 */
		private class CheckClearingFinalizationBehaviour extends Behaviour {
			
			private final ACLMessage request;
			private boolean clearingIsDone = false;
			
			
			/**
			 * Constructor.
			 */
			public CheckClearingFinalizationBehaviour(ACLMessage request) {
				this.request = request;
				
				return;
			}
			
			
			/**
			 * Tell if the broker has finished the Clearing process.
			 */
			@Override
			public boolean done() {
				return clearingIsDone;
			}

			
			/**
			 * 
			 */
			@Override
			public void action() {
				
				if (!wd.clearingFinishedAsConsumer || !wd.clearingFinishedAsProducer) {
//					if (!wd.clearingFinishedAsConsumer && !wd.clearingFinishedAsProducer) {
//						auctionLogger.log("# Not both: " + agent.getLocalName());
//					} else if (!wd.clearingFinishedAsConsumer) {
//						auctionLogger.log("# Not consumer: " + agent.getLocalName());
//					} else if (!wd.clearingFinishedAsProducer) {
//						auctionLogger.log("# Not producer: " + agent.getLocalName());
//					}
					return;
				}
				
				ACLMessage reply = request.createReply();
				reply.setPerformative(ACLMessage.INFORM);
				try {
					agent.getContentManager().fillContent(reply, new ResultClearAuctionPredicate());
				} catch (Exception ex) {
					getLogger().log(Level.SEVERE, "Failed to fill the content of the message.");
					throw new IllegalStateException();
				}
				
				agent.send(reply);
				
				clearingIsDone = true;
				return;
			}
		}
		
		
		
		/**
		 * 
		 * 
		 * Receive the confirmations of the offers sent to the consumers.
		 * 
		 * 
		 */
		private class ReceiveAuctionOfferConfirmation extends OneShotBehaviour {
			
			private final String notificationsKey;
			
			
			/**
			 * Constructor.
			 */
			public ReceiveAuctionOfferConfirmation(String notificationsKey) {
				this.notificationsKey = notificationsKey;
				return;
			}

			
			/**
			 * Check the responses from the producers.
			 */
			@Override
			public void action() {
				
				// All confirmations to the production offers have been received.
				
				// Collect all the offers.
				@SuppressWarnings("unchecked")
				Vector<ACLMessage> resultMessages =
						(Vector<ACLMessage>)this.getDataStore().get(this.notificationsKey);
				

				BlockDescription[] blocks = wd.blocks;
				for (ACLMessage msg : resultMessages) {
					OffersConfirmationPredicate predicate;
					try {
						predicate = ((OffersConfirmationPredicate)agent.getContentManager().extractContent(msg));
					} catch (Exception ex) {
						getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
						throw new IllegalStateException();
					}
					
					List<OfferConfirmationConcept> confirms = predicate.getOffersConfirmations();
					if (confirms != null) {
						for (OfferConfirmationConcept confirm : confirms) {
							blocks[confirm.getIdxBlock()].demandedOverOffer += confirm.getAmount();
						}						
					}
				}
				

				if (--wd.nExpectedOfferscConfirmations > 0) {
					return;
				}
				

				// Remove the Producer role from all the blocks for which no offer has been accepted.
//				for (int idxBlock = 0; idxBlock < blocks.length; idxBlock++) {
//					BlockDescription block = blocks[idxBlock];
//					if ((block.role == AuctionAgentRole.PRODUCER) && (blocks[idxBlock].demandedOverOffer == 0)) {
//						block.clear();
//					}
//				}
				
				wd.clearingFinishedAsProducer = true;
				
				return;
			}
		}
	}
	
	
	
	/**
	 * 
	 * 
	 * Collects the offers sent by the producers in response to the call
	 * for demands. When the last offer is received, it is activated the process
	 * to determine which offers are accepted.
	 * 
	 * 
	 */
	private class ReceiveProductionOffersBehaviour extends AchieveREResponder {
		
		
		private Map<AID, String> producers = new HashMap<AID, String>();

		
		/**
		 * Constructor.
		 */
		public ReceiveProductionOffersBehaviour(MessageTemplate mt) {
			super(agent, mt);
			return;
		}
		
		
		/**
		 * Collect the offers.
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			RegisterOffersAction action;
			try {
				action = (RegisterOffersAction)agent.getContentManager().extractContent(request);
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
				throw new IllegalStateException();				
			}
			
			
			AID producerAID = request.getSender();
			BlockDescription[] blocks = wd.blocks;
			if (action.getOfferBlocks() != null) {
				for (OfferBlockConcept offerBlock : action.getOfferBlocks()) {
					blocks[offerBlock.getIdx()].receivedOffers.add(
							new AuctionOffer(producerAID, offerBlock.getOffers(), false));
				}
			}
			producers.put(producerAID, request.getReplyWith() + ";" + request.getConversationId());
			
			if (--wd.nExpectedProducersOffers > 0) {
				return null;
			}
			
			// The offers from all producers have been received.
			OffersSolver solver = InjectionUtils.injector().getInstance(OffersSolver.class);
			Map<AID, List<AcceptedOffer>> acceptanceMap = solver.solve(wd);
            
			
			// Send to each producer the confirmation of its offers.
            final String platformCodec = agent.getPlatform().getCodec().getName();
            final String platformOntology = agent.getPlatform().getOntology().getName();
            ContentManager cm = agent.getContentManager();
            Set<Integer> idxAuctionedBlocks = new HashSet<Integer>();
            for (Map.Entry<AID, String> producer: producers.entrySet()) {
            	ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        		msg.setLanguage(platformCodec);
        		msg.setOntology(platformOntology);
        		msg.addReceiver(producer.getKey());
        		String[] tokens = producer.getValue().split(";");
        		msg.setInReplyTo(tokens[0]);
        		msg.setConversationId(tokens[1]);
        		
        		OffersConfirmationPredicate predicate = new OffersConfirmationPredicate();
        		List<AcceptedOffer> acceptedOffers = acceptanceMap.get(producer.getKey());
        		if (acceptedOffers != null) {
        			List<OfferConfirmationConcept> confirmations = new ArrayList<OfferConfirmationConcept>();
        			for (AcceptedOffer acceptedOffer : acceptedOffers) {
        				confirmations.add(new OfferConfirmationConcept(acceptedOffer.idxBlock, acceptedOffer.amount));
        				idxAuctionedBlocks.add(acceptedOffer.idxBlock);
        				BlockDescription block = wd.blocks[acceptedOffer.idxBlock];
        				block.demandCovered += acceptedOffer.amount; // for the final report.
        				if (block.coveredDemandItems == null) {
        					block.coveredDemandItems = new ArrayList<CoveredDemandItem>();
        				}
        				block.coveredDemandItems.add(
        						new CoveredDemandItem(acceptedOffer.amount, acceptedOffer.price));
        			}
        			predicate.setOffersConfirmations(confirmations);
        		}
        		
        		try {
        			cm.fillContent(msg, predicate);
        		} catch (Exception ex) {
        			getLogger().log(Level.SEVERE, "Failed to fill the content of the message.");
        			throw new IllegalStateException();
        		}
        		
        		agent.send(msg);
            }	
            

            // For all the blocks for which no offers have been accepted, the total amount of offered capacity is calculated.
            // This capacity helps to identify how much energy left to hold the auction.
            for (int idxBlock = 0; idxBlock < blocks.length; idxBlock++) {
            	BlockDescription block = blocks[idxBlock];
            	if ((block.role == AuctionAgentRole.CONSUMER) && (block.demandCovered == 0)) {
            		for (AuctionOffer offer : block.receivedOffers) {
            			for (OfferLevelConcept levelOffer : offer.levelOffers) {
            				block.demandCovered += levelOffer.getAmount();
            			}
            		}
            	}
            }
				
            
            // Tell that the Clearing process has finished as Consumer.
			wd.clearingFinishedAsConsumer = true;
			
			return null;
		}
	}
	
	
	
	/**
	 * 
	 * 
	 * Receive a message indicating that the auction process has been aborted.
	 * 
	 * 
	 */
	private class ReceiveAbortAuctionBehaviour extends AchieveREResponder {
		
		/**
		 * Constructor.
		 */
		public ReceiveAbortAuctionBehaviour(MessageTemplate mt) {
			super(agent, mt);
		}
		
		
		/**
		 * Process the finalization of the Auction. Blocks for which the load has not been bargained have
		 * to send the DistributeEvent.
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			
			if (agent.getTracker() != null) {
				agent.getTracker().abortAuctionReceived();
			}
			
			// Clear and reset the working data.
			if (wd != null) {
				wd.clear();
			}
			
			// Reply to the ASPEM agent.
			ACLMessage reply = request.createReply();
			reply.setPerformative(ACLMessage.INFORM);
			try {
				agent.getContentManager().fillContent(reply, new ResultAbortAuctionPredicate());
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to fill the content of the message.");
				throw new IllegalStateException();
			}
			
			return reply;
		}
		
	}
	
	

	
	/**
	 * 
	 * 
	 * Receive a message indicating that the auction is over.
	 * 
	 * 
	 */
	private class ReceiveFinishAuctionBehaviour extends AchieveREResponder {
		
		private final static float NO_LEVEL = -1;

		/**
		 * Constructor.
		 */
		public ReceiveFinishAuctionBehaviour(MessageTemplate mt) {
			super(agent, mt);
		}
		
		
		/**
		 * Process the finalization of the Auction. Blocks for which the load has not been bargained have
		 * to send the DistributeEvent.
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			FinishAuctionAction action;
			try {
				action = (FinishAuctionAction)agent.getContentManager().extractContent(request);
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
				throw new IllegalStateException();				
			}
			
			// Iterate over the blocks and send a distribute event for all of them that have not participated
			// in the auction.
			List<AsdrEvent> generatedEvents = generatedSignalEvents(action.getEvent());
			
			// Send the generated events to the local agent.
			AgentTracker tracker = agent.getTracker();
			if (generatedEvents.size() > 0) {
				final long modelTimestamp = action.getModelTimestamp();
				EasyDistributeEvent easy = agent.getDistributeEvent();
				easy.updateEventsStatus(modelTimestamp);
				for (AsdrEvent newEvent : generatedEvents) {
					agent.getDistributeEvent().addEvent(newEvent, modelTimestamp);
					trackDistributeEvent(newEvent);
				}
				agent.getSimpleXmpp().send(easy.toString());
			} else {
				agent.getSimpleXmpp().send(SimpleXmpp.EMPTY_MESSAGE);
			}
			
			if (tracker != null) {
				BlockDescription[] blocks = wd.blocks;
				for (int nBlock = 0; nBlock < blocks.length; nBlock++) {
					BlockDescription block = blocks[nBlock];
					if (block.role == AuctionAgentRole.CONSUMER) {
						tracker.auctionDemandCovered(nBlock, block.demandCovered);
					} else if (block.role == AuctionAgentRole.PRODUCER) {
						tracker.auctionOfferAccepted(nBlock, block.demandedOverOffer);
					}
				}
			}

			// Log the result of this agent's participation.
			auctionLogger.logBrokerResult(
					agent.getLocalName(), wd.auctionFacilitatorAID.getLocalName(),
					wd.blocks, action.getEvent().getStart(), generatedEvents);			
			
			// Clear and rest the working data.
			wd.clear();
			
			
			// Reply to the ASPEM agent.
			ACLMessage reply = request.createReply();
			reply.setPerformative(ACLMessage.INFORM);
			try {
				agent.getContentManager().fillContent(reply, new ResultFinishAuctionPredicate());
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to fill the content of the message.");
				throw new IllegalStateException();
			}
			
			return reply;
		}
		
		
		/**
		 * Once the auction has finished, this method determines the list of events
		 * to be sent to local agent. 
		 */
		private List<AsdrEvent> generatedSignalEvents(EventConcept event) {
			
			BlockDescription[] blocks = wd.blocks;
			
			final long NO_MARK = -1;
 			long currentMillis = event.getStart().getTime();
 			long beginMillis = NO_MARK;
 			long firstIntervalMillis = -1;
 			
			final long notifDuration = event.getNotificationDuration();
			final long priority = event.getPriority();
			
			final float NO_LEVEL = -1;
			
			ArrayList<AsdrEvent> generatedEvents = new ArrayList<AsdrEvent>();
			ArrayList<AsdrInterval> asdrIntervals = null                                                                                                                                   ;
			float[] blocksLevels = getBlocksLevels(event);
			float currentLevel = NO_LEVEL;
			for (int nBlock = 0; nBlock < blocks.length; nBlock++, currentMillis += BLOCK_MILLIS) {
				
				final float level = blocksLevels[nBlock];
				if (level == NO_LEVEL) {
					if (beginMillis != NO_MARK) {
						asdrIntervals.add(new AsdrInterval((currentMillis - beginMillis) / 1000, currentLevel));
						generatedEvents.add(
								new AsdrEvent(
										AsdrSignalTypeEnum.LEVEL, new Date(firstIntervalMillis), notifDuration, priority, asdrIntervals));
					}
					
					beginMillis = NO_MARK;
					currentLevel = NO_LEVEL;
					
				} else {
					if (currentLevel == NO_LEVEL) {
						asdrIntervals = new ArrayList<AsdrInterval>();
						beginMillis = currentMillis;
						firstIntervalMillis = beginMillis;
						currentLevel = level;
					} else if (level != currentLevel) {
						asdrIntervals.add(new AsdrInterval((currentMillis - beginMillis) / 1000, currentLevel));
						beginMillis = currentMillis;
						currentLevel = level;
					}
				}
			}
			
			if (beginMillis != NO_MARK) {
				asdrIntervals.add(new AsdrInterval((currentMillis - beginMillis) / 1000, currentLevel));
				generatedEvents.add(
						new AsdrEvent(
								AsdrSignalTypeEnum.LEVEL, new Date(firstIntervalMillis), notifDuration, priority, asdrIntervals));
			}
			
			return generatedEvents;
		}
		
		
		/**
		 * After the auction, this method determines the level that corresponds to each
		 * block. The level of the producing blocks depends on the amount bargained. The consuming
		 * blocks do not have associated a level, as they are compensated by producing blocks.
		 * The blocks without role have associated the level of the interval to which they
		 * belong.
		 */
		private float[] getBlocksLevels(EventConcept event) {
			
 			long currentMillis = event.getStart().getTime();
 			Iterator<IntervalConcept> itIntervals = event.getIntervals().iterator();
 			IntervalConcept currentInterval = itIntervals.next();
 			long endCurrentInterval = currentMillis + (currentInterval.getDuration() * 1000);

 			BlockDescription[] blocks = wd.blocks;
			final int N_BLOCKS = blocks.length;
			float[] levels = new float[N_BLOCKS];
			for (int nBlock = 0; nBlock < blocks.length; nBlock++, currentMillis += BLOCK_MILLIS) {
				BlockDescription block = blocks[nBlock];
				
				if (currentMillis == endCurrentInterval) {
					currentInterval = itIntervals.next();
					endCurrentInterval = currentMillis + (currentInterval.getDuration() * 1000);
				}
				
				if ((block.role == AuctionAgentRole.CONSUMER) && (block.demandCovered >= block.demand)) {
					levels[nBlock] = NO_LEVEL;
				} else if ((block.role == AuctionAgentRole.PRODUCER) && (block.demandedOverOffer > 0)) {
					levels[nBlock] = getProducerPayload(block, currentInterval.getPayload());
				} else {
					levels[nBlock] = currentInterval.getPayload();
				}
			}
			
			return levels;
		}
		
		/**
		 * Return the level that must assume a Producing block in order to cover
		 * the amount of load offered.
		 */
		private float getProducerPayload(BlockDescription block, final float currentLevelPayload) {
			final float[] offers = block.offerAmount;
			float totalAmount = EnergyAgentsUtils.round(offers[0] + (offers[0] * OVERBOOKING_FACTOR), 2);
			int nLevel = 1;
			final float demandedRounded = EnergyAgentsUtils.round(block.demandedOverOffer, 2);
			while ((totalAmount < demandedRounded) && (nLevel < offers.length))  {
				try {
					totalAmount += EnergyAgentsUtils.round(
							offers[nLevel] + (offers[nLevel++] * OVERBOOKING_FACTOR), 2);
				} catch (RuntimeException rex) {
					throw rex;
					//System.out.println("ta: " + totalAmount + " ,,, doo: " + block.demandedOverOffer);
				}
			}
			
			return currentLevelPayload + nLevel;
		}
	}	
}



/**
 * 
 * 
 * Global role that assumes the agent during the auction.
 *  
 *
 */
enum AuctionAgentRole {NONE, CONSUMER, PRODUCER, PROSUMER};



/**
 * 
 * 
 * Structure used by the agents in order to resolve the auctions. 
 * 
 *
 */
class AuctionWorkingData {
	AID auctionFacilitatorAID;
	int auctionId;
	BlockDescription[] blocks;
	AuctionAgentRole role;
	int nExpectedProducersOffers;
	int nExpectedOfferscConfirmations;
	boolean clearingFinishedAsConsumer;
	boolean clearingFinishedAsProducer;
	String aspemName;
	String agentName;
	
	/**
	 * Constructor.
	 */
	public AuctionWorkingData() {
		clear();
	}
	
	
	/**
	 * Clear and reset the member variables.
	 */
	public void clear() {
		auctionFacilitatorAID = null;
		auctionId = -1;
		blocks = null;
		role = AuctionAgentRole.NONE;
		nExpectedProducersOffers = 0;
		nExpectedOfferscConfirmations = 0;
		clearingFinishedAsConsumer = false;
		clearingFinishedAsProducer = false;
		aspemName = null;
		agentName = null;
		
		return;
	}
}


/*
 * 
 * 
 * Working data of a broker agent on a Block of time.
 * 
 * 
 */
class BlockDescription {
	AuctionAgentRole role = AuctionAgentRole.NONE;
	boolean combined = false;
	int eventLevel;
	int cronLevel; // candiate to be removed.
	float demand;
	float demandCovered;
	float[] offerAmount;
	float[] offerPrice;
	float demandedOverOffer;
	List<AuctionOffer> receivedOffers = new ArrayList<AuctionOffer>();
	List<CoveredDemandItem> coveredDemandItems = null;
	List<BoardItem> consumers = null;
	

	
	public void clear() {
		role = AuctionAgentRole.NONE;
		offerAmount = null;
		offerPrice = null;
		demandedOverOffer = 0;
		receivedOffers.clear();
		coveredDemandItems = null;
		consumers = null;
		
		return;
	}
}


/**
 * 
 * 
 * Offer of production. It contains a value for each level of production.
 * 
 *
 */
class AuctionOffer {
	final boolean combined;
	final AID producer;
	final List<OfferLevelConcept> levelOffers;


	
	/**
	 * Constructor.
	 */
	public AuctionOffer(AID producer, List<OfferLevelConcept> levelOffers, boolean combined) {
		this.producer = producer;
		this.combined = combined;
		this.levelOffers = levelOffers;
		return;
	}
}


/**
 * 
 * 
 * Description of an offer that has been preliminarily accepted by the consumer.
 * 
 *
 */
class AcceptedOffer {
	final int idxBlock;
	final AID producer;
	final float amount;
	final float price;
	
	/**
	 * Constructor.
	 */
	public AcceptedOffer(int idxBlock, AID producer, float amount, float price) {
		this.idxBlock = idxBlock;
		this.producer = producer;
		this.amount = amount;
		this.price = price;
		return;
	}
}


/**
 * 
 * 
 * Describe part (an item) of the covered demand of a block.
 * The description contains the amount and the price.
 * 
 * 
 */
class CoveredDemandItem {
	final float amount;
	final float price;
	
	/**
	 * Constructor.
	 */
	public CoveredDemandItem(float amount, float price) {
		this.amount = amount;
		this.price = price;
		return;
	}
}