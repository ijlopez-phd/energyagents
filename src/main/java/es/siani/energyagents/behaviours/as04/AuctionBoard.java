package es.siani.energyagents.behaviours.as04;

import jade.core.AID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.siani.energyagents.ontology.as04.DemandBlockConcept;

public abstract class AuctionBoard {
	
	protected Map<String, List<List<BoardItem>>> data = new HashMap<String, List<List<BoardItem>>>();
	
	protected Map<String, List<Integer>> producers = new HashMap<String, List<Integer>>();
	
	
	/**
	 * Initialize the internal state of the service.
	 * This method should be called when a fresh start is required.
	 */
	public void init(String aspemName, int nBlocks) {
		List<List<BoardItem>> blocks = new ArrayList<List<BoardItem>>();
		List<Integer> blockProducers = new ArrayList<Integer>();
		for (int n = 0; n < nBlocks; n++) {
			blocks.add(new ArrayList<BoardItem>());
			blockProducers.add(0);
		}
		
		data.put(aspemName, blocks);
		producers.put(aspemName, blockProducers);
		
		return;
	}
	
	
	/**
	 * Clear the internal state of the service.
	 */
	public void clear(String aspemName) {
		data.put(aspemName, null);
		producers.put(aspemName, null);
		return;
	}
	
	
	/**
	 * Add the blocks for which a specific node behaves as a consumer.
	 */
	public void addBlocks(String aspemName, AID node, List<DemandBlockConcept> blocksDemand) {
		List<List<BoardItem>> blocks = data.get(aspemName);
		for (DemandBlockConcept blockDemand : blocksDemand) {
			blocks.get(blockDemand.getIdx()).add(
					new BoardItem(node, blockDemand.getLoad(), blockDemand.getStartPrice()));
		}
		
		return;
	}
	
	
	/**
	 * Remove all entries of a specific agent.
	 */
	public synchronized void remove(String aspemName, AID node) {
		final BoardItem toRemove = new BoardItem(node, 0f, 0f);
		for (List<BoardItem> blockList : data.get(aspemName)) {
			blockList.remove(toRemove);
		}
		
		return;
	}
	
	
	/**
	 * Increment the number of producers for a block.
	 */
	public void addProducer(String aspemName, int nBlock, float capacity) {
		List<Integer> blocksCount = producers.get(aspemName);
		blocksCount.set(nBlock, (int)(blocksCount.get(nBlock) + 1));
		return;
	}
	
	/**
	 * Add a new producer.
	 */
	public void addProducer(String aspemName, int idProducer, int nBlock, float[] capacity, float[] price) {
		return;
	}
	
	
	/**
	 * Close the stage in which new producers and consumers are added to the board.
	 */
	public void closeRegistering(String aspemName) {
		return;
	}
	
	
	/**
	 * Close the auction.
	 */
	public void closeAuction() {
		return;
	}
	

	/**
	 * Get the list of consumers for a specific block.
	 */	
	public abstract List<BoardItem> getConsumersList(String aspemName, int nBlock, int nProducer, float[] capacity, float[] price);	

}

/**
 * 
 * 
 * Item of the ConsumersBoardService.
 * 
 *
 */
class BoardItem implements Comparable<BoardItem> {

	int idx;
	AID aid;
	float amount;
	float startPrice;
	List<Integer> producers;
	int previousSurplusProducers;
	float totalCapacity;
	
	
	/**
	 * Constructor.
	 */
	public BoardItem(AID aid, float amount, float startPrice) {
		this.aid = aid;
		this.amount = amount;
		this.startPrice = startPrice;
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public BoardItem(int idx, AID aid, float amount, float startPrice) {
		this(aid, amount, startPrice);
		this.idx = idx;
		return;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((this.aid == null) ? 0 : this.aid.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof BoardItem)) {
			return false;
		}
		BoardItem other = (BoardItem) obj;
		if (this.aid == null) {
			if (other.aid != null) {
				return false;
			}
		} else if (!this.aid.equals(other.aid)) {
			return false;
		}
		return true;
	}


	
	/**
	 * Compare two instances of BoardItem according to the size of the demand.
	 */
	@Override
	public int compareTo(BoardItem other) {
		if (this.amount < other.amount) {
			return -1;
		} else if (this.amount > other.amount) {
			return 1;
		}
		return 0;
	}
}
