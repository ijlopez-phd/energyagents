package es.siani.energyagents.behaviours.as04;

import jade.core.AID;

import java.util.List;
import java.util.Map;

public interface OffersSolver {
	
	/**
	 * Determine which production offers are accepted and which are denied.
	 */
	public Map<AID, List<AcceptedOffer>> solve(AuctionWorkingData workingData);

}
