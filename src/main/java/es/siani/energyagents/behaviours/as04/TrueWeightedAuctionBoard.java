package es.siani.energyagents.behaviours.as04;

import jade.core.AID;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.google.inject.Injector;

import es.siani.energyagents.config.Configuration;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.ontology.as04.DemandBlockConcept;

public class TrueWeightedAuctionBoard extends AuctionBoard {
	
	private float[] capacityMean;
	private float[] demandMean;
	private int[] nMinProducersPerConsumerGroup;
	private int[] nConsumersGroups;	
	private int[] nConsumersPerGroup;
	private float[] delator;
	
	private List<ConsumerGroup[]> blocksConsumersGroups;
	

	

	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#init(String, int)
	 */	
	@Override
	public void init(String aspemName, int nBlocks) {
		super.init(aspemName, nBlocks);
		
		capacityMean = new float[nBlocks];
		demandMean = new float[nBlocks];
		nMinProducersPerConsumerGroup = new int[nBlocks];
		nConsumersGroups = new int[nBlocks];
		nConsumersPerGroup = new int[nBlocks];		
		delator = new float[nBlocks];
		blocksConsumersGroups = new ArrayList<ConsumerGroup[]>();		
		
		return;
	}
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#clear(String)
	 */	
	@Override
	public void clear(String aspemName) {
		super.clear(aspemName);
		capacityMean = null;
		demandMean = null;
		nMinProducersPerConsumerGroup = null;
		nConsumersPerGroup = null;
		blocksConsumersGroups = null;
		
		return;
	}
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#addBlocks(String, AID, List)
	 */	
	@Override
	public void addBlocks(String aspemName, AID node, List<DemandBlockConcept> blocksDemand) {
		List<List<BoardItem>> blocks = data.get(aspemName);
		for (DemandBlockConcept blockDemand : blocksDemand) {
			final int idx = blockDemand.getIdx();
			final float amount = blockDemand.getLoad();
			blocks.get(idx).add(new BoardItem(node, amount, blockDemand.getStartPrice()));
			demandMean[blockDemand.getIdx()] += amount;
		}
		
		return;
	}
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#addProducer(String, int, float)
	 */	
	@Override
	public void addProducer(String aspemName, int nBlock, float capacity) {
		super.addProducer(aspemName, nBlock, capacity);
		capacityMean[nBlock] += capacity;
		
		return;
	}
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#closedRegistering()
	 */
	@Override
	public void closeRegistering(String aspemName) {
		super.closeRegistering(aspemName);
		
		List<List<BoardItem>> blocks = data.get(aspemName);
		final int N_BLOCKS = blocks.size();
		for (int nBlock = 0; nBlock < N_BLOCKS; nBlock++) {
			demandMean[nBlock] = demandMean[nBlock] / (float)blocks.get(nBlock).size();
		}
		
		List<Integer> aspemProducers = producers.get(aspemName);
		for (int nBlock = 0; nBlock < N_BLOCKS; nBlock++) {
			final int nProducers = aspemProducers.get(nBlock);
			capacityMean[nBlock] = capacityMean[nBlock] / (float)nProducers;			
		}
		
		
		Injector injector = InjectionUtils.injector();
		AuctionLogger alogger = injector.getInstance(AuctionLogger.class);
		int constantF = Integer.parseInt(injector.getInstance(Configuration.class).get(Configuration.PROP_AUCTION_CF));
		for (int nBlock = 0; nBlock < N_BLOCKS; nBlock++) {
			
			final int nProducers = aspemProducers.get(nBlock);
			final int nConsumers = blocks.get(nBlock).size();
			
			nConsumersGroups[nBlock] = nConsumers;
			nConsumersPerGroup[nBlock] = 1;			
			
			nMinProducersPerConsumerGroup[nBlock] = (int)Math.ceil(demandMean[nBlock] / capacityMean[nBlock]) + constantF;
			delator[nBlock] = (nProducers / (float)nMinProducersPerConsumerGroup[nBlock]) / nConsumers;
			if  ((int)delator[nBlock] > 1) {
				nMinProducersPerConsumerGroup[nBlock] = nProducers / nConsumers;
			} else if ((int)delator[nBlock] == 0) {
				nConsumersGroups[nBlock] = (int)(nProducers / nMinProducersPerConsumerGroup[nBlock]);
				if (nConsumersGroups[nBlock] == 0) {
					nConsumersGroups[nBlock] = 1;
				}
				nConsumersPerGroup[nBlock] = (int)Math.ceil(nConsumers / (float)nConsumersGroups[nBlock]);
				//nConsumersGroups[nBlock] = nConsumers / nConsumersPerGroup[nBlock]; 
			}
			
			alogger.log("# "
					+ "     dt: " + (int)delator[nBlock]
					+ "     nC: " + nConsumers
					+ "     nP: " + nProducers
					+ "     nBpS: " + nMinProducersPerConsumerGroup[nBlock]
					+ "     nG: " + nConsumersGroups[nBlock]
					+ "     nSpG: " + nConsumersPerGroup[nBlock]
					+ "     CF: " + constantF);
		}
		
		
		// Sort the groups of consumers by the size of their demand.
		ConsumerGroupComparatorDesc consumerGroupComparator = new ConsumerGroupComparatorDesc();
		for (int nBlock = 0; nBlock < N_BLOCKS; nBlock++) {
			if ((int)delator[nBlock] == 0) {
				blocksConsumersGroups.add(new ConsumerGroup[0]);
				continue;
			}
			
			List<BoardItem> items = blocks.get(nBlock);
			final int nConsumers = items.size();
			final int nCpG = nConsumersPerGroup[nBlock];
 
			ConsumerGroup[] consumersGroups = new ConsumerGroup[nConsumersGroups[nBlock]];
			ConsumerGroup cg = null;
			for (int nItem = 0, idxGroup = 0; nItem < nConsumers; nItem++) {
				if ((nItem % nCpG) == 0) {
					cg = new ConsumerGroup(idxGroup);
					consumersGroups[idxGroup++] = cg;
				}
				
				cg.demand += items.get(nItem).amount;
			}
			
			Arrays.sort(consumersGroups, consumerGroupComparator);
			blocksConsumersGroups.add(consumersGroups);
		}
		
		return;
	}
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as04.AuctionBoard#getConsumersList(String, int, int, float)
	 */	
	@Override
	public List<BoardItem> getConsumersList(
			String aspemName, final int nBlock,
			final int idProducer, final float[] capacity, final float[] price) {
		
		ArrayList<BoardItem> selectedItems = new ArrayList<BoardItem>();
		
		List<BoardItem> items = data.get(aspemName).get(nBlock);		
		final int nConsumers = items.size();
		int startIdx;
		if (delator[nBlock] >= 1) {
			int idxGroup = idProducer / nMinProducersPerConsumerGroup[nBlock];
			startIdx = idxGroup * nConsumersPerGroup[nBlock];			
			if (startIdx >= nConsumers) {
				// this is a producer that hadn't been associated to a group previously.
				// It is associated to a high-demanding group.
				int nDemandingGroup = blocksConsumersGroups.get(nBlock)[(startIdx - nConsumers)].idx; 
				startIdx = nDemandingGroup * nConsumersPerGroup[nBlock];
			}

			
		} else {
			final int nProducers = producers.get(aspemName).get(nBlock);
			int nProducersPerGroup = (int)Math.floor(nProducers / (float)nConsumersGroups[nBlock]);
			if ((nProducersPerGroup < nMinProducersPerConsumerGroup[nBlock]) || (nConsumersGroups[nBlock] == 1)) {
//				AuctionLogger alogger = InjectionUtils.injector().getInstance(AuctionLogger.class);
//				alogger.log("# TODOS LOS ITEMS");
				return items;
			}
			
			int idxGroup = (idProducer / nMinProducersPerConsumerGroup[nBlock]) % nConsumersGroups[nBlock];
			startIdx = idxGroup * nConsumersPerGroup[nBlock];
		}
		
		float totalCapacity = 0;
		for (float levelCapacity : capacity) {
			totalCapacity += levelCapacity;
		}

		if (delator[nBlock] >= 1) {
			float allocatedCapacity = 0;
			for (int idx = startIdx; (allocatedCapacity < totalCapacity) && (idx < nConsumers); idx++) {
				BoardItem item = items.get(idx);
				selectedItems.add(item);
				allocatedCapacity += item.amount;
			}

			if (allocatedCapacity < totalCapacity) {
				for (int idx = 0; (allocatedCapacity < totalCapacity) && (idx < startIdx); idx++) {
					BoardItem item = items.get(idx);
					selectedItems.add(item);
					allocatedCapacity += item.amount;
				}
			}
		} else {
			float allocatedCapacity = 0;
			final int nSpG = nConsumersPerGroup[nBlock];
			for (int idx = startIdx, idxSeller = 0; (idx < nConsumers) && ((idxSeller < nSpG) || (allocatedCapacity < totalCapacity)); idx++, idxSeller++) {
				BoardItem item = items.get(idx);
				selectedItems.add(item);
				allocatedCapacity += item.amount;
			}

			if (allocatedCapacity < totalCapacity) {
				for (int idx = 0; (allocatedCapacity < totalCapacity) && (idx < startIdx); idx++) {
					BoardItem item = items.get(idx);
					selectedItems.add(item);
					allocatedCapacity += item.amount;
				}
			}			
		}
		
		return selectedItems;		
	}
	
	
	/**
	 * 
	 * 
	 * Group of consumers. It contains the index of group within the array of groups and
	 * the amount demanded by it.
	 * 
	 *
	 */
	class ConsumerGroup {
		
		final int idx;
		float demand;
		
		
		/**
		 * Constructor.
		 */
		public ConsumerGroup(int idx) {
			this.idx = idx;
			return;
		}
		
		
		/**
		 * Constructor.
		 */
		public ConsumerGroup(int idx, float demand) {
			this.idx = idx;			
			this.demand = demand;
			return;
		}
	}
	
	
	
	/**
	 * 
	 * 
	 * Comparator for sorting the class ConsumerGroup by the amount in descending order.
	 * 
	 *
	 */
	class ConsumerGroupComparatorDesc implements Comparator<ConsumerGroup> {

		@Override
		public int compare(ConsumerGroup o1, ConsumerGroup o2) {
			if (o1.demand > o2.demand) {
				return -1;
			} else if (o1.demand < o2.demand) {
				return 1;
			}
			
			return 0;
		}
		
	}
}



