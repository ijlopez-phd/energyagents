package es.siani.energyagents.behaviours.as04;

import jade.core.AID;

/**
 * 
 * 
 * Call for demand.
 * 
 * 
 */
class CallForDemand implements Comparable<CallForDemand> {
	
	private final AID sender;
	private final float demand;
	
	/**
	 * Constructor.
	 */
	public CallForDemand(AID sender, float demand) {
		this.sender = sender;
		this.demand = demand;
		return;
	}
	
	/**
	 * Get the AID of the agent that demands load.
	 */
	public AID getSender() {
		return this.sender;
	}
	
	/**
	 * Get the demand.
	 */
	public float getDemand() {
		return this.demand;
	}

	
	/**
	 * Compare two instances of CallForDemand according to the size of the demand.
	 */
	@Override
	public int compareTo(CallForDemand other) {
		if (this.demand < other.demand) {
			return -1;
		} else if (this.demand > other.demand) {
			return 1;
		}
		
		return 0;
	}
}