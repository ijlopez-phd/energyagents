package es.siani.energyagents.behaviours.as04;

import jade.core.AID;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import es.siani.energyagents.behaviours.as04.AuctionLogger;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.ontology.as04.OfferBlockConcept;

public class StartPriceProductionAssigner implements ProductionAssigner {
	
	
	private boolean flagUseRandomItems = false;
	
	
	/**
	 * Constructor.
	 */
	public StartPriceProductionAssigner() {
		AuctionLogger auclog = InjectionUtils.injector().getInstance(AuctionLogger.class);
		auclog.log("# StartPrice");
		return;
	}
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as03.ProductionAssigner#useRandomItems(boolean)
	 */
	public void useRandomItems(boolean flagUseRandomItems) {
		this.flagUseRandomItems = flagUseRandomItems;
		return;
	}		
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as03.ProductionAssigner#assign(AuctionWorkingData)
	 */	
	@Override
	public Map<AID, List<OfferBlockConcept>> assign(AuctionWorkingData wd, float overbookingFactor) {
		
		final Comparator<BoardItem> startPriceComparator = new BoardItemStartPriceComparator();
		
		Map<AID, List<OfferBlockConcept>> offersMap = new HashMap<AID, List<OfferBlockConcept>>();
		for (int nBlock = 0; nBlock < wd.blocks.length; nBlock++) {
			BlockDescription block = wd.blocks[nBlock];
			
			if (block.role != AuctionAgentRole.PRODUCER) {
				continue;
			}
			
			List<BoardItem> items = block.consumers;
			if (items.size() == 0) {
				continue;
			}
			
			// Production capacity for this block.
			float capacity = 0;
			final int N_LEVELS = block.offerAmount.length;
			for (int nLevel = 0; nLevel < N_LEVELS; nLevel++) {
				capacity += block.offerAmount[nLevel];
			}
			capacity += capacity * overbookingFactor;
			
			// Assign the capacity among the calls for demand.
			int currentLevel = 0;
			float remainderLevelCapacity = block.offerAmount[0];
			remainderLevelCapacity += remainderLevelCapacity * overbookingFactor;
			if (this.flagUseRandomItems) {
				Collections.shuffle(items);
			} else {
				Collections.sort(items, startPriceComparator);
			}
			Iterator<BoardItem> it = items.iterator();
			float totalOffered = 0;
			while ((totalOffered < capacity) && it.hasNext()) {
				BoardItem item = it.next();
				float toCover = item.amount;
				
				OfferBlockConcept offerBlock = new OfferBlockConcept(nBlock);
				while ((toCover > 0) && (totalOffered < capacity) && (currentLevel < N_LEVELS)) {
					float assigned =
							(remainderLevelCapacity >= toCover)? toCover : remainderLevelCapacity;
					
					offerBlock.addOffer(assigned, block.offerPrice[currentLevel]);
					totalOffered += assigned;
					toCover -= assigned;
					remainderLevelCapacity -= assigned;
					
					if (remainderLevelCapacity == 0) {
						currentLevel++;
						if (currentLevel < N_LEVELS) {
							remainderLevelCapacity = block.offerAmount[currentLevel];
							remainderLevelCapacity += remainderLevelCapacity * overbookingFactor;
						}
					}
				}
				
				List<OfferBlockConcept> brokerOffers = offersMap.get(item.aid);
				if (brokerOffers == null) {
					brokerOffers = new ArrayList<OfferBlockConcept>();
					offersMap.put(item.aid, brokerOffers);
				}
				brokerOffers.add(offerBlock);
			}
		}
		
		return offersMap;
	}
	
	
	/**
	 * 
	 * 
	 * Compare two instances of BoardItem according to their start-price in ascending order.
	 * 
	 *
	 */
	private class BoardItemStartPriceComparator implements Comparator<BoardItem> {
		@Override
		public int compare(BoardItem o1, BoardItem o2) {
			if (o1.startPrice < o2.startPrice) {
				return -1;
			} else if (o1.startPrice > o2.startPrice) {
				return 1;
			}

			return 0;
		}
	}

}
