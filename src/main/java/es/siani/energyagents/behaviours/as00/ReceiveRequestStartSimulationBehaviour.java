package es.siani.energyagents.behaviours.as00;

import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.ontology.as00.FinishSimulationAction;
import es.siani.energyagents.ontology.as00.PauseSimulationAction;
import es.siani.energyagents.ontology.as00.SimulationDescriptionConcept;
import es.siani.energyagents.ontology.as00.SimulationIdConcept;
import es.siani.energyagents.ontology.as00.SimulationStartedPredicate;
import es.siani.energyagents.ontology.as00.StartSimulationAction;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;

@SuppressWarnings("serial")
public class ReceiveRequestStartSimulationBehaviour extends AchieveREResponder {
	
	protected final EnergyAgent agent;
	
	
	/**
	 * Constructor.
	 */
	public ReceiveRequestStartSimulationBehaviour(EnergyAgent agent, MessageTemplate mt) {
		super(agent, mt);
		this.agent = agent;
		return;
	}
	
	
	/**
	 * Handles the request to start the simulation.
	 */
	@Override
	protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
		
		// Record this action in the tracker.
		if (this.agent.getTracker() != null) {
			this.agent.getTracker().simulationStarted();
		}
		
		// Extract the content of the message.
		StartSimulationAction startAction;
		try {
			startAction = ((StartSimulationAction)this.agent.getContentManager().extractContent(request));
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to extract the content of the message.");
			throw new IllegalStateException(ex);
		}
		
		// The agent starts to listen to the PauseSimulation and FinishedSimulation events.
		final SimulationDescriptionConcept sd = startAction.getSimulation();
		this.agent.addBehaviour(new ReceiveRequestPauseSimulationBehaviour(
				this.agent,
				new MessageTemplate(
						new SimulationRequestMatcher(this.agent, PauseSimulationAction.class)),
				sd.getProgram(),
				sd.getScenarioCode(),
				sd.getStoreName()));
		
		this.agent.addBehaviour(new ReceiveRequestFinishSimulationBehaviour(
				this.agent,
				new MessageTemplate(
						new SimulationRequestMatcher(this.agent, FinishSimulationAction.class))));
		
		
		// Builds the reply.
		ACLMessage replyMessage = request.createReply();
		replyMessage.setPerformative(ACLMessage.INFORM);
		try {
			this.agent.getContentManager().fillContent(
					replyMessage,
					new SimulationStartedPredicate(new SimulationIdConcept(startAction.getSimulation().getId())));
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to fill the content of the message.");
		}
		
		return replyMessage;
	}
	
	
	/**
	 * Logger.
	 */
	public static final Logger getLogger() {
		return Logger.getLogger(ReceiveRequestStartSimulationBehaviour.class.getName());
	}
	
}
