package es.siani.energyagents.behaviours.as00;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.agent.EnergyAgent;
import jade.content.ContentElement;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

@SuppressWarnings("serial")
public class SimulationRequestMatcher implements MessageTemplate.MatchExpression {
	
	protected final AID receiver;
	protected final EnergyAgent agent;
	protected final Class<? extends ContentElement> targetContentType;
	
	
	/**
	 * Constructor.
	 */
	public SimulationRequestMatcher(EnergyAgent agent, Class<? extends ContentElement> targetContentType) {
		this(agent, null, targetContentType);
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public SimulationRequestMatcher(EnergyAgent agent, AID receiver, Class<? extends ContentElement> targetContentType) {
		this.agent = agent;
		this.receiver = receiver;
		this.targetContentType = targetContentType;
		return;
	}
	
	
	/**
	 * Determines whether the message matches the criteria.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean match(ACLMessage msg) {
		final AgentsPlatform platform = this.agent.getPlatform();
		if ((msg.getLanguage() == null) || (!msg.getLanguage().equals(platform.getCodec().getName()))) {
			return false;
		} else if (!msg.getOntology().equals(platform.getOntology().getName())) {
			return false;
		} else if (!contains(msg.getAllReceiver(), this.agent.getAID())) {
			return false;
		} else if (msg.getPerformative() != ACLMessage.REQUEST) {
			return false;
		} else if ((this.receiver != null) && (!contains(msg.getAllReceiver(), this.receiver))) {
			return false;
		}
		
		ContentElement content;
		try {
			content = this.agent.getContentManager().extractContent(msg);
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
			throw new IllegalStateException(ex);
		}
		
		if (!targetContentType.isInstance(content)) {
			return false;
		} 
		
		return true;
	}
	
	
	/**
	 * Checks if a given AID is contained in a list of AID (specified by an iterator).
	 */
	boolean contains(Iterator<AID> allAIDs, AID targetAID) {
		while(allAIDs.hasNext()) {
			if (allAIDs.next().equals(targetAID)) {
				return true;
			}
		}

		return false;
	}
	
	
	/**
	 * Logger.
	 */
	private static Logger getLogger() {
		return Logger.getLogger(SimulationRequestMatcher.class.getName());
	}

}
