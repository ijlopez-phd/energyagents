package es.siani.energyagents.behaviours.as00;

import java.util.logging.Level;
import java.util.logging.Logger;

import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;
import es.siani.energyagents.agent.BrokerAgent;
import es.siani.energyagents.ontology.as00.CreatedEventPredicate;
import es.siani.energyagents.ontology.as00.DistributeEventAction;
import es.siani.energyagents.ontology.as00.EiResponseConcept;

@SuppressWarnings("serial")
public class BrokerProcessBasicBehaviour extends BrokerProcessBehaviour {
	
	
	/**
	 * Constructor.
	 */
	public BrokerProcessBasicBehaviour() {
		return;
	}
	
	
	/**
	 * Initializes the instance.
	 */
	public void init(BrokerAgent agent, DistributeEventAction content) {
		super.init(agent, content);
		return;
	}

	
	/**
	 * Add a behaviour that listens to DistributeEvents.
	 */
	@Override
	public void action() {
		this.agent.addBehaviour(new BrokerReceiveDistributeEventBehaviour(
				this.agent,
				new MessageTemplate(
						new SimulationRequestMatcher(this.agent, this.agent.getAID(), DistributeEventAction.class))));
	}
	
	
	/**
	 * Finish.
	 */
	@Override
	public boolean done() {
		return true;
	}	
	
	
	/**
	 * Inner class.
	 * Behaviours that sets the broker waiting for DistributeEventAction messages.
	 * This behaviours is added by the BrokerProcessBasicBehaviour.	 *
	 */
	private class BrokerReceiveDistributeEventBehaviour extends AchieveREResponder {
		
		protected final BrokerAgent agent;

		
		/**
		 * Constructor.
		 */
		public BrokerReceiveDistributeEventBehaviour(BrokerAgent agent, MessageTemplate mt) {
			super(agent, mt);
			this.agent = agent;
			return;
		}
		
		
		/**
		 * Process the DistributeEvent.
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			// Record this action in the tracker.
			if (this.agent.getTracker() != null) {
				this.agent.getTracker().distributeEventReceived();
			}
			
			// Extract the content of the message.
			DistributeEventAction eventAction;
			try {
				eventAction = (DistributeEventAction)(this.agent.getContentManager().extractContent(request));
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to extract the content of the message.");
				throw new IllegalStateException(ex);
			}			
			
			
			this.agent.getSimpleXmpp().send(eventAction.getDistributeEvent().getContent());
			
			// Build the reply (CreatedEvent).
			ACLMessage replyMessage = request.createReply();
			replyMessage.setPerformative(ACLMessage.INFORM);
			try {
				this.agent.getContentManager().fillContent(
						replyMessage,
						new CreatedEventPredicate(
								new EiResponseConcept(
										EiResponseConcept.CODE_OK,
										request.getConversationId())));
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to fill the content of the message.", ex);
			}
			
			return replyMessage;
		}
	};	

	
	/**
	 * Logger.
	 */
	public static final Logger getLogger() {
		return Logger.getLogger(BrokerProcessBasicBehaviour.class.getName());
	}
}
