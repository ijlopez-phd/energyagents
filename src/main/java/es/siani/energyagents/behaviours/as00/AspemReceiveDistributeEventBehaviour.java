package es.siani.energyagents.behaviours.as00;

import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.agent.AspemAgent;
import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.ontology.as00.CreatedEventPredicate;
import es.siani.energyagents.ontology.as00.DistributeEventAction;
import es.siani.energyagents.ontology.as00.EiResponseConcept;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;

@SuppressWarnings("serial")
public class AspemReceiveDistributeEventBehaviour extends AchieveREResponder {
	
	protected final EnergyAgent agent;

	
	/**
	 * Constructor.
	 */
	public AspemReceiveDistributeEventBehaviour(EnergyAgent agent, MessageTemplate mt) {
		super(agent, mt);
		super.setBehaviourName(AspemReceiveDistributeEventBehaviour.class.getSimpleName());
		
		this.agent = agent;
		return;
	}
	
	
	/**
	 * Process the DistributeEvent.
	 */
	@Override
	protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
		
		// Record this action in the tracker.
		if (this.agent.getTracker() != null) {
			this.agent.getTracker().distributeEventReceived();
		}
		
		// Extract the content of the message.
		DistributeEventAction content;
		try {
			content = (DistributeEventAction)(this.agent.getContentManager().extractContent(request));
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to extract the content of the message.");
			throw new IllegalStateException(ex);
		}
		
		// Get the behaviour in charge of processing the DistributeEvent.
		AspemProcessBehaviour processBehaviour =
				InjectionUtils.injector().getInstance(AspemProcessBehaviour.class);
		processBehaviour.init((AspemAgent)agent, content);

		// Builds the reply (CreatedEvent).
		ACLMessage replyMessage = request.createReply();
		replyMessage.setPerformative(ACLMessage.INFORM);
		try {
			
			this.agent.getContentManager().fillContent(
					replyMessage,
					new CreatedEventPredicate(
							new EiResponseConcept(
									EiResponseConcept.CODE_OK,
									request.getConversationId())));
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to fill the content of the message.");
		}
		
		// The processBehaviour and the reply are executed in sequential order.
		SequentialBehaviour seqBehaviours = new SequentialBehaviour(this.agent);
		seqBehaviours.addSubBehaviour(processBehaviour);
		seqBehaviours.addSubBehaviour(new ReplyDistributeEventBehaviour(replyMessage));
		this.agent.addBehaviour(seqBehaviours);		
		

		return null;
	}
	
	
	/**
	 * Inner class that sends the reply message to the Sender of the DistributeEvent.
	 */
	private class ReplyDistributeEventBehaviour extends OneShotBehaviour {
		
		private final ACLMessage reply;
		
		/**
		 * Constructor.
		 */
		public ReplyDistributeEventBehaviour(ACLMessage reply) {
			this.reply = reply;
			return;
		}

		
		/**
		 * Send the reply to the source of the RequestPause.
		 */
		@Override
		public void action() {
			agent.send(reply);
			return;
		}
		
	}
	
	
	/**
	 * Logger.
	 */
	public static final Logger getLogger() {
		return Logger.getLogger(AspemReceiveDistributeEventBehaviour.class.getName());
	}
}
