package es.siani.energyagents.behaviours.as00;

import es.siani.energyagents.agent.AspemAgent;
import jade.content.ContentElement;
import jade.core.behaviours.Behaviour;

@SuppressWarnings("serial")
public abstract class AspemProcessBehaviour extends Behaviour {
	
	protected AspemAgent agent;
	protected ContentElement content;
	protected boolean isInitiated = false;
	
	/**
	 * Constructor.
	 */
	protected AspemProcessBehaviour() {
		return;
	}
	
	
	/**
	 * Initializes the instance.
	 */
	public void init(AspemAgent agent, ContentElement content) {
		this.agent = agent;
		this.content = content;
		this.isInitiated = true;
		return;
	}
	
	
	/**
	 * Actions of the behaviour.
	 * At this level, it only checks whether the Behaviour has been initiated or not.
	 */
	@Override
	public void action() {
		if (!this.isInitiated) {
			throw new IllegalStateException(
					"The " + AspemProcessBehaviour.class.getSimpleName() + " hast not been initiated.");
		}
		
		return;
	}	
}
