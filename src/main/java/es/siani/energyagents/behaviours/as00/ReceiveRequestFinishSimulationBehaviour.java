package es.siani.energyagents.behaviours.as00;

import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.ontology.as00.FinishSimulationAction;
import es.siani.energyagents.ontology.as00.SimulationFinishedPredicate;
import es.siani.energyagents.ontology.as00.SimulationIdConcept;
import es.siani.energyagents.ontology.as00.StartSimulationAction;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;

@SuppressWarnings("serial")
public class ReceiveRequestFinishSimulationBehaviour extends AchieveREResponder {
	
	protected final EnergyAgent agent;
	
	
	/**
	 * Constructor.
	 */
	public ReceiveRequestFinishSimulationBehaviour(EnergyAgent agent, MessageTemplate mt) {
		super(agent, mt);
		super.setBehaviourName(ReceiveRequestFinishSimulationBehaviour.class.getSimpleName());
		this.agent = agent;
		return;
	}
	
	
	/**
	 * Handles the request to finish the simulation.
	 */
	@Override
	protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
		
		if (this.agent.getTracker() != null) {
			this.agent.getTracker().simulationFinished();
		}
		
		// Gets the content of the message.
		FinishSimulationAction finishAction;
		try {
			finishAction = (FinishSimulationAction)(this.agent.getContentManager().extractContent(request));
		} catch (Throwable t) {
			getLogger().log(Level.SEVERE, "Failed to extract the content of the message.");
			throw new IllegalStateException(t);
		}
		
		
		// The agent starts again to listen to the StartSimulation event.
		this.agent.addBehaviour(new ReceiveRequestStartSimulationBehaviour(
				this.agent,
				new MessageTemplate(new SimulationRequestMatcher(this.agent, StartSimulationAction.class))));
		
		
		// Builds the Reply.
		ACLMessage message = request.createReply();
		message.setPerformative(ACLMessage.INFORM);
		try {
			this.agent.getContentManager().fillContent(
					message,
					new SimulationFinishedPredicate(new SimulationIdConcept(finishAction.getSimulation().getId())));
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to fill content of the message.");
		}
		
		return message;
	}
	
	
	/**
	 * Logger.
	 */
	public static final Logger getLogger() {
		return Logger.getLogger(ReceiveRequestFinishSimulationBehaviour.class.getName());
	}

}
