package es.siani.energyagents.behaviours.as00;

import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.agent.ProxyAgent;
import es.siani.energyagents.model.PauseResult;
import es.siani.energyagents.ontology.as00.NextSimulationPausePredicate;
import es.siani.energyagents.ontology.as00.PauseSimulationAction;
import es.siani.energyagents.ontology.as00.ScheduledPauseConcept;
import es.siani.energyagents.ontology.as00.SimulationPauseConcept;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

@SuppressWarnings("serial")
public class SendRequestPauseSimulationBehaviour extends AchieveREInitiator {
	
	private final ProxyAgent.Command command;
	

	/**
	 * Constructor.
	 */
	public SendRequestPauseSimulationBehaviour(AgentsPlatform platform, Agent sender, AID receiverAID, ACLMessage requestMessage,
			SimulationPauseConcept simulationPause, ProxyAgent.Command command) {
		
		super(sender, requestMessage);
		
		if (requestMessage.getPerformative() != ACLMessage.REQUEST) {
			throw new IllegalArgumentException("The ACL message passed as argument must be of the type REQUEST.");
		}		
		
		// Set up the fields of the message.		
		requestMessage.addReceiver(receiverAID);
		requestMessage.setLanguage(platform.getCodec().getName());
		requestMessage.setOntology(platform.getOntology().getName());
		requestMessage.setProtocol(FIPANames.InteractionProtocol.FIPA_QUERY);
		
		
		try {
			sender.getContentManager().fillContent(requestMessage, new PauseSimulationAction(simulationPause));
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Error during building the message of the body '"
					+ SendRequestPauseSimulationBehaviour.class.getSimpleName() + "'.");
		}
		
		this.command = command;		
		
		return;
	}
	
	
	/**
	 * Handles the Inform message that contains the reply:
	 *    the nextTime at which the simulator should pause and call the Gridop.
	 */
	@Override
	protected void handleInform(ACLMessage informMessage) {
		
		// Get the content of the message.
		ScheduledPauseConcept pause = null;
		boolean distributeActionTriggered;
		try {
			NextSimulationPausePredicate predicate = 
					(NextSimulationPausePredicate)this.myAgent.getContentManager().extractContent(informMessage);
			pause = predicate.getPause();
			distributeActionTriggered = predicate.getDistributeActionTriggered();
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Error while extracting the content of the message", ex);
			throw new IllegalStateException(ex);
		}
		
		this.command.setOutput(new Object[] {
				new PauseResult(
						(pause.getTime() == null)? -1l : pause.getTime().getTime(),
								distributeActionTriggered)
		});
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(SendRequestPauseSimulationBehaviour.class.getName());
	}
}
