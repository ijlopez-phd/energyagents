package es.siani.energyagents.behaviours.as00;

import static es.siani.energyagents.utils.NotificationUtils.toAsdrEvent;

import javax.inject.Inject;

import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.lang.acl.ACLMessage;
import es.siani.energyagents.ontology.as00.DistributeEventAction;
import es.siani.energyagents.ontology.as00.DistributeEventConcept;
import es.siani.energyagents.ontology.as00.EventConcept;
import es.siani.simpledr.EasyDistributeEvent;

@SuppressWarnings("serial")
public class GridopBasicProcessBehaviour extends GridopProcessBehaviour {
	
	private boolean isDone;
	private boolean waitingForReply;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public GridopBasicProcessBehaviour() {
		this.isDone = false;
		this.waitingForReply = false;
		return;
	}
	
	
	/**
	 * Basic actions of the Grid Operator. It sends all distribute events to the ASPEM in order for
	 * they resend the events to the Brokers.
	 */
	@Override
	public void action() {
		
		// Parent's checks.
		super.action();
		
		if (this.waitingForReply) {
			// The message has already been sent to the ASPEMs.
			return;
		}
		
		// Check if there are events to be sent or enabled ASPEMs.
		AID[] aspemsAIDs = this.agent.getPlatform().getAspemAgentsAID(this.agent);
		if ((schedule.getEvents().size() == 0) || (aspemsAIDs.length == 0)) {
			if (this.tearDownBehaviour != null) {
				this.agent.addBehaviour(this.tearDownBehaviour);
			}
			this.isDone = true;
			return;
		}
		
		// Before adding new events, DistributeEvent of the agent is updated.
		//  - Remove events that previously were cancelled or completed.
		//  - Adjust the status of the oadrEvents to the current time.			
		//  - Add the new oadrEvents.
		final long currentModelTime = this.schedule.getCurrentModelTime();
		final EasyDistributeEvent easy = this.agent.getDistributeEvent();
		easy.removeFinishedEvents();
		easy.updateEventsStatus(currentModelTime);
		for (EventConcept event : schedule.getEvents()) {
			this.agent.getDistributeEvent().addEvent(toAsdrEvent(event), currentModelTime);
		}
		
		// Add the behaviour that spreads the DistributeEvent to all the ASPEMs.
		SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
				this.agent,
				aspemsAIDs,
				new ACLMessage(ACLMessage.REQUEST),
				new DistributeEventAction(
						new DistributeEventConcept(this.agent.getPlatform().getId(), this.agent.getDistributeEvent().toString()),
						currentModelTime));
		sendBehaviour.registerHandleAllResultNotifications(new DoneSendBehaviour());
		
		if (this.tearDownBehaviour == null) {
			this.agent.addBehaviour(sendBehaviour);
		} else {
			SequentialBehaviour seqBehaviour = new SequentialBehaviour();
			seqBehaviour.addSubBehaviour(sendBehaviour);
			seqBehaviour.addSubBehaviour(this.tearDownBehaviour);
			this.agent.addBehaviour(seqBehaviour);
		}
		
		this.waitingForReply = true;		
		
		return;
	}
	

	/**
	 * Tell if the behaviour has finished its operation.
	 * It is determined by the flag "isDone".
	 */
	@Override
	public boolean done() {
		return this.isDone;
	}
	
	
	/**
	 * Inner class that is called when all notifications are received for the SendBehaviour.
	 * This class sets the flag 'isDone' to true.
	 */
	private class DoneSendBehaviour extends OneShotBehaviour {

		/**
		 * Change the flag 'isDone' of the outter behaviour to 'true'.
		 * It implies the finialization of the outter behaviour. 
		 */
		@Override
		public void action() {
			GridopBasicProcessBehaviour.this.isDone = true;
			// All the notifications are in the DataStore (ALL_RESULT_NOTIFICATIONS_KEY).
			return;
		}
	}

}
