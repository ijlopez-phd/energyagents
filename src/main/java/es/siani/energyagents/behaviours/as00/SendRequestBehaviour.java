package es.siani.energyagents.behaviours.as00;

import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.agent.ProxyAgent;
import jade.content.Predicate;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

@SuppressWarnings("serial")
public class SendRequestBehaviour extends AchieveREInitiator {
	
	/**
	 * Constructor.
	 */
	public SendRequestBehaviour(Agent agent, AID[] receiversAID, ACLMessage msg, Predicate action) {
		
		super(agent, msg);
		
		if (msg.getPerformative() != ACLMessage.REQUEST) {
			throw new IllegalArgumentException("The ACL message passed as argument must be of the type REQUEST.");
		}
		
		AgentsPlatform platform = getAgentsPlatform(agent);
		
		// Set up the message.
		msg.setLanguage(platform.getCodec().getName());
		msg.setOntology(platform.getOntology().getName());
		msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
		for (AID aid : receiversAID) {
			msg.addReceiver(aid);
		}
		
		
		try {
			agent.getContentManager().fillContent(msg, action);
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to build the body of the message.");
			throw new IllegalArgumentException(ex);
		}
		
		return;
	}
	
	
	/**
	 * Get the AgentsPlatform to which the agent belongs.
	 */
	private static AgentsPlatform getAgentsPlatform(Agent agent) {
		if (agent instanceof EnergyAgent) {
			return ((EnergyAgent) agent).getPlatform();
		} else if (agent instanceof ProxyAgent) {
			return ((ProxyAgent) agent).getPlatform();
		}
		
		throw new IllegalArgumentException("Type of agent not supported.");
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(SendRequestBehaviour.class.getName());
	}

}
