package es.siani.energyagents.behaviours.as00;

import es.siani.energyagents.agent.BrokerAgent;
import jade.content.ContentElement;
import jade.core.behaviours.Behaviour;

@SuppressWarnings("serial")
public abstract class BrokerProcessBehaviour extends Behaviour {
	
	
	protected BrokerAgent agent;
	protected ContentElement content;
	
	
	/**
	 * Constructor.
	 */
	protected BrokerProcessBehaviour() {
		return;
	}
	
	
	/**
	 * Initializes the instance.
	 */
	public void init(BrokerAgent agent, ContentElement content) {
		this.agent = agent;
		this.content = content;
		return;
	}
}
