package es.siani.energyagents.behaviours.as00;

import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import es.siani.energyagents.ontology.as00.DistributeEventAction;


@SuppressWarnings("serial")
public class AspemProcessBasicBehaviour extends AspemProcessBehaviour {
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public AspemProcessBasicBehaviour() {
		return;
	}
	
	
	/**
	 * The behaviour always finish after being execute one time.
	 */
	@Override
	public boolean done() {
		return true;
	}	
	
	
	/**
	 * Send the DistributeEvent message to all the broker agents of the container.
	 */
	@Override
	public void action() {
		super.action();
		
		this.agent.addBehaviour(new ReceiveDistributeEventBehaviour(
				MessageTemplate.and(
						this.agent.getPlatform().createBasicMessageTemplate(),
						MessageTemplate.MatchSender(agent.getPlatform().getGridopAgentAID(this.agent)))));
				
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static Logger getLogger() {
		return Logger.getLogger(AspemProcessBasicBehaviour.class.getName());
	}	
	
	
	/**
	 * Inner class.
	 */
	private class ReceiveDistributeEventBehaviour extends AchieveREResponder {

		/**
		 * Constructor.
		 */
		public ReceiveDistributeEventBehaviour(MessageTemplate mt) {
			super(agent, mt);
		}
		
		
		/**
		 * Handle request for processing DistributeEvent messages.
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			// Extract the content of the message.
			DistributeEventAction eventAction;
			try {
				eventAction = (DistributeEventAction)(agent.getContentManager().extractContent(request));
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to extract the content of the message.");
				throw new IllegalStateException();
			}
			
			SendDistributeEventBehaviour sendBehaviour = new SendDistributeEventBehaviour(
					agent,
					agent.getPlatform().getBrokerAgentsAID(agent, agent.getContainerName()),
					new ACLMessage(ACLMessage.REQUEST),
					eventAction.getDistributeEvent(),
					eventAction.getModelTimestamp());

			agent.addBehaviour(sendBehaviour);
			
			return null;
		}
		
	}
}
