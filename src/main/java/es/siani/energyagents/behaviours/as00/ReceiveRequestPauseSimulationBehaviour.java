package es.siani.energyagents.behaviours.as00;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.ontology.as00.NextSimulationPausePredicate;
import es.siani.energyagents.ontology.as00.PauseSimulationAction;
import es.siani.energyagents.ontology.as00.ProgramConcept;
import es.siani.energyagents.ontology.as00.ScheduleConcept;
import es.siani.energyagents.ontology.as00.ScheduledPauseConcept;
import es.siani.energyagents.utils.PauseScheduler;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;

@SuppressWarnings("serial")
public class ReceiveRequestPauseSimulationBehaviour extends AchieveREResponder {
	
	protected final EnergyAgent agent;
	protected final PauseScheduler pauseScheduler;
	protected final String scenarioCode;
	protected final String storeName;

	
	/**
	 * Constructor.
	 */
	public ReceiveRequestPauseSimulationBehaviour(
			EnergyAgent agent, MessageTemplate mt, ProgramConcept program, String scenarioCode, String storeName) {
		super(agent, mt);
		super.setBehaviourName(ReceiveRequestPauseSimulationBehaviour.class.getSimpleName());
		this.agent = agent;
		this.scenarioCode = scenarioCode;
		this.storeName = storeName;
		this.pauseScheduler = InjectionUtils.injector().getInstance(PauseScheduler.class);
		this.pauseScheduler.init(program);
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ReceiveRequestPauseSimulationBehaviour(
			EnergyAgent agent, MessageTemplate mt, PauseScheduler pauseScheduler, String scenarioCode, String storeName) {
		super(agent, mt);
		this.agent = agent;
		this.scenarioCode = scenarioCode;
		this.storeName = storeName;
		this.pauseScheduler = pauseScheduler;
		return;
	}
	
	
	/**
	 * Handles the request to calculate the next time the simulator must pause and inform the
	 * Gridop agent.
	 */
	@Override
	protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
		
		if (this.agent.getTracker() != null) {
			this.agent.getTracker().simulationPaused();
		}
		
		// Gets the content of the message.
		PauseSimulationAction action;
		try {
			action = (PauseSimulationAction)this.agent.getContentManager().extractContent(request);
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to extract the content of the ReceiveQueryNextPause message.");
			throw new IllegalStateException(ex);
		}
		
		// Get the scheduled events until the next pause.
		final long currentModelTime = action.getSimulation().getModelTime().getTime();
		ScheduleConcept schedule = this.pauseScheduler.process(currentModelTime);		

		
		// Add the behaviour that encapusales the actions of the GridOperator in response
		// to this PauseRequest.
		GridopProcessBehaviour gridopBehaviour = InjectionUtils.injector().getInstance(GridopProcessBehaviour.class);
		gridopBehaviour.init(
				action.getSimulation().getIdSimulation(),
				this.agent,
				schedule,
				scenarioCode,
				storeName,
				new ReplyRequestPauseBehaviour(request, schedule));
		this.agent.addBehaviour(gridopBehaviour);
		
		return null;
	}
	
	
	/**
	 * Inner class that sends the reply message to the Sender of the RequestPause (the outer class).
	 */
	private class ReplyRequestPauseBehaviour extends OneShotBehaviour {
		
		private final ACLMessage request;
		private final ScheduleConcept schedule;
		
		/**
		 * Constructor.
		 */
		public ReplyRequestPauseBehaviour(ACLMessage request, ScheduleConcept schedule) {
			this.request = request;
			this.schedule = schedule;
			return;
		}

		
		/**
		 * Send the reply to the source of the RequestPause.
		 */
		@Override
		public void action() {
			
			ACLMessage reply = request.createReply();
			reply.setPerformative(ACLMessage.INFORM);		
			try {
				ScheduledPauseConcept scheduledPause = new ScheduledPauseConcept(
						0l,
						(schedule.getNextPause() == PauseScheduler.NO_PAUSE)? null : new Date(schedule.getNextPause()));
				
				agent.getContentManager().fillContent(
						reply,
						new NextSimulationPausePredicate(scheduledPause, schedule.getEvents().size() > 0));
				
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to fill the content of the message.");
			}
			
			agent.send(reply);
			return;
		}
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(ReceiveRequestPauseSimulationBehaviour.class.getName());
	}

}
