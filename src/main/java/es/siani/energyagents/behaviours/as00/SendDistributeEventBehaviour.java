package es.siani.energyagents.behaviours.as00;

import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.ontology.as00.DistributeEventAction;
import es.siani.energyagents.ontology.as00.DistributeEventConcept;
import jade.core.AID;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

@SuppressWarnings("serial")
public class SendDistributeEventBehaviour extends AchieveREInitiator {
	
	private final EnergyAgent agent;
	
	
	/**
	 * Constructor.
	 */
	public SendDistributeEventBehaviour(
			EnergyAgent agent,
			AID[] receiversAID,
			ACLMessage requestMessage,
			DistributeEventConcept distributeEvent,
			long modelTimestamp) {
		
		super(agent, requestMessage);
		super.setBehaviourName(SendDistributeEventBehaviour.class.getSimpleName());
		
		if (requestMessage.getPerformative() != ACLMessage.REQUEST) {
			throw new IllegalArgumentException("The ACL message passed as argument must be of the type REQUEST.");
		}
		
		this.agent = agent;
		
		// Set up the message.
		AgentsPlatform platform = agent.getPlatform();
		requestMessage.setLanguage(platform.getCodec().getName());
		requestMessage.setOntology(platform.getOntology().getName());
		requestMessage.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
		for (AID aid : receiversAID) {
			requestMessage.addReceiver(aid);
		}
		
		try {
			this.agent.getContentManager().fillContent(
					requestMessage,
					new DistributeEventAction(distributeEvent, modelTimestamp));
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to build the body of the message.");
			throw new IllegalArgumentException(ex);
		}
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(SendDistributeEventBehaviour.class.getName());
	}

}
