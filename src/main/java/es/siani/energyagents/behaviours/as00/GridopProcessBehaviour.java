package es.siani.energyagents.behaviours.as00;

import jade.core.behaviours.Behaviour;
import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.ontology.as00.ScheduleConcept;

@SuppressWarnings("serial")
public abstract class GridopProcessBehaviour extends Behaviour {
	
	protected long idSimulation;
	protected EnergyAgent agent;
	protected ScheduleConcept schedule;
	protected String scenarioCode;
	protected String storeName;
	protected Behaviour tearDownBehaviour;
	protected boolean isInitiated = false;
	
	
	/**
	 * Constructor.
	 */
	protected GridopProcessBehaviour() {
		return;
	}
	
	
	/**
	 * Initializes the instance.
	 */
	public void init(
			long idSimulation, EnergyAgent agent, ScheduleConcept schedule, String scenarioCode, String storeName,
			Behaviour tearDownBehaviour) {
		this.idSimulation = idSimulation;
		this.agent = agent;
		this.schedule = schedule;
		this.scenarioCode = scenarioCode;
		this.storeName = storeName;
		this.tearDownBehaviour = tearDownBehaviour;
		this.isInitiated = true;
		return;
	}
	

	/**
	 * Basic action of the behaviour.
	 * At this level, the action only checks whether the Behaviour has been initiated or not.
	 */
	@Override
	public void action() {
		
		if (!this.isInitiated) {
			throw new IllegalStateException();
		}
		
		return;
	}
}
