package es.siani.energyagents.behaviours.as03;

import jade.core.AID;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.siani.energyagents.ontology.as03.OfferLevelConcept;

public class BasicOffersSolver implements OffersSolver {

	
	/**
	 * @see es.siani.energyagents.behaviours.as03.OffersSolver#solve(AuctionWorkingData)
	 */
	@Override
	public Map<AID, List<AcceptedOffer>> solve(AuctionWorkingData wd) {
		
        // Initially all producers that have made offers are classified as if none of their offers had been accepted.
		BlockDescription[] blocks = wd.blocks;
		Set<AID> nonAcceptedProducers = new HashSet<AID>();
		for (BlockDescription block : blocks){
			for (AuctionOffer offer : block.receivedOffers) {
				nonAcceptedProducers.add(offer.producer);
			}
		}			
		
        // Determine the offers that are accepted for each consuming block.
        Map<AID, List<AcceptedOffer>> acceptanceMap = new HashMap<AID, List<AcceptedOffer>>();
        for (int nBlock = 0; nBlock < blocks.length; nBlock++) {
        	BlockDescription block = blocks[nBlock];

        	if (block.role != AuctionAgentRole.CONSUMER) {
        		continue;
        	}

        	List<AcceptedOffer> acceptedOffers = new ArrayList<AcceptedOffer>();
        	float toCover = block.demand;
        	Iterator<InternalOffer> itOffers = getOffersInOrder(block.receivedOffers).iterator();
        	while ((toCover > 0) && itOffers.hasNext()) {
        		InternalOffer offer = itOffers.next();
        		if (toCover >= offer.amount) {
        			acceptedOffers.add(new AcceptedOffer(nBlock, offer.sender, offer.amount, offer.price));
        			toCover -= offer.amount;
        		} else {
        			acceptedOffers.add(new AcceptedOffer(nBlock, offer.sender, toCover, offer.price));
        			toCover = 0;
        			offer.amount -= toCover;
        		}
        	}
        	
        	
        	if (toCover == 0) {
        		for (AcceptedOffer acceptedOffer : acceptedOffers) {
        			List<AcceptedOffer> producerAcceptedOffers = acceptanceMap.get(acceptedOffer.producer);
        			if (producerAcceptedOffers == null) {
        				producerAcceptedOffers = new ArrayList<AcceptedOffer>();
        				acceptanceMap.put(acceptedOffer.producer, producerAcceptedOffers);
        			}
        			producerAcceptedOffers.add(acceptedOffer);
        			nonAcceptedProducers.remove(acceptedOffer.producer);
        		}

        	}
        }
        
        // Producers from whom none offer has been accepted are added to the map with a NULL list.
        for (AID aid : nonAcceptedProducers) {
        	acceptanceMap.put(aid, null);
        }
        
        
		return acceptanceMap;
	}
	
	
	/**
	 * Get all offers ordered by price in descending order.
	 */
	private List<InternalOffer> getOffersInOrder(List<AuctionOffer> offers) {
		
		ArrayList<InternalOffer> list = new ArrayList<InternalOffer>();
		for (AuctionOffer auctionOffer : offers) {
			if (auctionOffer.combined) {
				continue;
			}
			
			final AID producer = auctionOffer.producer;
			for (OfferLevelConcept levelOffer : auctionOffer.levelOffers) {
				list.add(new InternalOffer(producer, levelOffer.getAmount(), levelOffer.getPrice()));
			}
		}
		
		Collections.sort(list, new InternalOfferComparatorAsc());
		
		return list;
	}
	
	
	/**
	 * 
	 * Internal structure used to process and select the offers from producers.
	 *
	 */
	private class InternalOffer {
		public final AID sender;
		public float amount;
		public final float price;
		
		public InternalOffer(AID sender, float amount, float price) {
			this.sender = sender;
			this.amount = amount;
			this.price = price;
			return;
		}
	}
	
	
	/**
	 * 
	 * Comparator for ordering by price the InternalOffer in descending order.
	 *
	 */
	private class InternalOfferComparatorAsc implements Comparator<InternalOffer> {

		@Override
		public int compare(InternalOffer o1, InternalOffer o2) {
			if (o1.price < o2.price) {
				return -1;
			} else if (o1.price > o2.price) {
				return 1;
			}
			
			return 0;
		}
		
	}	

}
