package es.siani.energyagents.behaviours.as03;


import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import es.siani.energyagents.EnergyAgentsGlobals;
import es.siani.energyagents.config.Configuration;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;

public class AuctionLogger {

	private static final long NO_ID = -999;
	private final long BLOCK_MILLIS;
	
	private long currentId = NO_ID;
	private String loggerRootPath;
	private Logger logger;

	
	/**
	 * Constructor.
	 */
	public AuctionLogger() {
		
		Configuration config = InjectionUtils.injector().getInstance(Configuration.class);
		BLOCK_MILLIS = Integer.parseInt(config.get(Configuration.PROP_AUCTION_BLOCK_SECONDS)) * 1000;		
		
		loggerRootPath = EnergyAgentsGlobals.AUCTION_LOGGING_PATH;
		if (loggerRootPath.startsWith("//")) {
			// This is an internal path.
			loggerRootPath = BrokerExp03Behaviour.class.getResource(loggerRootPath.substring(1)).getFile();
		}
		if (!loggerRootPath.endsWith("/")) {
			loggerRootPath += "/";
		}
		
		return;
	}
	
	
	/**
	 * Init the handler to use in the simulation to log the auction.
	 */
	public void init(long id, String algName, int nDirectories, float overbookingFactor) {
		
		if (id == this.currentId) {
			return;
			
		} else if (this.currentId != NO_ID) {
			this.close();
		}
		
		
		this.logger = Logger.getLogger(AuctionLogger.class.getName() + "-" + id);
		logger.setUseParentHandlers(false);		

		try {
			FileHandler fh = new FileHandler(
					loggerRootPath + "auc_"
							+ algName + "_" + id + "_" + nDirectories + "_"
							+ Float.toString(overbookingFactor) + "-%u.log",
					500000,
					100,
					false);
			fh.setLevel(Level.INFO);
			fh.setFormatter(new AuctionLoggerFormatter());
			logger.addHandler(fh);
		} catch (SecurityException | IOException e) {
			getModuleLogger().log(Level.WARNING, "Failed to create the auction's log file.");
		}
		
		this.currentId = id;
		
		return;
	}
	
	
	public void close() {
		if (logger == null) {
			return;
		}
		
		for (Handler handler : this.logger.getHandlers()) {
			handler.close();
		}
		this.logger = null;
		this.currentId = NO_ID;
		
		return;
	}
	
	
	/**
	 * Log (into a file) the result of the auction for a broker.
	 * The log is built from the payloads of a specific event.
	 */	
	public void logBrokerResult(String agentName, String dfName, AsdrEvent event) {
		
		if (logger == null) {
			// The logging mechanism is not initiated.			
			return;
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append(agentName);
		sb.append(",");
		sb.append((dfName == null)? "-" : dfName);
		sb.append(",");		
		
		final int nBlocks = (int)((event.getDuration() * 1000) / BLOCK_MILLIS);
		long currentMillis = event.getStart().getTime();
		Iterator<AsdrInterval> itInterval = event.getIntervals().iterator();
		AsdrInterval currentInterval = itInterval.next();
		long currentIntervalEnd = currentMillis + (currentInterval.getDuration() * 1000);
		for (int nBlock = 0; nBlock < nBlocks; nBlock++, currentMillis += BLOCK_MILLIS) {
			
			if (nBlock != 0) {
				sb.append(",");
			}
			
			if (currentMillis == currentIntervalEnd) {
				currentInterval = itInterval.next();
				currentIntervalEnd = currentMillis + (currentInterval.getDuration() * 1000);
			}
			
			sb.append("-/");
			sb.append(currentInterval.getPayload());
		}
		
		logger.log(Level.INFO, sb.toString());
		
		return;
	}
	
	
	/**
	 * Log the result of the auction for a broker.
	 * The log is built from the information blocks and the list of generated events.
	 */
	public void logBrokerResult(
			String agentName, String dfName,
			BlockDescription[] blocks, Date startTime, List<AsdrEvent> events) {
		
		if (logger == null) {
			// The logging mechanism is not initiated.
			return;
		}		
		
		StringBuilder sb = new StringBuilder();
		sb.append(agentName);
		sb.append(",");
		sb.append(dfName);
		sb.append(",");
		
		long currentMillis = startTime.getTime();

		for (int nBlock = 0; nBlock < blocks.length; nBlock++, currentMillis += BLOCK_MILLIS) {
			
			if (nBlock != 0) {
				sb.append(",");
			}
			
			BlockDescription block = blocks[nBlock];
			if (block.role == AuctionAgentRole.CONSUMER) {
				sb.append("C-");
				sb.append(block.demandCovered);
				sb.append("/");
				sb.append(block.demand);
			} else if (block.role == AuctionAgentRole.PRODUCER) {
				sb.append("P-");
				sb.append(block.demandedOverOffer);
				sb.append("/");
				float capacity = block.offerAmount[0];
				for (int n = 1; n < block.offerAmount.length; n++) {
					capacity += block.offerAmount[n];
				}
				sb.append(capacity);
			} else {
				sb.append("-");
			}
			
			sb.append("/");
			sb.append(getEventPayloadAt(currentMillis, events));
			
			if (block.role == AuctionAgentRole.CONSUMER) {
				sb.append("#");
				if (block.coveredDemandItems != null) {
					boolean firstCoveredDemandItem = true;
					for (CoveredDemandItem coveredItem : block.coveredDemandItems) {
						if (coveredItem.amount == 0) {
							continue;
						}
						
						if (!firstCoveredDemandItem) {
							sb.append(";");
						} else {
							firstCoveredDemandItem = false;
						}
						sb.append(coveredItem.amount);
						sb.append("-");
						sb.append(coveredItem.price);
					}
				}
			}
		}
		
		logger.log(Level.INFO, sb.toString());
		
		return;
	}
	
	
	public void log(String line) {
		logger.log(Level.INFO, line);
	}	
	
	
	/**
	 * For a list of events, return the payload corresponding to a specific moment.
	 */
	private float getEventPayloadAt(long targetMillis, List<AsdrEvent> events) {
		
		if (events == null) {
			return -1;
		}
		
		for (AsdrEvent event : events) {
			final long eventStart = event.getStart().getTime();
			final long eventEnd = eventStart + (event.getDuration() * 1000);
			if ((targetMillis >= eventStart) && (targetMillis < eventEnd)) {
				long intervalStart = eventStart;
				for (AsdrInterval interval : event.getIntervals()) {
					if ((targetMillis >= intervalStart) && (targetMillis < (intervalStart + (interval.getDuration() * 1000)))) {
						return interval.getPayload();
					}
					intervalStart += interval.getDuration() * 1000;
				}
			} else if (eventStart > targetMillis) {
				return 0;
			}
		}
		
		return 0;
		
	}	
	
	
	/**
	 * Logger.
	 */
	private static final Logger getModuleLogger() {
		return Logger.getLogger(AuctionLogger.class.getName());
	}
}


/**
 * 
 * 
 * Formatter to log the results of the auction.
 * 
 *
 */
class AuctionLoggerFormatter extends Formatter {

	@Override
	public String format(LogRecord record) {
		String msg = record.getMessage();
		if (msg == null) {
			return "";
		}
		return msg + "\n";
	}
	
}