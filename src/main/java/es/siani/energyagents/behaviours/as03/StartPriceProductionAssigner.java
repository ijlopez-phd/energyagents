package es.siani.energyagents.behaviours.as03;

import jade.core.AID;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;

import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.ontology.as03.OfferBlockConcept;

public class StartPriceProductionAssigner implements ProductionAssigner {
	
	private boolean flagUseRandomItems = false;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public StartPriceProductionAssigner() {
		AuctionLogger auclog = InjectionUtils.injector().getInstance(AuctionLogger.class);
		auclog.log("# StartPrice");
		return;
	}
	
	
	/**
	 * @see es.siani.energyagents.behaviours.as03.ProductionAssigner#useRandomItems(boolean)
	 */
	public void useRandomItems(boolean flagUseRandomItems) {
		this.flagUseRandomItems = flagUseRandomItems;
		return;
	}

	
	/**
	 * @see es.siani.energyagents.behaviours.as03.ProductionAssigner#assign(AuctionWorkingData, float)
	 */
	@Override
	public Map<AID, List<OfferBlockConcept>> assign(AuctionWorkingData wd, float overbookingFactor) {
		
		Map<AID, List<OfferBlockConcept>> offersMap = new HashMap<AID, List<OfferBlockConcept>>();
		for (int nBlock = 0; nBlock < wd.blocks.length; nBlock++) {
			
			BlockDescription block = wd.blocks[nBlock];
			
			if ((block.role != AuctionAgentRole.PRODUCER) || (block.callsForDemand == null)) {
				continue;
			}
			
			// Production capacity for this block.
			float capacity = 0;
			final int N_LEVELS = block.offerAmount.length;
			for (int nLevel = 0; nLevel < N_LEVELS; nLevel++) {
				capacity += block.offerAmount[nLevel];
			}
			capacity += capacity * overbookingFactor;
			
			int currentLevel = 0;
			float remainderLevelCapacity = block.offerAmount[0];
			float currentLevelPrice = block.offerPrice[0];
			remainderLevelCapacity += remainderLevelCapacity * overbookingFactor;
			if (this.flagUseRandomItems) {
				Collections.shuffle(block.callsForDemand);
			} else {
				Collections.sort(block.callsForDemand);
			}
			
			Iterator<CallForDemand> it = block.callsForDemand.iterator();
			float totalOffered = 0;
			while ((totalOffered < capacity) && it.hasNext()) {
				CallForDemand call = it.next();
				float toCover = call.getDemand();
				final float startingPrice = call.getStartPrice();
				
				OfferBlockConcept offerBlock = new OfferBlockConcept(nBlock);
				while ((toCover > 0) && (totalOffered < capacity) && (currentLevel < N_LEVELS) && (currentLevelPrice <= startingPrice)) {
					float assigned =
							(remainderLevelCapacity >= toCover)? toCover : remainderLevelCapacity;
					
					offerBlock.addOffer(assigned, block.offerPrice[currentLevel]);
					totalOffered += assigned;
					toCover -= assigned;
					remainderLevelCapacity -= assigned;
					
					if (remainderLevelCapacity == 0) {
						currentLevel++;
						if (currentLevel < N_LEVELS) {
							remainderLevelCapacity = block.offerAmount[currentLevel];
							remainderLevelCapacity += remainderLevelCapacity * overbookingFactor;
							currentLevelPrice = block.offerPrice[currentLevel];
						}
					}
				}
				
				if (offerBlock.getOffers().size() > 0) {
					List<OfferBlockConcept> brokerOffers = offersMap.get(call.getSender());
					if (brokerOffers == null) {
						brokerOffers = new ArrayList<OfferBlockConcept>();
						offersMap.put(call.getSender(), brokerOffers);
					}
					brokerOffers.add(offerBlock);
				}
			}
		}
		
		return offersMap;
	}
}
