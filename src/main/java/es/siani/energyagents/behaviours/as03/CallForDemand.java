package es.siani.energyagents.behaviours.as03;

import jade.core.AID;

/**
 * 
 * 
 * Call for demand.
 * 
 * 
 */
class CallForDemand implements Comparable<CallForDemand> {
	
	private final AID sender;
	private final float demand;
	private final float startPrice;
	
	/**
	 * Constructor.
	 */
	public CallForDemand(AID sender, float demand, float startPrice) {
		this.sender = sender;
		this.demand = demand;
		this.startPrice = startPrice;
		return;
	}
	
	/**
	 * Get the AID of the agent that demands load.
	 */
	public AID getSender() {
		return this.sender;
	}
	
	/**
	 * Get the demand.
	 */
	public float getDemand() {
		return this.demand;
	}
	
	
	/**
	 * Get the start price.
	 */
	public float getStartPrice() {
		return this.startPrice;
	}

	
	/**
	 * Comparte two instances of CallForDemand according to the size of the demand.
	 */
	@Override
	public int compareTo(CallForDemand other) {
		if (this.demand < other.demand) {
			return -1;
		} else if (this.demand > other.demand) {
			return 1;
		}
		
		return 0;
	}
}