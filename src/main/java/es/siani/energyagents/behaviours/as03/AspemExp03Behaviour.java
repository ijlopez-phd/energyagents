package es.siani.energyagents.behaviours.as03;

import static es.siani.energyagents.utils.NotificationUtils.toAsdrEvent;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.xml.transform.stream.StreamSource;

import com.google.inject.Injector;

import es.siani.energyagents.EnergyAgentsGlobals;
import es.siani.energyagents.agent.EnergyAgentsDF;
import es.siani.energyagents.behaviours.as00.AspemProcessBehaviour;
import es.siani.energyagents.behaviours.as00.SendRequestBehaviour;
import es.siani.energyagents.behaviours.as00.SimulationRequestMatcher;
import es.siani.energyagents.config.Configuration;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.ontology.as00.CreatedEventPredicate;
import es.siani.energyagents.ontology.as00.DistributeEventAction;
import es.siani.energyagents.ontology.as00.DistributeEventConcept;
import es.siani.energyagents.ontology.as00.EiResponseConcept;
import es.siani.energyagents.ontology.as00.EventConcept;
import es.siani.energyagents.ontology.as03.AbortAuctionAction;
import es.siani.energyagents.ontology.as03.AuctionRoleConcept;
import es.siani.energyagents.ontology.as03.ClearAuctionAction;
import es.siani.energyagents.ontology.as03.FinishAuctionAction;
import es.siani.energyagents.ontology.as03.PrepareAuctionAction;
import es.siani.energyagents.ontology.as03.ReadyAuctionPredicate;
import es.siani.energyagents.ontology.as03.ResultStartAuctionStagePredicate;
import es.siani.energyagents.ontology.as03.StartAuctionAction;
import es.siani.energyagents.utils.NotificationUtils;
import es.siani.simpledr.EasyDistributeEvent;
import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;

@SuppressWarnings("serial")
public class AspemExp03Behaviour extends AspemProcessBehaviour {
	
	/** Maximum number of clear rounds in an auction. */
	private final int MAX_CLEAR_ROUNDS;
	
	/** Duration in seconds of a block of the auction. */
	private final long BLOCK_SECONDS;
	
	
	/** Number of DF Agents that are instantiated per ASPEM. */
	private final int N_DF_AGENTS;
	
	/** Path of the Jade configuration file. */
	private final String jadeConfigFilePath;

	
	/** Identifier of the current auction. It increases as auctions are held. */
	private static int idAuction = 0;
	
	AuctionLogger auclogger;
	
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public AspemExp03Behaviour() {
		Configuration config = InjectionUtils.injector().getInstance(Configuration.class);
		MAX_CLEAR_ROUNDS = Integer.parseInt(config.get(Configuration.PROP_AUCTION_CLEAR_ROUNDS));
		BLOCK_SECONDS = Integer.parseInt(config.get(Configuration.PROP_AUCTION_BLOCK_SECONDS));
		N_DF_AGENTS = Integer.parseInt(config.get(Configuration.PROP_AUCTION_DF_AGENTS));
		
		String dfPath = config.get(Configuration.PROP_JADE_DF_PATH);
		if (dfPath == null) {
			// Use the internal path.
			dfPath = AspemExp03Behaviour.class.getResource(EnergyAgentsGlobals.DF_INTERNAL_PATH).getPath();
		}
		jadeConfigFilePath = dfPath;
		
		auclogger = InjectionUtils.injector().getInstance(AuctionLogger.class);
		
		return;
	}

	
	/**
	 * Indicates if the behaviour has finished its execution.
	 */
	@Override
	public boolean done() {
		return true;
	}
	
	
	/**
	 * Handle the messages sent by the GridOpeator.
	 */
	@Override
	public void action() {
		// Parent's checks.
		super.action();
		
		// Behaviour that listens to the requests of the GridOperator.
		agent.addBehaviour(new ReceiveDistributeActionBehaviour(
				new MessageTemplate(
						new SimulationRequestMatcher(agent, agent.getAID(), DistributeEventAction.class))));
		
		return;
	}
	

	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(AspemExp03Behaviour.class.getName());
	}
	
	
	/**
	 *
	 *
	 *Receive requests from the GridOperator.
	 *
	 *
	 */
	private class ReceiveDistributeActionBehaviour extends AchieveREResponder {
		
		/** Number of expected notifications from Distribute events. */
		private int expectedNotifications = 0;
		
		/** Number of notifications received from Distribute events. */
		private int receivedNotifications = 0;
		
		private ACLMessage request;
		
		private long modelTimestamp;
		
		private AsdrEvent newEvent = null;
		
		private AgentController[] dfAgents = null;

		/**
		 * Constructor.
		 */
		public ReceiveDistributeActionBehaviour(MessageTemplate mt) {
			super(agent,mt);
			return;
		}
		
		
		/**
		 * Process a request received from the Grid Operator.
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			// Extract the content of the message.
			DistributeEventAction action;
			try {
				action = (DistributeEventAction)(agent.getContentManager().extractContent(request));
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to extact the content of the message.", ex);
				throw new IllegalStateException();
			}
			
			if (agent.getTracker() != null) {
				agent.getTracker().distributeEventReceived();
			}

			this.request = request;
			this.modelTimestamp = action.getModelTimestamp();
			EasyDistributeEvent receivedEasy = new EasyDistributeEvent(
					action.getDistributeEvent().getMarketContext(),
					new StreamSource(new StringReader(action.getDistributeEvent().getContent())));

			// Find the new events of the DistributeEvent.
			EasyDistributeEvent easy = agent.getDistributeEvent();
			List<AsdrEvent> newOadrEvents = easy.findNewEvents(receivedEasy);
			final int nEvents = newOadrEvents.size();
			
			if (nEvents == 0) {
				// There are not new events. Nothing to do. Send the reply to the GridOperator.
				return createReplyGridOperator(request);
			} else if (nEvents > 1) {
				throw new RefuseException("Processing multiple events at the same time is not supported.");
			}
			
			newEvent = newOadrEvents.get(0);
			if (!newEvent.getSignalType().value().equals("level")) {
				throw new RefuseException("Only signals of the type 'level' are supported.");
			}
			

			if ((newEvent.getPriority() > 0) || !isCompatibleWithAuction(newEvent)) {
				// This event does not permit running internal auctions. It has to be applied as such.
				easy.addEvent(newEvent, modelTimestamp);
				SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
						agent,
						agent.getPlatform().getBrokerAgentsAID(agent, agent.getContainerName()),
						new ACLMessage(ACLMessage.REQUEST),
						new DistributeEventAction(
								new DistributeEventConcept(
										easy.getMarketContext(), easy.toString()), modelTimestamp));
				sendBehaviour.registerHandleAllResultNotifications(new ReceiveNoAuctionDistributeEventNotification());
				agent.addBehaviour(sendBehaviour);

			} else {
				// Auctions can be runned.
				Behaviour auctionBehaviour = new AuctionManagerBehaviour(action);
				agent.addBehaviour(auctionBehaviour);
			}
			
			return null;
		}
		
		
		/**
		 * Check if the duration of the event and the duration of the intervals
		 * is compatible with the duration of the auction's blocks.
		 */
		private boolean isCompatibleWithAuction(AsdrEvent event) {
			
			if (((event.getDuration() * 1000) % (float)BLOCK_SECONDS) != 0) {
				return false;
			}
			
			for (AsdrInterval interval : event.getIntervals()) {
				if ((interval.getDuration() % (float)BLOCK_SECONDS) != 0) {
					return false;
				}
			}
			
			return true;
		}
		
		
		/**
		 * Send a DistributeEvent message to the specified agent.
		 */
		private void sendDistributeEvent(Collection<AID> aids, AsdrEvent event) {
			
			
			for (AID receiverAID : aids) {
				agent.addChildEvent(receiverAID, NotificationUtils.toEventConcept(event));

				// Add the child events to the Easy message before it is sent.
				EasyDistributeEvent easy = agent.getDistributeEvent();
				List<String> childEventsIds = new ArrayList<String>();
				for (EventConcept childEvent : agent.getChildEvents(receiverAID)) {
					childEventsIds.add(easy.addEvent(toAsdrEvent(childEvent), modelTimestamp));
				}

				SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
						agent,
						new AID[]{receiverAID},
						new ACLMessage(ACLMessage.REQUEST),
						new DistributeEventAction(
								new DistributeEventConcept(easy.getMarketContext(), easy.toString()), modelTimestamp));
				sendBehaviour.registerHandleAllResultNotifications(new ReceiveAuctionDistributeEventNotification());

				// Remove the child events from the Easy message.
				for (String childEventId : childEventsIds) {
					easy.removeEvent(childEventId);
				}

				agent.addBehaviour(sendBehaviour);
			}
			
			expectedNotifications += aids.size();

			return;
		}
		
		
		/**
		 * Send the reply to the GridOperator.
		 */
		private ACLMessage createReplyGridOperator(ACLMessage request) {
			// There are not new events. Nothing to do. Send the reply to the GridOperator.
			ACLMessage reply = request.createReply();
			reply.setPerformative(ACLMessage.INFORM);
			try {
				agent.getContentManager().fillContent(
						reply,
						new CreatedEventPredicate(
								new EiResponseConcept(EiResponseConcept.CODE_OK, request.getConversationId())));
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to fill the content of the message.");
			}
			
			return reply;
		}
		
		
		/**
		 * Create a new name for a DF Agent.
		 */
		private String dfName(int n) {
			return "df" + n;
		}
		
		
		/**
		 * Create the DF Agents.
		 */
		private void createDFAgents() {
			this.dfAgents = new AgentController[N_DF_AGENTS];
			Object[] args = new Object[] {jadeConfigFilePath}; 
			for (int n = 0; n < N_DF_AGENTS; n++) {
				try {
					AgentController ac = agent.getContainerController().createNewAgent(
							dfName(n),
							EnergyAgentsDF.class.getName(),
							args);
					ac.start();
					dfAgents[n] = ac;
				} catch (StaleProxyException ex) {
					getLogger().log(Level.SEVERE, "ASPEM: Failed to create the Directory Facilitator agent.", ex);
					throw new IllegalStateException(ex);
				}
			}
			
			return;
		}
		
		
		/**
		 * Kill the DF Agents.
		 */
		private void killDFAgents() {
			for (int n = 0; n < N_DF_AGENTS; n++) {
				try {
					dfAgents[n].kill();
				} catch (StaleProxyException ex) {
					getLogger().log(Level.WARNING, "ASPEM: Failed to kill the Directory Facilitator agent.", ex);
				}
			}
			
			return;
		}
		
		
		/**
		 * 
		 * 
		 * Behaviour responsible for initiating the auction process, as well as for replying the Grid Operator
		 * when it has finished.
		 * 
		 * 
		 */
		private class AuctionManagerBehaviour extends Behaviour {
			
			private boolean[] auctionsFinished;
			private boolean auctionInitiated = false;
			private final DistributeEventAction action;
			
			
			/**
			 * Constructor.
			 */
			public AuctionManagerBehaviour(DistributeEventAction action) {
				this.action = action;
				this.auctionsFinished = new boolean[N_DF_AGENTS];
				return;
			}
			
			
			/**
			 * Tell whether all DFs have finished their corresponding auctions.
			 */
			public boolean allDFsAuctionsFinished() {
				for (int n = 0; n < N_DF_AGENTS; n++) {
					if (!auctionsFinished[n]) {
						return false;
					}
				}
				
				return true;
			}
			
			
			/**
			 * Tell when the auction has finished.
			 */
			@Override
			public boolean done() {
				if (allDFsAuctionsFinished() && (expectedNotifications == receivedNotifications)) {
					
					killDFAgents();
					agent.send(createReplyGridOperator(request));
					
					// The behaviour is over.
					return true;
				}
				
				return false;
			}

			
			/**
			 * Manage the whole auction process.
			 */
			@Override
			public void action() {
				
				if (auctionInitiated) {
					// The auction has already been initiated.
					return;
				}
				
				auclogger.log("# Inside Managing the auction!!!");
				
				// Create the DF Agents.
				createDFAgents();
				
				// Send the prepare auction message to all the agents.
				AID[][] brokersDF = distributeBrokersAmongDFs();
				++idAuction;
				for (int nDF = 0; nDF < dfAgents.length; nDF++) {
					String dfName;
					try {
						dfName = dfAgents[nDF].getName();
					} catch (StaleProxyException ex) {
						getLogger().log(Level.SEVERE, "Failed to get the name of the DF Agent.", ex);
						throw new IllegalStateException(ex);
					}
					
					SendRequestBehaviour prepareBehaviour = new SendRequestBehaviour(
							agent,
							brokersDF[nDF],
							new ACLMessage(ACLMessage.REQUEST),
							new PrepareAuctionAction(
									idAuction,
									action.getStoreName(),
									dfName,
									newEvent.getStart(),
									newEvent.getEndTime(),
									NotificationUtils.toIntervalConcept(newEvent.getIntervals())));
					prepareBehaviour.registerHandleAllResultNotifications(
							new ReceivePrepareAuctionNotification(nDF, prepareBehaviour.ALL_RESULT_NOTIFICATIONS_KEY));
					
					agent.addBehaviour(prepareBehaviour);					
				}

				auctionInitiated = true;
				
				return;
			}
			
			
			/**
			 * Distribute the brokers AIDs among all the DFs.
			 */
			private AID[][] distributeBrokersAmongDFs() {
				
				final AID[] brokersAids = agent.getPlatform().getBrokerAgentsAID(agent, agent.getContainerName());
				final int N_BROKERS = brokersAids.length;
				
				AID[][] dfAids = new AID[N_DF_AGENTS][];
				{
					final int nBrokersPerDF = N_BROKERS / N_DF_AGENTS;
					final int module = N_BROKERS % N_DF_AGENTS;
					if (module == 0) {
						for (int n = 0; n < N_DF_AGENTS; n++) {
							dfAids[n] = new AID[nBrokersPerDF];
						}
					} else {
						for (int n = 0; n < N_DF_AGENTS; n++) {
							if (n < module) {
								dfAids[n] = new AID[nBrokersPerDF + 1];
							} else {
								dfAids[n] = new AID[nBrokersPerDF];
							}
						}
					}
				}
				

				for (int nBroker = 0, nItem = 0; nBroker < N_BROKERS; nBroker++) {
					int nDf = nBroker % N_DF_AGENTS;
					dfAids[nDf][nItem] = brokersAids[nBroker];
					
					if (nDf == (N_DF_AGENTS - 1)) {
						nItem++;
					}
				}
				
				return dfAids;
			}
			
			
			/**
			 * 
			 * 
			 * Receive the notifications corresponding to the PrepareAuction request.
			 * 
			 * 
			 */
			private class ReceivePrepareAuctionNotification extends OneShotBehaviour {
				
				private final String notificationsKey;
				private final int nDF;
				
				
				/**
				 * Constructor.
				 */
				public ReceivePrepareAuctionNotification( int nDF, String notificationsKey) {
					this.notificationsKey = notificationsKey;
					this.nDF = nDF;
					return;
				}

				
				/**
				 * Send the corresponding messages to the brokers according to whether they participate or not
				 * in the Auction.
				 */
				@SuppressWarnings("unchecked")
				@Override
				public void action() {
					
					if (agent.getTracker() != null) {
						agent.getTracker().prepareStageCompleted();
					}
					
					auclogger.log("# Received the notification for the PrepareAuction!!!");
					
					Vector<ACLMessage> notifsBrokers = (Vector<ACLMessage>)this.getDataStore().get(this.notificationsKey);
					
					ArrayList<AID> consumersAIDs = new ArrayList<AID>();
					ArrayList<AID> producersAIDs = new ArrayList<AID>();
					ArrayList<AID> leavingAIDs = new ArrayList<AID>();
					for (ACLMessage notifBroker : notifsBrokers) {
						ReadyAuctionPredicate predicate;
						try {
							predicate = (ReadyAuctionPredicate)agent.getContentManager().extractContent(notifBroker);
						} catch (Exception ex) {
							getLogger().log(Level.SEVERE, "Failed to extract the content of ReadyAuctionPredicate.");
							throw new IllegalStateException();
						}
						
						AuctionRoleConcept role = predicate.getRole();
						final AID brokerAID = notifBroker.getSender();
						
						final boolean isConsumer = role.consumer();
						final boolean isProducer = role.producer();
						if (isConsumer || isProducer) {
							if (isConsumer) {
								consumersAIDs.add(brokerAID);
							}
							if (isProducer) {
								producersAIDs.add(brokerAID);
							}
						} else {
							// This agent is not participating in the auction. It leaves the auction.
							leavingAIDs.add(brokerAID);
						}
					}
					
					if ((consumersAIDs.size() == 0) || (producersAIDs.size() == 0)) {
						// Abort the auction.
						Set<AID> allAIDs = new HashSet<AID>();
						allAIDs.addAll(producersAIDs);
						allAIDs.addAll(consumersAIDs);
						allAIDs.addAll(leavingAIDs);
						SendRequestBehaviour abortBehaviour = new SendRequestBehaviour(
								agent,
								allAIDs.toArray(new AID[0]),
								new ACLMessage(ACLMessage.REQUEST),
								new AbortAuctionAction(idAuction));
						abortBehaviour.registerHandleAllResultNotifications(
								new ReceiveAbortAuctionNotification(nDF, abortBehaviour.ALL_RESULT_NOTIFICATIONS_KEY));
						agent.addBehaviour(abortBehaviour);
						return;
					}
					
					
					// Send the DistributeEvent to the brokers that are not participating in the auction anymore.
					if (leavingAIDs.size() > 0) {
						sendDistributeEvent(leavingAIDs, newEvent);
					}
					
					auclogger.log("# Sending the message for the Start stage.");					
					
					// Send the StartAuction message to the consumers.
					SendRequestBehaviour startBehaviour = new SendRequestBehaviour(
							agent,
							consumersAIDs.toArray(new AID[0]),
							new ACLMessage(ACLMessage.REQUEST),
							new StartAuctionAction(idAuction, newEvent.getStart()));
					startBehaviour.registerHandleAllResultNotifications(
							new ReceiveStartAuctionNotifications(nDF, producersAIDs, startBehaviour.ALL_RESULT_NOTIFICATIONS_KEY));
					agent.addBehaviour(startBehaviour);						
						
					return;
				}
			}
			
			

			/**
			 * 
			 * 
			 * Receive the notifications corresponding to the StartAuction request.
			 * 
			 * 
			 */		
			private class ReceiveStartAuctionNotifications extends OneShotBehaviour {
				
				private final List<AID> producersAIDs;
				private final int nDF;				
				private final String notificationsKey;
				
				/**
				 * Constructor.
				 */
				public ReceiveStartAuctionNotifications(int nDF, List<AID> producersAIDs, String notificationsKey) {
					this.nDF = nDF;
					this.producersAIDs = producersAIDs;
					this.notificationsKey = notificationsKey;
					return;
				}

				@Override
				public void action() {
					
					if (agent.getTracker() != null) {
						agent.getTracker().startStageCompleted();
					}
					
					auclogger.log("# Received the notification for the StartAuction!!!");					
					
					@SuppressWarnings("unchecked")
					Vector<ACLMessage> notifsBrokers =
							(Vector<ACLMessage>)this.getDataStore().get(this.notificationsKey);
					
					Set<AID> leavingBrokers = new HashSet<AID>();
					Set<AID> nextStageBrokers = new HashSet<AID>();
					for (ACLMessage msg : notifsBrokers) {
						boolean passToClearStage;
						try {
							passToClearStage = 
									((ResultStartAuctionStagePredicate)agent.getContentManager().extractContent(msg)).getPassToClearStage();
						} catch (Exception ex) {
							getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
							throw new IllegalStateException();
						}
						
						if (passToClearStage) {
							nextStageBrokers.add(msg.getSender());
						} else {
							// This broker doesn't participate anymore in the auction.
							// The DistributeEvent is sent to it.
							leavingBrokers.add(msg.getSender());
						}
					}
					
					if (nextStageBrokers.size() == 0) {
						// There are not consumers willing to pass to the next stage. The auction is aborted.
						leavingBrokers.addAll(producersAIDs);
						SendRequestBehaviour abortBehaviour = new SendRequestBehaviour(
								agent,
								leavingBrokers.toArray(new AID[0]),
								new ACLMessage(ACLMessage.REQUEST),
								new AbortAuctionAction(idAuction));
						abortBehaviour.registerHandleAllResultNotifications(
								new ReceiveAbortAuctionNotification(nDF, abortBehaviour.ALL_RESULT_NOTIFICATIONS_KEY));
						agent.addBehaviour(abortBehaviour);
						return;					
					}
					
					
					// Send the event to the brokers that do not continue in the auction.
					leavingBrokers.removeAll(producersAIDs);
					if (leavingBrokers.size() > 0) {
						sendDistributeEvent(leavingBrokers, newEvent);
					}
					
					auclogger.log("# Before sending the ClearAuctionAction.");
					
					// Send the ClearAuction message to the brokers that continue in the auction.
					nextStageBrokers.addAll(producersAIDs);
					final int remainingClearRounds = MAX_CLEAR_ROUNDS - 1;
					SendRequestBehaviour sendRequest = new SendRequestBehaviour(
							agent,
							nextStageBrokers.toArray(new AID[0]),
							new ACLMessage(ACLMessage.REQUEST),
							new ClearAuctionAction(idAuction, remainingClearRounds));
					sendRequest.registerHandleAllResultNotifications(
							new ReceiveClearStageNotificationBehaviour(nDF, sendRequest.ALL_RESULT_NOTIFICATIONS_KEY));
					agent.addBehaviour(sendRequest);					
					
					return;
				}
			}
			
			
			/**
			 * 
			 * 
			 * Manage the Clear stage of the auctions.
			 * 
			 * 
			 */
			private class ReceiveClearStageNotificationBehaviour extends OneShotBehaviour {
				
				private final int nDF;
				private final String notificationsKey;
				
				
				/**
				 * Constructor.
				 */
				public ReceiveClearStageNotificationBehaviour(int nDF, String notificationsKey) {
					this.nDF = nDF;
					this.notificationsKey = notificationsKey;
					return;
				}

				
				/**
				 * 
				 */
				@Override
				public void action() {
					
					if (agent.getTracker() != null) {
						agent.getTracker().clearStageCompleted();
					}
					
					auclogger.log("# Received the notification for the Clear stage.");
					
					@SuppressWarnings("unchecked")
					Vector<ACLMessage> msgs = (Vector<ACLMessage>)this.getDataStore().get(this.notificationsKey);
					AID[] brokersAIDs = new AID[msgs.size()];				
					Iterator<ACLMessage> itNotifs = msgs.iterator();
					for (int nBroker = 0; itNotifs.hasNext(); nBroker++) {
						brokersAIDs[nBroker] = itNotifs.next().getSender();
					}
					
					SendRequestBehaviour sendRequest = new SendRequestBehaviour(
							agent,
							brokersAIDs,
							new ACLMessage(ACLMessage.REQUEST),
							new FinishAuctionAction(
									NotificationUtils.toEventConcept(newEvent), modelTimestamp));
					sendRequest.registerHandleAllResultNotifications(
							new ReceiveFinishAuctionNotificationBehaviour(nDF));
					agent.addBehaviour(sendRequest);				
					
					
					return;
				}
			}
			
			
			/**
			 * 
			 * 
			 * Receive the notifications corresponding to the AbortAuction request.
			 * 
			 * 
			 */
			public class ReceiveAbortAuctionNotification extends OneShotBehaviour {
				
				private final int nDF;
				private final String notificationsKey;
				
				
				/**
				 * Constructor.
				 */
				public ReceiveAbortAuctionNotification(int nDF, String notificationsKey) {
					this.nDF = nDF;
					this.notificationsKey = notificationsKey;
					return;
				}

				
				/**
				 * Enable the flag that indicates that the auction is over, and also sends
				 * the DistributeEvent to all the agents.
				 */
				@Override
				public void action() {
					
					@SuppressWarnings("unchecked")
					Vector<ACLMessage> notifsBrokers =
							(Vector<ACLMessage>)this.getDataStore().get(this.notificationsKey);
					
					List<AID> aids = new ArrayList<AID>();
					for (ACLMessage msg : notifsBrokers) {
						aids.add(msg.getSender());
					}
					
					sendDistributeEvent(aids, newEvent);					
					auctionsFinished[nDF] = true;

					return;
				}
			}			
			

			/**
			 * 
			 * 
			 * Receive the notifications of the Finish action.
			 * 
			 * 
			 */
			private class ReceiveFinishAuctionNotificationBehaviour extends OneShotBehaviour {
				
				private final int nDF;				
				
				
				/**
				 * Constructor.
				 */
				public ReceiveFinishAuctionNotificationBehaviour(int nDF) {
					this.nDF = nDF;
					return;
				}
				
				/**
				 * Close the auction resources and inform the Grid Operator about the end of the process.
				 */
				@Override
				public void action() {
					auctionsFinished[nDF] = true;
					return;
				}
			}
		}
		
		
		
		/**
		 * 
		 * 
		 * Receive the notifications from the brokers that had to leave the auction at some point.
		 * 
		 * 
		 */
		private class ReceiveAuctionDistributeEventNotification extends OneShotBehaviour {
			
			/**
			 * Send the notification INFORM message to the GridOperator.
			 */
			@Override
			public void action() {
				receivedNotifications++;
				return;
			}
		}
		
		
		/**
		 * 
		 * 
		 * Receive the notifications from the brokers when the auction is not launched.
		 * 
		 * 
		 */
		private class ReceiveNoAuctionDistributeEventNotification extends OneShotBehaviour {
			
			/**
			 * Send the notification INFORM message to the GridOperator.
			 */
			@Override
			public void action() {
				
				// Send the reply to the Grid Operator.
				ACLMessage reply = request.createReply();
				reply.setPerformative(ACLMessage.INFORM);
				try {
					agent.getContentManager().fillContent(
							reply,
							new CreatedEventPredicate(
									new EiResponseConcept(EiResponseConcept.CODE_OK, request.getConversationId())));
				} catch (Exception ex) {
					getLogger().log(Level.SEVERE, "Failed to fill the content of the message.");
				}
				
				agent.send(reply);
				return;
			}
		}		
	}

}
