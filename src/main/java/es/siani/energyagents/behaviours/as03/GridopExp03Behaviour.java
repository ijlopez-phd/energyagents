package es.siani.energyagents.behaviours.as03;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import com.google.inject.Injector;

import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.behaviours.as00.GridopProcessBehaviour;
import es.siani.energyagents.behaviours.as00.SendRequestBehaviour;
import es.siani.energyagents.config.Configuration;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.ontology.as00.DistributeEventAction;
import es.siani.energyagents.ontology.as00.DistributeEventConcept;
import es.siani.energyagents.ontology.as00.EventConcept;
import es.siani.energyagents.ontology.as00.ScheduleConcept;
import es.siani.energyagents.utils.NotificationUtils;
import es.siani.simpledr.EasyDistributeEvent;

@SuppressWarnings("serial")
public class GridopExp03Behaviour extends GridopProcessBehaviour {
	
	private static final String ALGORITHM_NAME = "yos";
	
	
	private boolean isDone;
	private boolean waitingForReply;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public GridopExp03Behaviour() {
		isDone = false;
		waitingForReply = false;
		return;
	}
	
	
	/**
	 * Setup of the behaviour before it starts running.
	 */
	@Override
	public void init(
			long idSimulation, EnergyAgent agent,
			ScheduleConcept schedule, String scenarioCode, String storeName,
			Behaviour tearDownBehaviour) {
		
		super.init(idSimulation, agent, schedule, scenarioCode, storeName, tearDownBehaviour);
		
		Injector injector = InjectionUtils.injector();
		Configuration config = injector.getInstance(Configuration.class);
		
		AuctionLogger auctionLogger = injector.getInstance(AuctionLogger.class);
		auctionLogger.init(
				idSimulation,
				ALGORITHM_NAME,
				Integer.parseInt(config.get(Configuration.PROP_AUCTION_DF_AGENTS)),
				Float.parseFloat(config.get(Configuration.PROP_AUCTION_OVERBOOKING_FACTOR)));
		
		return;
	}

	
	/**
	 * Tell if the behaviour has finished its operation.
	 */
	@Override
	public boolean done() {
		return isDone;
	}
	
	
	/**
	 * Buisness logic of the Grid Operator.
	 */
	@Override
	public void action() {
		
		if (waitingForReply) {
			// The message has already been sent to the ASPEMs.
			return;
		}
		
		
		// Parent's checks.
		super.action();
		
		// Check both if there are events to be sent and there are ASPEMs enabled.
		AID[] aspemsAIDs = agent.getPlatform().getAspemAgentsAID(agent);
		if ((schedule.getEvents().size() == 0) || (aspemsAIDs.length == 0)) {
			if (tearDownBehaviour != null) {
				agent.addBehaviour(tearDownBehaviour);
			}
			isDone = true;
			return;
		} else if (schedule.getEvents().size() > 1) {
			getLogger().log(Level.SEVERE, "Processing multiple events at the same time is not supported.");
			throw new UnsupportedOperationException();
		}
		
		SequentialBehaviour seqBehaviour = new SequentialBehaviour(agent);
		final long currentModelTime = schedule.getCurrentModelTime();
		EventConcept event = schedule.getEvents().get(0);
		if (event.getSignalType().equals("level")) {
			
			// Update the DistributeEvent associated to the Gridop agent.
			this.agent.updateDistributeEvent(currentModelTime, null);
			EasyDistributeEvent easy = agent.getDistributeEvent();
			easy.addEvent(NotificationUtils.toAsdrEvent(event), currentModelTime);
			
			// Forward the message to all the ASPEMs.
			SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
					agent,
					aspemsAIDs,
					new ACLMessage(ACLMessage.REQUEST),
					new DistributeEventAction(
							new DistributeEventConcept(agent.getPlatform().getId(), easy.toString()),
							schedule.getCurrentModelTime(),
							storeName));
			sendBehaviour.registerHandleAllResultNotifications(new FinishGridopExp03Behaviour());
			seqBehaviour.addSubBehaviour(sendBehaviour);
		}
		
		if (tearDownBehaviour != null) {
			seqBehaviour.addSubBehaviour(tearDownBehaviour);
		}
		agent.addBehaviour(seqBehaviour);
		
		waitingForReply = true;
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(GridopExp03Behaviour.class.getName());
	}
	
	
	/**
	 * Inner class.
	 * This behaviour is invoked in order to finish the main behaviour of the Gridop (GridopExp03Behaviour).
	 */
	private class FinishGridopExp03Behaviour extends OneShotBehaviour {

		/**
		 * Change the flag 'isDone' of the main behaviour to 'true'. It means the
		 * completion of that behaviour.
		 */
		@Override
		public void action() {
			GridopExp03Behaviour.this.isDone = true;
			return;
		}
	}
}
