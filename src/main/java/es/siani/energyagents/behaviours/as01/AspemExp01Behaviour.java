package es.siani.energyagents.behaviours.as01;

import jade.content.Predicate;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.stream.StreamSource;

import com.google.inject.Inject;

import es.siani.energyagents.behaviours.as00.AspemProcessBehaviour;
import es.siani.energyagents.behaviours.as00.SendRequestBehaviour;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.ontology.as00.CreatedEventPredicate;
import es.siani.energyagents.ontology.as00.DistributeEventAction;
import es.siani.energyagents.ontology.as00.DistributeEventConcept;
import es.siani.energyagents.ontology.as00.EiResponseConcept;
import es.siani.energyagents.ontology.as00.EventConcept;
import es.siani.energyagents.ontology.as00.IdleAction;
import es.siani.energyagents.ontology.as01.BrokerCreateReportAction;
import es.siani.energyagents.ontology.as01.BrokerUpdateReportPredicate;
import es.siani.energyagents.ontology.as01.CreateReportAction;
import es.siani.energyagents.ontology.as01.UpdateReportPredicate;
import es.siani.energyagents.utils.EnergyAgentsUtils;
import es.siani.energyagents.utils.NotificationUtils;
import es.siani.simpledr.EasyCreateReport;
import es.siani.simpledr.EasyDistributeEvent;
import es.siani.simpledr.EasyUpdateReport;
import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;
import es.siani.simpledr.model.AsdrSignalTypeEnum;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import static es.siani.energyagents.utils.NotificationUtils.*;

@SuppressWarnings("serial")
public class AspemExp01Behaviour extends AspemProcessBehaviour {
	
	static final int MODE_EASY = BrokerUpdateReportPredicate.MODE_EASY;
	static final int MODE_HARD = BrokerUpdateReportPredicate.MODE_HARD;
	static final int MODE_NORMAL = BrokerUpdateReportPredicate.MODE_NONE;
	
	private static int NO_LEVEL = 0;
	private static int IDX_LEVEL_0 = 0;
	private static int IDX_LEVEL_1 = 1;
	private static int IDX_LEVEL_2 = 2;
	private static int IDX_LEVEL_3 = 3;
	private static int IDX_EASY = 4;
	private static int IDX_HARD = 5;
	

	/**
	 * Constructor.
	 */
	@Inject
	public AspemExp01Behaviour() {
		return;
	}
	
	
	/**
	 * Indicates if the behaviour has finished its execution. If it returns false, then
	 * the action method may be called more than once.
	 */
	@Override
	public boolean done() {
		return true;
	}
	
	
	/**
	 * Handle the messages from the GridOperator (to this ASPEM) and manage the
	 * activity of the Brokers.
	 */
	@Override
	public void action() {
		super.action();
		
		// Behaviour that listens to requests of the Grid Operator.
		this.agent.addBehaviour(new ReceiveGridopRequestBehaviour(
				MessageTemplate.and(
						this.agent.getPlatform().createBasicMessageTemplate(),
						MessageTemplate.MatchSender(agent.getPlatform().getGridopAgentAID(this.agent)))));
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(AspemExp01Behaviour.class.getName());
	}	
	
	
	/**
	 * Inner class.
	 * ReceiveRequest from the Grid Operator. This behaviour, like all the behaviours of the type
	 *   AchieveREResponder are cyclic. That is, it is continuosly running (listening to requests).
	 */
	private class ReceiveGridopRequestBehaviour extends AchieveREResponder {
		
		/** Cache of the last UpdateReport sent by each broker agent. */
		private List<List<BrokerIntervalData>> brokerReportsCache = new ArrayList<List<BrokerIntervalData>>();
		
		/** Cache of the totals gotten from the UpdateReport sent by the broker agents. */
		private List<float[]> intervalTotalsCache = new ArrayList<float[]>();
		private List<float[]> hardPerLevel = new ArrayList<float[]>();
		
		/** Number of expected notifications from Distribute events. */
		private int expectedNotifications = 0;
		
		/** Number of notifications received from Distribute events. */
		private int receivedNotifications = 0;
		
		/**
		 * Constructor.
		 */
		public ReceiveGridopRequestBehaviour(MessageTemplate mt) {
			super(agent, mt);
			return;
		}
		
		
		/**
		 * Process a request received from the Grid Operator.
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			// Extract the content of the message.
			Predicate action;
			try {
				action = (Predicate)(agent.getContentManager().extractContent(request));
			} catch (Codec.CodecException | OntologyException ex) {
				getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
				throw new IllegalStateException();
			}
			
			SequentialBehaviour seqBehaviour = new SequentialBehaviour(agent);
			if (action instanceof CreateReportAction) { // Gridop is requesting a Report.
				CreateReportAction createReportAction = (CreateReportAction)action;
				EasyCreateReport easy = new EasyCreateReport(
						new StreamSource(new StringReader((createReportAction.getCreateReportMessage()))));
				SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
						agent,
						agent.getPlatform().getBrokerAgentsAID(agent, agent.getContainerName()),
						new ACLMessage(ACLMessage.REQUEST),
						new BrokerCreateReportAction(
								createReportAction.getScenarioCode(),
								createReportAction.getStoreName(),
								easy.getStartTime(),
								easy.getDuration()));
				
				sendBehaviour.registerHandleAllResultNotifications(
						new BrokerUpdateReportHandler(
								request, easy, createReportAction.getModelTimestamp(), sendBehaviour.ALL_RESULT_NOTIFICATIONS_KEY));
				
				seqBehaviour.addSubBehaviour(sendBehaviour);
				
			} else if (action instanceof DistributeEventAction) { // Gridop is sending a DistributeEvent.
				
				if (agent.getTracker() != null) {
					agent.getTracker().distributeEventReceived();
				}
				
				DistributeEventAction distributeAction = (DistributeEventAction)action;
				final long modelTimestamp = distributeAction.getModelTimestamp();
				EasyDistributeEvent receivedEasy = new EasyDistributeEvent(
						distributeAction.getDistributeEvent().getMarketContext(),
						new StreamSource(new StringReader(distributeAction.getDistributeEvent().getContent())));				
				
				// Find the new events of the DistributeEvent.
				EasyDistributeEvent easy = agent.getDistributeEvent();
				List<AsdrEvent> newOadrEvents = easy.findNewEvents(receivedEasy);
				if (newOadrEvents.size() > 0) {
					AsdrEvent newEvent = newOadrEvents.get(0); // Only one event is supported.
					final String signalType = newEvent.getSignalType().value();
					if (signalType.equals("level")) {
						easy.addEvent(newEvent, modelTimestamp);
						AID[] brokersAids = agent.getPlatform().getBrokerAgentsAID(agent, agent.getContainerName());
						expectedNotifications = 1;
						SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
								agent,
								brokersAids,
								new ACLMessage(ACLMessage.REQUEST),
								new DistributeEventAction(
										new DistributeEventConcept(
												easy.getMarketContext(), easy.toString()),
												modelTimestamp));
						sendBehaviour.registerHandleAllResultNotifications(new DistributeEventNotificationBehaviour(request));
						
						seqBehaviour.addSubBehaviour(sendBehaviour);
						
					} else if (signalType.equals("delta")) {
						seqBehaviour.addSubBehaviour(
								this.processDeltaSignal(easy, newEvent,  modelTimestamp, request));
						
					} else {
						throw new IllegalArgumentException("SignalType non-supported");
					}
				} else {
					// There aren't new events. Nothing to do. Send the reply to the GridOperator.
					seqBehaviour.addSubBehaviour(new DistributeEventNotificationBehaviour(request));
				}
				
			} else {
				throw new IllegalArgumentException("Action non-supported.");
			}
			
			agent.addBehaviour(seqBehaviour);
			
			return null;
		}
		
		
		/**
		 * Process a Delta signal and builds the DistributeEvent that must be sent to each broker agent.
		 * The DistributeEvent are of the type Level. 
		 */
		private SequentialBehaviour processDeltaSignal(
				EasyDistributeEvent easy, AsdrEvent newEvent, long modelTimestamp, ACLMessage request) {
			
			
			if ((brokerReportsCache.isEmpty()) || (intervalTotalsCache.isEmpty())) {
				throw new IllegalStateException("Caches are out of sync.");
			}
			
			
			// Calculate the assignments to the brokers.
			Assignments assignments = new Assignments();
			float[] intervalsPayload = newEvent.getIntervalsPayload();
			final int N_INTERVALS = intervalsPayload.length;
			for (int nInterval = 0; nInterval < N_INTERVALS; nInterval++) {
				
				float[] coveredPerMode = new float[3];
				
				final float delta = intervalsPayload[nInterval];
				float coveredDelta = 0;
				
				List<BrokerIntervalData> intervalCache = brokerReportsCache.get(nInterval);
				float[] totalsCache = intervalTotalsCache.get(nInterval);
				
				if (totalsCache[IDX_EASY] >= delta) {
					Collections.sort(intervalCache, new DescBrokerIntervalComparator());
					Iterator<BrokerIntervalData> itIntervalCache = intervalCache.iterator();
					while (coveredDelta < delta) {
						BrokerIntervalData bid = itIntervalCache.next();
						if ((bid.mode == MODE_EASY) && (bid.payload != NO_LEVEL)) {
							assignments.add(bid.aid, new BrokerAssignment(bid.payload, nInterval));
							coveredDelta += bid.loads[IDX_LEVEL_0] - bid.loads[bid.payload];
						}
					}
					
					coveredPerMode[MODE_EASY] += coveredDelta;
					
					
				} else if ((totalsCache[IDX_LEVEL_0] - totalsCache[IDX_LEVEL_3] - totalsCache[IDX_HARD]) >= delta) {
					
					// Find out which amount of load is protected for each level
					float[] hardPerLevel = new float[3];
					for (BrokerIntervalData bid : intervalCache) {
						if ((bid.mode == MODE_HARD) && (bid.payload > 0)) {
							hardPerLevel[bid.payload - 1] += bid.loads[bid.payload - 1] - bid.loads[bid.payload];
							for (int n = bid.payload + 1; n <= IDX_LEVEL_3; n++) {
								hardPerLevel[n - 1] += bid.loads[n - 1] - bid.loads[n]; 
							}
						}
					}
					
					int idxLevel = IDX_LEVEL_3;
					if ((totalsCache[IDX_LEVEL_0] - (totalsCache[IDX_LEVEL_1] + hardPerLevel[IDX_LEVEL_1 - 1])) >= delta) {
						idxLevel = IDX_LEVEL_1;
					} else if ((totalsCache[IDX_LEVEL_0] - (totalsCache[IDX_LEVEL_2] + hardPerLevel[IDX_LEVEL_2 - 1] + hardPerLevel[IDX_LEVEL_1 - 1])) >= delta) {
						idxLevel = IDX_LEVEL_2;
					}
					
//					final float expected = totalsCache[IDX_LEVEL_0] - (totalsCache[IDX_LEVEL_3] + hardPerLevel[0] + hardPerLevel[1] + hardPerLevel[2]);
					
					// Firstly, all the easy-loads are dispatched.
					for (BrokerIntervalData bid : intervalCache) {
						if (bid.mode == MODE_EASY) {
							
							int assignmentLevel = idxLevel;
							if (idxLevel <= bid.payload) {
								coveredDelta += bid.loads[IDX_LEVEL_0] - bid.loads[idxLevel];
								coveredPerMode[MODE_EASY] += bid.loads[IDX_LEVEL_0] - bid.loads[idxLevel];
								
								// Check if more easy load can be used.
								for (int n = idxLevel; (n < bid.payload) && (coveredDelta < delta); n++) {
									coveredDelta += bid.loads[n] - bid.loads[n + 1];
									coveredPerMode[MODE_EASY] += bid.loads[n] - bid.loads[n + 1];
									assignmentLevel = n + 1;
								}
							} else {
								coveredDelta += bid.loads[IDX_LEVEL_0] - bid.loads[bid.payload];
								coveredPerMode[MODE_EASY] += bid.loads[IDX_LEVEL_0] - bid.loads[bid.payload];
								
								coveredDelta += bid.loads[bid.payload] - bid.loads[idxLevel];
								coveredPerMode[MODE_NORMAL] += bid.loads[bid.payload] - bid.loads[idxLevel];
							}
							
							assignments.add(bid.aid, new BrokerAssignment(assignmentLevel, nInterval));							
							
							if (coveredDelta >= delta) {
								break;
							}
						}
					}
					

					// Next, the non-hard loads are dispatched.
					Collections.sort(intervalCache, new AscBrokerIntervalComparator());
					Iterator<BrokerIntervalData> itIntervalCache = intervalCache.iterator();
					while (coveredDelta < delta) {
						if (!itIntervalCache.hasNext()) {
							throw new IllegalStateException();
						}
						BrokerIntervalData bid = itIntervalCache.next();
						if (bid.mode == MODE_NORMAL) {
							assignments.add(bid.aid, new BrokerAssignment(idxLevel, nInterval));
							coveredDelta += bid.loads[IDX_LEVEL_0] - bid.loads[idxLevel];
							coveredPerMode[MODE_NORMAL] += bid.loads[IDX_LEVEL_0] - bid.loads[idxLevel];
						} else if (bid.mode == MODE_HARD) {
							if (bid.payload > idxLevel) {
								assignments.add(bid.aid, new BrokerAssignment(idxLevel, nInterval));
								coveredDelta += bid.loads[IDX_LEVEL_0] - bid.loads[idxLevel];
								coveredPerMode[MODE_NORMAL] += bid.loads[IDX_LEVEL_0] - bid.loads[idxLevel];
							} else if (bid.payload > IDX_LEVEL_1) {
								final int assignmentLevel = bid.payload - 1;
								assignments.add(bid.aid, new BrokerAssignment(assignmentLevel, nInterval));
								coveredDelta += bid.loads[IDX_LEVEL_0] - bid.loads[assignmentLevel];
								coveredPerMode[MODE_NORMAL] += bid.loads[IDX_LEVEL_0] - bid.loads[assignmentLevel];
							}
						}
					}
					
					
				} else { // It is necessary to resort to the hard loads.
					Collections.sort(intervalCache, new AscBrokerIntervalComparator());
					
					// First pass. All the non-hard load is requested.
					for (BrokerIntervalData bid : intervalCache) {
						if (bid.mode != MODE_HARD) {
							assignments.add(bid.aid, new BrokerAssignment(IDX_LEVEL_3, nInterval));
							coveredDelta += bid.loads[IDX_LEVEL_0] - bid.loads[IDX_LEVEL_3];
							
							if (bid.mode == MODE_EASY) {
								coveredPerMode[MODE_EASY] += bid.loads[IDX_LEVEL_0] - bid.loads[bid.payload];
								coveredPerMode[MODE_NORMAL] += bid.loads[bid.payload] - bid.loads[IDX_LEVEL_3];
							} else {
								coveredPerMode[MODE_NORMAL] += bid.loads[IDX_LEVEL_0] - bid.loads[IDX_LEVEL_3];
							}
						} else if (bid.payload > IDX_LEVEL_1) {
							assignments.add(bid.aid, new BrokerAssignment(bid.payload -1, nInterval));
							coveredDelta += bid.loads[IDX_LEVEL_0] - bid.loads[bid.payload - 1];
							coveredPerMode[MODE_NORMAL] += bid.loads[IDX_LEVEL_0] - bid.loads[bid.payload - 1];
						}
					}
					
					// Second pass. The hard-load is requested.
					Iterator<BrokerIntervalData> itIntervalCache = intervalCache.iterator();
					while ((coveredDelta < delta) && itIntervalCache.hasNext()) {
						BrokerIntervalData bid = itIntervalCache.next();
						BrokerAssignment assignment = assignments.getAssignment(bid.aid, nInterval);
						if (assignment == null) {
							// TODOIGNIMP: This distribution can be improved. May be it is not necessary
							// to set the level 3 for the loads. It can be used an approximative approach.
							assignments.add(bid.aid, new BrokerAssignment(IDX_LEVEL_3, nInterval));
							coveredDelta += bid.loads[IDX_LEVEL_0] - bid.loads[IDX_LEVEL_3];
							coveredPerMode[MODE_NORMAL] += bid.loads[IDX_LEVEL_0] - bid.loads[IDX_LEVEL_1];
							coveredPerMode[MODE_HARD] += bid.loads[IDX_LEVEL_1] - bid.loads[IDX_LEVEL_3];
						} else {
							coveredDelta -= bid.loads[IDX_LEVEL_0] - bid.loads[assignment.idxLevel];
							coveredDelta += bid.loads[IDX_LEVEL_0] - bid.loads[IDX_LEVEL_3];
							coveredPerMode[MODE_HARD] += bid.loads[assignment.idxLevel] - bid.loads[IDX_LEVEL_3];
							assignment.idxLevel = IDX_LEVEL_3;
						}
					}
				}
				
				Exp01Logger logger = InjectionUtils.injector().getInstance(Exp01Logger.class);
				logger.logIntervalInfo(agent.getLocalName(), nInterval, delta, coveredDelta, coveredPerMode);
				brokerReportsCache.get(nInterval).clear();
			}
			
			final AID[] BROKERS_AIDS = agent.getPlatform().getBrokerAgentsAID(agent, agent.getContainerName());
			EnergyAgentsUtils.log("ASPEM " + agent.getContainerName() + ": " + BROKERS_AIDS.length);
			expectedNotifications = BROKERS_AIDS.length;
			
			final String platformId = agent.getPlatform().getId();
			long[] intervalsDuration = newEvent.getIntervalsDuration();
			SequentialBehaviour seqBehaviour = new SequentialBehaviour(agent);
			final Map<AID, List<BrokerAssignment>> assignmentsMap = assignments.getValues();
			for (AID brokerAID : BROKERS_AIDS) {
				
				List<BrokerAssignment> brokerAssignments = assignmentsMap.get(brokerAID);
				
				// Check if this broker has assignments.
				if (brokerAssignments == null) {
					// A "no-op" is sent to this broker.
					SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
							agent,
							new AID[] {brokerAID},
							new ACLMessage(ACLMessage.REQUEST),
							new IdleAction());
					sendBehaviour.registerHandleAllResultNotifications(new DistributeEventNotificationBehaviour(request));
					seqBehaviour.addSubBehaviour(sendBehaviour);
					continue;
				}
				
				// This broker does have assignments, so DistributeEvent actions have to be built.
				
				// - Build the list of intervals for the broker agent.
				List<AsdrInterval> intervals = new ArrayList<AsdrInterval>();
				int idxNextInterval = 0;
				for (BrokerAssignment ba : brokerAssignments) {
					if (ba.nInterval != idxNextInterval) {
						// Fulfill the missing intervals. These auto-created intervals have payload 0 (means LEVEL_0).
						for (int n = idxNextInterval; n < ba.nInterval; n++) {
							intervals.add(new AsdrInterval(intervalsDuration[n], 0));
						}
					}
					intervals.add(new AsdrInterval(intervalsDuration[ba.nInterval], ba.idxLevel));
					idxNextInterval = ba.nInterval + 1;
				}
				for (int nInterval = idxNextInterval; nInterval < N_INTERVALS; nInterval++) {
					// Fulfill the last missing intervals. These auto-created intervals have payload 0.
					intervals.add(new AsdrInterval(intervalsDuration[nInterval], 0));
				}
				
				// - Build the Level event to be added to the DistributeEventAction of the broker.
				agent.addChildEvent(
						brokerAID,
						NotificationUtils.toEventConcept(
								new AsdrEvent(
										AsdrSignalTypeEnum.LEVEL,
										newEvent.getStart(),
										newEvent.getNotificationDuration(),
										newEvent.getPriority(),
										intervals)));
				
				// - Add child events to the Easy message.
				List<String> childEventsIds = new ArrayList<String>();
				for (EventConcept childEvent : agent.getChildEvents(brokerAID)) {
					childEventsIds.add(easy.addEvent(toAsdrEvent(childEvent), modelTimestamp));
				}
				
				
				// - Build the behaviour to be sent to the broker agent.
				SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
						agent,
						new AID[] {brokerAID},
						new ACLMessage(ACLMessage.REQUEST),
						new DistributeEventAction(
								new DistributeEventConcept(
										platformId,
										agent.getDistributeEvent().toString()),
								modelTimestamp));
				sendBehaviour.registerHandleAllResultNotifications(new DistributeEventNotificationBehaviour(request));
				seqBehaviour.addSubBehaviour(sendBehaviour);
				
				// Remove the child events from the Easy message.
				for (String childEventId : childEventsIds) {
					easy.removeEvent(childEventId);
				}
				
				//EnergyAgentsUtils.logBrokersLoadDistribution(brokerAID, intervals);
			}
			
			// Clear the cache until the next CreateReport message.
			brokerReportsCache.clear();
			intervalTotalsCache.clear();
			
			return seqBehaviour;
		}
		
		
		/**
		 * Inner class.
		 * Send to the GridOperator the reply to a DistributeEvent.
		 */
		private class DistributeEventNotificationBehaviour extends  OneShotBehaviour {
			
			private final ACLMessage requestMessage;
			
			
			/**
			 * Constructor.
			 */
			public DistributeEventNotificationBehaviour(ACLMessage requestMessage) {
				this.requestMessage = requestMessage;
				return;
			}

			
			/**
			 * Send the notification INFORM message to the GridOperator.
			 */
			@Override
			public void action() {

				if (++receivedNotifications < expectedNotifications) {
					return;
				}
				
				// All expected notifications have been received. The notification from the ASPEM
				// is sent to the Grid Operator.
				ACLMessage reply = requestMessage.createReply();
				reply.setPerformative(ACLMessage.INFORM);
				try {
					agent.getContentManager().fillContent(
							reply,
							new CreatedEventPredicate(
									new EiResponseConcept(EiResponseConcept.CODE_OK, requestMessage.getConversationId())));
				} catch (Exception ex) {
					getLogger().log(Level.SEVERE, "Failed to fill the content of the message.");
				}
				
				agent.send(reply);
				
				return;
			}
			
		}
		
		
		/**
		 * Inner class.
		 * This behaviour is invoked when all notifications for a message of the type CreateReport have
		 * been received. A CreateReport message has been sent for each interval.
		 */
		private class BrokerUpdateReportHandler extends OneShotBehaviour {
			
			private final String notificationsKey;
			private final ACLMessage requestMessage;
			private final EasyCreateReport easyCreateReport;
			private final long modelTimestamp;
			
			
			/**
			 * Constructor.
			 */
			public BrokerUpdateReportHandler(
					ACLMessage requestMessage, EasyCreateReport easyCreateReport, long modelTimestamp, String notificationsKey) {
				this.notificationsKey = notificationsKey;
				this.requestMessage = requestMessage;
				this.easyCreateReport = easyCreateReport;
				this.modelTimestamp = modelTimestamp;
				return;
			}
			
			
			/**
			 * Add up the load each Broker will demand for each level during the interval.
			 */
			@Override
			public void action() {
				
				@SuppressWarnings("unchecked")
				Vector<ACLMessage> notifsBrokers = 
					(Vector<ACLMessage>)this.getDataStore().get(this.notificationsKey);
				
				List<BrokerIntervalData> intervalsData = new ArrayList<BrokerIntervalData>();
				float totals[] = new float[6];
				Exp01Logger logger = InjectionUtils.injector().getInstance(Exp01Logger.class);
				for (ACLMessage msg : notifsBrokers) {
					try {
						BrokerUpdateReportPredicate predicate = 
								(BrokerUpdateReportPredicate)agent.getContentManager().extractContent(msg);
						float loadPerLevel[] = new float[4];
						loadPerLevel[IDX_LEVEL_0] = predicate.getLevel0();
						totals[IDX_LEVEL_0] += loadPerLevel[IDX_LEVEL_0];
						loadPerLevel[IDX_LEVEL_1] = predicate.getLevel1();
						totals[IDX_LEVEL_1] += loadPerLevel[IDX_LEVEL_1];
						loadPerLevel[IDX_LEVEL_2] = predicate.getLevel2();
						totals[IDX_LEVEL_2] += loadPerLevel[IDX_LEVEL_2];
						loadPerLevel[IDX_LEVEL_3] = predicate.getLevel3();
						totals[IDX_LEVEL_3] += loadPerLevel[IDX_LEVEL_3];
						if ((predicate.getMode() == BrokerUpdateReportPredicate.MODE_EASY) && (predicate.getSignalLevel() > 0)) {
							totals[IDX_EASY] += loadPerLevel[IDX_LEVEL_0] - loadPerLevel[(int)predicate.getSignalLevel()];
						} else if ((predicate.getMode() == BrokerUpdateReportPredicate.MODE_HARD) && (predicate.getSignalLevel() > 0)) {
							totals[IDX_HARD] += loadPerLevel[(int)predicate.getSignalLevel() - 1];
						}
						
						intervalsData.add(new BrokerIntervalData(msg.getSender(), predicate));
						
					} catch (Exception ex) {
						getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
						throw new IllegalStateException();
					}
				}
				
				// Update the cache with the notifications' data.
				brokerReportsCache.add(intervalsData);			
				intervalTotalsCache.add(totals);
				
				// Forward to the Grid Operator the data.
				//  - build the UpdateReport message.
				Map<String, Float> resources = new HashMap<String, Float>(totals.length);
				resources.put("level0", totals[IDX_LEVEL_0]);
				resources.put("level1", totals[IDX_LEVEL_1]);
				resources.put("level2", totals[IDX_LEVEL_2]);
				resources.put("level3", totals[IDX_LEVEL_3]);
				resources.put("easy", totals[IDX_EASY]);
				resources.put("hard", totals[IDX_HARD]);
				EasyUpdateReport easy = new EasyUpdateReport(agent.getLocalName(), easyCreateReport, resources);
				
				logger.logAspemTotals(agent.getLocalName(), totals);
				
				
				// Build the message and send it.
				ACLMessage reply = requestMessage.createReply();
				reply.setPerformative(ACLMessage.INFORM);					
				try {
					agent.getContentManager().fillContent(reply, new UpdateReportPredicate(easy, this.modelTimestamp));
				} catch (Exception ex) {
					getLogger().log(Level.SEVERE, "Failed to fill the content of the message.");
				}
			
				agent.send(reply);				
			}
		} // end of the inner class: UpdateReportHandler.

	} // end of the inner class: ReceiveGridopRequestBehaviour.
	
	
	/**
	 * Inner class.
	 * Store the amount of load that a broker agent will consume for each level, as well as
	 * the levels of the easy and hard loads, and the priority.
	 * This class is used during the process of the Delta signals.
	 * 
	 */
	private class BrokerIntervalData {
		public final AID aid;
		public final float[] loads;
		public final int payload;
		public final int priority;
		public final int mode;
		
		public BrokerIntervalData(AID aid, BrokerUpdateReportPredicate pred) {
			this.aid = aid;
			this.payload = pred.getSignalLevel();
			this.mode = pred.getMode();
			this.priority = pred.getPriority();
			this.loads = new float[] {pred.getLevel0(), pred.getLevel1(), pred.getLevel2(), pred.getLevel3()};
			return;
		}
	}
	
	
	/**
	 * Sort instances of BrokerIntervalData on the priority in ascending order.
	 *
	 */
	private class AscBrokerIntervalComparator implements Comparator<BrokerIntervalData> {
		@Override
		public int compare(BrokerIntervalData o1, BrokerIntervalData o2) {
			if (o1.priority < o2.priority) {
				return -1;
			} else if (o1.priority > o2.priority) {
				return 1;
			}

			return 0;
		}
	}
	
	
	/**
	 * Sort instances of BrokerIntervalData on the priority in descending order.
	 */
	private class DescBrokerIntervalComparator implements Comparator<BrokerIntervalData> {
		@Override
		public int compare(BrokerIntervalData o1, BrokerIntervalData o2) {
			if (o1.priority < o2.priority) {
				return 1;
			} else if (o1.priority > o2.priority) {
				return -1;
			}
			return 0;
		}
		
	}
	
	
	/**
	 * Value object that store for each broker the list of intervals to which
	 * it should apply a demand-response signal.
	 */
	private class Assignments {
		
		Map<AID, List<BrokerAssignment>> map = new HashMap<AID, List<BrokerAssignment>>();
		
		
		/**
		 * Add a new assignment to the broker's list.
		 */
		void add(AID aid, BrokerAssignment assignment) {
			List<BrokerAssignment> list = map.get(aid);
			if (list == null) {
				list = new ArrayList<BrokerAssignment>();
				map.put(aid, list);
			}
			
			list.add(assignment);
			
			return;
		}
		
		
		/**
		 * Get the assignment of a broker for a specific interval.
		 */
		BrokerAssignment getAssignment(AID aid, int nInterval) {
			List<BrokerAssignment> list = map.get(aid);
			if (list == null) {
				return null;
			}
			
			for (BrokerAssignment assignment : list) {
				if (assignment.nInterval == nInterval) {
					return assignment;
				}
			}
			
			return null;
		}
		
		
		/**
		 * Get the map that contains all the assignments.
		 */
		Map<AID, List<BrokerAssignment>> getValues() {
			return this.map;
		}
	}
	
	
	/**
	 * Value object that stores the amount of load (delta) that a broker has to shed
	 * during an interval.
	 *
	 */
	private class BrokerAssignment {

		public int idxLevel;
		public final int nInterval;
		
		/**
		 * Constructor.
		 */
		public BrokerAssignment(int idxLevel, int nInterval) {
			this.idxLevel = idxLevel;
			this.nInterval = nInterval;
		}
	}
}