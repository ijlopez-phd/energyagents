package es.siani.energyagents.behaviours.as01;

import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import es.siani.energyagents.EnergyAgentsGlobals;
import es.siani.energyagents.behaviours.as04.BrokerExp04Behaviour;

public class Exp01Logger {
	
	private static final long NO_ID = -999;
	private long currentId = NO_ID;	
	private String loggerRootPath;
	private Logger logger;
	
	
	/**
	 * Constructor.
	 */
	public Exp01Logger() {
		
		loggerRootPath = EnergyAgentsGlobals.AUCTION_LOGGING_PATH;
		if (loggerRootPath.startsWith("//")) {
			// This is an internal path.
			loggerRootPath = BrokerExp04Behaviour.class.getResource(loggerRootPath.substring(1)).getFile();
		}
		if (!loggerRootPath.endsWith("/")) {
			loggerRootPath += "/";
		}		
	}
	
	
	/**
	 * Init the logging handler used during the simulation.
	 */
	public void init(long id) {
		
		if (id == this.currentId) {
			return;
		} else if (this.currentId != NO_ID) {
			this.close();
		}
		
		
		this.logger = Logger.getLogger(Exp01Logger.class.getName() + "-" + id);
		logger.setUseParentHandlers(false);
		
		try {
			FileHandler fh = new FileHandler(loggerRootPath + "exp01_" + id + "-%u.log");
			fh.setLevel(Level.INFO);
			fh.setFormatter(new Exp01LoggerFormatter());
			logger.addHandler(fh);
		} catch (Exception e) {
			getModuleLogger().log(Level.WARNING, "Failed to create the Exp01's log file.");
		}
		
		this.currentId = id;
		
		return;
	}
	
	
	/**
	 * Close the active handler.
	 */
	public void close() {
		
		if (logger == null) {
			return;
		}
		
		for (Handler handler : this.logger.getHandlers()) {
			handler.close();
		}
		
		this.logger = null;
		this.currentId = NO_ID;
		
		return;
	}
	
	
	
	/**
	 * Log a string.
	 */
	public void log(String content) {
		logger.log(Level.INFO, content);
		return;
	}
	
	
	
	/**
	 * Log broker action.
	 */
	public void logBrokerResult(String agentName, int action) {
		StringBuilder sb = new StringBuilder();
		sb.append(agentName);
		sb.append(",");
		sb.append(action);
		
		logger.log(Level.INFO, sb.toString());
		
		return;
	}
	
	
	/**
	 * Log totals of each type of load.
	 */
	public void logAspemTotals(String agentName, float[] totals) {
		StringBuilder sb = new StringBuilder();
		String[] suffix = new String[] {"0", "1", "2", "3", "EASY", "HARD"};
		sb.append(agentName);
		for (int n = 0; n < 6; n++) {
			sb.append(", l");
			sb.append(suffix[n]);
			sb.append(": ");
			sb.append(totals[n]);
		}
		
		logger.log(Level.INFO, sb.toString());
		
		return;
	}
	
	
	/**
	 * Log info of intervals.
	 */
	public void logIntervalInfo(String agentName, int nInterval, float delta, float coveredDelta, float[] coveredPerMode) {
		StringBuilder sb = new StringBuilder();
		sb.append("# INT, ");
		sb.append(nInterval);
		sb.append(", ");
		sb.append(delta);
		sb.append(", ");
		sb.append(coveredDelta);
		sb.append(", e:");
		sb.append(coveredPerMode[AspemExp01Behaviour.MODE_EASY]);
		sb.append(", h:");
		sb.append(coveredPerMode[AspemExp01Behaviour.MODE_HARD]);
		sb.append(", n:");
		sb.append(coveredPerMode[AspemExp01Behaviour.MODE_NORMAL]);
		
		logger.log(Level.INFO, sb.toString());
		
		return;
	}
	
	
	/**
	 * Log the broker action.
	 */
	public void logBrokerAction(String agentName, int payload, float delta) {
		StringBuilder sb = new StringBuilder();
		sb.append(agentName);
		sb.append(",");
		sb.append(payload);
		sb.append(",");
		sb.append(delta);
		
		logger.log(Level.INFO, sb.toString());
		
		return;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getModuleLogger() {
		return Logger.getLogger(Exp01Logger.class.getName());
	}

}


/**
 * 
 * 
 * Formatter to log the results of the auction.
 * 
 *
 */
class Exp01LoggerFormatter extends Formatter {

	@Override
	public String format(LogRecord record) {
		String msg = record.getMessage();
		if (msg == null) {
			return "";
		}
		return msg + "\n";
	}
	
}
