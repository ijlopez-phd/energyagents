package es.siani.energyagents.behaviours.as01;

import jade.content.Predicate;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;

import java.io.StringReader;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.stream.StreamSource;

import com.google.inject.Inject;
import es.siani.energyagents.agent.AgentTracker;
import es.siani.energyagents.behaviours.as00.BrokerProcessBehaviour;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.model.UserPrefs;
import es.siani.energyagents.ontology.as00.CreatedEventPredicate;
import es.siani.energyagents.ontology.as00.DistributeEventAction;
import es.siani.energyagents.ontology.as00.EiResponseConcept;
import es.siani.energyagents.ontology.as00.IdleAction;
import es.siani.energyagents.ontology.as01.BrokerCreateReportAction;
import es.siani.energyagents.ontology.as01.BrokerUpdateReportPredicate;
import es.siani.energyagents.services.LoadForecastService;
import es.siani.energyagents.utils.CronLevelsScheduleStore;
import es.siani.energyagents.utils.CronScheduleStore;
import es.siani.simpledr.EasyDistributeEvent;
import es.siani.simpledr.model.AsdrEvent;

@SuppressWarnings("serial")
public class BrokerExp01Behaviour extends BrokerProcessBehaviour {
	
	private float[] loadPerLevel = null; /** for logging purposes */
	
	/**
	 * Constructor.
	 */
	@Inject
	public BrokerExp01Behaviour() {
		return;
	}
	
	
	/**
	 * Indicate if the behaviour has finished its execution. If it returns false, then
	 * the 'action' method may be called more than once.
	 */
	@Override
	public boolean done() {
		return true;
	}	

	
	/**
	 * Handle the messages from the parent ASPEM.
	 */
	@Override
	public void action() {
		
		// Add behaviour that listens to the requests of the ASPEM.
		this.agent.addBehaviour(new ReceiveAspemRequestBehaviour(this.agent.getPlatform().createBasicMessageTemplate()));
		
		return;
	}
	
	
	/**
	 * Logger.
	 *
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(BrokerExp01Behaviour.class.getName());
	}
	
	
	/**
	 * Track distribute events.
	 */
	private void trackDistributeEvent(AsdrEvent event) {
		AgentTracker tracker = agent.getTracker();
		if (tracker == null) {
			return;
		}
		
		tracker.distributeEventReceived();
		if (event != null) {
			tracker.distributeEvent(event.getIntervalsPayload());
		}
		
		return;
	}	
	
	
	/**
	 * Inner class.
	 * Receive requests from the ASPEM. This behaviour, like all the behaviours of the type
	 *   AchieveREResponder is cyclic.
	 *
	 */
	private class ReceiveAspemRequestBehaviour extends AchieveREResponder {

		/**
		 * Constructor.
		 */
		public ReceiveAspemRequestBehaviour(MessageTemplate mt) {
			super(agent, mt);
			return;
		}
		
		
		/**
		 * Process a request received from the ASPEM.
		 */
		@Override
		protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
			
			// Extract the content of the message.
			Predicate action;
			try {
				action = (Predicate)agent.getContentManager().extractContent(request);
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
				throw new IllegalStateException();
			}
			
			Predicate replyPredicate = null;
			if (action instanceof BrokerCreateReportAction) {
				BrokerCreateReportAction createReportAction = (BrokerCreateReportAction)action;
				LoadForecastService forecastService = InjectionUtils.injector().getInstance(LoadForecastService.class);
				final Date endDate = this.endDate(createReportAction.getStartTime(), (int)createReportAction.getDuration());
				Float[] loadLevels = forecastService.getLoadForAllLevels(
						createReportAction.getStoreName(),
						agent.getLocalName(),
						createReportAction.getStartTime(),
						endDate);
				
				BrokerUpdateReportPredicate updateReportPredicate = new BrokerUpdateReportPredicate();
				final UserPrefs userPrefs = agent.getUserPrefs();
				CronScheduleStore scheduler = InjectionUtils.injector().getInstance(CronScheduleStore.class);
				int[] payloads = 
						scheduler.getValuesByBlock(
								userPrefs.getLevelsSchedule().getName(),
								createReportAction.getStartTime(),
								endDate);
				final int payload = payloads[0];
				updateReportPredicate.setPriority(userPrefs.getPriority());
				updateReportPredicate.setLevel0(loadLevels[0]);
				updateReportPredicate.setLevel1(loadLevels[1]);
				updateReportPredicate.setLevel2(loadLevels[2]);
				updateReportPredicate.setLevel3(loadLevels[3]);
				updateReportPredicate.setSignalLevel(CronLevelsScheduleStore.getLevel(payload));
				if (CronLevelsScheduleStore.isConsumer(payload)) {
					updateReportPredicate.setMode(BrokerUpdateReportPredicate.MODE_HARD);
				} else if (CronLevelsScheduleStore.isProducer(payload)) {
					updateReportPredicate.setMode(BrokerUpdateReportPredicate.MODE_EASY);
				} else {
					updateReportPredicate.setMode(BrokerUpdateReportPredicate.MODE_NONE);
				}				
				replyPredicate = updateReportPredicate;
				
				loadPerLevel = new float[4];
				for (int n= 0; n < 4; n++) {
					loadPerLevel[n] = loadLevels[n];
				}
				
			} else if (action instanceof DistributeEventAction) {
				
				DistributeEventAction distributeAction = (DistributeEventAction)action;
				EasyDistributeEvent receivedEasy = new EasyDistributeEvent(
						distributeAction.getDistributeEvent().getMarketContext(),
						new StreamSource(new StringReader(distributeAction.getDistributeEvent().getContent())));
				
				if (agent.getTracker() != null) { // Register the information of the new DistributeEvent.
					// Find the new events of the DistributeEvent and track all of them.	
					List<AsdrEvent> newOadrEvents = agent.getDistributeEvent().findNewEvents(receivedEasy);
					for (AsdrEvent oadrEvent : newOadrEvents) {
						trackDistributeEvent(oadrEvent);
					}
				}				
				
				// Sed the DistributeAction to the local agent.
				agent.getSimpleXmpp().send(distributeAction.getDistributeEvent().getContent());
				agent.setDistributeEvent(receivedEasy);
				
				// Reply to the ASPEM.
				CreatedEventPredicate createdEventPredicate = new CreatedEventPredicate(
						new EiResponseConcept(
								EiResponseConcept.CODE_OK,
								request.getConversationId()));
				replyPredicate = createdEventPredicate;
				
				if (loadPerLevel != null) {
					String content = distributeAction.getDistributeEvent().getContent();
					int idxEiValue = content.indexOf("<ei:value>") + 10;
					int payload = Integer.valueOf(content.substring(idxEiValue, idxEiValue + 1));
					Exp01Logger logger = InjectionUtils.injector().getInstance(Exp01Logger.class);				
					logger.logBrokerAction(agent.getLocalName(), payload, loadPerLevel[0] - loadPerLevel[payload]);
				}
				
				
			} else if (action instanceof IdleAction) {
				
				agent.getSimpleXmpp().send("no-message");
				
				// Reply to the ASPEM.
				CreatedEventPredicate createdEventPredicate = new CreatedEventPredicate(
						new EiResponseConcept(
								EiResponseConcept.CODE_OK,
								request.getConversationId()));
				replyPredicate = createdEventPredicate;				
			}
			
			// Build the reply message.
			ACLMessage reply = request.createReply();
			reply.setPerformative(ACLMessage.INFORM);
			try {
				agent.getContentManager().fillContent(reply, replyPredicate);
			} catch (Exception ex) {
				throw new IllegalStateException(ex);
			}
			
			return reply;
		}
		
		
		/**
		 * Calculate the EndDate from both the StartDate and the Duration of the period.
		 */
		private Date endDate(Date startDate, int secondsDuration) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(startDate);
			cal.add(Calendar.SECOND, secondsDuration);
			return cal.getTime();
		}
	}
}
