package es.siani.energyagents.behaviours.as01;

import static es.siani.energyagents.utils.NotificationUtils.toAsdrEvent;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.lang.acl.ACLMessage;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.xml.transform.stream.StreamSource;

import com.google.inject.Injector;

import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.behaviours.as00.GridopProcessBehaviour;
import es.siani.energyagents.behaviours.as00.SendRequestBehaviour;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.ontology.as00.DistributeEventAction;
import es.siani.energyagents.ontology.as00.DistributeEventConcept;
import es.siani.energyagents.ontology.as00.EventConcept;
import es.siani.energyagents.ontology.as00.IntervalConcept;
import es.siani.energyagents.ontology.as00.ScheduleConcept;
import es.siani.energyagents.ontology.as01.CreateReportAction;
import es.siani.energyagents.ontology.as01.UpdateReportPredicate;
import es.siani.simpledr.EasyCreateReport;
import es.siani.simpledr.EasyDistributeEvent;
import es.siani.simpledr.EasyUpdateReport;

@SuppressWarnings("serial")
public class GridopExp01Behaviour extends GridopProcessBehaviour {
	
	private static final String[] RESOURCES_IDS = 
			new String[] {"level0", "level1", "level2", "level3", "easy", "hard"};
	private static final String REPORT_SPECIFIER = "rep-as-01";
	
	private static final int IDX_PROFILE_0 = 0;
	private static final int IDX_PROFILE_1 = 1;
	private static final int IDX_PROFILE_2 = 2;
	private static final int IDX_PROFILE_3 = 3;
	private static final int IDX_EASY = 4;
	private static final int IDX_HARD = 5;	
			
	
	private boolean isDone;
	private boolean waitingForReply;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public GridopExp01Behaviour() {
		this.isDone = false;
		this.waitingForReply = false;
		return;
	}
	
	
	/**
	 * Setup the behaviour before it starts running.
	 */
	@Override
	public void init(long idSimulation, EnergyAgent agent,
			ScheduleConcept schedule, String scenarioCode, String storeName,
			Behaviour tearDownBehaviour) {

		super.init(idSimulation, agent, schedule, scenarioCode, storeName, tearDownBehaviour);
		
		Injector injector = InjectionUtils.injector();
		Exp01Logger logger = injector.getInstance(Exp01Logger.class);
		logger.init(idSimulation);
		return;
	}
	
	
	/**
	 * Tell if the behaviour has finished its operation.
	 */
	@Override
	public boolean done() {
		return this.isDone;
	}	
	
	
	/**
	 * Actions of the Grid Operator.
	 */
	@Override
	public void action() {
		
		if (this.waitingForReply) {
			// The message has already been sent to the ASPEMs.
			return;
		}		
		
		
		// Parent's checks.
		super.action();
		
		// Check both if there are events to be sent and enabled ASPEMs.
		AID[] aspemsAIDs = this.agent.getPlatform().getAspemAgentsAID(this.agent);
		if ((schedule.getEvents().size() == 0) || (aspemsAIDs.length == 0)) {
			if (this.tearDownBehaviour != null) {
				this.agent.addBehaviour(this.tearDownBehaviour);
			}
			this.isDone = true;
			return;
		} else if (schedule.getEvents().size() > 1) {
			getLogger().log(Level.SEVERE, "Processing multiple events at the same time is not supported.");
			throw new UnsupportedOperationException();
		}
		
		SequentialBehaviour seqBehaviour = new SequentialBehaviour(this.agent);
		
		EventConcept event = schedule.getEvents().get(0);
		if (event.getSignalType().equals("level")) {
			
			// Update the DistributeEvent associated with the agent.
			final long currentModelTime = schedule.getCurrentModelTime();
			this.agent.updateDistributeEvent(currentModelTime, null);
			EasyDistributeEvent easy = this.agent.getDistributeEvent();
			easy.addEvent(toAsdrEvent(event), currentModelTime);
			
			// Add the behaviour that spreads the DistributeEvent to all the ASPEMs.
			SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
					this.agent,
					aspemsAIDs,
					new ACLMessage(ACLMessage.REQUEST),
					new DistributeEventAction(
							new DistributeEventConcept(this.agent.getPlatform().getId(), easy.toString()),
							schedule.getCurrentModelTime()));
			sendBehaviour.registerHandleAllResultNotifications(new FinishGridopExp01Behaviour());
			seqBehaviour.addSubBehaviour(sendBehaviour);
			
		} else if (event.getSignalType().equals("delta")) {
			// Build the structure in which the load of each ASPEM for each interval is stored.
			Map<AID, List<AspemIntervalLoad>> loadData = new HashMap<AID, List<AspemIntervalLoad>>(aspemsAIDs.length);
			for (AID aid : aspemsAIDs) {
				loadData.put(aid, new ArrayList<AspemIntervalLoad>(event.getIntervals().size()));
			}

			seqBehaviour.addSubBehaviour(this.buildCreateReportActions(aspemsAIDs, event, loadData));
			seqBehaviour.addSubBehaviour(new DistributeLoadBehaviour(loadData));
			seqBehaviour.addSubBehaviour(new FinishGridopExp01Behaviour());
			
		} else {
			getLogger().log(Level.SEVERE, "Only singals of the types 'level' and 'delta' are supported.");
			throw new UnsupportedOperationException();			
		}
		
		
		if (this.tearDownBehaviour != null) {
			seqBehaviour.addSubBehaviour(this.tearDownBehaviour);
		}
		this.agent.addBehaviour(seqBehaviour);
		
		
		this.waitingForReply = true;
		
		
		return;
	}
	
	
	/**
	 * Build the EasyCreateReport objects to be sent in order to request information about the
	 * loads during the specific intervals.
	 */
	private SequentialBehaviour buildCreateReportActions(
			AID[] aspemsAIDs,
			EventConcept event,
			Map<AID, List<AspemIntervalLoad>> loadData) {
		
		// Request information to all the ASPEMs about the load amount for each interval of the event.
		SequentialBehaviour seqBehaviour = new SequentialBehaviour();
		
		final String nodeCode = this.agent.getAID().toString();
		
		
		// Add the behaviour that requests information about load amount during a specific period of time.
		Calendar startTime = Calendar.getInstance();
		startTime.setTime(event.getStart());
		for (IntervalConcept interval : event.getIntervals()) {
			final long intervalDuration = interval.getDuration();
			SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
					agent,
					aspemsAIDs,
					new ACLMessage(ACLMessage.REQUEST),
					new CreateReportAction(
							new EasyCreateReport(
									nodeCode,
									REPORT_SPECIFIER,
									startTime.getTime(),
									intervalDuration,
									intervalDuration,
									RESOURCES_IDS),
							scenarioCode,
							storeName,
							schedule.getCurrentModelTime()));
			sendBehaviour.registerHandleAllResultNotifications(
					new UpdateReportHandler(loadData, sendBehaviour.ALL_RESULT_NOTIFICATIONS_KEY));
			seqBehaviour.addSubBehaviour(sendBehaviour);
			startTime.add(Calendar.SECOND, (int)intervalDuration);
		}
		
		return seqBehaviour;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(GridopExp01Behaviour.class.getName());
	}
	
	
	/**
	 * Inner class.
	 * This behaviour is invoked in order to finish the GridopExp01Behaviour.
	 */
	private class FinishGridopExp01Behaviour extends OneShotBehaviour {
		
		/**
		 * Change the flag 'isDone' of the send-level-request behaviour to 'true'.
		 * It implies the finalization of the send-level-request behaviour.
		 */
		@Override
		public void action() {
			GridopExp01Behaviour.this.isDone = true;
			return;
		}
	}	
	
	
	/**
	 * Inner class.
	 * This behaviour is invoked when all notifications for a message of the type CreateReport have been
	 * received. A CreateReport message has been sent for each interval.
	 */
	private class UpdateReportHandler extends Behaviour {
		
		private final Map<AID, List<AspemIntervalLoad>> loadData;
		private final String notificationsKey;
		
		
		/**
		 * Constructor.
		 */
		public UpdateReportHandler(Map<AID, List<AspemIntervalLoad>> loadData, String notificationsKey) {
			this.loadData = loadData;
			this.notificationsKey = notificationsKey;
			return;
		}
		
		
		/**
		 * @see jade.core.behaviours.Behaviour#done()
		 */
		@Override
		public boolean done() {
			return true;
		}				

		
		/**
		 * Called after receiving the information of the ASPEM on the amount of load they expect to demand
		 * on the defined period of time.
		 * 
		 * According to the global amount of load that must be shedded (the payload value of the Delta signal),
		 *  this method says to each ASPEM the amount they have to shed. 
		 */
		@Override
		public void action() {
			
			@SuppressWarnings("unchecked")
			Vector<ACLMessage> notifsAspems = (Vector<ACLMessage>)this.getDataStore().get(notificationsKey);

			for (int n = 0; n < notifsAspems.size(); n++) {
				final ACLMessage msg = notifsAspems.get(n);
				try {
					UpdateReportPredicate predicate = (UpdateReportPredicate)agent.getContentManager().extractContent(msg);
					EasyUpdateReport easy = new EasyUpdateReport(
							new StreamSource(new StringReader(predicate.getUpdateReportMessage())));
					AspemIntervalLoad aspemIntervalLoad = new AspemIntervalLoad();
					for (Map.Entry<String, Float> entry : easy.getValues().entrySet()) {
						aspemIntervalLoad.setLoad(entry.getKey(), entry.getValue());
					}
					
					this.loadData.get(msg.getSender()).add(aspemIntervalLoad);
				} catch (Exception ex) {
					getLogger().log(Level.SEVERE, "Failed to extract the content of the message.", ex);
					throw new IllegalStateException();
				}
			}
			
			
			return;
		}
	}
	
	
	/**
	 * Inner class.
	 */
	private class DistributeLoadBehaviour extends Behaviour {
		
		private final Map<AID, List<AspemIntervalLoad>> loadData;
		private final int expectedNotifications;
		private int receivedNotifications;
		private boolean waitingForNotifications;
		
		
		/**
		 * Constructor.
		 */
		public DistributeLoadBehaviour(Map<AID, List<AspemIntervalLoad>> loadData) {
			this.loadData = loadData;
			this.expectedNotifications = loadData.keySet().size();
			this.receivedNotifications = 0;
			this.waitingForNotifications = false;
			return;
		}
		
		
		/**
		 * Finish the behaviour when the notifications corresponding to the DistributeEvent
		 * messages have been received. 
		 */
		@Override
		public boolean done() {
			return this.expectedNotifications == this.receivedNotifications;
		}		


		/**
		 * Send DistributeEvent messages to all the ASPEMs indicating the amount of load
		 * they have to shed in each interval of the event.
		 */
		@Override
		public void action() {
			
			if (this.waitingForNotifications) {
				return;
			}
			
			// Update the status of the DistributeEvent attached to the agent.
			final long currentModelTime = schedule.getCurrentModelTime();
			agent.updateDistributeEvent(currentModelTime, null);

			// Collect the load that must shed each ASPEM during each interval.
			
			// - Structure that stores the load to be shedded.
			final EventConcept event = schedule.getEvents().get(0);
			final int N_INTERVALS = event.getIntervals().size();
			Map<AID, Float[]> aspemsShedData = new HashMap<AID, Float[]>();
			for (AID aid : this.loadData.keySet()) {
				aspemsShedData.put(aid, new Float[N_INTERVALS]);
			}
			
			// - Loop that collects the information.
			for (int nInterval = 0; nInterval < N_INTERVALS; nInterval++) {
				float[] totals = this.getTotalLoads(nInterval);
				final float deltaLoad = event.getIntervals().get(nInterval).getPayload();
				
				Map<AID, Float> shedData = null;
				if ((totals[IDX_PROFILE_0] - totals[IDX_PROFILE_3]) <= deltaLoad) {
					shedData = new HashMap<AID, Float>();
					for (AID aspemAID : this.loadData.keySet()) {
						shedData.put(aspemAID, this.loadData.get(aspemAID).get(nInterval).getLoad(IDX_PROFILE_0));
					}
					
				} else if ((totals[IDX_EASY] > 0) && ((totals[IDX_PROFILE_0] - totals[IDX_EASY]) >= deltaLoad)) {
					shedData = this.distributeEasyLoads(nInterval, totals[IDX_PROFILE_0] - totals[IDX_EASY], deltaLoad);
					
				} else if ((totals[IDX_HARD] > 0) && ((totals[IDX_PROFILE_0] - totals[IDX_PROFILE_3] - totals[IDX_HARD]) >= deltaLoad)) {
					shedData = this.distributeWithoutHardLoads(
							nInterval,
							totals[IDX_PROFILE_0] - totals[IDX_PROFILE_3] - totals[IDX_HARD],
							deltaLoad);
				} else {
					 shedData = this.distributeWithHardLoads(
                             nInterval,
                             totals[IDX_PROFILE_0] - totals[IDX_PROFILE_3],
                             totals[IDX_PROFILE_0] - totals[IDX_PROFILE_3] + totals[IDX_HARD],
                             deltaLoad);					
				}
				
				for(Map.Entry<AID, Float> entry : shedData.entrySet()) {
					Float[] aspemIntervalsValues = aspemsShedData.get(entry.getKey());
					aspemIntervalsValues[nInterval] = shedData.get(entry.getKey());
				}
			}
			
			// A message with the data to be shedded is sent to each ASPEM.
			final String platformId = agent.getPlatform().getId();	
			EasyDistributeEvent easy = agent.getDistributeEvent();
			SequentialBehaviour seqBehaviour = new SequentialBehaviour(agent);
			for (AID aspemAID : this.loadData.keySet()) {
				
				EventConcept shedEvent = this.buildEvent(event, aspemsShedData.get(aspemAID));
				agent.addChildEvent(aspemAID, shedEvent);
				
				// Add child events to the Easy message that is going to be sent.
				List<String> childEventsIds = new ArrayList<String>();
				for (EventConcept childEvent : agent.getChildEvents(aspemAID)) {
					childEventsIds.add(easy.addEvent(toAsdrEvent(childEvent), currentModelTime));
				}
				
				SendRequestBehaviour sendBehaviour = new SendRequestBehaviour(
						agent,
						new AID[] {aspemAID},
						new ACLMessage(ACLMessage.REQUEST),
						new DistributeEventAction(
								new DistributeEventConcept(
										platformId,
										easy.toString()),
								currentModelTime));
				sendBehaviour.registerHandleAllResultNotifications(new DistributeLoadNotificationHandler());
				seqBehaviour.addSubBehaviour(sendBehaviour);
				
				// Remove the child events from the Easy message.
				for (String childEventId : childEventsIds) {
					easy.removeEvent(childEventId);
				}
			}
			
			//EnergyAgentsUtils.logAspemsLoadDistribution(aspemsShedData);
			
			agent.addBehaviour(seqBehaviour);
			
			this.waitingForNotifications = true;
			
			return;
		}
		
		
        /**
         * Return the maximum amount of load that consumes the ASPEMs for a specific
         * interval of the event.
         */
        private float[] getTotalLoads(int nInterval) {
        	float[] totals = new float[6];
        	for (List<AspemIntervalLoad> intervals : this.loadData.values()) {
        		AspemIntervalLoad interval = intervals.get(nInterval);
        		totals[IDX_PROFILE_0] += interval.getLoad(IDX_PROFILE_0);
        		totals[IDX_PROFILE_1] += interval.getLoad(IDX_PROFILE_1);
        		totals[IDX_PROFILE_2] += interval.getLoad(IDX_PROFILE_2);
        		totals[IDX_PROFILE_3] += interval.getLoad(IDX_PROFILE_3);
        		totals[IDX_EASY] += interval.getLoad(IDX_EASY);
        		totals[IDX_HARD] += interval.getLoad(IDX_HARD);

        	}

        	return totals;
        }
        
        
        /**
         * Build a new version of a EventConcept by overriding the values of the intervals
         * with new ones.
         */
        private EventConcept buildEvent(EventConcept sourceEvent, Float[] intervalsNewValues) {
        	List<IntervalConcept> newIntervals = new ArrayList<IntervalConcept>();
        	List<IntervalConcept> sourceIntervals = sourceEvent.getIntervals();
        	for (int n = 0; n < sourceIntervals.size()	; n++) {
        		newIntervals.add(new IntervalConcept(
        				sourceIntervals.get(n).getDuration(),
        				intervalsNewValues[n]));
        	}
        	
        	return new EventConcept(
        			sourceEvent.getSignalType(),
        			sourceEvent.getStart(),
        			sourceEvent.getDuration(),
        			sourceEvent.getNotificationDuration(),
        			sourceEvent.getPriority(),
        			false,
        			newIntervals,
        			null);
        }
        
        
        /**
         * Set the distribution of the load among the ASPEMs when all of it can be meet
         * by means of the easy-load.
         */
        private Map<AID, Float> distributeEasyLoads(int nInterval, float totalEasy, float delta) {
        	
        		Map<AID, Float> shedData = new HashMap<AID, Float>(this.loadData.size());
        		for (Map.Entry<AID, List<AspemIntervalLoad>> entry : this.loadData.entrySet()) {
        			AspemIntervalLoad itv = entry.getValue().get(nInterval);
        			shedData.put(
        					entry.getKey(),
        					((itv.getLoad(IDX_PROFILE_0) - itv.getLoad(IDX_EASY)) / totalEasy) * delta);
        					
        		}
                
                return shedData;
        }
        
        /**
         * Set the distribution of the load among the ASPEMs when all of it can be meet
         * without using the hard-loads.
         */
        private Map<AID, Float> distributeWithoutHardLoads(int nInterval, float totalWithoutHard, float delta) {
        	Map<AID, Float> shedData = new HashMap<AID, Float>(this.loadData.size());
    		for (Map.Entry<AID, List<AspemIntervalLoad>> entry : this.loadData.entrySet()) {
    			AspemIntervalLoad itv = entry.getValue().get(nInterval);
    			shedData.put(
    					entry.getKey(),
    					((itv.getLoad(IDX_PROFILE_0) - itv.getLoad(IDX_PROFILE_3) - itv.getLoad(IDX_HARD)) / totalWithoutHard) * delta);
                }
                
                return shedData;
        }
        
        
        /**
         * Set the distribution of the load among the ASPEMs when the hard loads are
         * needed to meet it.
         */
        private Map<AID, Float> distributeWithHardLoads(int nInterval, float totalSaved, float totalSavedWithoutHard, float delta) {
        	
        	final float deltaHard = delta - totalSavedWithoutHard;
        	final float totalHard = totalSaved - totalSavedWithoutHard;
        	
        	Map<AID, Float> shedData = new HashMap<AID, Float>(this.loadData.size());
        	
        	if (totalHard > 0) {
        		// There are hard loads. The load is distributed among the ASPEMs considering the
        		// amount of hard load contracted by each one.
        		for (Map.Entry<AID, List<AspemIntervalLoad>> entry : this.loadData.entrySet()) {
        			AspemIntervalLoad itv = entry.getValue().get(nInterval);
        			shedData.put(
        					entry.getKey(),
        					(itv.getLoad(IDX_PROFILE_0) - itv.getLoad(IDX_PROFILE_3) - itv.getLoad(IDX_HARD)
        							+ ((itv.getLoad(IDX_HARD) / totalHard) * deltaHard)));
                }
        	} else {
        		// There are not hard loads.
        		for (Map.Entry<AID, List<AspemIntervalLoad>> entry : this.loadData.entrySet()) {
        			AspemIntervalLoad itv = entry.getValue().get(nInterval);
        			shedData.put(
        					entry.getKey(),
        					((itv.getLoad(IDX_PROFILE_0) - itv.getLoad(IDX_PROFILE_3)) / totalSaved) * delta);

        		}
        	}
        	
        	return shedData;
        }
        
        /**
         * Inner class.
         * Increase the number of received notifications of the DistributeLoadBehaviour.
         */
        private class DistributeLoadNotificationHandler extends OneShotBehaviour {

			@Override
			public void action() {
				receivedNotifications++;
				return;
			}
        	
        }
	} // End of inner class: DistributeLoadBehaviour.
	
	
	/**
	 * Inner class.
	 * Store the amount of load of an interval for each demand profile.
	 * 
	 */
	private class AspemIntervalLoad {

		public static final int IDX_PROFILE_0 = 0;
		public static final int IDX_PROFILE_1 = 1;
		public static final int IDX_PROFILE_2 = 2;
		public static final int IDX_PROFILE_3 = 3;
		public static final int IDX_PROFILE_EASY = 4;
		public static final int IDX_PROFILE_HARD = 5;

		private float[] loads;

		public AspemIntervalLoad() {

			this.loads = new float[6];
			for (int n = 0; n < 6; n++) {
				this.loads[n] = 0f;
			}
		}

		public void setLoad(String profile, float load) {

			if (profile.equals("level0")) {
				this.loads[IDX_PROFILE_0] = load;
			} else if (profile.equals("level1")) {
				this.loads[IDX_PROFILE_1] = load;
			} else if (profile.equals("level2")) {
				this.loads[IDX_PROFILE_2] = load;
			} else if (profile.equals("level3")) {
				this.loads[IDX_PROFILE_3] = load;
			} else if (profile.equals("easy")) {
				this.loads[IDX_PROFILE_EASY] = load;
			} else if (profile.equals("hard")) {
				this.loads[IDX_PROFILE_HARD] = load;
			} else {
				throw new IllegalArgumentException();
			}

			return;
		}


		public float getLoad(int profile) {
			if ((profile < 0) || (profile > 5)) {
				throw new IllegalArgumentException();
			}

			return this.loads[profile];
		}
	} // End of inner class: AspemIntervalLoad.
}
	

