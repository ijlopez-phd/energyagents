package es.siani.energyagents.services;

import java.util.Date;

public interface LoadForecastService {
	
	/**
	 * Get the load for a specific scenario, asbox and demand-response level during an interval of time.
	 */
	public Float getLoadForLevel(String scenarioId, String asboxId, int level, Date startTime, Date endTime);
	
	
	/**
	 * Get the all levels load for a specific scenario and asbox during an interval of time.
	 */
	public Float[] getLoadForAllLevels(String scenarioId, String asboxId, Date startTime, Date endTime);

}