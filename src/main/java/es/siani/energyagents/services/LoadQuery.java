package es.siani.energyagents.services;

import java.util.Date;

public class LoadQuery {
	
	/** Code of the scenario. */
	private final String scenario;
	
	/** Id of the Asbox. */
	private final String asbox;
	
	/** Demand-response level. */
	private final int profile;
	
	/** Start point of the interval. */
	private final Date startTime;
	
	/** End point of the interval. */
	private final Date endTime;

	
	/**
	 * Constructor.
	 */
	public LoadQuery(String scenario, String asbox, int profile, Date startTime, Date endTime) {
		
		if ((scenario == null) || (asbox == null) || (profile < 0) || (profile > 3) || (startTime == null)) {
			throw new IllegalArgumentException();
		}
		
		this.scenario = scenario;
		this.asbox = asbox;
		this.profile = profile;
		this.startTime = startTime;
		this.endTime = endTime;
		
		return;
	}

	
	/**
	 * Get the Scenario code.
	 */
	public String getScenario() {
		return this.scenario;
	}


	/**
	 * Get the Id of the Asbox.
	 */
	public String getAsbox() {
		return this.asbox;
	}


	/**
	 * Get the demand-response level.
	 */
	public int getProfile() {
		return this.profile;
	}


	/**
	 * Get the Start point of the interval.
	 */
	public Date getStartTime() {
		return this.startTime;
	}

	
	/**
	 * Get the End point of the interval.
	 */
	public Date getEndTime() {
		return this.endTime;
	}	
}
