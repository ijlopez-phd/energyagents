package es.siani.energyagents.services;

import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;

import es.siani.energyagents.EnergyAgentsException;
import es.siani.energyagents.data.DataProviderHelper;

public class LoadForecastServiceBatisImpl implements LoadForecastService {
	
	/** Data provider for accessing the database. */
	private final DataProviderHelper dataProvider;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public LoadForecastServiceBatisImpl(DataProviderHelper dataProvider) {
		this.dataProvider = dataProvider;
		return;
	}

	
	/**
	 * @see es.siani.energyagents.services.LoadForecastService#getLoadForLevel(String, String, int, Date, Date)
	 */
	@Override
	public Float getLoadForLevel(String scenarioId, String asboxId, int level, Date startTime, Date endTime) {
		
		SqlSession session = this.dataProvider.getSQLSessionFactory().openSession();
		
		Float load = null;
		try {
			load = session.selectOne("getLoad", new LoadQuery(scenarioId, asboxId, level, startTime, endTime));
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to access the database from Batis.", ex);
			throw new EnergyAgentsException(ex);
		} finally {
			session.close();
		}
		
		return load;
	}
	
	
	/**
	 * @see es.siani.energyagents.services.LoadForecastService#getLoadForAllLevels(String, String, Date, Date)
	 */	
	@Override
	public Float[] getLoadForAllLevels(String scenarioId, String asboxId, Date startTime, Date endTime) {
		
		SqlSession session = this.dataProvider.getSQLSessionFactory().openSession();
		Map<Integer, Map<String, Object>> dbValues;
		try {
			dbValues = session.selectMap(
					"getLevelsLoad",
					new LoadQuery(scenarioId, asboxId, 0, startTime, endTime),
					"profile");
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to acces to the database from Batis.", ex);
			throw new EnergyAgentsException(ex);
		} finally {
			session.close();
		}
		
		if (dbValues.size() != 4) {
			throw new IllegalStateException(
					"Database inconsistence: the number of profile values"
					+ " for a specific asbox must be four.");
		}
		
		Float[] values = new Float[4];
		for (Map.Entry<Integer, Map<String, Object>> dbEntry : dbValues.entrySet()) {
			values[dbEntry.getKey()] = Float.parseFloat(dbEntry.getValue().get("load").toString());
		}
		return values;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(LoadForecastServiceBatisImpl.class.getName());
	}
}
