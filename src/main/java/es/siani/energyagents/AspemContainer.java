package es.siani.energyagents;

import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import es.siani.energyagents.agent.AgentTracker;
import es.siani.energyagents.agent.AspemAgent;
import es.siani.energyagents.agent.BrokerAgent;
import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.agent.XmlUserPrefsHandler;
import es.siani.energyagents.model.UserPrefs;
import static es.siani.energyagents.EnergyAgentsGlobals.*;

public class AspemContainer extends AgentsContainer {
	
	static final String SCHEDULE_XSD_PATH = "/schemas/user-prefs.xsd";
	
	/** Agents Platform to which belongs the container. */
	final AgentsPlatform platform;
	
	/** Jade Controller of the AspemAgent. */
	protected AgentController aspemAgent;
	
	/** XML handler to parse the XML that describes the UserPrefs. */
	final XmlUserPrefsHandler userPrefsHandler;
	
	/** Registered brokers agents registered in this container. */
	ArrayList<AgentController> brokers = new ArrayList<AgentController>();
	
	
	/**
	 * Constructor.
	 */
	AspemContainer(String name, AgentsPlatform platform, XmlUserPrefsHandler userPrefsHandler) {
		this.platform = platform;
		this.userPrefsHandler = userPrefsHandler;
		
		jade.util.leap.Properties props = new jade.util.ExtendedProperties();
		props.setProperty(jade.core.Profile.CONTAINER_NAME, name);
		props.setProperty(jade.domain.DFService.DF_SEARCH_TIMEOUT_KEY, "300000");
		this.container = jade.core.Runtime.instance().createAgentContainer(new jade.core.ProfileImpl(props));
		return;
	}
	
	
	/**
	 * Get the AgentContainer associated to the AspemAgent.
	 */
	public AgentController getAspemAgentController() {
		return this.aspemAgent;
	}
	
	
	/**
	 * Adds the Aspem agent to the container.
	 */
	AgentController initAspemAgent(AgentTracker tracker) {
		
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(EnergyAgent.IDX_ARG_PLATFORM, this.platform);
		args.put(EnergyAgent.IDX_ARG_CONTAINER,  this.getName());
		args.put(EnergyAgent.IDX_ARG_TRACKER, tracker);
		
		AgentController agent = null;
		try {
			agent = this.container.createNewAgent(
					this.getName(),
					AspemAgent.class.getName(),
					new Object[] {args});
			agent.start();
		} catch (StaleProxyException ex) {
			getLogger().log(Level.SEVERE, "Failed to create the Aspem agent.", ex);
			throw new IllegalStateException(ex);
		}
		
		this.aspemAgent = agent;
		return agent;
	}
	
	
	/**
	 * Adds a BrokerAgent to the container (the user preferences are not setted).
	 */
	public void addBrokerAgent(String name, AgentTracker tracker) throws EnergyAgentsException {
		this.addBrokerAgent(name, null, tracker);
		return;
	}	
	
	
	/**
	 * Add a BrokerAgent to the container. User preferences can be setted.
	 */
	public void addBrokerAgent(String name, StreamSource userPrefsSource, AgentTracker tracker) throws EnergyAgentsException {
		
		// Check that the container is active and the agent is not already registered.
		if (this.isKilled()) {
			throw new IllegalStateException("The container is not operative.");
		} else if (existBrokerAgent(name)) {
			throw new IllegalArgumentException("A broker agent with the name '" + name + "' already exists in the container.");
		}
		
		try {
			Map<String, Object> args = new HashMap<String, Object>();
			args.put(EnergyAgent.IDX_ARG_PLATFORM, this.platform);
			args.put(EnergyAgent.IDX_ARG_CONTAINER, this.getName());
			if (userPrefsSource != null) {
				args.put(EnergyAgent.IDX_ARG_USER_PREFS, this.parseXmlUserPrefs(userPrefsSource, false));
			}
			if (tracker != null) {
				args.put(EnergyAgent.IDX_ARG_TRACKER, tracker);
			}
			
			AgentController agent =
					this.container.createNewAgent(name, BrokerAgent.class.getName(), new Object[] {args});
			agent.start();
			this.brokers.add(agent);
			
		} catch (StaleProxyException e) {
			getLogger().log(Level.SEVERE, "Error: the broker agent with the name '" + name + "' could not be created.", e);			
			throw new EnergyAgentsException(e);
		}
		
		return;		
	}
	
	
	/**
	 * Parse a XML source and builds an UserPrefsConcept object from its content. 
	 */
	UserPrefs parseXmlUserPrefs(StreamSource xmlSource, boolean useSchema) throws EnergyAgentsException {
		
		SAXParserFactory saxFactory = SAXParserFactory.newInstance();
		saxFactory.setNamespaceAware(true);
		if (useSchema) {
			try {
				Schema schema = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI).newSchema(
						AspemContainer.class.getResource(SCHEDULE_XSD_PATH));
				saxFactory.setSchema(schema);
			} catch (SAXException ex) {
				getLogger().log(Level.SEVERE, "Failed to load the schema (XSD) corresponding to the User preferences.", ex);
				throw new EnergyAgentsException(ex.getMessage());				
			}
		}
		
		UserPrefs userPrefs = null;
		try {
			saxFactory.newSAXParser().parse(xmlSource.getInputStream(), this.userPrefsHandler);
			userPrefs = this.userPrefsHandler.getUserPrefs();
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "Failed to parse the XML that contains the User preferences.", ex);
			throw new EnergyAgentsException(ex.getMessage());
		} finally {
			this.userPrefsHandler.reset();
		}
		
		return userPrefs;
	}	
	
	
	/**
	 * Tells if the specified Broker Agent is already instantiated in the container.
	 */
	boolean existBrokerAgent(String name) {
		
		try {
			this.container.getAgent(name);
		} catch (ControllerException ex) {
			// An agent with that name does not exist yet.
			return false;
		}
		
		// An agent with the same name does already exist.
		return true;
	}
	
	
	/**
	 * Logger.
	 */
	private static Logger getLogger() {
		return Logger.getLogger(AspemContainer.class.getName());
	}
}
