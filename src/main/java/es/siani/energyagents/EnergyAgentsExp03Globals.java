package es.siani.energyagents;

public class EnergyAgentsExp03Globals {
	
	public static final String SD_CONSUMER_TYPE = "consumer";
	public static final String SD_PRODUCER_TYPE = "producer";
	public static final long BLOCK_SIZE_SECONDS = 1800;
	public static final long BLOCK_SIZE_MILLIS = BLOCK_SIZE_SECONDS * 1000;

}
