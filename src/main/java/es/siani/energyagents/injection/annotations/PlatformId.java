package es.siani.energyagents.injection.annotations;

import com.google.inject.BindingAnnotation;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.lang.annotation.ElementType.PARAMETER;

@BindingAnnotation @Target({PARAMETER}) @Retention(RUNTIME)
public @interface PlatformId {}
