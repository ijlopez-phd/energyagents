package es.siani.energyagents.injection;

import com.google.inject.AbstractModule;
import es.siani.energyagents.xmpp.SimpleXmpp;
import static org.mockito.Mockito.*;

public class XmppMockModule extends AbstractModule {
	
	
	/**
	 * Dependencies injector module.
	 */
	@Override
	protected void configure() {
		SimpleXmpp xmppMock = mock(SimpleXmpp.class);
		bind(SimpleXmpp.class).toInstance(xmppMock);
		return;
	}

}
