package es.siani.energyagents.injection;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;

import es.siani.energyagents.data.DataProviderHelper;

public class DataModule extends AbstractModule {
	
	
	/**
	 * Dependency Injector module for the data provider objects.
	 */
	@Override
	protected void configure() {
		bind(DataProviderHelper.class).in(Scopes.SINGLETON);

		return;
	}

}
