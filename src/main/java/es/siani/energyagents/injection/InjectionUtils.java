package es.siani.energyagents.injection;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

public class InjectionUtils {
	
	private static Injector _injector = null;
	private static boolean isChangedModule = false;
	
	private static Module energyAgentsModule = new EnergyAgentsModule();
	private static Module platformModule = new PlatformExp04Module();
	private static Module behavioursModule = new BehavioursExp04Module();
	private static Module xmppModule = new XmppModule();
	private static Module configModule = new ConfigModule();
	private static Module dataModule = new DataModule();
	private static Module servicesModule = new ServicesModule();
	
	/**
	 * Constructor.
	 */
	private InjectionUtils() {
		return;
	}
	
	
	/**
	 * Return the standard Injector of the framework.
	 */
	public static Injector injector() {
		
		if ((_injector == null) || (isChangedModule)) {
			_injector = Guice.createInjector(
					energyAgentsModule,
					platformModule,
					behavioursModule,
					xmppModule,
					configModule,
					dataModule,
					servicesModule);
			isChangedModule = false;
		}
		
		return _injector;
	}
	
	
	/**
	 * Set the PlatformModule to be used.
	 */
	public static void setPlatformModule(Module module) {
		platformModule = module;
		isChangedModule = true;
		return;
	}
	
	
	/**
	 * Set the EnergyAgentsModule to be used.
	 */
	public static void setEnergyAgentsModule(Module module) {
		energyAgentsModule = module;
		isChangedModule = true;
		return;
	}
	
	
	/**
	 * Set the BehavioursModule to be used.
	 */
	public static void setBehavioursModule(Module module) {
		behavioursModule = module;
		isChangedModule = true;
		return;
	}	
	
	
	/**
	 * Set the XmppModule to be used.
	 */
	public static void setXmppModule(Module module) {
		xmppModule = module;
		isChangedModule = true;
		return;
	}
	
	
	/**
	 * Set the ConfigModule to be used.
	 */
	public static void setConfigModule(Module module) {
		configModule = module;
		isChangedModule = true;
		return;
	}
	
	
	/**
	 * Set the DataModule to be used.
	 */
	public static void setDataModule(Module module) {
		dataModule = module;
		isChangedModule = true;
		return;
	}
	
	
	/**
	 * Set the ServicesModule to be used.
	 */
	public static void setServicesModule(Module module) {
		servicesModule = module;
		isChangedModule = true;
		return;
	}
}
