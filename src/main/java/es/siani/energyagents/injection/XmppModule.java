package es.siani.energyagents.injection;

import com.google.inject.AbstractModule;
import es.siani.energyagents.injection.annotations.XmppServerPort;
import es.siani.energyagents.injection.annotations.XmppServerUrl;
import es.siani.energyagents.xmpp.SimpleXmpp;
import es.siani.energyagents.xmpp.SimpleXmppImpl;
import es.siani.energyagents.xmpp.XmppEntitiesGenerator;
import es.siani.energyagents.xmpp.XmppEntitiesGeneratorImpl;

public class XmppModule extends AbstractModule {

	private final static String SERVER_URL = "amd64";	
	private final static int SERVER_PORT = 5222;

	
	/**
	 * Dependencies injector module.
	 */
	@Override
	protected void configure() {
		bind(XmppEntitiesGenerator.class).to(XmppEntitiesGeneratorImpl.class);
		bindConstant().annotatedWith(XmppServerUrl.class).to(SERVER_URL);		
		bindConstant().annotatedWith(XmppServerPort.class).to(SERVER_PORT);
		bind(SimpleXmpp.class).to(SimpleXmppImpl.class);
		
		return;
	}

}
