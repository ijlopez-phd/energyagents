package es.siani.energyagents.injection;

import com.google.inject.AbstractModule;

import es.siani.energyagents.behaviours.as00.AspemProcessBasicBehaviour;
import es.siani.energyagents.behaviours.as00.AspemProcessBehaviour;
import es.siani.energyagents.behaviours.as00.BrokerProcessBasicBehaviour;
import es.siani.energyagents.behaviours.as00.BrokerProcessBehaviour;
import es.siani.energyagents.behaviours.as00.GridopBasicProcessBehaviour;
import es.siani.energyagents.behaviours.as00.GridopProcessBehaviour;

public class BehavioursModule extends AbstractModule {

	
	@Override
	protected void configure() {
		bind(GridopProcessBehaviour.class).to(GridopBasicProcessBehaviour.class);
		bind(AspemProcessBehaviour.class).to(AspemProcessBasicBehaviour.class);
		bind(BrokerProcessBehaviour.class).to(BrokerProcessBasicBehaviour.class);
		return;
	}

}
