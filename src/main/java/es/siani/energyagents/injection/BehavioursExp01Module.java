package es.siani.energyagents.injection;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.Singleton;

import es.siani.energyagents.behaviours.as00.AspemProcessBehaviour;
import es.siani.energyagents.behaviours.as00.BrokerProcessBehaviour;
import es.siani.energyagents.behaviours.as00.GridopProcessBehaviour;
import es.siani.energyagents.behaviours.as01.AspemExp01Behaviour;
import es.siani.energyagents.behaviours.as01.BrokerExp01Behaviour;
import es.siani.energyagents.behaviours.as01.Exp01Logger;
import es.siani.energyagents.behaviours.as01.GridopExp01Behaviour;

public class BehavioursExp01Module extends AbstractModule {

	/**
	 * Bind the behaviours of the GridOperator, ASPEM and Brokers for the
	 * Experiment 01.
	 */
	@Override
	protected void configure() {
		bind(GridopProcessBehaviour.class).to(GridopExp01Behaviour.class);
		bind(AspemProcessBehaviour.class).to(AspemExp01Behaviour.class);
		bind(BrokerProcessBehaviour.class).to(BrokerExp01Behaviour.class);
		bind(Exp01Logger.class).in(Scopes.SINGLETON);;
		return;
	}

}
