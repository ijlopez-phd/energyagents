package es.siani.energyagents.injection;

import java.util.Comparator;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;

import es.siani.energyagents.ontology.as00.EventConcept;
import es.siani.energyagents.utils.CronLevelsScheduleStore;
import es.siani.energyagents.utils.CronScheduleStore;
import es.siani.energyagents.utils.CronStandardScheduleStore;
import es.siani.energyagents.utils.EventNotificationStartComparator;
import es.siani.energyagents.utils.PauseScheduler;
import es.siani.energyagents.utils.PauseSchedulerNotificationStart;


public class EnergyAgentsModule extends AbstractModule {
	
	/**
	 * Dependencies Injector module.
	 */
	@Override
	protected void configure() {
		bind(PauseScheduler.class).to(PauseSchedulerNotificationStart.class);
		bind(new TypeLiteral<Comparator<EventConcept>>(){}).to(EventNotificationStartComparator.class);
		bind(CronStandardScheduleStore.class).in(Scopes.SINGLETON);
		bind(CronLevelsScheduleStore.class).in(Scopes.SINGLETON);		
		bind(CronScheduleStore.class).to(CronLevelsScheduleStore.class);
		return;
	}
}
