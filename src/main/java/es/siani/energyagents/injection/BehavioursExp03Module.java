package es.siani.energyagents.injection;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;

import es.siani.energyagents.behaviours.as00.AspemProcessBehaviour;
import es.siani.energyagents.behaviours.as00.BrokerProcessBehaviour;
import es.siani.energyagents.behaviours.as00.GridopProcessBehaviour;
import es.siani.energyagents.behaviours.as03.AspemExp03Behaviour;
import es.siani.energyagents.behaviours.as03.AuctionLogger;
import es.siani.energyagents.behaviours.as03.BasicOffersSolver;
import es.siani.energyagents.behaviours.as03.BrokerExp03Behaviour;
import es.siani.energyagents.behaviours.as03.NonStartPriceProductionAssigner;
import es.siani.energyagents.behaviours.as03.GridopExp03Behaviour;
import es.siani.energyagents.behaviours.as03.OffersSolver;
import es.siani.energyagents.behaviours.as03.ProductionAssigner;
import es.siani.energyagents.behaviours.as03.StartPriceProductionAssigner;
import es.siani.energyagents.config.Configuration;

public class BehavioursExp03Module extends AbstractModule {

	/**
	 * Bind the behaviours of the GridOperator, ASPEM and brokers for the
	 * Experiment 03.
	 */
	@Override
	protected void configure() {
		bind(GridopProcessBehaviour.class).to(GridopExp03Behaviour.class);
		bind(AspemProcessBehaviour.class).to(AspemExp03Behaviour.class);
		bind(BrokerProcessBehaviour.class).to(BrokerExp03Behaviour.class);
		
		bind(OffersSolver.class).to(BasicOffersSolver.class);
		bind(AuctionLogger.class).in(Scopes.SINGLETON);
		
		return;
	}
	
	
	@Provides
	ProductionAssigner getProductionAssigner(Configuration config) {
		ProductionAssigner assigner;
		if (Boolean.parseBoolean(config.get(Configuration.PROP_AUCTION_USE_START_PRICE)) == true) {
			assigner = new StartPriceProductionAssigner();
		} else {
			assigner = new NonStartPriceProductionAssigner();
		}
		
		assigner.useRandomItems(new Boolean(config.get(Configuration.PROP_AUCTION_USE_RANDOM)));
		
		return assigner;
	}

}
