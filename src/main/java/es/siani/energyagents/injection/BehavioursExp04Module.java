package es.siani.energyagents.injection;

import javax.inject.Singleton;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;

import es.siani.energyagents.behaviours.as00.AspemProcessBehaviour;
import es.siani.energyagents.behaviours.as00.BrokerProcessBehaviour;
import es.siani.energyagents.behaviours.as00.GridopProcessBehaviour;
import es.siani.energyagents.behaviours.as04.AuctionBoard;
import es.siani.energyagents.behaviours.as04.AuctionLogger;
import es.siani.energyagents.behaviours.as04.NonStartPriceProductionAssigner;
import es.siani.energyagents.behaviours.as04.ProductionAssigner;
import es.siani.energyagents.behaviours.as04.AspemExp04Behaviour;
import es.siani.energyagents.behaviours.as04.BasicOffersSolver;
import es.siani.energyagents.behaviours.as04.BrokerExp04Behaviour;
import es.siani.energyagents.behaviours.as04.GridopExp04Behaviour;
import es.siani.energyagents.behaviours.as04.OffersSolver;
import es.siani.energyagents.behaviours.as04.StartPriceAuctionBoard;
import es.siani.energyagents.behaviours.as04.StartPriceProductionAssigner;
import es.siani.energyagents.behaviours.as04.TrueWeightedAuctionBoard;
import es.siani.energyagents.config.Configuration;

public class BehavioursExp04Module extends AbstractModule {

	/**
	 * Bind the behaviours of the GridOperator, ASPEM and brokers for the
	 * Experiment 04.
	 */
	@Override
	protected void configure() {
		bind(GridopProcessBehaviour.class).to(GridopExp04Behaviour.class);
		bind(AspemProcessBehaviour.class).to(AspemExp04Behaviour.class);
		bind(BrokerProcessBehaviour.class).to(BrokerExp04Behaviour.class);
		
		bind(OffersSolver.class).to(BasicOffersSolver.class);
		bind(AuctionLogger.class).in(Scopes.SINGLETON);
		
		return;
	}
	
	
	@Provides
	ProductionAssigner getProductionAssigner(Configuration config) {
		ProductionAssigner assigner;
		if (Boolean.parseBoolean(config.get(Configuration.PROP_AUCTION_USE_START_PRICE)) == true) {
			assigner = new StartPriceProductionAssigner();
		} else {
			assigner = new NonStartPriceProductionAssigner(); 
		}
		
		boolean flagUseRandom = new Boolean(config.get(Configuration.PROP_AUCTION_USE_RANDOM));
		assigner.useRandomItems(flagUseRandom);
		
		return assigner;
	}
	
	
	@Provides @Singleton
	AuctionBoard getAuctionBoard(Configuration config) {
		if (Boolean.parseBoolean(config.get(Configuration.PROP_AUCTION_USE_START_PRICE)) == true) {
			return new StartPriceAuctionBoard();
		}
		
		return new TrueWeightedAuctionBoard(); 
	}
}
