package es.siani.energyagents.injection;

//import static org.mockito.Matchers.eq;
//import static org.mockito.Matchers.isA;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//import java.util.Date;

import com.google.inject.AbstractModule;

import es.siani.energyagents.services.LoadForecastService;
import es.siani.energyagents.services.LoadForecastServiceBatisImpl;

public class ServicesModule extends AbstractModule {

	/**
	 * Dependency Injector module for the services provider by the ASPEM.
	 */
	@Override
	protected void configure() {
		bind(LoadForecastService.class).to(LoadForecastServiceBatisImpl.class);
		
		return;
	}

}
