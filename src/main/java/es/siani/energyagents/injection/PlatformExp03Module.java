package es.siani.energyagents.injection;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;

import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.AgentsPlatformImpl;
import es.siani.energyagents.agent.XmlUserPrefsHandler;
import es.siani.energyagents.agent.XmlUserPrefsHandlerImpl;
import es.siani.energyagents.injection.annotations.OntologyName;
import es.siani.energyagents.injection.annotations.OntologyPackage;
import es.siani.energyagents.injection.annotations.PlatformId;
import es.siani.energyagents.ontology.as00.EnergyAgentsOntology;

public class PlatformExp03Module extends AbstractModule {
	
	protected static final String PLATFORM_ID = "http://siani.es/ignphd/agency-services";
	protected static final String ONTOLOGY_NAME = "agency-services-ontology";
	protected static final String ONTOLOGY_PACKAGE = 
			"es.siani.energyagents.ontology.as00;es.siani.energyagents.ontology.as03";
	
	
	@Override
	protected void configure() {
		bind(AgentsPlatform.class).to(AgentsPlatformImpl.class).in(Scopes.SINGLETON);
		bindConstant().annotatedWith(OntologyName.class).to(ONTOLOGY_NAME);
		bindConstant().annotatedWith(OntologyPackage.class).to(ONTOLOGY_PACKAGE);
		bind(Codec.class).to(SLCodec.class);
		bind(Ontology.class).to(EnergyAgentsOntology.class);
		bind(XmlUserPrefsHandler.class).to(XmlUserPrefsHandlerImpl.class);
		
		bindConstant().annotatedWith(PlatformId.class).to(PLATFORM_ID);
		
		return;
	}	
}
