package es.siani.energyagents.injection;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Singleton;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

import es.siani.energyagents.config.Configuration;
import es.siani.energyagents.config.ConfigurationLoader;


public class ConfigModule extends AbstractModule {

	/**
	 * Dependency Injector module for the configuration object.
	 */
	@Override
	protected void configure() {
		return;
	}
	
	
	/**
	 * Provider for instancing the Configuration object.
	 */
	@Provides @Singleton
	Configuration configurationLoader() {
		ConfigurationLoader loader = new ConfigurationLoader();
		Configuration config = null;
		try {
			config = loader.getConfiguration();
		} catch (IllegalArgumentException argex) {
			try {
				getLogger().log(Level.SEVERE, "The proposed configuration file couldn't be loaded. Loading the internal configuration file.");
				return loader.getConfiguration();
			} catch (IllegalArgumentException innerargex) {
				getLogger().log(Level.SEVERE, "No configuration file has been loaded.");
			}
		}
		
		return config;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(ConfigModule.class.getName());
	}
}
