package es.siani.energyagents.config;

import java.util.Hashtable;


public class ConfigurationImpl implements Configuration {
	
	/** Store of the configuration properties. */
	private Hashtable<String, String> props;
	
	
	/**
	 * Constructor.
	 */
	public ConfigurationImpl() {
		this.props = new Hashtable<String, String>();
		return;
	}
	

	/**
	 * Return the value of a property as a String object.
	 * @throws IllegalArgumentException if the property is not recognized.
	 */	
	@Override
	public String get(String propName) {
		
		if (!isValidPropName(propName)) {
			throw new IllegalArgumentException(
					"Failed to read property '" + propName + "' from the configuration file."
					+ " The specified property is not supported.");
		}
		
		return this.props.get(propName);
	}
	
	
	/**
	 * Set a configuration property.
	 */
	@Override
	public void set(String propName, String propValue) {
		this.props.put(propName, propValue);
		return;
	}
	
	
	/**
	 * Determines if the specified property name is valid.
	 */
	static boolean isValidPropName(String propName) {
		final String[] validFieldNames = ConfigurationLoader.validFieldNames;
		for (int n = 0; n < validFieldNames.length; n++) {
			if (propName.equals(validFieldNames[n])) {
				return true;
			}
		}
		
		return false;
	}	

}
