package es.siani.energyagents.config;

public interface Configuration {
	
	public static final String PROP_DB_DRIVER = "db.forecast.driver";
	public static final String PROP_DB_URL = "db.forecast.url";
	public static final String PROP_DB_USERNAME = "db.forecast.username";
	public static final String PROP_DB_PASSWORD = "db.forecast.password";
	
	public static final String PROP_JADE_DF_PATH = "jade.df.path";
	
	public static final String PROP_AUCTION_BLOCK_SECONDS = "auction.blockSeconds";
	public static final String PROP_AUCTION_CLEAR_ROUNDS = "auction.clearRounds";
	public static final String PROP_AUCTION_DF_AGENTS = "auction.dfAgents";
	public static final String PROP_AUCTION_OVERBOOKING_FACTOR = "auction.overbooking";
	public static final String PROP_AUCTION_USE_START_PRICE = "auction.useStartPrice";
	public static final String PROP_AUCTION_USE_RANDOM = "auction.useRandom";
	public static final String PROP_AUCTION_CF = "auction.cf";
	
	/**
	 * Set the value of a property.
	 */
	public void set(String propName, String propValue);
	
	/**
	 * Return the value of a property.
	 */
	public String get(String propName);

}
