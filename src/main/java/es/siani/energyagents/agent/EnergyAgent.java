package es.siani.energyagents.agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.ontology.as00.EventConcept;
import es.siani.simpledr.EasyDistributeEvent;
import jade.content.ContentManager;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import static es.siani.energyagents.utils.NotificationUtils.*;

@SuppressWarnings("serial")
public class EnergyAgent extends Agent {
	
	public static final String IDX_ARG_PLATFORM = "platform";
	public static final String IDX_ARG_USER_PREFS = "user-prefs";
	public static final String IDX_ARG_TRACKER = "tracker";
	public static final String IDX_ARG_CONTAINER = "container";
	public static final String IDX_ARG_ASPEM_AID = "aspem-aid";
	
	/** Name of the service to identify the type of agent. */
	public static final String SD_AGENT_TYPE = "agent-type";
	
	/** Name of the service to identify the name of the container. */
	public static final String SD_CONTAINER_NAME = "container-name";
	
	/** Broker Agent service's type of the FIPA's Services Descriptor. */
	public static final String SD_TYPE_BROKER = "broker";
	
	/** Gridop Agent service's type of the FIPA's Services Descriptor. */
	public static final String SD_TYPE_GRIDOP = "gridop";
	
	/** Aspem Agent service's type of the FIPA's Services Descriptor. */
	public static final String SD_TYPE_ASPEM = "aspem";
	
	/** Agents platform to which the agent belongs. */
	protected AgentsPlatform platform;
	
	/** Name of the container in which the agent is executed. */
	protected String containerName;
	
	/** Tracker of the actions of the agent. */
	protected AgentTracker tracker;
	
	/** OadrDistributeEvent that contains the active events of the DR program. */
	protected EasyDistributeEvent oadrDistributeEvent;
	
	/**
	 * Children events. List of the particular events (events that are not shared by the rest
	 * of the children) that have sent to each child.
	 */
	protected Map<AID, List<EventConcept>> childEvents;
	
	
	/**
	 * Placeholder for the startup code of the agents. This method must be called by the
	 * subclasses in order to get initialized member fields.
	 */
	@Override
	protected void setup() {
		this.parseBasicArguments();
		
		// Registers the codec and ontology used in the platform.
		ContentManager contentManager = this.getContentManager();
		contentManager.registerLanguage(platform.getCodec());
		contentManager.registerOntology(platform.getOntology());
		
		HashMap<String, String> servicesMap = new HashMap<String, String>();
		servicesMap.put(SD_CONTAINER_NAME, containerName);
		if (this instanceof GridopAgent) {
			servicesMap.put(SD_AGENT_TYPE, SD_TYPE_GRIDOP);
		} else if (this instanceof BrokerAgent) {
			servicesMap.put(SD_AGENT_TYPE, SD_TYPE_BROKER);
		} else if (this instanceof AspemAgent) {
			servicesMap.put(SD_AGENT_TYPE, SD_TYPE_ASPEM);
		}
		registerWithYellowPages(servicesMap);
		
		// Init the map of children events.
		this.childEvents = new HashMap<AID, List<EventConcept>>();
		
		return;
	}
	
	
	/**
	 * Get the platform to which belongs the agent.
	 */
	final private void parseBasicArguments() {
		
		Object[] args = this.getArguments();
		if ((args.length < 1) || (!(args[0] instanceof Map<?, ?>))) {
			getLogger().log(Level.SEVERE, "Failed to read the basic arguments of the agent.");
		}
		
		// Gets the AgentsPlatform object.
		try {
			@SuppressWarnings("unchecked")
			Map<String, Object> argsMap = (Map<String, Object>)args[0]; 
			this.platform = (AgentsPlatform)argsMap.get(IDX_ARG_PLATFORM);
			this.containerName = (String)argsMap.get(IDX_ARG_CONTAINER);
			this.tracker = (argsMap.get(IDX_ARG_TRACKER) != null)? (AgentTracker)argsMap.get(IDX_ARG_TRACKER) : null;
		} catch (RuntimeException ex) {
			getLogger().log(Level.SEVERE, "Failed to get an AgentsPlatform instance in the arguments of the agent.");
			throw new IllegalArgumentException(ex);
		}
		
		return;
	}
	
	
	/**
	 * Register the Gridop agent with the FIPA yellow pages.
	 */
	protected void registerWithYellowPages(Map<String, String> servicesMap) {
		
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.addLanguages(platform.getCodec().getName());
		dfd.addOntologies(platform.getOntology().getName());
		
		for (Map.Entry<String, String> srv : servicesMap.entrySet()) {
			ServiceDescription sd = new ServiceDescription();
			sd.setName(srv.getKey());
			sd.setType(srv.getValue());
			dfd.addServices(sd);
		}
		
		try {
			DFService.register(this, this.platform.getDefaultFacilitatorAID(), dfd);
		} catch (FIPAException ex) {
			getLogger().log(Level.SEVERE, "Failed to register the agent's services with the FIPA yellow-pages service.");
			throw new IllegalStateException(ex);
		}
	}	
	
	
	/**
	 * @see jade.core.Agent#takeDown()
	 */
	@Override
	protected void takeDown() {
		
		// Deregister from the yellow-pages.
		try {
			DFService.deregister(this, this.platform.getDefaultFacilitatorAID());
		} catch (FIPAException fex) {
			getLogger().log(Level.WARNING, "Error during deregistering the Brokering Service from the yellow-pages.");
		}
		
		return;	
	}
	
	
	/**
	 * Return the AgentTracker.
	 */
	public AgentTracker getTracker() {
		return this.tracker;
	}
	
	
	/**
	 * Set the AgentTracker.
	 */
	public void setTracker(AgentTracker tracker) {
		this.tracker = tracker;
		return;
	}
	
	
	/**
	 * Get the AgentsPlatform to which the agents belong.
	 */
	public AgentsPlatform getPlatform() {
		return this.platform;
	}
	
	
	/**
	 * Get the OadrDistributeEvent.
	 */
	public EasyDistributeEvent getDistributeEvent() {
		return this.oadrDistributeEvent;
	}
	
	
	/**
	 * Set the OadrDistributeEvent.
	 */
	public void setDistributeEvent(EasyDistributeEvent oadrDistributeEvent) {
		this.oadrDistributeEvent = oadrDistributeEvent;
		return;
	}
	
	
	/**
	 * Update the status of the Distribute Event according to the current time of the model.
	 */
	public void updateDistributeEvent(long currentModelTime, List<EventConcept> newEvents) {
		
		this.oadrDistributeEvent.removeFinishedEvents();
		this.oadrDistributeEvent.updateEventsStatus(currentModelTime);
		
		// Remove the children particular events that have expired.
		for (Map.Entry<AID, List<EventConcept>> entry : this.childEvents.entrySet()) {
			Iterator<EventConcept> it = entry.getValue().iterator();
			while (it.hasNext()) {
				EventConcept event = it.next();
				if (currentModelTime > (event.getStart().getTime() + (event.getDuration() * 1000))) {
					it.remove();
				}
			}
		}
		
		
		// If there are new events, add them to the EasyDistributeEvent.
		if (newEvents != null) {
			for (EventConcept event : newEvents) {
				this.oadrDistributeEvent.addEvent(toAsdrEvent(event), currentModelTime);
			}
		}
		
		return;
	}
	
	
	/**
	 * Add a new child specific event.
	 */
	public void addChildEvent(AID aid, EventConcept event) {
		
		List<EventConcept> events = this.childEvents.get(aid);
		if (events == null) {
			events = new ArrayList<EventConcept>();
			events.add(event);
			this.childEvents.put(aid, events);
		} else {
			events.add(event);
		}
		
		return;
		
	}
	
	
	/**
	 * Return the particular events of a child.
	 */
	public List<EventConcept> getChildEvents(AID aid) {
		return this.childEvents.get(aid);
	}
	
	
	/**
	 * Get the container's name.
	 */
	public String getContainerName() {
		return this.containerName;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(EnergyAgent.class.getName());
	}

}
