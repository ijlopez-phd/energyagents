package es.siani.energyagents.agent;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import es.siani.energyagents.model.UserPrefs;
import es.siani.energyagents.model.UserPrefsSchedule;

public class XmlUserPrefsHandlerImpl extends XmlUserPrefsHandler {
	
	/** String-based buffer to store content of the elements while they are parsed. */
	private StringBuilder strBuffer;
	
	/** Array to store the prices for each level. */
	private float[] levelsPrices; 
	
	/** Value object that stores the preferences of the user. */
	private UserPrefs userPrefs;
	
	
	
	/**
	 * Constructor.
	 */
	public XmlUserPrefsHandlerImpl() {
		return;
	}
	
	
	/** 
	 * @see es.siani.energyagents.agent.XmlUserPrefsHandler#getUserPrefs()
	 */
	@Override
	public UserPrefs getUserPrefs() {
		return this.userPrefs;
	}
	
	
	/**
	 * @see es.siani.energyagents.agent.XmlUserPrefsHandler#reset()
	 */
	@Override
	public void reset() {
		this.strBuffer = null;
		this.userPrefs = null;
		
		return;
	}
	
	
	/**
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(String, String, String, Attributes)
	 */
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attr) throws SAXException {
		
		if (localName.equals("userPrefs")) {
			this.userPrefs = new UserPrefs();
			
		} else if (localName.equals("levelsSchedule")) {
			this.strBuffer = new StringBuilder();
			String scheduleName = attr.getValue("name");
			if (scheduleName == null) {
				throw new SAXException("Attribute 'name' not found in the element 'levelsSchedule'.");
			}
			UserPrefsSchedule sc = new UserPrefsSchedule(scheduleName);
			this.userPrefs.setLevelsSchedule(sc);
			
		} else if (localName.equals("priority") || localName.startsWith("levelPrice") || localName.equals("startingPrice")) {
			this.strBuffer = new StringBuilder();
			
		} else if (localName.equals("levelsPrices")) {
			this.levelsPrices = new float[3];
		}
		
		return;
	}
	
	
	/**
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		if (this.strBuffer == null) {
			return;
		}
		
		this.strBuffer.append(ch, start, length);
		return;
	}
	
	
	/**
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(String, String, String)
	 */
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		
		if (localName.equals("levelsSchedule")) {
			this.userPrefs.getLevelsSchedule().setDefinition(this.strBuffer.toString());

		} else if (localName.equals("priority")) {
			this.userPrefs.setPriority(Integer.parseInt(this.strBuffer.toString()));
			
		} else if (localName.equals("levelPrice1")) {
			this.levelsPrices[0] = Integer.parseInt(this.strBuffer.toString());
			
		} else if (localName.equals("levelPrice2")) {
			this.levelsPrices[1] = Integer.parseInt(this.strBuffer.toString());
			
		} else if (localName.equals("levelPrice3")) {
			this.levelsPrices[2] = Integer.parseInt(this.strBuffer.toString());
			
		} else if (localName.equals("levelsPrices")) {
			this.userPrefs.setLevelsPrices(this.levelsPrices);
			this.levelsPrices = null;
			
		} else if (localName.equals("startingPrice")) {
			this.userPrefs.setStartingPrice(Float.parseFloat(this.strBuffer.toString()));
		}
		
		this.strBuffer = null;		
		
		return;
	}
}