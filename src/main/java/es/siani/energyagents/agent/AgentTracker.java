package es.siani.energyagents.agent;

public abstract class AgentTracker {
	
	public abstract void simulationStarted();
	
	public abstract void simulationFinished();
	
	public abstract void simulationPaused();
	
	public abstract void distributeEventReceived();
	
	public abstract void distributeEvent(float[] payloads);
	
	public abstract void prepareAuctionReceived();
	
	public abstract void startAuctionReceived();
	
	public abstract void clearAuctionReceived();
	
	public abstract void abortAuctionReceived();
	
	public abstract void prepareStageCompleted();
	
	public abstract void startStageCompleted();
	
	public abstract void clearStageCompleted();
	
	public abstract void callForProducingOfferReceived();
	
	public abstract void auctionDemandCovered(int idxBlock, float amount);
	
	public abstract void auctionOfferAccepted(int idxBlock, float amount);
	
}
