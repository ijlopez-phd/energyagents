package es.siani.energyagents.agent;

import jade.core.AID;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.EnergyAgentsException;
import es.siani.energyagents.behaviours.as00.BrokerProcessBehaviour;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.energyagents.model.UserPrefs;
import es.siani.energyagents.model.UserPrefsSchedule;
import es.siani.energyagents.utils.CronScheduleStore;
import es.siani.energyagents.xmpp.SimpleXmpp;
import es.siani.simpledr.EasyDistributeEvent;

@SuppressWarnings("serial")
public class BrokerAgent extends EnergyAgent {
	
	SimpleXmpp simpleXmpp;
	UserPrefs userPrefs;
	AID aspemAID;
	
	private static final Object lock = new Object();
	
	
	/**
	 * Initialize the resources used by the agent and add the default behaviours. 
	 */
	@Override
	protected void setup() {
		super.setup();
		
		// Parse the user preferences passed as arguments.
		this.userPrefs = this.parseUserPrefs();
		if (userPrefs != null) {
			UserPrefsSchedule levelsSchedule = userPrefs.getLevelsSchedule();
			if (levelsSchedule != null) { // The scheduled is stored in the cache of schedules.
				synchronized(lock) {
					CronScheduleStore cronSchedules = InjectionUtils.injector().getInstance(CronScheduleStore.class);
					cronSchedules.saveSchedule(levelsSchedule.getName(), levelsSchedule.getDefinition());
				}
			}
			
			userPrefs.compact();
		}
		
		// Get the Aspem's AID.
		this.aspemAID = this.getAspemAID();
		
		// Connect to the XMPP server.
		try {
			this.simpleXmpp = InjectionUtils.injector().getInstance(SimpleXmpp.class);
			this.simpleXmpp.connect(this.getLocalName());
		} catch (EnergyAgentsException ex) {
			getLogger().log(Level.SEVERE, "Failed to connect to the XMPP server.");
		}
		
		// Initialize the EasyDistributeEvent attached to the agent.
		this.oadrDistributeEvent = 
				new EasyDistributeEvent(this.platform.getId(), this.getAID().getLocalName());		
		
		// Add the default behaviour.
		BrokerProcessBehaviour processBehaviour =
				InjectionUtils.injector().getInstance(BrokerProcessBehaviour.class);
		processBehaviour.init(this, null);
		this.addBehaviour(processBehaviour);
		
		return;
	}
	
	
	/**
	 * Get the Aspem's AID to which the Broker agent is attached to.
	 * The AID is gotten from the agent's input arguments.
	 */
	private AID getAspemAID() {
		Object[] args = this.getArguments();
		if (! (args[0] instanceof Map<?, ?>)) {
			getLogger().log(Level.SEVERE, "Falied to read the Aspem's AID from the arguments of the agent.");
		}
		
		AID aspemAID;
		try {
			@SuppressWarnings("unchecked")
			Map<String, Object> argsMap = (Map<String, Object>)args[0];
			aspemAID = (AID)argsMap.get(IDX_ARG_ASPEM_AID);
		} catch (RuntimeException ex) {
			getLogger().log(Level.SEVERE, "Falied to read the argument of the Broker Agent.", ex);
			throw new IllegalArgumentException(ex);			
		}
		
		return aspemAID;
	}
	
	
	/**
	 * Parse the UserPrefs of the user.
	 */
	private UserPrefs parseUserPrefs() {
		
		Object[] args = this.getArguments();
		if (! (args[0] instanceof Map<?,?>)) {
			getLogger().log(Level.SEVERE, "Falied to read the basic arguments of the agent.");
		}
		
		// Get the user preferences.
		UserPrefs userPrefs = null;
		try {
			@SuppressWarnings("unchecked")
			Map<Integer, Object> argsMap = (Map<Integer, Object>)args[0];
			userPrefs = (UserPrefs)argsMap.get(IDX_ARG_USER_PREFS);
		} catch (RuntimeException ex) {
			getLogger().log(Level.SEVERE, "Falied to read the argument of the Broker Agent.", ex);
			throw new IllegalArgumentException(ex);
		}
		
		return userPrefs;
	}
	
	
	/**
	 * Close the resources used by the agent.
	 */
	@Override
	protected void takeDown() {
		super.takeDown();
		
		// Disconnect from the XMPP server.
		this.simpleXmpp.disconnect();
		
		return;
	}
	
	
	/**
	 * Return the SimpleXmpp instance attached to the agent.
	 */
	public SimpleXmpp getSimpleXmpp() {
		return this.simpleXmpp;
	}
	
	
	/**
	 * Return the user preferences.
	 */
	public UserPrefs getUserPrefs() {
		return this.userPrefs;
	}
	
	
	/**
	 * Logger.
	 */
    private static final Logger getLogger() {
        return Logger.getLogger(BrokerAgent.class.getName());
    }	

}
