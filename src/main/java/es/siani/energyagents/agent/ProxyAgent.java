package es.siani.energyagents.agent;

import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.AgentsPlatform;
import es.siani.energyagents.behaviours.as00.SendRequestBehaviour;
import es.siani.energyagents.behaviours.as00.SendRequestPauseSimulationBehaviour;
import es.siani.energyagents.ontology.as00.FinishSimulationAction;
import es.siani.energyagents.ontology.as00.SimulationDescriptionConcept;
import es.siani.energyagents.ontology.as00.SimulationIdConcept;
import es.siani.energyagents.ontology.as00.SimulationPauseConcept;
import es.siani.energyagents.ontology.as00.StartSimulationAction;
import jade.content.ContentManager;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.lang.acl.ACLMessage;
import jade.wrapper.gateway.GatewayAgent;

@SuppressWarnings("serial")
public class ProxyAgent extends GatewayAgent {
	
	// Constants used to parse the arguments array
	private static final int N_REQUIRED_ARGUMENTS = 1;
	static final int IDX_PLATFORM = 0;	
	
	/** Agents platform to which the agent belongs. */
	AgentsPlatform platform;
	
	
	/**
	 * Initializes the agent.
	 */
	@Override
	protected void setup() {
		super.setup();
		
		// Parses the arguments.
		this.parseArguments();
		
		// Registers the codec and ontology used in the platform.
		ContentManager contentManager = this.getContentManager();
		contentManager.registerLanguage(platform.getCodec());
		contentManager.registerOntology(platform.getOntology());		
		
		return;
	}
	
	
	/**
	 * Actions that are carried out when this agent is shutting down.
	 */
	@Override
	protected void takeDown() {
		super.takeDown();
		return;
	}
	
	
	/**
	 * Initializes member variables with the corresponding values of the arguments array.
	 */
	private void parseArguments() {
		
		Object[] args = this.getArguments();
		
		if (args.length < N_REQUIRED_ARGUMENTS) {
			getLogger().log(Level.SEVERE, "Missing arguments of the Proxy agent");			
			throw new IllegalArgumentException();
		}
		
		// Get the AgentsPlatform object.
		try {
			this.platform = (AgentsPlatform)this.getArguments()[IDX_PLATFORM];
		} catch (Exception ex) {
			getLogger().log(Level.SEVERE, "A valid AgentsPlatform instance was not found as argument of the Broker Agent.");
			throw new IllegalArgumentException(ex);
		}
		
		return;
	}	
	
	
	/**
	 * Process the command passed as argument.
	 */
	@Override
	protected void processCommand(final Object oCommand) {
		
		if (!(oCommand instanceof Command)) {
			throw new IllegalArgumentException();
		}
		
		Command command = (Command)oCommand;
		String commandName = command.getName();
		
		AID gridopAID = this.platform.getGridopAgentAID(this);;

		
		SequentialBehaviour sb = new SequentialBehaviour(this);
		
		if (commandName.equals(Command.CMD_REQUEST_PAUSE_SIMULATION)) {
			sb.addSubBehaviour(new SendRequestPauseSimulationBehaviour(
					this.platform,
					this,
					gridopAID,
					new ACLMessage(ACLMessage.REQUEST),
					(SimulationPauseConcept)(command.getArguments()[0]),
					command));
			
		} else if (commandName.equals(Command.CMD_REQUEST_START_SIMULATION)) {
			sb.addSubBehaviour(new SendRequestBehaviour(
					this,
					new AID[] {gridopAID},
					new ACLMessage(ACLMessage.REQUEST),
					new StartSimulationAction(
							(SimulationDescriptionConcept)(command.getArguments()[0]))));
			
		} else if (commandName.equals(Command.CMD_REQUEST_FINISH_SIMULATION)) {
			sb.addSubBehaviour(new SendRequestBehaviour(
					this,
					new AID[] {gridopAID},
					new ACLMessage(ACLMessage.REQUEST),
					new FinishSimulationAction(
							(SimulationIdConcept)(command.getArguments()[0]))));
			
		} else if (commandName.equals(Command.CMD_SEARCH_GRIDOP_AGENT)) {
			command.setOutput(new Object[] {gridopAID});
			
		} else if (commandName.equals(Command.CMD_SEARCH_ASPEM_AGENTS)) {
			AID[] aspemAIDs = this.platform.getAspemAgentsAID(this);
			command.setOutput(aspemAIDs);
		} else if (commandName.equals(Command.CMD_SEARCH_BROKER_AGENTS)) {
			AID[] brokersAIDs =
					this.platform.getBrokerAgentsAID(this, (String)command.getArguments()[0]);
			command.setOutput(brokersAIDs);
		}
		
		sb.addSubBehaviour(new OneShotBehaviour(this) {
			public void action() {
				ProxyAgent.this.releaseCommand(oCommand);
			}
		});
		
		this.addBehaviour(sb);
		
		return;
	}
	
	
	/**
	 * Get the AgentsPlatform to which the agent belongs.
	 */
	public AgentsPlatform getPlatform() {
		return this.platform;
	}
	

	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(GridopAgent.class.getName());
	}
	
	
	/**
	 * Command accepted by the ProxyAgent.
	 */
	public static class Command {
		
		// Names of the commands
		public static final String CMD_REQUEST_PAUSE_SIMULATION = "cmd_pause_simulation";
		public static final String CMD_REQUEST_START_SIMULATION = "cmd_start_simulation";
		public static final String CMD_REQUEST_FINISH_SIMULATION = "cmd_end_simulation";
		public static final String CMD_SEARCH_GRIDOP_AGENT = "cmd_search_gridop_agent";
		public static final String CMD_SEARCH_ASPEM_AGENTS = "cmd_search_aspem_agents";
		public static final String CMD_SEARCH_BROKER_AGENTS = "cmd_search_broker_agents";
		
		
		
		/** Name of the command. */
		private final String name;
		
		/** Array of data objects that are passed as input arguments. */
		private Object[] arguments = null;
		
		/** Array of data object that work as output arguments. */
		private Object[] output = null;
		
		
		/**
		 * Constructor.
		 */
		public Command(String name) {
			this.name = name;
			return;
		}
		
		
		/**
		 * Constructor.
		 */
		public Command(String name, Object[] data) {
			this(name);
			this.arguments = data;
			return;
		}
		
		
		/**
		 * Get the Name.
		 */
		public String getName() {
			return this.name;
		}
		
		
		/**
		 * Get the array of input arguments.
		 */
		public Object[] getArguments() {
			return this.arguments;
		}
		
		
		/**
		 * Get the output data.
		 */
		public Object[] getOutput() {
			return this.output;
		}
		
		
		/**
		 * Set the output.
		 */
		public void setOutput(Object[] output) {
			this.output = output;
		}
	}
}
