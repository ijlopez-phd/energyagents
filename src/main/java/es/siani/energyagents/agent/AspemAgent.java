package es.siani.energyagents.agent;

import es.siani.energyagents.behaviours.as00.AspemProcessBehaviour;
import es.siani.energyagents.injection.InjectionUtils;
import es.siani.simpledr.EasyDistributeEvent;

@SuppressWarnings("serial")
public class AspemAgent extends EnergyAgent {
	
	
	/**
	 * Initializes the resources used by the agent.
	 */
	@Override
	protected void setup() {
		super.setup();
		
		// Initialize the EasyDistributeEvent attached to the agent.
		this.oadrDistributeEvent = 
				new EasyDistributeEvent(this.platform.getId(), this.getAID().getLocalName());		
		
		// Add the default behaviours of the agent.
		AspemProcessBehaviour processBehaviour = InjectionUtils.injector().getInstance(AspemProcessBehaviour.class);
		processBehaviour.init(this, null);
		this.addBehaviour(processBehaviour);

		return;
	}
	
	
//	/**
//	 * Logger.
//	 */
//	private static Logger getLogger() {
//		return Logger.getLogger(AspemAgent.class.getName());
//	}
}
