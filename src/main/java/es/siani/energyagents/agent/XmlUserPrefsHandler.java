package es.siani.energyagents.agent;

import org.xml.sax.helpers.DefaultHandler;
import es.siani.energyagents.model.UserPrefs;

public abstract class XmlUserPrefsHandler extends DefaultHandler {

	/**
	 * Get the UserPrefs.
	 */
	public abstract UserPrefs getUserPrefs();

	/**
	 * Reset the information stored in this instance from previous parsings. 
	 */
	public abstract void reset();
}