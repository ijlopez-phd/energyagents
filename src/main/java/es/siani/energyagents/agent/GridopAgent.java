package es.siani.energyagents.agent;

import jade.lang.acl.MessageTemplate;
import es.siani.energyagents.behaviours.as00.ReceiveRequestStartSimulationBehaviour;
import es.siani.energyagents.behaviours.as00.SimulationRequestMatcher;
import es.siani.energyagents.ontology.as00.StartSimulationAction;
import es.siani.simpledr.EasyDistributeEvent;

@SuppressWarnings("serial")
public class GridopAgent extends EnergyAgent {
	

	/**
	 * @see jade.core.Agent#setup()
	 */
	@Override
	protected void setup() {
		super.setup();
		
		this.oadrDistributeEvent = 
				new EasyDistributeEvent(this.platform.getId(), this.getAID().getLocalName());
		
		// Adds default behaviours.
		// - Receives the request for managing the start simulation event.
		this.addBehaviour(new ReceiveRequestStartSimulationBehaviour(
				this,
				new MessageTemplate(
						new SimulationRequestMatcher(this, StartSimulationAction.class))));

		
		return;
	}
	
	
	/**
	 * @see jade.core.Agent#takeDown()
	 */
	@Override
	protected void takeDown() {
		super.takeDown();
		
		return;
	}
	
	
//	/**
//	 * Logger.
//	 */
//	private static final Logger getLogger() {
//		return Logger.getLogger(GridopAgent.class.getName());
//	}
}