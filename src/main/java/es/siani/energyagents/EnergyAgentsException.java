package es.siani.energyagents;

@SuppressWarnings("serial")
public class EnergyAgentsException extends RuntimeException {

	/**
	 * Constructor.
	 */
	public EnergyAgentsException() {
		super();
	}
	

	/**
	 * Constructor.
	 */
	public EnergyAgentsException(String message, Throwable cause) {
		super(message, cause);
	}

	
	/**
	 * Constructor.
	 */
	public EnergyAgentsException(String message) {
		super(message);
	}

	
	/**
	 * Constructor.
	 */
	public EnergyAgentsException(Throwable cause) {
		super(cause);
	}
}
