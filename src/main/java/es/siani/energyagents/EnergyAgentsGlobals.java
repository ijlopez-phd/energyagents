package es.siani.energyagents;

public class EnergyAgentsGlobals {
	
	// Data provider constants.
	public static final String STORE_DEFAULT_ENVIRONMENT = "development";	
	
	// Configuration constants
	public static final String VAR_ENV_CONFIG_PATH = "EAGENTS_CONFIG";
	public static final String PARAM_SYS_CONFIG_PATH = "eagentsConfig";
	public static final String INTERNAL_CONFIG_PATH = "config.properties";
	
	public static final String W3C_XML_SCHEMA_NS_URI = "http://www.w3.org/2001/XMLSchema";
	
	public static final String DF_NAME = "energyAgentsDF";
	public static final String DF_INTERNAL_PATH = "/jade/df.properties";
	public static final int DF_DEFAULT_NUMBER = 1;
	
	public static final float OVERBOOKING_FACTOR = 0;
	public static final boolean USE_START_PRICE = false;
	public static final boolean USE_RANDOM = true;
	public static final int CONSTANT_F = 0;
	
	//public static String AUCTION_LOGGING_PATH = "//log/";
	public static String AUCTION_LOGGING_PATH = "/var/log/agency-services";
}
