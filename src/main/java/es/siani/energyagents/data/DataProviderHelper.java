package es.siani.energyagents.data;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.google.inject.Inject;

import es.siani.energyagents.EnergyAgentsGlobals;
import es.siani.energyagents.config.Configuration;

public class DataProviderHelper {
	
	/** Cache of SqlSessions. It stores the last opened for each environment. */
	private static HashMap<String, SqlSessionFactory> sessionsCache = new HashMap<String, SqlSessionFactory>();
	
	/** Properties object with the parameters to connect with the DB. */
	private final Properties props;
	
	
	/**
	 * Constructor.
	 */
	@Inject
	public DataProviderHelper(Configuration config) {
		this.props = new Properties();
		props.setProperty("driver", config.get(Configuration.PROP_DB_DRIVER));
		props.setProperty("url", config.get(Configuration.PROP_DB_URL));
		props.setProperty("username", config.get(Configuration.PROP_DB_USERNAME));
		props.setProperty("password", config.get(Configuration.PROP_DB_PASSWORD));
		return;
	}
	
	
	/**
	 * Get a instance of SqlSessionFactory that corresponds to the default 'environment'.
	 */
	public SqlSessionFactory getSQLSessionFactory() {
		return getSQLSessionFactory(EnergyAgentsGlobals.STORE_DEFAULT_ENVIRONMENT);
	}
	
	
	/**
	 * Get a SqlSessionFactory instance.
	 */
	public SqlSessionFactory getSQLSessionFactory(String environment) {
		
		SqlSessionFactory sqlSession = sessionsCache.get(environment);
		
		if (sqlSession == null) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						this.getClass().getResourceAsStream("/mybatis/config.xml")));
				sqlSession = new SqlSessionFactoryBuilder().build(reader, environment, this.props);
			} catch (Exception ex) {
				getLogger().log(Level.SEVERE, "Trying to create the SqlSessionFactory of mybatis.");
			}
		}
		
		return sqlSession;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(DataProviderHelper.class.getName());
	}
}
