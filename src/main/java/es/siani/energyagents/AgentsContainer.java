package es.siani.energyagents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import es.siani.energyagents.agent.EnergyAgentsDF;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

public abstract class AgentsContainer {
	
	/** Reference to the encapsulated JADE container. */
	protected ContainerController container;
	
	/** Registered listeners. */
	ArrayList<AgentsContainerListener> listeners =
			new ArrayList<AgentsContainerListener>();
	
	/** Store of the created directory facilitators. */
	Map<String, AgentController> cacheDF = new HashMap<String, AgentController>();
	

	/**
	 * Tells whether the container is killed.
	 */
	public boolean isKilled() {
		if (this.container == null) {
			return true;
		} else if (!this.container.isJoined()) {
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Kills the container.
	 */
	public void kill() {
		
		if (this.isKilled()) {
			return;
//			throw new IllegalStateException("This container is not alive.");
		}
		
		String containerName = this.getName();
		try {
			this.container.kill();
		} catch (Throwable t) {
			getLogger().log(Level.WARNING, "Warning: Failed to close the container.", t);
		} finally {
			this.container = null;
			for (AgentsContainerListener listener : this.listeners) {
				listener.containerKilled(containerName, this);
			}
		}
		
		return;
	}
	
	
	/**
	 * Returns the Name of the container.
	 */
	public String getName() {

		if (this.isKilled()) {
			throw new IllegalStateException("The container is not operative.");			
		}
		
		String containerName;
		try {
			containerName = this.container.getContainerName();
		} catch (Exception ex) {
			throw new IllegalStateException("The container is not operative.");
		}
		
		return containerName;
	}
	
	
	/**
	 * Adds a new listener of the container's events.
	 */
	public void addListener(AgentsContainerListener listener) {
		this.listeners.add(listener);
		return;
	}
	
	
	/** Create a new Directory Facilitator attached to the container. */
	public void createFacilitator(String name, String jadePropertiesFilePath) {
		
		if (jadePropertiesFilePath == null) {
			// Use the internal file configuration.
			jadePropertiesFilePath = AgentsContainer.class.getResource(EnergyAgentsGlobals.DF_INTERNAL_PATH).getPath();
		}
		
		try {
			AgentController dfAgent = this.container.createNewAgent(
					name,
					EnergyAgentsDF.class.getName(),
					new Object[] {jadePropertiesFilePath});
			dfAgent.start();
			this.cacheDF.put(name, dfAgent);
		} catch (StaleProxyException ex) {
			getLogger().log(Level.SEVERE, "Failed to create the Directory Facilitator agent.", ex);
			throw new IllegalStateException(ex);			
		}
		
		return;
	}
	
	
	/** Kill the agent that representes a Directory Facilitator attached to the container. */
	public void killFacilitator(String name) {
		
		AgentController dfAgent = this.cacheDF.get(name);
		if (dfAgent == null) {
			throw new IllegalArgumentException("Failed to find the Directory Facilitator named '" + name + "'.");
		}
		
		try {
			dfAgent.kill();
		} catch (StaleProxyException e) {
			getLogger().log(Level.WARNING, "Failed to kill the Directory Facilitator named '" + name + "'.");
		}
		
		return;
	}
	
	
	/**
	 * Returns the JADE's container (for testing purposes).
	 */
	ContainerController container() {
		return this.container;
	}
	
	
	/**
	 * Logger.
	 */
	private static Logger getLogger() {
		return Logger.getLogger(AgentsContainer.class.getName());
	}

}
