package es.siani.energyagents;

public interface AgentsContainerListener {
	
	public void containerKilled(String containerName, AgentsContainer container);

}
