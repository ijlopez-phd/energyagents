package es.siani.energyagents;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import jade.wrapper.gateway.JadeGateway;
import es.siani.energyagents.agent.AgentTracker;
import es.siani.energyagents.agent.EnergyAgent;
import es.siani.energyagents.agent.GridopAgent;

public class GridopContainer extends AgentsContainer {
	
	/** Agents Platform to which belongs the container. */
	protected final AgentsPlatform platform;
	
	/** Gridop agent attached to the container. */
	protected AgentController gridopAgent;
	
	
	/**
	 * Constructor.
	 */
	GridopContainer(final AgentsPlatform platform) {
		this.platform = platform;
		
		jade.util.leap.Properties props = new jade.util.ExtendedProperties();
		props.setProperty(jade.core.Profile.MAIN, "true");
		props.setProperty(jade.core.Profile.PLATFORM_ID, platform.getId());
		jade.core.Profile jadeProfile = new jade.core.ProfileImpl(props);
		this.container = jade.core.Runtime.instance().createMainContainer(jadeProfile);	
		
		return;
	}
	
	
	/**
	 * Adds the Gridop agent to the platform.
	 */
	AgentController initGridopAgent(AgentTracker tracker) {
		
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(EnergyAgent.IDX_ARG_PLATFORM, this.platform);
		args.put(EnergyAgent.IDX_ARG_CONTAINER, this.getName());
		
		if (tracker != null) {
			args.put(EnergyAgent.IDX_ARG_TRACKER, tracker);
		}
		
		this.gridopAgent = null;
		try {
			this.gridopAgent = this.container.createNewAgent(
					AgentsPlatform.GRIDOP_AGENT_NAME,
					GridopAgent.class.getName(),
					new Object[] {args});
			this.gridopAgent.start();
		} catch (StaleProxyException ex) {
			getLogger().log(Level.SEVERE, "Failed to create the Gridop agent.", ex);
			throw new IllegalStateException(ex);
		}
		
		return this.gridopAgent;
	}
	
	
	/**
	 * Kill the main container and the JadeGateway is clear.
	 */
	@Override
	public void kill() {
		JadeGateway.shutdown();		
		super.kill();
		return;
	}
	
	
	
	
	/**
	 * Logger.
	 */
	private static Logger getLogger() {
		return Logger.getLogger(GridopContainer.class.getName());
	}
}
