package es.siani.energyagents.model;

public class UserPrefsSchedule {
	
	/** Name attached to the schedule. */
	private String name;
	
	
	/** Definition of the schedule. */
	private String definition;
	
	
	/** Flag that indicates if the information of this schedule has been compacted. */
	private boolean compacted;
	
	
	/**
	 * Constructor.
	 */
	public UserPrefsSchedule(String name) {
		this(name, null);
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public UserPrefsSchedule(String name, String definition) {
		this.name = name;
		this.definition = definition;
		this.compacted = (this.definition == null)? true : false;
		
		return;
	}
	
	
	/**
	 * Compact the information: the definition is removed.
	 */
	public void compact() {
		this.definition = null;
		return;
	}
	
	
	/**
	 * Get the Compacted flag.
	 */
	public boolean isCompacted() {
		return this.compacted;
	}


	/**
	 * Get the Name.
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * Set the Name.
	 */
	public void setName(String name) {
		this.name = name;
		return;
	}


	/**
	 * Get the Definition.
	 */
	public String getDefinition() {
		return this.definition;
	}


	/**
	 * Set the Definition.
	 */
	public void setDefinition(String definition) {
		this.definition = definition;
		this.compacted = (this.definition == null)? true : false;
		return;
	}

}
