package es.siani.energyagents.model;

public class PauseResult {
	
	/** Constant value that indicates that no more pauses are required. */
	public static final int NO_PAUSE = -1;
	
	/** Next time the GridOperator must be notified about a pause. */
	protected long nextPause;
	
	/** Tells whether DistributeActionEvents have been triggered during this call. */
	protected boolean distributeActionTriggered;
	
	
	/**
	 * Constructor.
	 */
	public PauseResult() {
		this.nextPause = -1;
		this.distributeActionTriggered = false;
	}
	
	
	/**
	 * Constructor.
	 */
	public PauseResult(long nextPause, boolean distributeActionTriggered) {
		this.nextPause = nextPause;
		this.distributeActionTriggered = distributeActionTriggered;
		return;
	}
	
	
	/**
	 * Get the NextPause.
	 */
	public long getNextPause() {
		return this.nextPause;
	}
	
	
	/**
	 * Set the NextPause.
	 */
	public void setNextPause(long nextPause) {
		this.nextPause = nextPause;
		return;
	}
	
	
	/**
	 * Get DistributeActionTriggered.
	 */
	public boolean getDistributeActionTriggered() {
		return this.distributeActionTriggered;
	}
	
	
	/**
	 * Set DistributeActionTriggered.
	 */
	public void setDistributeActionTriggered(boolean distributeActionTriggered) {
		this.distributeActionTriggered = distributeActionTriggered;
		return;
	}
}
