package es.siani.energyagents.model;

import es.siani.energyagents.utils.CronLevelsScheduleStore;

public class UserPrefs {
	
	/**
	 * Enumeration for defining the mode of the contract: none, easy or hard.
	 *
	 */
	public enum LoadMode {
		NONE("none"),
		EASY("easy"),
		HARD("hard");
		
		private final String text;
		
		/**
		 * Constructor.
		 */
		LoadMode(String text) {
			this.text = text;
		}
		
		@Override
		public String toString() {
			return this.text;
		}
	};
	
	public static final int NO_LEVEL = -1;
	
	public static final int NULL_PAYLOAD = 0;
	
	private static final float[] DEFAULT_LEVELS_PRICES = new float[] {100, 100, 100};
	
	/** Name of the schedule that defines the willigness of the client to accept DR levels. */
	private UserPrefsSchedule levelsSchedule;
	
	/** Prices (over the 100%) to which the client is willing to offer the energy of each level. */
	private float[] levelsPrices = DEFAULT_LEVELS_PRICES;
	
	/** Level of priority contracted by the user. */
	private Integer priority = 0;
	
	/** Level at which the user is willing to shed or protect his/her load. */
	private float payload = NULL_PAYLOAD;
	
	/** Starting price of the consumer. */
	private float startingPrice = 0;
	
	
	/**
	 * Constructor.
	 */
	public UserPrefs() {
		return;
	}
	
	
	/**
	 * Compact the information of the object.
	 * Specifically it removes the definitions of all the schedules.
	 */
	public void compact() {
		
		if (this.levelsSchedule != null) {
			this.levelsSchedule.compact();
		}
		
		return;
	}


	/**
	 * Get the LevelsScheduleName.
	 */
	public UserPrefsSchedule getLevelsSchedule() {
		return levelsSchedule;
	}


	/**
	 * Set the LevelsScheduleName.
	 */
	public void setLevelsSchedule(UserPrefsSchedule levelsSchedule) {
		this.levelsSchedule = levelsSchedule;
	}


	/**
	 * Get the Priority.
	 */
	public Integer getPriority() {
		return priority;
	}


	/**
	 * Set the priority.
	 */
	public void setPriority(Integer priority) {
		this.priority = (priority == null)? 0 : priority;
	}
	
	
	/**
	 * Get the array of levels prices.
	 */
	public float[] getLevelsPrices() {
		return this.levelsPrices;
	}
	
	
	/**
	 * Set the array of levels prices.
	 */
	public void setLevelsPrices(float[] levelsPrices) {
		this.levelsPrices = (levelsPrices == null)? DEFAULT_LEVELS_PRICES : levelsPrices;
		return;
	}
	
	
	/**
	 * Get the starting price.
	 */
	public float getStartingPrice() {
		return this.startingPrice;
	}
	
	
	/**
	 * Set the starting price.
	 */
	public void setStartingPrice(float startingPrice) {
		this.startingPrice = startingPrice;
		return;
	}
	
	
	/**
	 * Get the payload.
	 */
	public float getPayload() {
		return this.payload;
	}
	
	
	/**
	 * Set the payload.
	 */
	public void setPayload(float payload) {
		this.payload = payload;
		return;
	}
	
	
	/**
	 * Get token that identifies the load mode.
	 */
	public String getLoadMode() {
		if (CronLevelsScheduleStore.isConsumer((int)payload)) {
			return LoadMode.HARD.toString();
		} else if (CronLevelsScheduleStore.isProducer((int)payload)) {
			return LoadMode.EASY.toString();
		}
		
		return LoadMode.NONE.toString();
	}
}
