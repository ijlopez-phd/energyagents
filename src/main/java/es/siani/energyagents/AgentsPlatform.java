package es.siani.energyagents;

import java.util.Date;

import jade.content.lang.Codec;
import jade.content.onto.Ontology;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.MessageTemplate;
import es.siani.energyagents.agent.AgentTracker;
import es.siani.energyagents.model.PauseResult;
import es.siani.simpledr.model.AsdrProgram;

public interface AgentsPlatform {
	
	public static final String PROP_JADE_DF_PATH = "jade.df.path";
	public static final String GRIDOP_AGENT_NAME = "MasterAgent";
	
	/** Gets the Id of the agents' platform. */
	public String getId();
	
	/** Gets the Codec used by the Platform. */
	public Codec getCodec();
	
	/** Gets the ontology used by the Platform. */
	public Ontology getOntology();
	
	/** Creates a container of Broker Agents. */
	public AspemContainer createAspemContainer(String name, AgentTracker tracker);
	
	/** Creates the container of the Grid Operator. */
	public GridopContainer createGridopContainer(AgentTracker tracker);
	
	/** Looks for the Gridop agent that is commanding the platform. */
	public AID getGridopAgentAID(Agent searcher);
	
	/** Looks for the ASPEM agents registered in the platform.*/
	public AID[] getAspemAgentsAID(Agent searcher);
	
	/** Looks for the Broker agents registered in the specified container. */
	public AID[] getBrokerAgentsAID(Agent searcher, String containerName);
	
	/** Return the AID of the Directory Facilitator agent. */
	public AID getDefaultFacilitatorAID();
	
	/**
	 * Returns a Message template that includes the basic filters corresponding
	 * to the properties of the platform.
	 */
	public MessageTemplate createBasicMessageTemplate();	
	
	/** Informs that a new Simulation has started. */
	public void informSimulationStarted(int idSimulation, String scenarioCode, String storeName, AsdrProgram program) throws EnergyAgentsException;
	
	/** Informs that a specific simulation has finished. */
	public void informSimulationFinished(int idSimulation) throws EnergyAgentsException;
	
	/** Informs that a specific simulation is paused. */
	public PauseResult informSimulationPaused(int idSimulation, Date realTime, Date modelTime) throws EnergyAgentsException;
	
	/** Set the value of a property. */
	public void setProperty(String name, String value);
	
	/** Get the value of a property. */
	public String getProperty(String name);
	
	/** Clear the values of the properties. */
	public void clearProperties();
}
