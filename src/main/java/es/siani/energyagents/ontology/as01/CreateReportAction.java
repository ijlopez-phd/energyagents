package es.siani.energyagents.ontology.as01;

import es.siani.simpledr.EasyCreateReport;
import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class CreateReportAction implements Predicate {
	
	private String createReportMessage;
	
	private long modelTimestamp;
	
	private String scenarioCode;
	
	private String storeName;
	
	
	/**
	 * Constructor (default).
	 */
	public CreateReportAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public CreateReportAction(EasyCreateReport easyCreateReport, String scenarioCode, String storeName, long modelTimestamp) {
		this.createReportMessage = easyCreateReport.toString();
		this.modelTimestamp = modelTimestamp;
		this.scenarioCode = scenarioCode;
		this.storeName = storeName;
		return;
	}


	/**
	 * Get the description of the Query.
	 */
	@Slot(mandatory=true)
	public String getCreateReportMessage() {
		return this.createReportMessage;
	}


	/**
	 * Set the description of the Query. 
	 */
	public void setCreateReportMessage(String createReportMessage) {
		this.createReportMessage = createReportMessage;
		return;
	}
	
	
	/**
	 * Get the current time of the simulator model.
	 */
	@Slot(mandatory = true)
	public long getModelTimestamp() {
		return this.modelTimestamp;
	}	
	
	
	/**
	 * Set the current time of the simulator model.
	 */
	public void setModelTimestamp(long modelTimestamp) {
		this.modelTimestamp = modelTimestamp;
		return;
	}
	
	
	/**
	 * Get the code of the scenario that is being simulated.
	 */
	@Slot(mandatory = true)
	public String getScenarioCode() {
		return this.scenarioCode;
	}
	
	
	/**
	 * Set the code of the scenario that is being simulated.
	 */
	public void setScenarioCode(String scenarioCode) {
		this.scenarioCode = scenarioCode;
		return;
	}
	
	
	/**
	 * Get the name of the store in which data related to the scenario is saved.
	 */
	@Slot
	public String getStoreName() {
		return this.storeName;
	}
	
	
	/**
	 * Set the name of the store in which data related to the scenario is saved.
	 */
	public void setStoreName(String storeName) {
		this.storeName = storeName;
		return;
	}
}
