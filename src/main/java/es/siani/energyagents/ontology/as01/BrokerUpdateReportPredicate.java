package es.siani.energyagents.ontology.as01;

import es.siani.energyagents.utils.CronLevelsScheduleStore;
import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class BrokerUpdateReportPredicate implements Predicate {
	
	public static int MODE_EASY = 0;
	public static int MODE_HARD = 1;
	public static int MODE_NONE = 2;
	
	protected float level0;
	protected float level1;
	protected float level2;
	protected float level3;
	protected int signalLevel;
	protected int priority;
	protected int mode;
	
	
	/**
	 * Constructor.
	 */
	public BrokerUpdateReportPredicate() {
		return;
	}


	/**
	 * Get the amount of load consumed with the profile Normal (0).
	 */
	@Slot(mandatory = true)
	public float getLevel0() {
		return this.level0;
	}


	/**
	 * Set the amount of load consumed with the profile Normal (0).
	 */	
	public void setLevel0(float level0) {
		this.level0 = level0;
	}


	/**
	 * Get the amount of load consumed with the profile Moderate (1).
	 */
	@Slot(mandatory = true)	
	public float getLevel1() {
		return this.level1;
	}


	/**
	 * Set the amount of load consumed with the profile Moderate (1).
	 */	
	public void setLevel1(float level1) {
		this.level1 = level1;
	}


	/**
	 * Get the amount of load consumed with the profile High (2).
	 */
	@Slot(mandatory = true)	
	public float getLevel2() {
		return this.level2;
	}


	/**
	 * Set the amount of load consumed with the profile High (2).
	 */	
	public void setLevel2(float level2) {
		this.level2 = level2;
	}


	/**
	 * Get the amount of load consumed with the profile Critical (3).
	 */
	@Slot(mandatory = true)	
	public float getLevel3() {
		return this.level3;
	}


	/**
	 * Set the amount of load consumed with the profile Critical (3).
	 */
	public void setLevel3(float level3) {
		this.level3 = level3;
	}
	
	
	/**
	 * Get the payload of the OadrDistributeEvent.
	 */
	@Slot(mandatory = true)
	public int getSignalLevel() {
		return this.signalLevel;
	}
	
	
	/**
	 * Set the payload of the OadrDistributeEvent.
	 */
	public void setSignalLevel(int signalLevel) {
		this.signalLevel = signalLevel;
	}


	/**
	 * Get the priority related to the broker.
	 */
	@Slot(mandatory = true)	
	public int getPriority() {
		return this.priority;
	}


	/**
	 * Set the priority related to the broker.
	 */	
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	
	/**
	 * Get the mode of the load (Easy or Hard).
	 */
	@Slot(mandatory = true)
	public int getMode() {
		return this.mode;
	}
	
	
	/**
	 * Set the mode of the load (Easy or Hard).
	 */
	public void setMode(int mode) {
		this.mode = mode;
	}
	
	
//	/**
//	 * Get the load for the specified level.
//	 */
//	public float getLoad(int level) {
//		if (level == 0) {
//			return this.getLevel0();
//		} else if (level == 1) {
//			return this.getLevel1();
//		} else if (level == 2) {
//			return this.getLevel2();
//		} else if (level == 3) {
//			return this.getLevel3();
//		}
//		
//		throw new IllegalArgumentException();
//		
//	}


//	/**
//	 * Compare this instance with other on the priority (natural order).
//	 */
//	@Override
//	public int compareTo(BrokerUpdateReportPredicate other) {
//		if (this.priority < other.priority) {
//			return -1;
//		} else if (this.priority > other.priority) {
//			return 1;
//		}
//		return 0;
//	}
}
