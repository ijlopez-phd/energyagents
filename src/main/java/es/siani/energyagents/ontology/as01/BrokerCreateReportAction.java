package es.siani.energyagents.ontology.as01;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

import java.util.Date;

@SuppressWarnings("serial")
public class BrokerCreateReportAction implements Predicate {
	
	protected String scenarioCode;
	protected String storeName;
	protected Date startTime;
	protected long duration;
	
	
	/**
	 * Constructor.
	 */
	public BrokerCreateReportAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public BrokerCreateReportAction(String scenarioCode, String storeName, Date startTime, long duration) {
		this.scenarioCode = scenarioCode;
		this.storeName = storeName;
		this.startTime = startTime;
		this.duration = duration;
		return;
	}
	
	
	/**
	 * Get the scenario code.
	 */
	@Slot(mandatory = true)
	public String getScenarioCode() {
		return this.scenarioCode;
	}
	
	
	/**
	 * Set the scenario code.
	 */
	public void setScenarioCode(String scenarioCode) {
		this.scenarioCode = scenarioCode;
		return;
	}
	
	
	/**
	 * Get the store name.
	 */
	@Slot
	public String getStoreName() {
		return this.storeName;
	}
	
	
	/**
	 * Set the store name.
	 */
	public void setStoreName(String storeName) {
		this.storeName = storeName;
		return;
	}

	/**
	 * Get the start time of the period.
	 */
	@Slot(mandatory=true)
	public Date getStartTime() {
		return this.startTime;
	}


	/**
	 * Set the start time of the period.
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}


	/**
	 * Get the duration of the period.
	 */
	@Slot(mandatory=true)
	public long getDuration() {
		return this.duration;
	}


	/**
	 * Set the duration of the period.
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}
}
