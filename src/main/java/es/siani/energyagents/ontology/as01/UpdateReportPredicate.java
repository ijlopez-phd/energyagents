package es.siani.energyagents.ontology.as01;

import es.siani.simpledr.EasyUpdateReport;
import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class UpdateReportPredicate implements Predicate {
	
	private String updateReportMessage;
	
	private long modelTimestamp;
	
	
	/**
	 * Constructor.
	 */
	public UpdateReportPredicate() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public UpdateReportPredicate(EasyUpdateReport easyUpdateReport, long modelTimestamp) {
		this.updateReportMessage = easyUpdateReport.toString();
		this.modelTimestamp = modelTimestamp;
		return;
	}
	
	
	/**
	 * Get the XML of the OadrUpdateReport service.
	 */
	@Slot(mandatory=true)
	public String getUpdateReportMessage() {
		return this.updateReportMessage;
	}


	/**
	 * Set the XML of the OadrUpdateReport service. 
	 */
	public void setUpdateReportMessage(String updateReportMessage) {
		this.updateReportMessage = updateReportMessage;
		return;
	}
	
	
	/**
	 * Get the current time of the simulator model.
	 */
	@Slot(mandatory = true)
	public long getModelTimestamp() {
		return this.modelTimestamp;
	}	
	
	
	/**
	 * Set the current time of the simulator model.
	 */
	public void setModelTimestamp(long modelTimestamp) {
		this.modelTimestamp = modelTimestamp;
		return;
	}	
}
