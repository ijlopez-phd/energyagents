package es.siani.energyagents.ontology.as04;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class ClearAuctionAction implements Predicate {
	
	protected int id;
	protected int remainingRounds;
	
	/**
	 * Constructor.
	 */
	public ClearAuctionAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ClearAuctionAction(int id, int remainingRounds) {
		this.id = id;
		this.remainingRounds = remainingRounds;
		return;
	}
	
	
	/**
	 * Get the Id of the auction.
	 */
	@Slot(mandatory = true)
	public int getId() {
		return this.id;
	}
	
	
	/**
	 * Set the Id of the auction.
	 */
	public void setId(int id) {
		this.id = id;
		return;
	}	


	/**
	 * Get the RemainingRounds.
	 */
	@Slot(mandatory = true)
	public int getRemainingRounds() {
		return this.remainingRounds;
	}


	/**
	 * Set the RemainingRounds.
	 */
	public void setRemainingRounds(int remainingRounds) {
		this.remainingRounds = remainingRounds;
	}
}
