package es.siani.energyagents.ontology.as04;

import java.util.Date;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class StartAuctionAction implements Predicate {
	
	protected int id;
	protected Date startTime;
	protected int idxProducer;
	
	
	/**
	 * Constructor.
	 */
	public StartAuctionAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public StartAuctionAction(int id, Date startTime, int idxProducer) {
		this.id = id;
		this.startTime = startTime;
		this.idxProducer = idxProducer;
		return;
	}
	
	
	/**
	 * Get the Id of the auction.
	 */
	@Slot(mandatory = true)
	public int getId() {
		return this.id;
	}
	
	
	/**
	 * Set the Id of the auction.
	 */
	public void setId(int id) {
		this.id = id;
		return;
	}
	
	/**
	 * Get the start time of the DR event.
	 */
	@Slot(mandatory = true)	
	public Date getStartTime() {
		return this.startTime;
	}


	/**
	 * Set the start time of the DR event.
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	
	/**
	 * Get the index associated to the producer.
	 */
	@Slot(mandatory = true)
	public int getIdxProducer() {
		return this.idxProducer;
	}
	
	
	/**
	 * Set the index associated to the producer.
	 */
	public void setIdxProducer(int idxProducer) {
		this.idxProducer = idxProducer;
	}
}
