package es.siani.energyagents.ontology.as04;

import jade.content.onto.annotations.Slot;

public class AuctionRoleConcept {
	
	public static final int NONE = 0;
	public static final int PRODUCER = 1;
	public static final int CONSUMER = 2;
	public static final int PROSUMER = 3;
	
	protected int role;	
	
	
	/**
	 * Consturctor.
	 */
	public AuctionRoleConcept() {
		this.role = NONE;
		return;
	}
	
	
	/**
	 * Set the role of the Agent.
	 */
	public void setRole(int role) {
		if ((role != NONE) && (role != PRODUCER) && (role != CONSUMER) && (role != PROSUMER)) {
			throw new IllegalArgumentException();
		}
		
		this.role = role;
		return;
	}
	
	
	/**
	 * Get the role of the Agent.
	 */
	@Slot(mandatory = true)
	public int getRole() {
		return this.role;
	}
	
	
	/**
	 * Add the Consumer role.
	 */
	public void markAsConsumer() {
		this.role |= CONSUMER;
	}
	
	
	/**
	 * Add the Producer role.
	 */
	public void markAsProducer() {
		this.role |= PRODUCER;
	}
	
	
	/**
	 * Tell whether it has the Consumer role.
	 */
	public boolean consumer() {
		return (this.role & CONSUMER) != 0;
	}
	
	
	/**
	 * Tell whether it has the Producer role.
	 */
	public boolean producer() {
		return (this.role & PRODUCER) != 0;
	}
}
