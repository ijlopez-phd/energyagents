package es.siani.energyagents.ontology.as04;

import java.util.List;
import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class ReadyAuctionPredicate implements Predicate {
	
	protected AuctionRoleConcept role;
	protected List<DemandBlockConcept> demandBlocks;
	protected List<ProducerBlockConcept> producerBlocks;
	
	
	/**
	 * Constructor.
	 */
	public ReadyAuctionPredicate() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ReadyAuctionPredicate(AuctionRoleConcept role) {
		this(role, null, null);
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ReadyAuctionPredicate(AuctionRoleConcept role, List<DemandBlockConcept> demandBlocks,
			List<ProducerBlockConcept> producerBlocks) {
		this.role = role;
		this.demandBlocks = demandBlocks;
		this.producerBlocks = producerBlocks;
		return;
	}		
	
	
	/**
	 * Tell if the agent participates in the auction.
	 */
	@Slot(mandatory = true)
	public AuctionRoleConcept getRole() {
		return this.role;
	}
	
	
	/**
	 * Set if the agent participates in the auction.
	 */
	public void setRole(AuctionRoleConcept role) {
		this.role = role;
		return;
	}
	
	
	/**
	 * Get the indexes of the auction's blocks in which the agent behaves as a consumer.
	 */
	@Slot(mandatory=true)
	public List<DemandBlockConcept> getDemandBlocks() {
		return this.demandBlocks;
	}
	
	
	/**
	 * Set the indexes of the auction's blocks in which the agent behaves as a consumer.
	 */
	public void setDemandBlocks(List<DemandBlockConcept> demandBlocks) {
		this.demandBlocks = demandBlocks;
		
		return;
	}
	
	
	/**
	 * Get the indexes of the blocks for which the agent will produce.
	 */
	@Slot(mandatory=true)
	public List<ProducerBlockConcept> getProducerBlocks() {
		return this.producerBlocks;
	}
	
	
	/**
	 * Set the indexes of the blocks for which the agent will produce.
	 */
	public void setProducerBlocks(List<ProducerBlockConcept> producerBlocks) {
		this.producerBlocks = producerBlocks;
		return;
	}
}