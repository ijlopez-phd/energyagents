package es.siani.energyagents.ontology.as04;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class ResultStartAuctionStagePredicate implements Predicate {
	
	protected boolean passToClearStage;
	
	
	/**
	 * Constructor. 
	 */
	public ResultStartAuctionStagePredicate() {
		this.passToClearStage = false;
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ResultStartAuctionStagePredicate(boolean passToClearStage) {
		this.passToClearStage = passToClearStage;
		return;
	}
	
	
	/**
	 * Tell if the agent wants to participate in the Clear stage of the auction.
	 */
	@Slot(mandatory = true)
	public boolean getPassToClearStage() {
		return this.passToClearStage;
	}
	
	
	/**
	 * Set if the agent wants to participate in the Clear stage of the auction.
	 */
	public void setPassToClearStage(boolean passToClearStage) {
		this.passToClearStage = passToClearStage;
		return;
	}

}
