package es.siani.energyagents.ontology.as04;

import java.util.ArrayList;
import java.util.List;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class ProducerBlockConcept implements Concept {

	protected float capacity;
	protected int idx;
	protected List<OfferLevelConcept> offers;
	
	
	/**
	 * Constructor.
	 */
	public ProducerBlockConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ProducerBlockConcept(int idx, float capacity) {
		this.idx = idx;
		this.capacity = capacity;
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ProducerBlockConcept(int idx, float[] levelsCapacity, float[] levelsPrice) {
		this.idx = idx;
		
		if ((levelsCapacity == null) || (levelsPrice == null) || (levelsCapacity.length != levelsPrice.length)) {
			throw new IllegalArgumentException();
		}
		
		offers = new ArrayList<OfferLevelConcept>();
		for (int n = 0; n < levelsCapacity.length; n++) {
			offers.add(new OfferLevelConcept(levelsCapacity[n], levelsPrice[n]));
			this.capacity += levelsCapacity[n];
		}
		
		return;
	}
	
	
	/**
	 * Get the index of the block.
	 */
	@Slot(mandatory = true)
	public int getIdx() {
		return this.idx;
	}
	
	
	/**
	 * Set the index of the block.
	 */
	public void setIdx(int idx) {
		this.idx = idx;
		return;
	}
	
	
	/**
	 * Get the capacity of the block.
	 */
	@Slot(mandatory = true)
	public float getCapacity() {
		return this.capacity;
	}
	
	
	/**
	 * Set the capacity of the block.
	 */
	public void setCapacity(float capacity) {
		this.capacity = capacity;
		return;
	}
	
	
	/**
	 * Get the capacity for each level.
	 */
	@Slot
	public List<OfferLevelConcept> getOffers() {
		return this.offers;
	}
	
	
	/**
	 * Set the capacity for each level.
	 */
	public void setOffers(List<OfferLevelConcept> offers) {
		this.offers = offers;
		return;
	}
}
