package es.siani.energyagents.ontology.as04;

import es.siani.energyagents.ontology.as00.EventConcept;
import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class FinishAuctionAction implements Predicate {
	
	protected EventConcept event;
	protected long modelTimestamp;
	
	/**
	 * Constructor.
	 */
	public FinishAuctionAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public FinishAuctionAction(EventConcept event, long modelTimestamp) {
		this.event = event;
		this.modelTimestamp = modelTimestamp;
		return;
	}


	/**
	 * Get the DR event.
	 */
	@Slot(mandatory = true)
	public EventConcept getEvent() {
		return this.event;
	}


	/**
	 * Set the DR event.
	 */
	public void setEvent(EventConcept event) {
		this.event = event;
		return;
	}
	
	
	/**
	 * Get the current time in the model.
	 */
	@Slot(mandatory = true)
	public long getModelTimestamp() {
		return this.modelTimestamp;
	}
	
	
	/**
	 * Set the current time in the model.
	 */
	public void setModelTimestamp(long modelTimestamp) {
		this.modelTimestamp = modelTimestamp;
		return;
	}
}
