package es.siani.energyagents.ontology.as04;

import java.util.ArrayList;
import java.util.List;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class AcceptedOffersPredicate implements Predicate{
	
	private List<AcceptedOfferConcept> acceptedOffers = new ArrayList<AcceptedOfferConcept>();
	
	/**
	 * Constructor.
	 */
	public AcceptedOffersPredicate() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public AcceptedOffersPredicate(List<AcceptedOfferConcept> acceptedOffers) {
		this.acceptedOffers = acceptedOffers;
		return;
	}
	
	
	/**
	 * Get the list of accepted offers.
	 */
	@Slot(mandatory = true)
	public List<AcceptedOfferConcept> getAcceptedOffers() {
		return this.acceptedOffers;
	}


	/**
	 * Set the list of accepted offers.
	 */
	public void setAcceptedOffers(List<AcceptedOfferConcept> acceptedOffers) {
		if (acceptedOffers == null) {
			acceptedOffers = new ArrayList<AcceptedOfferConcept>();
			return;
		}
		
		this.acceptedOffers = acceptedOffers;
		return;
	}
	
	
	/**
	 * Add a new offer to the internal list.
	 */
	public void addAcceptedOffer(int idxBlock, float amount) {
		this.acceptedOffers.add(new AcceptedOfferConcept(idxBlock, amount));
		return;
	}
}
