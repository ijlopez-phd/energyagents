package es.siani.energyagents.ontology.as04;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class AcceptedOfferConcept implements Concept {
	
	private int idxBlock;
	private float amount;
	
	/**
	 * Constructor.
	 */
	public AcceptedOfferConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public AcceptedOfferConcept(int idxBlock, float amount) {
		this.idxBlock = idxBlock;
		this.amount = amount;
		return;
	}


	/**
	 * Get the index of the block for which the offer has been accepted.
	 */
	@Slot(mandatory = true)
	public int getIdxBlock() {
		return this.idxBlock;
	}


	/**
	 * Set the index of the block for which the offer has been accepted.
	 */
	public void setIdxBlock(int idxBlock) {
		this.idxBlock = idxBlock;
	}


	/**
	 * Get the amount of energy will be consumed.
	 */
	@Slot(mandatory = true)
	public float getAmount() {
		return this.amount;
	}


	/**
	 * Set the amount of energy will be consumed.
	 */
	public void setAmount(float amount) {
		this.amount = amount;
	}
}
