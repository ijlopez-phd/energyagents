package es.siani.energyagents.ontology.as00;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import jade.content.Concept;
import jade.content.onto.annotations.AggregateSlot;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class ProgramConcept implements Concept {
	
	/** Name of the demand-response program. */
	private String name;
	
	/** List of events of the demand-response program. */
	private List<EventConcept> events;
	
	/** Tells if the list of events is sorted. */
	private boolean sorted = true;
	
	
	/**
	 * Constructor.
	 */
	public ProgramConcept() {
		this.events = new ArrayList<EventConcept>();
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ProgramConcept(String name, List<EventConcept> events) {
		this.name = name;
		this.events = (events != null)? events : new ArrayList<EventConcept>();
	}
	
	
	/**
	 * Get the Name.
	 */
	@Slot(mandatory=true)
	public String getName() {
		return this.name;
	}
	
	
	/**
	 * Set the Name.
	 */
	public void setName(String name) {
		this.name = name;
		return;
	}
	
	
	/**
	 * Get the list of Events.
	 */
	@AggregateSlot(cardMin=0)
	public List<EventConcept> getEvents() {
		return this.events;
	}
	
	
	/**
	 * Set the list of Events.
	 */
	public void setEvents(List<EventConcept> events) {
		this.events = (events != null)? events : new ArrayList<EventConcept>();
		this.sorted = false;
		return;
	}
	
	
	/**
	 * Add a set of events to the program.
	 */
	public void addEvents(Collection<EventConcept> events) {
		this.events.addAll(events);
		this.sorted = false;
		
		return;
	}
	
	
	/**
	 * Add an event to the program.
	 */
	public void addEvent(EventConcept event) {
		this.events.add(event);
		this.sorted = false;
	}
	
	
	/**
	 * Sort the events of the program by date.
	 */
	public void sort() {
		if (this.sorted == false) {
			Collections.sort(this.events);
			this.sorted = true;
		}
		
		return;
	}
}
