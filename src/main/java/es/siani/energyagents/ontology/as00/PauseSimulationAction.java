package es.siani.energyagents.ontology.as00;

import jade.content.Predicate;

@SuppressWarnings("serial")
public class PauseSimulationAction implements Predicate {
	
	private SimulationPauseConcept simulation;
	
	/**
	 * Constructor.
	 */
	public PauseSimulationAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public PauseSimulationAction(SimulationPauseConcept simulation) {
		this.simulation = simulation;
		return;
	}
	
	
	/**
	 * Get the Simulation.
	 */
	public SimulationPauseConcept getSimulation() {
		return this.simulation;
	}
	
	
	/**
	 * Set the Simulation.
	 */
	public void setSimulation(SimulationPauseConcept simulation) {
		this.simulation = simulation;
		return;
	}
}
