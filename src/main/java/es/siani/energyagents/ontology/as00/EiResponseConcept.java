package es.siani.energyagents.ontology.as00;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class EiResponseConcept implements Concept {
	
	public static final String CODE_OK = "200";
	
	/** Code that identifies the type of the response (three digits). */
	private String code;
	
	/** Description. */
	private String description;
	
	/** Id of the request to which is associated this reply. */
	private String requestId;
	
	
	/**
	 * Constructor.
	 */
	public EiResponseConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public EiResponseConcept(String code, String requestId) {
		this.code = code;
		this.requestId = requestId;
		return;
	}


	/**
	 * Get the Code.
	 */
	@Slot(mandatory=true)
	public String getCode() {
		return this.code;
	}


	/**
	 * Set the code.
	 */
	public void setCode(String code) {
		this.code = code;
	}


	/**
	 * Get the Description.
	 */
	@Slot(mandatory=false)
	public String getDescription() {
		return this.description;
	}


	/**
	 * Set the Description.
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * Get the Id of the request.
	 */
	@Slot(mandatory=true)
	public String getRequestId() {
		return this.requestId;
	}


	/**
	 * Set the Id of the request.
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
}
