package es.siani.energyagents.ontology.as00;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class IntervalConcept implements Concept {
	
	private long duration;
	private float payload;
	
	
	/**
	 * Constructor.
	 */
	public IntervalConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public IntervalConcept(long duration, float payload) {
		this.duration = duration;
		this.payload = payload;
		return;
	}
	
	
	/**
	 * Get the Duration.
	 */
	@Slot(mandatory=true)
	public long getDuration() {
		return this.duration;
	}
	
	
	/**
	 * Set the Duration.
	 */
	public void setDuration(long duration) {
		this.duration = duration;
		return;
	}
	
	
	/**
	 * Get the Payload.
	 */
	@Slot(mandatory=true)
	public float getPayload() {
		return this.payload;
	}
	
	
	/**
	 * Set the Payload.
	 */
	public void setPayload(float payload) {
		this.payload = payload;
		return;
	}
}
