package es.siani.energyagents.ontology.as00;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import es.siani.energyagents.injection.annotations.OntologyName;
import es.siani.energyagents.injection.annotations.OntologyPackage;
import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;
import es.siani.simpledr.model.AsdrProgram;
import es.siani.simpledr.model.AsdrTargets;
import jade.content.onto.BeanOntology;
import jade.content.onto.BeanOntologyException;

@SuppressWarnings("serial")
public class EnergyAgentsOntology extends BeanOntology {
	
	/**
	 * Constructor.
	 */
	@Inject
	public EnergyAgentsOntology(@OntologyName String name, @OntologyPackage String packages) {
		super(name);
		
		// Add the classes that make up the Ontology.
		try {
			String[] tokens = packages.split(";");
			for (String pkgUri : tokens) {
				this.add(pkgUri);
			}
		} catch (BeanOntologyException e) {
			getLogger().log(Level.SEVERE, "Error during defining the Agency-Services ontology.", e);
		}

	}
	
	
	/**
	 * Creates a ProgramConcept object from an AsdrProgram.
	 */
	public static ProgramConcept createProgram(AsdrProgram asdrProgram) {
		
		if (asdrProgram == null) {
			throw new IllegalArgumentException();
		}
		
		List<EventConcept> events = new ArrayList<EventConcept>();
		for (AsdrEvent asdrEvent : asdrProgram.getEvents()) {
			
			List<IntervalConcept> intervals = new ArrayList<IntervalConcept>();
			for (AsdrInterval asdrInterval : asdrEvent.getIntervals()) {
				intervals.add(new IntervalConcept(asdrInterval.getDuration(), asdrInterval.getPayload()));
			}
			
			
			AsdrTargets asdrTargets = asdrEvent.getTargets();
			TargetsConcept targets = (asdrTargets != null)?
					new TargetsConcept(asdrTargets.groups, asdrTargets.resources, asdrTargets.vendors, asdrTargets.parties) :
						new TargetsConcept();
			
			events.add(new EventConcept(
					asdrEvent.getSignalType().value(),
					asdrEvent.getStart(),
					asdrEvent.getDuration(),
					asdrEvent.getNotificationDuration(),
					asdrEvent.getPriority(),
					asdrEvent.isResponseRequired(),
					intervals,
					targets));
		}
		
		return new ProgramConcept(asdrProgram.getName(), events);
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(EnergyAgentsOntology.class.getName());
	}

}
