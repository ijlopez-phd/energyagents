package es.siani.energyagents.ontology.as00;

public class SimulationIdConcept {
	
	/** Identifier of the simulation. */
	private long id;
	
	
	/**
	 * Constructor.
	 */
	public SimulationIdConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public SimulationIdConcept(long id) {
		this.id = id;
	}


	/**
	 * Get the Id.
	 */
	public long getId() {
		return id;
	}


	/**
	 * Set the Id.
	 */
	public void setId(long id) {
		this.id = id;
	}
}
