package es.siani.energyagents.ontology.as00;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class ScheduleConcept implements Concept {
	
	/** Values that indicates that no more pauses are required. */
	public static final int NO_PAUSE = -1;
	
	/** Next time the scheduler must be called. */
	protected long nextPause;
	
	/** Current model time. */
	protected long currentModelTime;
	
	/** Code of the scenario. */
	protected String scenarioCode;
	
	/** Triggered events. */
	protected List<EventConcept> events;
	
	
	/** 
	 * Constructor.
	 */
	public ScheduleConcept() {
		this.nextPause = NO_PAUSE;
		this.events = new ArrayList<EventConcept>();
	}
	
	
	/**
	 * Constructor.
	 */
	public ScheduleConcept(long nextPause, long currentModelTime) {
		this(nextPause, currentModelTime, null);
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ScheduleConcept(long nextPause, long currentModelTime, List<EventConcept> events) {
		
		if ((nextPause < NO_PAUSE) || (currentModelTime < 0)) {
			throw new IllegalArgumentException();
		}
		
		this.nextPause = nextPause;
		this.currentModelTime = currentModelTime;		
		this.events = (events == null)? new ArrayList<EventConcept>() : events;
		return;
		
	}


	/**
	 * Get the NextPause.
	 */
	@Slot(mandatory = true)
	public long getNextPause() {
		return this.nextPause;
	}
	
	
	/**
	 * Set the NextPause.
	 */
	public void setNextPause(long nextPause) {
		this.nextPause = nextPause;
		return;
	}
	
	
	/**
	 * Get the CurrentModelTime.
	 */
	@Slot(mandatory = true)
	public long getCurrentModelTime() {
		return this.currentModelTime;
	}
	

	/**
	 * Set the CurrentModelTime.
	 */
	public void setCurrentModelTime(long currentModelTime) {
		this.currentModelTime = currentModelTime;
		return;
	}
	
	
	/**
	 * Ge the scenario code.
	 */
	@Slot(mandatory = true)
	public String getScenarioCode() {
		return this.scenarioCode;
	}
	
	
	/**
	 * Set the scenario code.
	 */
	public void setScenarioCode(String scenarioCode) {
		this.scenarioCode = scenarioCode;
		return;
	}
	
	
	/**
	 * Get the events
	 */
	@Slot
	public List<EventConcept> getEvents() {
		return events;
	}
	
	
	/**
	 * Set the events.
	 */
	public void setEvents(List<EventConcept> events) {
		this.events = events;
		return;
	}
	
	
	/**
	 * Adds a new event.
	 */
	public void addEvent(EventConcept event) {
		this.events.add(event);
		return;
	}
}
