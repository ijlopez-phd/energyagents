package es.siani.energyagents.ontology.as00;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class SimulationStartedPredicate implements Predicate {
	
	private SimulationIdConcept simulationId;
	
	
	/**
	 * Constructor.
	 */
	public SimulationStartedPredicate() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public SimulationStartedPredicate(SimulationIdConcept simulationId) {
		this.simulationId = simulationId;
		return;
	}
	
	
	/**
	 * Return the SimulationId.
	 */
	@Slot(mandatory=true)
	public SimulationIdConcept getSimulationId() {
		return this.simulationId;
	}
	
	
	/**
	 * Set the SimulationId.
	 */
	public void setSimulationId(SimulationIdConcept simulationId) {
		this.simulationId = simulationId;
		return;
	}	
}
