package es.siani.energyagents.ontology.as00;

import java.util.Date;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class ScheduledPauseConcept implements Concept {
	
	private long id = -1;
	private Date time;
	
	
	/**
	 * Constructor.
	 */
	public ScheduledPauseConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ScheduledPauseConcept(long id) {
		this.id = id;
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ScheduledPauseConcept(long id, Date time) {
		this.id = id;
		this.time = time;
		return;
	}
	
	
	/**
	 * Get the Id.
	 */
	@Slot(mandatory=false)
	public long getId() {
		return this.id;
	}
	
	
	/**
	 * Set the Id.
	 */
	public void setId(long id) {
		this.id = id;
		return;
	}
	
	
	/**
	 * Get the Time.
	 */
	@Slot(mandatory=false)
	public Date getTime() {
		return this.time;
	}
	
	
	/**
	 * Set the Time.
	 */
	public void setTime(Date time) {
		this.time = time;
		return;
	}

}
