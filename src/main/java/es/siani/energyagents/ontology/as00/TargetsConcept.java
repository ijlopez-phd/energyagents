package es.siani.energyagents.ontology.as00;

import java.util.ArrayList;
import java.util.List;

import jade.content.Concept;
import jade.content.onto.annotations.AggregateSlot;

@SuppressWarnings("serial")
public class TargetsConcept implements Concept {
	
	private List<String> groups = new ArrayList<String>();
	private List<String> resources = new ArrayList<String>();
	private List<String> vendors = new ArrayList<String>();
	private List<String> parties = new ArrayList<String>();
	
	
	/**
	 * Constructor.
	 */
	public TargetsConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public TargetsConcept(List<String> groups, List<String> resources, List<String> vendors, List<String> parties) {
		if (groups != null) {
			this.groups = groups;
		}
		
		if (resources != null) {
			this.resources = resources;
		}
		
		if (vendors != null) {
			this.vendors = vendors;
		}
		
		if (parties != null) {
			this.parties = parties;
		}
		
		return;
	}
	
	
	/**
	 * Get the list of Groups.
	 */
	@AggregateSlot
	public List<String> getGroups() {
		return this.groups;
	}
	
	
	/**
	 * Set the Groups.
	 */
	public void setGroups(List<String> groups) {
		this.groups = (groups != null)? groups : new ArrayList<String>();
		return;
	}
	
	
	/**
	 * Get the list of Resources.
	 */
	@AggregateSlot
	public List<String> getResources() {
		return this.resources;
	}
	
	
	/**
	 * Set the Resources.
	 */
	public void setResources(List<String> resources) {
		this.resources= (resources != null)? resources : new ArrayList<String>();
		return;
	}
	
	
	/**
	 * Get the list of Vendors.
	 */
	@AggregateSlot
	public List<String> getVendors() {
		return this.vendors;
	}
	
	
	/**
	 * Set the Vendors.
	 */
	public void setVendors(List<String> vendors) {
		this.vendors = (vendors != null)? vendors : new ArrayList<String>();
		return;
	}
	
	
	/**
	 * Get the list of Parties.
	 */
	@AggregateSlot
	public List<String> getParties() {
		return this.parties;
	}
	
	
	/**
	 * Set the Parties.
	 */
	public void setParties(List<String> parties) {
		this.parties = (parties != null)? parties : new ArrayList<String>();
		return;
	}	
}
