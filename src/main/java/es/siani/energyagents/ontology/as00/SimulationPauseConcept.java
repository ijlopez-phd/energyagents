package es.siani.energyagents.ontology.as00;

import java.util.Date;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class SimulationPauseConcept implements Concept {
	
	/** Identifer of the pause. */
	private long idPause = -1;
	
	/** Identifier of the simulation. */
	private long idSimulation;
	
	/** Current time in the model being simulated. */
	private Date modelTime;
	
	/** Current time in the real world. */
	private Date realTime;
	
	
	/**
	 * Constructor.
	 */
	public SimulationPauseConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public SimulationPauseConcept(long idSimulation, Date realTime, Date modelTime) {
		this(-1, idSimulation, realTime, modelTime);
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public SimulationPauseConcept(long idPause, long idSimulation, Date realTime, Date modelTime) {
		this.idPause = idPause;
		this.idSimulation = idSimulation;
		this.realTime = realTime;
		this.modelTime = modelTime;		
		return;
	}
	
	
	/**
	 * Get the Id of the pause.
	 */
	@Slot(mandatory=false)
	public long getIdPause() {
		return this.idPause;
	}
	
	
	/**
	 * Set the Id of the pause.
	 */
	public void setIdPause(long idPause) {
		this.idPause = idPause;
		return;
	}
	
	
	/**
	 * Get the Id of the simulation.
	 */
	@Slot(mandatory=true)
	public long getIdSimulation() {
		return idSimulation;
	}
	
	
	/**
	 * Set the Id of the simulation.
	 */
	public void setIdSimulation(long idSimulation) {
		this.idSimulation = idSimulation;
		return;
	}	
	
	
	/**
	 * Get the RealTime.
	 */
	@Slot(mandatory=true)
	public Date getRealTime() {
		return this.realTime;
	}
	
	
	/**
	 * Set the RealTime.
	 */
	public void setRealTime(Date realTime) {
		this.realTime = realTime;
		return;
	}
	
	/**
	 * Get the ModelTime.
	 */
	@Slot(mandatory=true)
	public Date getModelTime() {
		return this.modelTime;
	}
	
	
	/**
	 * Set the ModelTime.
	 */
	public void setModelTime(Date modelTime) {
		this.modelTime = modelTime;
		return;
	}
}
