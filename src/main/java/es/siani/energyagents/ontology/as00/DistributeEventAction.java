package es.siani.energyagents.ontology.as00;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class DistributeEventAction implements Predicate {
	
	private DistributeEventConcept distrubeEvent;
	private long modelTimestamp;
	private String storeName;
	
	
	/**
	 * Constructor.
	 */
	public DistributeEventAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public DistributeEventAction(DistributeEventConcept distributeEvent, long modelTimestamp) {
		this(distributeEvent, modelTimestamp, null);
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public DistributeEventAction(DistributeEventConcept distributeEvent, long modelTimestamp, String storeName) {
		this.distrubeEvent = distributeEvent;
		this.modelTimestamp = modelTimestamp;
		this.storeName = storeName;
		return;
	}	
	
	
	/**
	 * Get the concept that describes the DistributeEvent.
	 */
	@Slot(mandatory=true)
	public DistributeEventConcept getDistributeEvent() {
		return this.distrubeEvent;
	}
	
	
	/**
	 * Set the concept that describes the DistributeEvent.
	 */
	public void setDistributeEvent(DistributeEventConcept distributeEvent) {
		this.distrubeEvent = distributeEvent;
		return;
	}
	
	
	/**
	 * Get the timestamp of the model.
	 */
	@Slot(mandatory=true)
	public long getModelTimestamp() {
		return this.modelTimestamp;
	}
	
	
	/**
	 * Set the timestamp of the model.
	 */
	public void setModelTimestamp(long modelTimestamp) {
		this.modelTimestamp = modelTimestamp;
		return;
	}
	
	
	/**
	 * Get the name of the store that contains the forecasts. 
	 */
	@Slot
	public String getStoreName() {
		return this.storeName;
	}
	
	
	/**
	 * Set the name of the store that contains the forecasts. 
	 */
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}	
}
