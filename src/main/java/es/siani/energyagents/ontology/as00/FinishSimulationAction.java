package es.siani.energyagents.ontology.as00;

import jade.content.Predicate;

@SuppressWarnings("serial")
public class FinishSimulationAction implements Predicate {
	
	private SimulationIdConcept simulationId;
	
	
	/**
	 * Constructor.
	 */
	public FinishSimulationAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public FinishSimulationAction(SimulationIdConcept simulation) {
		this.simulationId = simulation;
		return;
	}
	
	/**
	 * Get the Simulation Id.
	 */
	public SimulationIdConcept getSimulation() {
		return this.simulationId;
	}
	
	
	/**
	 * Set the Simulation Id.
	 */
	public void setSimulation(SimulationIdConcept simulation) {
		this.simulationId = simulation;
		return;
	}	
}
