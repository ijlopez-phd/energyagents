package es.siani.energyagents.ontology.as00;

import jade.content.Predicate;

@SuppressWarnings("serial")
public class CreatedEventPredicate implements Predicate {
	
	/** General description of the response. */
	private EiResponseConcept eiResponse;
	
	
	/**
	 * Constructor.
	 */
	public CreatedEventPredicate() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public CreatedEventPredicate(EiResponseConcept eiResponse) {
		this.eiResponse = eiResponse;
	}


	/**
	 * Get the eiResponse.
	 */
	public EiResponseConcept getEiResponse() {
		return eiResponse;
	}


	/**
	 * Set the eiResponse.
	 */
	public void setEiResponse(EiResponseConcept eiResponse) {
		this.eiResponse = eiResponse;
	}
}
