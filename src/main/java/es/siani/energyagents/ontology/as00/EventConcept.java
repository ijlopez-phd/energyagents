package es.siani.energyagents.ontology.as00;

import java.util.Date;
import java.util.List;

import jade.content.Concept;
import jade.content.onto.annotations.AggregateSlot;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class EventConcept implements Concept, Comparable<EventConcept> {
	
	private String signalType;
	private Date start;
	private long duration;
	private long notificationDuration;
	private long priority;
	private boolean responseRequired;
	private List<IntervalConcept> intervals;
	private TargetsConcept targets;
	
	
	/**
	 * Constructor.
	 */
	public EventConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public EventConcept(
			String signalType, Date start, long duration, long notificationDuration,
			long priority, boolean responseRequired,
			List<IntervalConcept> intervals, TargetsConcept targets) {
		this.signalType = signalType;
		this.start = start;
		this.duration = duration;
		this.notificationDuration = notificationDuration;
		this.priority = priority;
		this.responseRequired = responseRequired;
		this.intervals = intervals;
		this.targets = targets;
		return;
	}

	
	/**
	 * Get the SignalType.
	 */
	@Slot(mandatory=true)
	public String getSignalType() {
		return signalType;
	}

	
	/**
	 * Set the SignalType.
	 */
	public void setSignalType(String signalType) {
		this.signalType = signalType;
		return;
	}

	
	/**
	 * Get the Start.
	 */
	@Slot(mandatory=true)
	public Date getStart() {
		return start;
	}

	
	/**
	 * Set the start.
	 */
	public void setStart(Date start) {
		this.start = start;
		return;
	}

	
	/**
	 * Get the Duration.
	 */
	@Slot(mandatory=true)
	public long getDuration() {
		return duration;
	}

	
	/**
	 * Set the Duration.
	 */
	public void setDuration(long duration) {
		this.duration = duration;
		return;
	}

	
	/**
	 * Get the NotificationDuration.
	 */
	@Slot(mandatory=false)
	public long getNotificationDuration() {
		return notificationDuration;
	}

	
	/**
	 * Set the NotificationDuration.
	 */
	public void setNotificationDuration(long notificationDuration) {
		this.notificationDuration = notificationDuration;
		return;
	}
	
	
	/**
	 * Set the Priority.
	 */
	@Slot(mandatory=true)
	public long getPriority() {
		return this.priority;
	}
	
	
	/**
	 * Get the Priority.
	 */
	public void setPriority(long priority) {
		this.priority = priority;
		return;
	}

	
	/**
	 * Get the ResponseRequired flag.
	 */
	@Slot(mandatory=true)
	public boolean isResponseRequired() {
		return responseRequired;
	}

	
	/**
	 * Set the ResponseRequired flag.
	 */
	public void setResponseRequired(boolean responseRequired) {
		this.responseRequired = responseRequired;
		return;
	}

	
	/**
	 * Get the list of Intervals.
	 */
	@AggregateSlot(cardMin=1)
	public List<IntervalConcept> getIntervals() {
		return intervals;
	}

	
	/**
	 * Set the list of Intervals.
	 */
	public void setIntervals(List<IntervalConcept> intervals) {
		this.intervals = intervals;
		return;
	}
	
	
	/**
	 * Get the Targets.
	 */
	@Slot(mandatory=false)
	public TargetsConcept getTargets() {
		return this.targets;
	}
	
	
	/**
	 * Set the Targets.
	 */
	public void setTargets(TargetsConcept targets) {
		this.targets = targets;
	}


	/**
	 * Compares two instances of EventConcept according to the Start date.
	 */
	@Override
	public int compareTo(EventConcept other) {
		
		long thisMillis = this.start.getTime();
		long otherMillis = other.getStart().getTime();
		
		if (thisMillis < otherMillis) {
			return -1;
		} else if (thisMillis > otherMillis){
			return 1;
		}
		
		return 0;
	}
}
