package es.siani.energyagents.ontology.as00;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class StartSimulationAction implements Predicate {
	
	private SimulationDescriptionConcept simulation;
	
	
	
	/**
	 * Constructor.
	 */
	public StartSimulationAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public StartSimulationAction(SimulationDescriptionConcept simulation) {
		this.simulation = simulation;
		return;
	}
	
	
	/**
	 * Get the Simulation.
	 */
	@Slot(mandatory=true)
	public SimulationDescriptionConcept getSimulation() {
		return this.simulation;
	}
	
	
	/**
	 * Set the Simulation.
	 */
	public void setSimulation(SimulationDescriptionConcept simulation) {
		this.simulation = simulation;
		return;
	}
}
