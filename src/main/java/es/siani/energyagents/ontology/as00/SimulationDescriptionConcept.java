package es.siani.energyagents.ontology.as00;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class SimulationDescriptionConcept implements Concept {
	
	/** Internal identifier of the simulation. */
	private long id;
	
	/** Code of the scenario to be simulated. */
	private String scenarioCode;
	
	/** Name of the table in which the forecast data is saved. */
	private String storeName;
	
	/** Demand-Response program associated to the simulation. */
	private ProgramConcept program;
	
	
	/**
	 * Constructor.
	 */
	public SimulationDescriptionConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public SimulationDescriptionConcept(long id, String scenarioCode, String storeName, ProgramConcept program) {
		this.id = id;
		this.scenarioCode = scenarioCode;
		this.storeName = storeName;
		this.program = program;
		return;
	}


	/**
	 * Get the Id.
	 */
	@Slot(mandatory=true)
	public long getId() {
		return id;
	}


	/**
	 * Set the Id.
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * Get the code of the scenario to be simulated.
	 */
	@Slot(mandatory=true)
	public String getScenarioCode() {
		return this.scenarioCode;
	}


	/**
	 * Set the Name.
	 */
	public void setScenarioCode(String scenarioCode) {
		this.scenarioCode = scenarioCode;
	}
	
	
	/**
	 * Get the name of the table in which the forecast data is saved.
	 */
	@Slot(mandatory = true)
	public String getStoreName() {
		return this.storeName;
	}
	
	
	/**
	 * Set the name of the table in which the forecast dta is saved.
	 */
	public void setStoreName(String storeName) {
		this.storeName = storeName;
		return;
	}


	/**
	 * Get the Program.
	 */
	@Slot(mandatory=true)
	public ProgramConcept getProgram() {
		return program;
	}


	/**
	 * Set the Program.
	 */
	public void setProgram(ProgramConcept program) {
		this.program = program;
	}
}
