package es.siani.energyagents.ontology.as00;


import jade.content.Predicate;

@SuppressWarnings("serial")
public class NextSimulationPausePredicate implements Predicate {
	
	private ScheduledPauseConcept pause;
	private boolean distributeActionTriggered;
	

	/**
	 * Constructor.
	 */
	public NextSimulationPausePredicate() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public NextSimulationPausePredicate(ScheduledPauseConcept pause, boolean distributeActionTriggered) {
		this.pause = pause;
		this.distributeActionTriggered = distributeActionTriggered;
		return;
	}
	
	
	/**
	 * Get the Pause.
	 */
	public ScheduledPauseConcept getPause() {
		return this.pause;
	}
	
	
	/**
	 * Set the Pause.
	 */
	public void setPause(ScheduledPauseConcept pause) {
		this.pause = pause;
		return;
	}
	
	
	/**
	 * Get the DistributeActionTriggered.
	 */
	public boolean getDistributeActionTriggered() {
		return this.distributeActionTriggered;
	}


	/**
	 * Set the DistributeActionTriggered.
	 */
	public void setDistributeActionTriggered(boolean distributeActionTriggered) {
		this.distributeActionTriggered = distributeActionTriggered;
	}	
}
