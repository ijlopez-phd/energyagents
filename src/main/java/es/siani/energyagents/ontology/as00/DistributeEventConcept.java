package es.siani.energyagents.ontology.as00;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class DistributeEventConcept implements Concept {
	
	private String marketContext;
	
	private String content;
	
	
	/**
	 * Constructor.
	 */
	public DistributeEventConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public DistributeEventConcept(String marketContext, String content) {
		this.marketContext = marketContext;
		this.content = content;
		return;
	}
	
	
	/**
	 * Get the Market context.
	 */
	@Slot(mandatory=true)
	public String getMarketContext() {
		return this.marketContext;
	}
	
	
	/**
	 * Set the market context.
	 */
	public void setMarketContext(String marketContext) {
		this.marketContext = marketContext;
		return;
	}
	
	
	/**
	 * Get the content.
	 */
	@Slot(mandatory=true)
	public String getContent() {
		return this.content;
	}	
	
	
	/**
	 * Set the content.
	 */
	public void setContent(String content) {
		this.content = content;
		return;
	}
}
