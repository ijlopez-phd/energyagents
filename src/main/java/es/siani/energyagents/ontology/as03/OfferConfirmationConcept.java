package es.siani.energyagents.ontology.as03;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class OfferConfirmationConcept implements Concept {
	
	private int idxBlock;
	private float amount;
	
	
	/**
	 * Constructor.
	 */
	public OfferConfirmationConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public OfferConfirmationConcept(int idxBlock, float amount) {
		this.idxBlock = idxBlock;
		this.amount = amount;
		return;
	}


	/**
	 * Get the index of the block.
	 */
	@Slot(mandatory = true)
	public int getIdxBlock() {
		return this.idxBlock;
	}


	/**
	 * Set the index of the block.
	 */
	public void setIdxBlock(int idxBlock) {
		this.idxBlock = idxBlock;
		return;
	}


	/**
	 * Get the amount of offer that has been accepted.
	 */
	@Slot(mandatory = true)
	public float getAmount() {
		return this.amount;
	}


	/**
	 * Set the amount of offer that has been accepted.
	 */
	public void setAmount(float amount) {
		this.amount = amount;
		return;
	}
}
