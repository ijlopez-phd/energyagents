package es.siani.energyagents.ontology.as03;

import java.util.ArrayList;
import java.util.List;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class OfferBlockConcept implements Concept {
	
	protected int idx;
	List<OfferLevelConcept> offers = new ArrayList<OfferLevelConcept>(); 

	
	/**
	 * Constructor.
	 */
	public OfferBlockConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public OfferBlockConcept(int idx) {
		this.idx = idx;
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public OfferBlockConcept(int idx, float amount, float price) {
		this.idx = idx;
		offers.add(new OfferLevelConcept(amount, price));
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public OfferBlockConcept(int idx, List<OfferLevelConcept> offers) {
		this.idx = idx;
    	if (offers == null) {
    		offers = new ArrayList<OfferLevelConcept>();
    	}
		this.offers = offers;
		return;
	}
	
	
	/**
	 * Get the index of the block.
	 */
	@Slot(mandatory = true)
	public int getIdx() {
		return this.idx;
	}
	
	
	/**
	 * Set the index of the block.
	 */
	public void setIdx(int idx) {
		this.idx = idx;
		return;
	}
	
	
    /**
     * Get the offer levels for the block.
     */
    @Slot(mandatory = true)
    public List<OfferLevelConcept> getOffers() {
            return this.offers;
    }
    
    
    /**
     * Get the offer levels for the block.
     */
    public void setOffers(List<OfferLevelConcept> offers) {
    	if (offers == null) {
    		offers = new ArrayList<OfferLevelConcept>();
    	}
    	
    	this.offers = offers;
    }
    
    
    /**
     * Add a new offer.
     */
    public void addOffer(float amount, float price) {
            this.offers.add(new OfferLevelConcept(amount, price));
            return;
    }
    
    
    /**
     * Calculate total offer for the block.
     */
    public float getTotalOfferAmount() {
    	float total = 0;
    	for (OfferLevelConcept offerLevel : offers) {
    		total+= offerLevel.getAmount();
    	}
    	
    	return total;
    }
}
