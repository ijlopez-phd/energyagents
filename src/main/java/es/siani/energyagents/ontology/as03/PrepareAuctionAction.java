package es.siani.energyagents.ontology.as03;

import java.util.Date;
import java.util.List;

import es.siani.energyagents.ontology.as00.IntervalConcept;
import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class PrepareAuctionAction implements Predicate {
	
	protected int id;
	protected String dfName;
	protected String storeName;
	protected Date startTime;
	protected Date endTime;
	protected List<IntervalConcept> intervals;
	
	
	/**
	 * Constructor.
	 */
	public PrepareAuctionAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public PrepareAuctionAction(int id, String storeName, String dfName, Date startTime, Date endTime, List<IntervalConcept> intervals) {
		this.id = id;
		this.storeName = storeName;
		this.dfName = dfName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.intervals = intervals;
		return;
	}
	
	
	/**
	 * Get the Id of the auction.
	 */
	@Slot(mandatory = true)
	public int getId() {
		return this.id;
	}
	
	
	/**
	 * Set the Id of the auction.
	 */
	public void setId(int id) {
		this.id = id;
		return;
	}
	
	
	/**
	 * Get the name of the store from which the forecasts are read.
	 */
	@Slot(mandatory = true)
	public String getStoreName() {
		return this.storeName;
	}
	
	
	/**
	 * Set the name of the store from which the forecasts are read.
	 */
	public void setStoreName(String storeName) {
		this.storeName = storeName;
		return;
	}
	

	/**
	 * Get the name of the Directory Facilitator to which the brokers have to be registered with.
	 */
	@Slot(mandatory = true)	
	public String getDfName() {
		return this.dfName;
	}


	/**
	 * Set the name of the Directory Facilitator to which the brokers have to be registered with.
	 */	
	public void setDfName(String dfName) {
		this.dfName = dfName;
	}


	/**
	 * Get the start time of the DR event.
	 */
	@Slot(mandatory = true)	
	public Date getStartTime() {
		return this.startTime;
	}


	/**
	 * Set the start time of the DR event.
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}


	/**
	 * Get the end time of the DR event.
	 */
	@Slot(mandatory = true)	
	public Date getEndTime() {
		return this.endTime;
	}


	/**
	 * Set the end time of the DR event.
	 */	
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	
	/**
	 * Get the list of intervals that make up the event.
	 */
	@Slot(mandatory = true)
	public List<IntervalConcept> getIntervals() {
		return this.intervals;
	}
	
	
	/**
	 * Set the list of intervals that make up the event.
	 */
	public void setIntervals(List<IntervalConcept> intervals) {
		this.intervals = intervals;
		return;
	}
}
