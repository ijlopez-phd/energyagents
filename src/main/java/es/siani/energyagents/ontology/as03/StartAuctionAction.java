package es.siani.energyagents.ontology.as03;

import java.util.Date;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class StartAuctionAction implements Predicate {
	
	protected int id;
	protected Date startTime;
	
	
	/**
	 * Constructor.
	 */
	public StartAuctionAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public StartAuctionAction(int id, Date startTime) {
		this.id = id;
		this.startTime = startTime;
		return;
	}
	
	
	/**
	 * Get the Id of the auction.
	 */
	@Slot(mandatory = true)
	public int getId() {
		return this.id;
	}
	
	
	/**
	 * Set the Id of the auction.
	 */
	public void setId(int id) {
		this.id = id;
		return;
	}
	
	/**
	 * Get the start time of the DR event.
	 */
	@Slot(mandatory = true)	
	public Date getStartTime() {
		return this.startTime;
	}


	/**
	 * Set the start time of the DR event.
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
}
