package es.siani.energyagents.ontology.as03;

import java.util.List;
import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class CallForProductionOfferAction implements Predicate {
	
	protected List<DemandBlockConcept> demandBlocks;
	
	
	/**
	 * Constructor.
	 */
	public CallForProductionOfferAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public CallForProductionOfferAction(List<DemandBlockConcept> demandBlocks) {
		this.demandBlocks = demandBlocks;
	}
	
	
	/**
	 * Get the list of blocks of demand.
	 */
	@Slot(mandatory = true)
	public List<DemandBlockConcept> getDemandBlocks() {
		return this.demandBlocks;
	}
	
	
	/**
	 * Set the list of blocks of demand.
	 */	
	public void setDemandBlocks(List<DemandBlockConcept> demandBlocks) {
		this.demandBlocks = demandBlocks;
	}
}
