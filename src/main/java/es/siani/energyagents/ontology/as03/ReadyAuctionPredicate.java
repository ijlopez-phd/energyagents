package es.siani.energyagents.ontology.as03;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class ReadyAuctionPredicate implements Predicate {
	
	protected AuctionRoleConcept role;
	
	
	/**
	 * Constructor.
	 */
	public ReadyAuctionPredicate() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public ReadyAuctionPredicate(AuctionRoleConcept role) {
		this.role = role;
		return;
	}
	
	
	/**
	 * Tell if the agent participates in the auction.
	 */
	@Slot(mandatory = true)
	public AuctionRoleConcept getRole() {
		return this.role;
	}
	
	
	/**
	 * Set if the agent participates in the auction.
	 */
	public void setRole(AuctionRoleConcept role) {
		this.role = role;
		return;
	}
}
