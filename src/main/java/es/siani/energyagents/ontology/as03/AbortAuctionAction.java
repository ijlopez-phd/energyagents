package es.siani.energyagents.ontology.as03;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class AbortAuctionAction implements Predicate {
	
	protected int auctionId;
	
	/**
	 * Constructor.
	 */
	public AbortAuctionAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public AbortAuctionAction(int auctionId) {
		this.auctionId = auctionId;
		return;
	}
	
	
	/**
	 * Get the Id of the Auction to be aborted.
	 */
	@Slot(mandatory = true)
	public int getAuctionId() {
		return this.auctionId;
	}
	
	
	/**
	 * Set the Id of the Auction to be aborted.
	 */	
	public void setAuctionId(int auctionId) {
		this.auctionId = auctionId;
		return;
	}
}
