package es.siani.energyagents.ontology.as03;

import java.util.HashSet;
import java.util.Set;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class CallsForDemandAcceptedPredicate implements Predicate {
	
	private Set<Integer> idxBlocksAccepted = new HashSet<Integer>();
	
	/**
	 * Constructor.
	 */
	public CallsForDemandAcceptedPredicate() {
		return;
	}
	
	
	/**
	 * Return the list of blocks' indexes to which may be production.
	 */
	@Slot(mandatory = true)
	public Set<Integer> getIdxBlocksAccepted() {
		return this.idxBlocksAccepted;
	}
	
	
	/**
	 * Set the list of blocks' indexes to which may be production.
	 */
	public void setIdxBlocksAccepted(Set<Integer> idxBlocksAccepted) {
		if (idxBlocksAccepted == null) {
			idxBlocksAccepted = new HashSet<Integer>();
		}
		
		this.idxBlocksAccepted = idxBlocksAccepted;
		return;
	}
	
	
	/**
	 * Add a new block index.
	 */
	public void addIdxBlock(int idxBlock) {
		this.idxBlocksAccepted.add(idxBlock);
		return;
	}
	
	
	

}
