package es.siani.energyagents.ontology.as03;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class OfferLevelConcept implements Concept {
	
	protected float amount;
	protected float price;
	
	/**
	 * Constructor.
	 */
	public OfferLevelConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public OfferLevelConcept(float amount, float price) {
		this.amount = amount;
		this.price = price;
		return;
	}


	/**
	 * Get the amount of offered load.
	 */
	@Slot(mandatory = true)
	public float getAmount() {
		return this.amount;
	}


	/**
	 * Set the amount of offered load.
	 */
	public void setAmount(float amount) {
		this.amount = amount;
	}


	/**
	 * Get the price of the load.
	 */
	public float getPrice() {
		return this.price;
	}


	/**
	 * Set the price of the load.
	 */
	public void setPrice(float price) {
		this.price = price;
	}
}
