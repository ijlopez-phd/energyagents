package es.siani.energyagents.ontology.as03;

import jade.content.Concept;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class DemandBlockConcept implements Concept {
	
	protected float load;
	protected int idx;
	protected float startPrice;
	
	/**
	 * Constructor.
	 */
	public DemandBlockConcept() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public DemandBlockConcept(int idx, float load, float startPrice) {
		this.idx = idx;
		this.load = load;
		this.startPrice = startPrice;
		return;
	}
	
	
	/**
	 * Get the index of the block.
	 */
	@Slot(mandatory = true)
	public int getIdx() {
		return this.idx;
	}
	
	
	/**
	 * Set the index of the block.
	 */
	public void setIdx(int idx) {
		this.idx = idx;
		return;
	}
	
	
	/**
	 * Get the load of the block.
	 */
	@Slot(mandatory = true)
	public float getLoad() {
		return this.load;
	}
	
	
	/**
	 * Set the load of the block.
	 */
	public void setLoad(float load) {
		this.load = load;
	}
	
	/**
	 * Get the start price.
	 */
	@Slot(mandatory = true)
	public float getStartPrice() {
		return this.startPrice;
	}
	
	
	/**
	 * Set the start price.
	 */
	public void setStartPrice(float startPrice) {
		this.startPrice = startPrice;
		return;
	}	
}
