package es.siani.energyagents.ontology.as03;

import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

import java.util.List;

@SuppressWarnings("serial")
public class OffersConfirmationPredicate implements Predicate {
	
	private List<OfferConfirmationConcept> offersConfirmations;
	
	
	/**
	 * Constructor.
	 */
	public OffersConfirmationPredicate() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public OffersConfirmationPredicate(List<OfferConfirmationConcept> offersConfirmations) {
		this.offersConfirmations = offersConfirmations;
		return;
	}
	
	
	/**
	 * Get the list of confirmations.
	 */
	@Slot
	public List<OfferConfirmationConcept> getOffersConfirmations() {
		return this.offersConfirmations;
	}
	
	
	/**
	 * Set the list of confirmations.
	 */
	public void setOffersConfirmations(List<OfferConfirmationConcept> offersConfirmations) {
		this.offersConfirmations = offersConfirmations;
		return;
	}
}
