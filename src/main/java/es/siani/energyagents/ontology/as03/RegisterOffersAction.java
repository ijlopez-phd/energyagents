package es.siani.energyagents.ontology.as03;

import java.util.List;
import jade.content.Predicate;
import jade.content.onto.annotations.Slot;

@SuppressWarnings("serial")
public class RegisterOffersAction implements Predicate {
	
	protected List<OfferBlockConcept> offerBlocks = null;
	protected boolean combined = false;
	
	
	/**
	 * Consturctor.
	 */
	public RegisterOffersAction() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public RegisterOffersAction(List<OfferBlockConcept> offerBlocks, boolean combined) {
		this.offerBlocks = offerBlocks;
		this.combined = combined;
		return;
	}
	
	
	/**
	 * Get the offers of the producers.
	 */
	@Slot
	public List<OfferBlockConcept> getOfferBlocks() {
		return this.offerBlocks;
	}
	
	
	/**
	 * Set the offers of the producers
	 */
	public void setOfferBlocks(List<OfferBlockConcept> offerBlocks) {
		this.offerBlocks = offerBlocks;
		return;
	}
	
	
	/**
	 * Tell whether the offer is combined or not.
	 */
	@Slot
	public boolean getCombined() {
		return this.combined;
	}
	
	
	/**
	 * Set the offer as combined.
	 */
	public void setCombined(boolean combined) {
		this.combined = combined;
	}	
}
