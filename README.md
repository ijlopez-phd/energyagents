## EnergyAgents ##

EnergyAgents is part of the PhD thesis of **Ignacio Lopez-Rodriguez** at the [Institute for Intelligent Systems and Numeric Applications (SIANI)](http://www.siani.es), [University of Las Palmas de Gran Canaria (ULPGC)](http://www.ulpgc.es).

*This library is still under development process*.


### Description ###

EnergyAgents framework provides Software Agents that negotiate on behalf of users in energy markets and demand-response programs. The agents work as **brokers** that are installed in the cloud and, after interacting in the markets and programs of the Grid Operator, tell the smart metering infrastructures (AMI) of buildings which commands should be applied to HVAC systems, the light and appliances.

These Broker Agents are part of an innovative infrastructure called *Agency Services*, which aims to make easier the adoption of the Software Agents in the future Smart Grid. In particular, the concept of installing Software Agents as cloud computing services is proposed and explained in the following papers:

* _Agency Services_, on the _International Conference on Agents and Artificial Intelligence_ (ICAART), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2010.  

* _Software Agents as Cloud Computing Services_, on Springer _Advances on Practical Applications of Agents and Multiagent Systems_ (PAAMS), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2011.

And the application of the Agency Services infrastructure to the Smart Grid context is explained in the papers:

* _Agent-Based Services for Building Markets in Distributed Energy Environments_, on the _International Conference on Renewable Energies and Power Quality_ (ICREPQ), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2010.  

* _Challenges of using smart local devices for the management of the Smart Grid_, on the _IEEE International Conference on Smart Grid Communications_ (SmartGridComm), Ignacio Lopez-Rodriguez and Mario Hernandez-Tejera, 2011.


### License ###

_EnergyAgents_ is free software: you can redistribute it and/or modify it under the terms of the [**GNU Lesser General Public License**](http://www.gnu.org/licenses/lgpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

In short, the  _EnergyAgents_ application guarantees the four essential freedoms of Free Software, **and** it can be linked by proprietary software.

EnergyAgents is copyright (c) 2013 Ignacio Lopez-Rodriguez.


### Technologies ###

EnergyAgents has been developed with Java. The following Java technologies have been used:

* [Jade][] for simulating a software agents' platform.

* [Smack][] for the XMPP communications.

* [Google Guice][] for implementing the dependency injection pattern.

* [JUnit][] and [Mockito][] for writing integration and unit tests.

* [Maven][] for the compilation and deploying processes.

* [Git][] as the version control system.

[Jade]: http://jade.tilab.com/
[Smack]: http://www.igniterealtime.org/projects/smack/
[Google Guice]: http://code.google.com/p/google-guice/
[Junit]: http://junit.org/
[Mockito]: https://github.com/mockito/mockito
[Maven]: http://maven.apache.org/
[Git]: http://git-scm.com/